import {Component,  OnInit} from '@angular/core';
import {ActivatedRoute,} from "@angular/router";
import {Subject} from "rxjs/Subject";
import {SuggestionLabelTypesUpsertComponent} from "./label-types.upsert/label-types.upsert.component";
import { CcltService, EntityListConfig, SocketService, ModalService, InfiniteScrollService} from "../../shared";
import {LabelTypesDeleteComponent} from "./label-types.delete/label-types.delete.component";
import {LabeltypeModel} from '../../shared/models/labeltype.model';
import {Subscription} from 'rxjs/Subscription';
@Component({
  selector: 'labelTypes-app',
  templateUrl: 'label-types.component.html',
  providers: [EntityListConfig]
})
export class SuggestionLabelTypesComponent implements OnInit{
  labeltypes: any[];
  categories: any[];
  labels:any[];
  types: any[];
  mode: any;
  fetchingData: boolean;
  collectionDirty: boolean;
  labelTypesFetchSub: Subscription;
  labelTypesObj: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private route: ActivatedRoute,
              private modalService: ModalService,
              private ccltService: CcltService,
              public entityListConfig: EntityListConfig,
              private socketService: SocketService,
              public infiniteScrollService: InfiniteScrollService
             ) {
    this.collectionDirty = false;
    this.labelTypesObj = {};
    this.labeltypes = [];

   this.route
      .params
      .subscribe(params => {
        // Récupération des valeurs de l'URL
        this.mode = params['id']; // --> Name must match wanted paramter
      });

    /*** websockets */
    this.socketService.subscribeToEvent(`labelTypes:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.labeltypes,this.labelTypesObj, this.entityListConfig);
        if(this.fetchingData){
          this.labelTypesFetchSub.unsubscribe();
          this.getAllLabelTypes(false);
        }
      });
    this.socketService.subscribeToEvent(`labelTypes:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data,this.labeltypes,this.labelTypesObj,this.entityListConfig);
        if(this.fetchingData){
          this.labelTypesFetchSub.unsubscribe();
          this.getAllLabelTypes(false);
        }
      });
    /*** websockets */
  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map((data=> data.data))
      .subscribe(
        (data) => {
          console.log(data);
          this.categories = data[0];
          this.labels = data[1];
          this.types = data[2];
          this.labeltypes = data[3];
          this.infiniteScrollService.computeEntity(this.labeltypes,this.labelTypesObj, this.entityListConfig);
        });
  }
  onScroll(){
    this.getAllLabelTypes(false);
  }
  getAllLabelTypes(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
    this.labelTypesFetchSub = this.ccltService
        .getAll(this.entityListConfig)
        .subscribe(
        (data)=> {
          this.fetchingData = false;
          if (this.collectionDirty) {
            this.getAllLabelTypes(false);
          } else {
            this.infiniteScrollService.getFromApi(data, this.labeltypes,this.labelTypesObj, this.entityListConfig, refreshData);
          }
        }
        ),(error)=>{this.fetchingData = false;};

  }
  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  editLabelTypes(labelType: LabeltypeModel) {
    const modalRef = this.modalService.open(SuggestionLabelTypesUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.labelType = labelType;
    modalRef.componentInstance.categories =  this.categories;
    modalRef.componentInstance.labels = this.labels;
    modalRef.componentInstance.types = this.types;
    modalRef.componentInstance.mode = this.mode;

  }
  deleteLabelTypes(labelType: LabeltypeModel ) {
    const modalRef = this.modalService.open(LabelTypesDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.labelType = labelType;
  }
  addLabelTypes() {
    const modalRef = this.modalService.open(SuggestionLabelTypesUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.categories = this.categories;
    modalRef.componentInstance.labels = this.labels;
    modalRef.componentInstance.types = this.types;
    modalRef.componentInstance.labelType = new LabeltypeModel();

  }
}
