import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class AnnouncementService {
  constructor (private apiService: ApiService) {}

  getAll(appID, entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/announcement/app/${appID}/${pageNumber}/${pageSize}`, query, isSocket);
  }

  getAllRole( AppId, entityListConfig:  EntityListConfig, isSocket? ): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/role/app/${AppId}/${pageNumber}/${pageSize}`, query, isSocket);
  }
  saveRole(data, appID ){
    // crete a new Role
    return this.apiService.post(`/announcement/app/${appID}`, data);

  }

  save(announcement, appID): Observable<any> {
      // Otherwise, create a new category
      return this.apiService.post(`/announcement/app/${appID}/`, announcement);
    }
  }
