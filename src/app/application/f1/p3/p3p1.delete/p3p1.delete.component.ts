

import {Component, Input, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P3P1Model, P3Service} from '../../../../shared';


@Component({
  selector: 'app-root',
  templateUrl: './p3p1.delete.component.html',
  encapsulation: ViewEncapsulation.None

})
export class P3P1DeleteComponent {
  @Input() p3p1;
  @Input() p3Id;
  @Input() appID;
  isSubmitting: Boolean;
  constructor(public activeModal: NgbActiveModal, private p3Service: P3Service) {
  }
  deletep2p1 (p3p1: P3P1Model) {
    this.isSubmitting = true;
    this.p3Service.destroyp3p1(p3p1._id, this.p3Id, this.appID)
      .subscribe (
        data => {
          this.isSubmitting = false;
          this.activeModal.close ('P3P1_Deleted')
        },
        error => this.isSubmitting = false
      );
  }
}
