import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {EntityListConfig} from '../../shared/models/entity-list-config.model';
import {DTOService} from '../../shared';
import {CcldService} from "../../shared/services/ccld.service";
import {DomSanitizer} from "@angular/platform-browser";
import {LabelNumberUpsertComponent} from "./label-number.upsert/label-number.upsert.component";
import {FilesDeleteComponent} from "../files/files.delete/files.delete.component";
import {LabelNumberDeleteComponent} from "./label-number.delete/label-number.delete.component";
import {LabelNumberModel} from "../../shared/models/labelNumber.model";

@Component({
  selector: 'label-number',
  templateUrl: './label-number.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]

})
export class LabelNumberComponent implements OnInit {

  private ngUnsubscribe: Subject<void> = new Subject<void>();
  labelNumbers;
  cclt;
  constructor(
    private dtoService: DTOService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private entityListConfig: EntityListConfig,
    private ccldService: CcldService,
    private _sanitizer: DomSanitizer,
  ) {
    this.cclt = this.dtoService.getValue('labelType');
    console.log(this.cclt);

  }
  ngOnInit() {
    this.route.data
      .map((data=> data.data))
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.labelNumbers = data;
      });
  }
  getBackground(image){
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }
  getAllLabelNumber() {
    console.log(this.cclt);
    const entitylistConfig = new EntityListConfig();
    this.ccldService
      .getAll(this.cclt, entitylistConfig)
      .subscribe(
        (data)=>{
          this.labelNumbers = data;
        });
  }
  // ****P2***
  editFile (labelNumber: any) {
    const modalRef = this.modalService.open ( LabelNumberUpsertComponent );
    modalRef.componentInstance.cclt = this.cclt;
    modalRef.componentInstance.labelNumbers = labelNumber;
    modalRef.result.then((closeReason) => {
      if (closeReason == 'labelNumber:updated'){
      this.getAllLabelNumber();
      }
    });

  }
  deleteFile(labelNumber: any) {
    const modalRef = this.modalService.open ( LabelNumberDeleteComponent );
    modalRef.componentInstance.labelNumbers = labelNumber;
    modalRef.result.then((closeReason) => {
      if (closeReason == 'labelNumber:remove'){
      this.getAllLabelNumber();
      }
    });


  }
  addfile() {
    const modalRef = this.modalService.open ( LabelNumberUpsertComponent );
    modalRef.componentInstance.cclt = this.cclt;
    modalRef.componentInstance.labelNumbers = new LabelNumberModel();
    modalRef.result.then((closeReason) => {
      if (closeReason == 'labelNumber:updated'){
        this.getAllLabelNumber();
      }
    });
  }


}
