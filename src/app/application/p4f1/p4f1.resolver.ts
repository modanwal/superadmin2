import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { UserService, AppService, P4Service,EntityListConfig, P4F1Service } from './../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P4F1Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router,
               private p4Service: P4Service,
               private p4F1Service: P4F1Service) {
  }
 ngOnInit(){
 }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      const entityListConfig = new EntityListConfig();
      const appID = state['url'].split("/")[2];
      return  this.p4Service.getAll(entityListConfig,appID).map((p4Data)=>{
        let data = {};
        data['appID'] = appID;
        data['p4Data'] = p4Data;
        return data;
      })
  }
}

