import {Injectable} from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {SuggestionComponent} from './widzard.component';
import { ModalService }from  './../../shared/modal.service';


@Injectable()
export class SuggestionService {
  constructor(private modalService: ModalService) {
  }
  public open() {
    const modalRef = this.modalService.openOverlayModal(SuggestionComponent, {
      size: 'lg',
      keyboard: false
    });
    return modalRef;
  }
}
