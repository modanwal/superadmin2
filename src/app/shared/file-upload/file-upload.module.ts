import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation, Input, Output, Injectable, NgModule, ModuleWithProviders } from '@angular/core';
import { Observable } from 'rxjs/Rx'
import { Subject, BehaviorSubject } from "rxjs/Rx";
import { FileUploadComponent } from './file-upload.component';
import { FileUploadService } from './file-upload.service';
import { ImageCropperModule } from 'ng2-img-cropper';
import { MediaUploadComponent } from './media-upload.component';
import { WebUploadComponent } from './web-upload.component';
import {FlickrService} from "../services";

@NgModule({
  imports: [CommonModule,NgbModule,ImageCropperModule],
  exports: [FileUploadComponent,WebUploadComponent],
  declarations: [FileUploadComponent,MediaUploadComponent,WebUploadComponent],
  entryComponents: [FileUploadComponent,MediaUploadComponent,WebUploadComponent],
  providers: [FileUploadService,  FlickrService]
})


export class FileUploadModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: FileUploadModule,
      providers: [FileUploadService]
    };
  }
}
