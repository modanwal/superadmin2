import { CommonModule } from '@angular/common';
import {NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DndModule } from 'ng2-dnd';
import { TableDataSorterComponent, } from './layout';
import { NotificationCardComponent, AnnouncementCardComponent } from './notifications';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { environment } from './../../environments/environment';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { SlickCaroselModule } from './slick-carosel.component';
import { ImageCropperModule } from "ng2-img-cropper";
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { TimeAgoPipe } from 'time-ago-pipe';
import {NgSelectizeModule} from 'ng-selectize';
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown'
import { ChartsModule } from 'ng2-charts';
import { ProgressHttpModule } from "angular-progress-http";
import { FileUploadModule } from './file-upload/file-upload.module';
import { SuggestionModule } from "./suggestion/widzard.module";
import { StickyModule } from 'ng2-sticky-kit/ng2-sticky-kit';
import { UploaderModule } from './uploader/uploader.module';
import { StickyElement } from './sticky.directive';
import { FullScreenDirective } from './full-screen.directive';




const socketIoConfig: SocketIoConfig = { url: `${environment.socket_url}`, options: {
  // query: `token=${new JwtService('BROWSER').getToken()}`,
  transports: ['websocket','polling'],
  path: '/socket.io-client'
}};

import { Draggable } from 'ng2draggable/draggable.directive';
// import { SlimLoadingBarfule } from 'ng2-slim-loading-bar';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ToastrModule } from 'ngx-toastr';
import { ShowAuthedDirective } from './show-authed.directive';
import { BreadCrumbModule } from "./breadcrumb.module";


@NgModule({
  imports: [
    NgbModule,  CommonModule, FormsModule, ReactiveFormsModule, HttpModule, RouterModule, NgSelectizeModule,
    SlimLoadingBarModule.forRoot(), InfiniteScrollModule, DndModule.forRoot(), NgSelectizeModule,
    ToastrModule.forRoot({timeOut: 0}), SocketIoModule.forRoot(socketIoConfig), Ng2AutoCompleteModule,
    BreadCrumbModule.forRoot(), SlickCaroselModule.forRoot(),  ChartsModule, ImageCropperModule,
    ImageCropperModule, FileUploadModule, ProgressHttpModule, SuggestionModule,StickyModule,UploaderModule,
    NgSelectizeModule
  ],
  declarations: [
    Draggable,
    TableDataSorterComponent,
    ShowAuthedDirective,
    TimeAgoPipe,
    NotificationCardComponent,
    AnnouncementCardComponent,
    StickyElement,
    FullScreenDirective
  ],
  exports: [
    CommonModule, FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    NgbModule,
    NgSelectizeModule,
    SocketIoModule,
    Draggable,
    TableDataSorterComponent,
    SlimLoadingBarModule,
    InfiniteScrollModule,
    DndModule,
    ToastrModule,
    ShowAuthedDirective,
    Ng2AutoCompleteModule,
    BreadCrumbModule,
    SlickCaroselModule,
    ImageCropperModule,
    TimeAgoPipe,
    NotificationCardComponent,
    AnnouncementCardComponent,
    FileUploadModule,
    ProgressHttpModule,
    SuggestionModule,
    StickyModule,
    UploaderModule,
    StickyElement,
    FullScreenDirective
  ],
  entryComponents:[
    NotificationCardComponent,
    AnnouncementCardComponent,
  ],
  providers: []
})
export class SharedModule {}
