import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { UserService, AppService, EntityListConfig, P4Service } from './../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P4Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router,
               private p4Service: P4Service,
  ) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const appComponents = route.parent.data.data.appComponents;
    const selectedComponent = appComponents.find(component=> component.componentID.route == '/p4');
    const nextComponent = appComponents[appComponents.indexOf(selectedComponent)+1]
    const appID = state['url'].split("/")[2];
    return this.p4Service.getAll(entityListConfig, appID, false).map(
      (p4Data) =>{
        let data = {};
        data['appID'] = appID;
        data['selectedComponent'] = selectedComponent;
        data['nextComponent'] = nextComponent;
        data['p4Data'] = p4Data;
        return data;
      }
    );
  }
}

