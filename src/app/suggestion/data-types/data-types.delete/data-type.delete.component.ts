import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {TypeService} from "../../../shared";
@Component({
  selector: 'type-delete',
  templateUrl: './data-types.delete.component.html'
})
export class SuggestionDataTypeDeleteComponent {
  @Input() dataType;
  isSubmited: Boolean;
  constructor( public activeModal: NgbActiveModal ,
               private typeService: TypeService
  ) {}
  deleteType(dataType: any) {
    this.typeService.destroy(dataType._id)
      .subscribe(
        data => this.activeModal.close('type:remove')
      );
  }
}
