import { Component, OnInit, Input } from '@angular/core';
import { User, EntityListConfig } from './../models';
import { Subject } from "rxjs/Subject";

@Component({
  selector: 'notification-card',
  templateUrl: './notification-card.component.html'
})
export class NotificationCardComponent implements OnInit{
  @Input() notification: any;
  constructor() {
  }

  ngOnInit() {
    
  }

}