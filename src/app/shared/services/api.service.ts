import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Headers, Http, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import { JwtService } from './jwt.service';
import { ToastrConfig , ToastContainerDirective, ToastrService} from 'ngx-toastr';
import { ProgressHttp } from "angular-progress-http";

@Injectable()
export class ApiService {

  constructor(
    private http: Http,
    private progressHttp: ProgressHttp,
    private jwtService: JwtService,
    toastrConfig: ToastrConfig,
    private slimLoadingBarService: SlimLoadingBarService,
    private toastrService: ToastrService,
    private router: Router
  ) { toastrConfig.timeOut = 2000; }

  private beforeRequest() {
    this.slimLoadingBarService.start();
  }
  private afterRequest(){
    this.slimLoadingBarService.complete();
  }

  private onCatch(error: any) {
    return Observable.throw(error.json);
  }

  private onError(error: any) {
    return Observable.throw(error.json);
  }

  private onSuccess(res: Response): void {
    this.slimLoadingBarService.complete();
  }


  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = `Bearer ${this.jwtService.getToken()}`;
    }
    return new Headers(headersConfig);
  }

  private getFormHeaders(): Headers{
    const headersConfig = {
      'Accept': 'application/json'
    };
    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = `Bearer ${this.jwtService.getToken()}`;
    }
    return new Headers(headersConfig);
  }

  private formatErrors(context) {
    return function(error){
      if (error.status == '500'){
        if (error._body){
          context.toastrService.error(error._body, 'Error');
        }else{
          context.toastrService.error('Interval Server Error', 'Error');
        }
      } else
           if (error.status == '404'){
             if (error._body){
                context.toastrService.error(error._body, 'Error');
              }else{
                context.toastrService.error('Not Found', '404');
              }
           }  else
             if (error.status == '400'){
               if (error._body){
                  context.toastrService.error(error._body, 'Error');
                }else{
                  context.toastrService.error('Bad Request', 'Error');
                }
             } else if (error.status == '401'){
               this.router.navigateByUrl(`/login`);
             }
             else
             {
               if (error._body){
                  context.toastrService.error(error._body, 'Error!');
                }else{
                  context.toastrService.error('An Unknown error has occcured!', 'Error');
                }
             }
      return Observable.throw(error.json());
    };
  }
  get(path: string, params: URLSearchParams = new URLSearchParams(), isSocket): Observable<any> {
    this.beforeRequest();
    let url = environment.api_url + path;
    if (isSocket) url = environment.socket_url + '/api' + path;

    return this.http.get(`${url}`, { headers: this.setHeaders(), search: params })
    .catch(this.formatErrors(this))
    .do((res: Response) => {
      this.onSuccess(res);
    }, (error: any) => {
      this.onError(error);
    })
    .map((res: Response) => res.json())
    .finally(() => {
       this.afterRequest();
    });
  }

  put(path: string, body: Object = {}, ): Observable<any> {
    return this.http.put(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors(this))
    .do((res: Response) => {
      this.onSuccess(res);
    }, (error: any) => {
      this.onError(error);
    })
    .map((res: Response) => res.json())
    .finally(() => {
       this.afterRequest();
    }); ;
  }

  post(path: string, body: Object = {}): Observable<any> {
    this.beforeRequest();
    return this.http.post(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors(this))
    .do((res: Response) => {
      this.onSuccess(res);
    }, (error: any) => {
      this.onError(error);
    })
    .map((res: Response) => res.json())
    .finally(() => {
       this.afterRequest();
    });
  }
  postAuth(path: string, body: Object = {}): Observable<any> {
    this.beforeRequest();
    return this.http.post(
      `${environment.auth_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors(this))
    .do((res: Response) => {
      this.onSuccess(res);
    }, (error: any) => {
      this.onError(error);
    })
    .map((res: Response) => res.json())
    .finally(() => {
       this.afterRequest();
    }); ; ;
  }


  delete(path): Observable<any> {
    this.beforeRequest();
    return this.http.delete(
      `${environment.api_url}${path}`,
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors(this))
    .do((res: Response) => {
      this.onSuccess(res);
    }, (error: any) => {
      this.onError(error);
    })
    .map((res: Response) => res.json())
    .finally(() => {
       this.afterRequest();
    });
  }
  postWithFormData(path: string, body: Object = {}){
    this.beforeRequest();
    return this.http.post(
      `${environment.api_url}${path}`,
      body,
      { headers: this.getFormHeaders() }
    )
    .catch(this.formatErrors(this))
    .do((res: Response) => {
      this.onSuccess(res);
    }, (error: any) => {
      this.onError(error);
    })
    .map((res: Response) => res.json())
    .finally(() => {
       this.afterRequest();
    });
  }
  putWithFormData(path: string, body: Object = {}): Observable<any> {
    this.beforeRequest();
    return this.http.put(
      `${environment.api_url}${path}`,
      body,
      { headers: this.getFormHeaders() }
    )
    .catch(this.formatErrors(this))
    .do((res: Response) => {
      this.onSuccess(res);
    }, (error: any) => {
      this.onError(error);
    })
    .map((res: Response) => res.json())
    .finally(() => {
       this.afterRequest();
    });
  }
  postWithProgressFormData(path: string, body: Object = {}, progressFunc: any){
    return this.progressHttp
          .withUploadProgressListener(progressFunc)
          .post(
              `${environment.api_url}${path}`,
              body,
              { headers: this.getFormHeaders() }
            )
            .catch(this.formatErrors(this))
            .map((res: Response) => res.json());
  }
  putWithProgressFormData(path: string, body: Object = {},progressFunc: any){
    return this.progressHttp
           .withUploadProgressListener(progressFunc)
           .put(
                `${environment.api_url}${path}`,
                body,
                { headers: this.getFormHeaders() }
              )
              .catch(this.formatErrors(this))
              .map((res: Response) => res.json());
  }

}
