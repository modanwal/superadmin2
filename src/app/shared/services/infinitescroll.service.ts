import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Socket } from 'ng-socket-io';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class InfiniteScrollService {

  constructor (private socket: Socket ) {}
  savedFromSocket(socketItem,collection: any[],collectionObj: any,entityListConfig?: EntityListConfig){
    console.log('---------------->', collectionObj);
    const index = this._getIndexOfItemInCollection(socketItem,collection);
    if(index!=-1){
        //update collection
        collection[index] = socketItem;
    }
    else{
         //console.log('collection before any computation being done:::',JSON.parse(JSON.stringify(collection)));
         if(!this._itemExistsInCollectionObj(socketItem,collectionObj)){
            //push item to the beginning of the  collection
            collection.unshift(socketItem);
            if(entityListConfig) this.computeEntity(collection,collectionObj,entityListConfig);
         }
    }
    //after changes values are
    console.log("after changes values are::entityListConfig:",entityListConfig)
    console.log("after changes values are::socketItem:",socketItem);
    console.log("after changes values are::collection:",collection);
  }
  removedFromSocket(socketItem,collection: any[],collectionObj: any, entityListConfig: EntityListConfig){
        //find index of socketItem in collection
        const index = this._getIndexOfItemInCollection(socketItem,collection);
        if(index!=-1){
          //remove item from collection
          collection.splice(index,1);
          //compute entity's state
          this.computeEntity(collection,collectionObj,entityListConfig);
        }
  }
  getFromApi(items:any[],collection: any[], collectionObj: any,entityListConfig: EntityListConfig, refreshData?: Boolean){
    console.log('---------------->', collectionObj);
      if(refreshData) collection = [];
      items.forEach(item=>{
          if(!collectionObj[item['_id']]) collection.push(item)
      });
      //compute state of entityListConfig
      this.computeEntity(collection,collectionObj,entityListConfig);
  }

  /**
   * get index of item to be found in collection with a given id.
   * @param item
   * @param collection
   */
  _getIndexOfItemInCollection(item,collection){
    return collection.findIndex(function(collectionItem){
                return item._id == collectionItem._id;
            });
  }
  _itemExistsInCollectionObj(item,collectionObj){
    return !!collectionObj[item._id];
  }
  /**
   * update entity as per the listing
   * @param collection
   * @param collectionObj
   * @param entityListConfig
   */
  computeEntity(collection,collectionObj,entityListConfig:EntityListConfig){
    collection.forEach(item=>collectionObj[item['_id']] = item['_id']);
    //entityListConfig.remainingItems = (collection.length)%(entityListConfig.params.pageSize);
    entityListConfig.params.pageNumber = Math.floor(collection.length/entityListConfig.params.pageSize);
    // entityListConfig.params.pageNumber = Math.ceil(collection.length/entityListConfig.params.pageSize);
    // if(entityListConfig.params.pageNumber!=0) entityListConfig.params.pageNumber--;
  }


}
