import { Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subject} from "rxjs/Subject";
import {SuggestionLabelsUpsertComponent} from "./labels.upsert/labels.upsert.component";
import {LabelModel, LabelService, ModalService} from "../../shared";
import {EntityListConfig} from "../../shared/models/entity-list-config.model";
import {SuggestionLabelDeleteComponent} from "./label.delete/label.delete.component";


@Component({
  selector: 'suggestion-app',
  templateUrl: 'labels.component.html',
})
export class SuggestionLabelsComponent implements OnInit {
  labels: any[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private route: ActivatedRoute, private modalService: ModalService, private labelService: LabelService) {
  }
  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        (val) => {
          this.labels = val['data'];
        });
  }
  onScroll(){

  }
  getAllLabels(){
    const entityListConfig = new EntityListConfig();
    this.labelService
      .getAll(entityListConfig)
      .subscribe(
        (data)=>{
          this.labels = data;
        });
  }
  addLabels(){
    const modalRef = this.modalService.open(SuggestionLabelsUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.label = new LabelModel();
    modalRef.result.then((closeReason) => {
      if (closeReason == 'label:updated'){
        this.getAllLabels();
      }
    });

  }
  editLabels(label: any){
    const modalRef = this.modalService.open(SuggestionLabelsUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.label = label;
    modalRef.result.then((closeReason) => {
      if (closeReason == 'label:updated'){
        this.getAllLabels();
      }
    });

  }
  deleteLabels(label: any){
    const modalRef = this.modalService.open(SuggestionLabelDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.label = label;
    modalRef.result.then((closeReason) => {
      if (closeReason == 'label:remove'){
        this.getAllLabels();
      }
    });
  }
}
