import { RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/core';
import { NgModule } from '@angular/core';
import { SharedModule } from './../../shared';
import { P4f1Component } from '../p4f1/p4f1.component';
import { P4f1UsersComponent } from './p4f1.users/p4f1.users.component';

import { P4F1Resolver } from "./p4f1.resolver";
import { P4F1UsersResolver } from "./p4f1.users/p4f1.users.resolver";

export const p4f1Routing = [
  {
      path: 'p4f1',
      component: P4f1Component,
      resolve: {data: P4F1Resolver},
      children:[
        {
          path: ':id',
          component: P4f1UsersComponent,
          resolve: {data: P4F1UsersResolver}
        }
      ]
  }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    P4f1Component,
    P4f1UsersComponent
  ],
  providers: [
    P4F1Resolver,
    P4F1UsersResolver
  ]
})
export class P4f1Module {}
