import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TestAppComponent } from './test-app.component';
import { SharedModule } from '../shared';


export const testAppRouting = [
  {
    path: 'test-app',
    component: TestAppComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    
  ],
  declarations: [
    TestAppComponent
  ],
  providers: [
    
  ],
  entryComponents: [
    
  ]
})
export class TestAppModule {
  
}
