import { Component, OnInit, Input } from '@angular/core';

import { RoleModel, RoleService,  ResourceModel, EntityListConfig , SocketService, InfiniteScrollService} from '../../shared';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { RoleDeleteComponent } from './role.delete/role.delete.component';
import { RoleUpsertComponent } from './role.upsert/role.upsert.component';
import { BreadCrumbService } from "./../../shared/breadcrumb.module";
import { ModalService } from './../../shared/modal.service';
import { Subject } from "rxjs/Subject";
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'app-roles',
  templateUrl: './role.component.html',
  providers: [EntityListConfig]
})
export class RoleComponent implements OnInit {
  roles: RoleModel[];
  fetchingData: boolean;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  lastFetchedItems: number;
  collectionDirty: boolean;
  roleFetchSub: Subscription;
  rolesObj: any;
  constructor(private router: Router,
              private modalService: ModalService,
              private route: ActivatedRoute,
              private roleService: RoleService,
              public entityListConfig: EntityListConfig,
              private breadCrumbService: BreadCrumbService,
              public infiniteScrollService: InfiniteScrollService,
              private socketService: SocketService) {
    this.collectionDirty = false;
    this.roles = [];
    this.rolesObj = {};
    this.lastFetchedItems = 0;

    // /*** websockets */
    // this.socketService.subscribeToEvent(`role:save`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllRoleForSockets())
    //
    // this.socketService.subscribeToEvent(`role:remove`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllRoleForSockets())
    // /*** websockets */

    /*** websockets */
    this.socketService.subscribeToEvent(`role:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.roles,this.rolesObj, this.entityListConfig);
        if(this.fetchingData){
          this.roleFetchSub.unsubscribe();
          this.getAllRole(false);
        }
      })
    this.socketService.subscribeToEvent(`role:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.roles,this.rolesObj,this.entityListConfig);
        if(this.fetchingData){
          this.roleFetchSub.unsubscribe();
          this.getAllRole(false);
        }
      })
    /*** websockets */

  }
  ngOnInit() {
    this.fetchingData = true;
    this.collectionDirty = false;
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        (val) => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllRole(false);
          }else {
            this.infiniteScrollService.computeEntity(this.roles, this.rolesObj, this.entityListConfig);
          }
          // this.lastFetchedItems = this.roles.length;
          // if(this.roles.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
        }), (err)=>{ this.fetchingData = true};
    this.breadCrumbService
        .pushBreadCrumbItem({url:'/roles',label:'Roles'},true);

  }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllRole(true);
  }
  setEntityConfig(event){
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy =event.sortBy;
    this.entityListConfig.query.order =event.sortOrder;
    this.getAllRole(true);

  }
  onScroll(){
    this.getAllRole(false);
  }

  resourcePrivilege(role: RoleModel) {
    this.breadCrumbService.pushBreadCrumbItem({url:'/roles/role/resource-privilege',params:role._id,label:role.name},false);
    this.router.navigateByUrl(`/roles/role/resource-privilege/${role._id}`);
  }
  // getAllRoleForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1) * (this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.roleService
  //     .getAll(this.entityListConfig, true)
  //     .subscribe(data =>{
  //       this.roles = data;
  //       this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //       this.entityListConfig.params.pageNumber = Math.ceil(data.length / this.entityListConfig.params.pageSize);
  //       if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //     });
  //
  // }
  getAllRole(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
  this.roleFetchSub =   this.roleService
      .getAll(this.entityListConfig, true)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if(this.collectionDirty) {
            this.getAllRole(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.roles, this.rolesObj, this.entityListConfig, refreshData);
          }
        }
          // if(refreshData) this.roles = [];
          // // remove last page entries if data less than ideal size
          // if(this.lastFetchedItems < this.entityListConfig.params.pageSize){
          //   this.roles.splice(
          //     (this.entityListConfig.params.pageNumber)
          //     *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          // }
          // this.lastFetchedItems = data.length;
          // if(data.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // // push new data
          // this.roles = [...this.roles, ...data];  }
        ),
      (error)=>{this.fetchingData = true;};
  }
  addRole() {
    const modalRef = this.modalService.open(RoleUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.role = new RoleModel;
    // modalRef.result.then((closeReason) => {
    //   if( closeReason === 'component:updated'){
    //     this.getAllRole(true);
    //   }
    // });
   // this.router.navigateByUrl('/roles/role-upsert/');
  }
  deleteRole(role: RoleModel) {
    const modalRef = this.modalService.open(RoleDeleteComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.role = role;
    // modalRef.result.then((closeReason) => {
    //   if( closeReason === 'component:updated'){
    //     this.getAllRole(true);
    //   }
    // });
  }
  editRole(role: RoleModel) {
    const modalRef = this.modalService.open(RoleUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.role = role;
    // modalRef.result.then((closeReason) => {
    //   if( closeReason === 'component:updated'){
    //     this.getAllRole(true);
    //   }
    // });

  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
