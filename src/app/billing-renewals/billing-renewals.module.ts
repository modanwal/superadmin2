import { SharedModule, AuthGuard } from '../shared';
import {BillingRenewalsComponent} from "./billing-renewals.component";
import {NgModule} from "@angular/core";
import {BillingRenewalsResolver} from "./billing-renewals.resolver";
import {CartService, CheckoutService} from '../shared';
import { BrowserModule } from '@angular/platform-browser';
import {TaxService, DTOService} from "../shared";


export const billingRenewalsRouting = [
  {
    path: 'billing-renewals',
    component: BillingRenewalsComponent,
    resolve: {
      data: BillingRenewalsResolver
    }
  }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    BillingRenewalsComponent
  ],
  providers: [AuthGuard, BillingRenewalsResolver, CartService, CheckoutService, TaxService, DTOService]

})
export class BillingRenewalsModule {}
