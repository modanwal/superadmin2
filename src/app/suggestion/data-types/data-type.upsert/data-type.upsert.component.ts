import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TypeService} from "../../../shared/services/type.service";

@Component({
  selector: 'app-root',
  templateUrl: './data-type.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SuggestionDataTypeUpsertComponent implements  OnInit {
 @Input() dataType;
  dataTypesForm: FormGroup;
  isSubmited: Boolean;
  isSubmitting: boolean;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private typeService: TypeService
                ) {
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.dataTypesForm.valid) {
      this.isSubmitting = true;
      const type = this.dataTypesForm.value;
      this.dataTypesForm.disable();
      this.typeService
        .save(type)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.activeModal.close('type:updated');
          },
          error => {
            this.isSubmitting = false;
            this.dataTypesForm.enable();
          });
    }
  }
  ngOnInit() {
      this.dataTypesForm = this.formBuilder.group({
        '_id': [this.dataType._id],
        'name': [this.dataType.name, Validators.required],
        'description': [this.dataType.description, Validators.required]
      });
  }
}
