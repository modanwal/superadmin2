import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import {Http} from "@angular/http";


@Injectable()
export class GiphyService {
  constructor (private _http: Http) {}

  getImages(searchElement): Observable<any> {
    let url = `http://api.giphy.com/v1/gifs/search?api_key=dc6zaTOxFJmzC&q=`;
    var apiLink = url + searchElement.value;
    return this._http
      .get(apiLink)
      .map(res => res.json().data);

  }
}
