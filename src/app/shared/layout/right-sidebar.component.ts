import { Component, OnInit } from '@angular/core';

import { User } from '../models';
import { UserService } from '../services';

@Component({
  selector: 'layout-right-sidebar',
  templateUrl: './right-sidebar.component.html'
})
export class RightSidebarComponent implements OnInit {
    currentUser: User;
  authenticated: Boolean;
  constructor(
    private userService: UserService
  ) {}



  ngOnInit() {
    this.userService.isAuthenticated.subscribe(authenticated => this.authenticated = authenticated )
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
  }
}
