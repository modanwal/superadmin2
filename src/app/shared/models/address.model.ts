/**
 * Created by rahul on 7/7/2017.
 */
export class AddressModel {
  _id: string;
  name: string;
  address1: string;
  address2: string;
  city: string;
  state: string;
  country: string;
  pincode: string;
  primary: string;
}
