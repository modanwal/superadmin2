import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FlickrService, DTOService, GiphyService, CcldService} from '../../shared';
@Component({
  selector: 'webimages-app',
  templateUrl: 'webimages.component.html',
})
export class WebimagesComponent implements OnInit {
  photos: any;
  @ViewChild('input')
  input: ElementRef;
  formData: FormData;
  cclt: any;
  file: any;
  constructor(private _flickrService: FlickrService,
              private giphyService: GiphyService,
              private ccldService: CcldService,
              private dtoService: DTOService,) {
    this.cclt = this.dtoService.getValue('labelType');
  }
  ngOnInit() {

  }
  searchfromFlickr(){
    console.log(this.input.nativeElement.value);
    const query = this.input.nativeElement.value;
    this._flickrService
      .getResult(query)
      .subscribe(value => {
        this.photos = value;
      });
  }
  searchfromGiphy(){
    const query = this.input.nativeElement.value;
    this.giphyService
      .getImages(query)
      .subscribe(
        (data)=>{
          this.photos = data;
          console.log(this.photos);
        });
  }
  checkstatus(url: string){
    if (typeof url === 'string') return true;
    return false;
  }
  saveIntoDB(photo: any){
    console.log('method called');
    this.formData = new FormData();
    this.formData.append('path', photo.url);
    this.formData.append('categoryID', this.cclt.categoryID._id);
    this.formData.append('componentID', this.cclt.componentID._id);
    this.formData.append('labelID', this.cclt.labelID._id);
    //  this.formData.append('data', 'https://farm8.staticflickr.com/7631/16236321533_1216582801.jpg');
    this.ccldService.
    saveFormData(this.formData)
      .subscribe(
        (data)=>{
         console.log('data save into db');
        });
  }
  getImagesfromApi(photo) {
    console.log(photo);
    if ((typeof photo.url === 'string') && (  photo.type !== 'gif')) {
      return photo.url;
    } else if (typeof photo.url === 'string') {
      console.log('else');
      return photo.images.original.url;
    }
  }
}
