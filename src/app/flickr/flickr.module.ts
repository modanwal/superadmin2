
import {FlickrComponent} from "./flickr.component";
import {SharedModule} from "../shared/shared.module";
import {NgModule} from "@angular/core";
import {FlickrService} from "../shared/services/flikr.service";

export const flickrRouting = [
  {
    path: 'flickr',
    component: FlickrComponent
  }
];

@NgModule( {
  imports: [
    SharedModule
  ],
  declarations: [
    FlickrComponent
  ],
  providers: [FlickrService],
  entryComponents: [

  ]
})



export class FlickrModule {  }
