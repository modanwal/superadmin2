import { Component, Input, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ImageCropperComponent, Bounds, CropperSettings } from "ng2-img-cropper";

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  providers: [CropperSettings]
})
export class FileUploadComponent {
  cropBounds: Bounds;
  data= {};
  @Input() file;
  @Input() cropDimension;
  cropperSetting;
  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;

  isSubmited: Boolean;
  isSubmitting: boolean;

  constructor(public activeModal: NgbActiveModal, public cropperSettings: CropperSettings) {
    this.cropperSettings.noFileInput = true;
  }
  submitForm() {
    this.activeModal.close({file: this.file, cropBounds: this.cropBounds});
  }
  ngOnInit() {
    if(this.cropDimension){
        this.cropperSettings.width = this.cropDimension.width;
        this.cropperSettings.height = this.cropDimension.height;
        this.data = {};
    }
    let image:any = new Image();
    if(this.file && this.cropDimension){
      let myReader:FileReader = new FileReader();
      let that = this;
      myReader.onloadend = function (loadEvent:any) {
          image.src = loadEvent.target.result;
          that.cropper.setImage(image);
      };
      myReader.readAsDataURL(this.file);
    }
  }
  cropped(bounds:Bounds){
    this.cropBounds = bounds;
  }
}
