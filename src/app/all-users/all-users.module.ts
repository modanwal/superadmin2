import { ModuleWithProviders, NgModule } from '@angular/core';
import { SharedModule, AuthGuard } from '../shared';
import { RouterModule } from '@angular/router';
import { AllUsersComponent } from '../all-users/all-users.component';
import { AllUserUpsertComponent } from './all-users.upsert/all-users.upsert.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AllUserDeleteComponent } from '../all-users/all-users.delete/all-users.delete.component';
import { AllUsersResolver } from './all-users.resolver';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
//import { AllUsersListComponent } from './all-users.list/all-users.list.component'; 

export const allUsersRouting = [
  {
    path: 'all-users',
    component: AllUsersComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
    resolve: {
      users: AllUsersResolver
    }
  }
];

@NgModule({
  imports: [
    SharedModule,
    NgbModule, Ng2SearchPipeModule
  ],
  declarations: [
    AllUsersComponent,
    AllUserUpsertComponent,
    AllUserDeleteComponent,
    //AllUsersListComponent
  ],
  providers: [
    NgbActiveModal,
    AuthGuard,
    AllUsersResolver
  ],
  entryComponents: [
    AllUserUpsertComponent , AllUserDeleteComponent
  ]
})
export class AllUserModule {
  
}
