/**
 * Created by rahul on 7/7/2017.
 */
import {Component, OnInit} from '@angular/core';
import { AppStatsModal, EntityListConfig } from '../shared/models';
import {Subject} from 'rxjs/Subject';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'page-root',
  templateUrl: './appstats.component.html',
  providers: [ EntityListConfig]
})
export class AppStatesComponent implements OnInit {
  appstats: any;
  title = 'app works!';
  centerText: any;
  items: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  appStatesObj: {};
  appStatesConstant: string[] = ['UNPUBLISH_INPROGRESS','UNPUBLISH_DELETED','UNPUBLISHED','UNPUBLISH_REQUESTED', 'PUBLISH_REQUESTED', 'PUBLISH_INPROGRESS','PUBLISHED', 'REJECTED']
  constructor(public route: ActivatedRoute) {
   // this.appstats = [
   //    {
   //      _id: '12345',
   //      status: 'Publishes',
   //      total: 150258,
   //      android: 90124,
   //      ios: 60364
   //    },
   //    {
   //      _id: '12345',
   //      status: 'Publish Inprogress',
   //      total: 61654,
   //      android: 6664,
   //      ios: 5499
   //    },
   //    {
   //      _id: '12345',
   //      status: 'Publish Requested',
   //      total: 1506,
   //      android: 901,
   //      ios: 603
   //    },
   //    {
   //      _id: '12345',
   //      status: 'Unpublished',
   //      total: 365,
   //      android: 264,
   //      ios: 36
   //    },
   //    {
   //      _id: '12345',
   //      status: 'Publishes',
   //      total: 99,
   //      android: 90,
   //      ios: 9
   //    },
   //    {
   //      _id: '12345',
   //      status: 'Rejected',
   //      total: 99,
   //      android: 9,
   //      ios: 9
   //    }
   //  ];

  }

  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data=> data.data)
      .subscribe(data => {
        this.appstats = data;
        this.appStatesObj = {
         data
        };
        console.log(this.appStatesObj);
      });
  }
}
