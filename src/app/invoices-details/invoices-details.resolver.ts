import { Injectable, OnInit, } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Rx';
import {  EntityListConfig, InvoicesService, DTOService } from '../shared';
import {TaxService} from "../shared/services/tax.service";

@Injectable()
export class InvoicesDetailsResolver implements Resolve<any> {

  constructor (private invoiceService: InvoicesService,
               private dtoService: DTOService,
               private taxService: TaxService
  ) {
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const invoiceID = state['url'].split('/')[1];
    const obs1 =  this.invoiceService.get(invoiceID, entityListConfig );
    const obs2 = Observable.of(invoiceID);
    const obs3 = Observable.of(this.dtoService.getValue('invoiceDate'));
    const obs4 = Observable.of(this.dtoService.getValue('invoiceNo'));
    const obs5 = this.taxService.get();
    return Observable.forkJoin(obs1, obs2, obs3, obs4, obs5);
}
}
