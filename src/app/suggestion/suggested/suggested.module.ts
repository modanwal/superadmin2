import { NgModule } from '@angular/core';
import {AuthGuard, SharedModule} from '../../shared';
import {SuggestedComponent} from './suggested.component';
import {SuggestedResolver} from './suggested.resolver';
import {SuggestedUpsertComponent} from './suggested.upsert/suggested.upsert.component';
import {SuggestedDeleteComponent} from './suggested.delete/suggested.delete.component';



export const suggestedRouting = [
  {
    path: 'suggested/:id',
    component: SuggestedComponent,
    canActivate: [AuthGuard],
    resolve: {
      data: SuggestedResolver
    }
  }
];
@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    SuggestedComponent, SuggestedUpsertComponent, SuggestedDeleteComponent
  ],
  providers: [
    AuthGuard, SuggestedResolver
  ],
  entryComponents: [ SuggestedUpsertComponent, SuggestedDeleteComponent]
})
export class SuggestedModule {}
