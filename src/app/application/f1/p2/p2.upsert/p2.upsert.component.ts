
import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P2Service} from '../../../../shared/services/p2.service';

@Component({
  selector: 'app-root',
  templateUrl: './p2.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class P2UpsertComponent implements  OnInit {
  invalidName: boolean;
  P2Form: FormGroup;
  @Input() appID;
  @Input() p2;
  @Input() pageName;
  isSubmited: Boolean;
  isSubmitting: boolean;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private p2Service: P2Service ) {
                  this.invalidName = true;
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.P2Form.valid && !this.invalidName) {
      this.isSubmitting = true;
      this.P2Form.disable();
      let p2 = this.P2Form.value;
      if(!p2._id) delete p2._id;
      this.p2Service
        .saveP2(p2, this.appID)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.P2Form.enable();
            this.activeModal.close('P2_Updated');
          },
          error => {
            this.isSubmitting = false;
            this.P2Form.enable();
          });
    }
  }
  checkNameNotChanged(event){
    this.invalidName = false;
    (event.target.value==this.p2.name) && (this.invalidName=true);
  }
  ngOnInit() {
    this.P2Form = this.formBuilder.group({
      '_id': [this.p2._id],
      'name': [this.p2.name, Validators.required]
    });

  }
}
