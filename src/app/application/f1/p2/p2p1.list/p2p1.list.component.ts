import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {P2Service, P2P1Model , EntityListConfig, DTOService, SocketService, InfiniteScrollService, ModalService} from './../../../../shared';
import {P2P1UpsertComponent} from './../p2p1.upsert/p2p1.upsert.component';
import {P2P1DeleteComponent} from './../p2p1.delete/p2p1.delete.component';

import {Subject} from "rxjs/Subject";
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'p2p1-list',
  templateUrl: './p2p1.list.component.html',
  providers: [EntityListConfig]
})
export class P2P1Component implements OnInit, OnDestroy {
  fetchingData: boolean;
  collectionDirty: boolean;
  p2p1FetchSub: Subscription;
  p2Id: string;
  p2FolderName: string;
  appID: any;
  p2p1: P2P1Model[];
  subscription: any;
  categoryID: any;
  componentID: any;
  lastFetchedItems: any;
  p2p1Obj: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor( private dtoService: DTOService,
               private route: ActivatedRoute,
               private p2Service: P2Service,
               public entityListConfig: EntityListConfig,
               private socketService: SocketService,
               private router: Router,
               private modalService: ModalService,
               public infiniteScrollService: InfiniteScrollService) {
    this.collectionDirty = false;
    this.p2p1Obj = {};
    // /*** websockets */
    // this.socketService.subscribeToEvent(`p2p1:save`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllP2P1FromSocket())
    // this.socketService.subscribeToEvent(`p2p1:remove`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllP2P1FromSocket())
    // /*** websockets */
    /*** websockets */
    this.p2p1Obj = {};
    this.socketService.subscribeToEvent(`p2p1:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.p2p1,this.p2p1Obj, this.entityListConfig);
        if(this.fetchingData){
          this.p2p1FetchSub.unsubscribe();
          this.getAllP2P1(false);
        }
      })
    this.socketService.subscribeToEvent(`p2p1:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.p2p1,this.p2p1Obj,this.entityListConfig);
        if(this.fetchingData){
          this.p2p1FetchSub.unsubscribe();
          this.getAllP2P1(false);
        }
      })
    /*** websockets */

    this.categoryID = this.dtoService.getValue('categoryID');
    this.componentID = this.dtoService.getValue('componentID');
    if(this.categoryID && this.componentID){
   console.log(this.categoryID, this.componentID, 'this one is printing all are printing');
    }
    this.p2Id = this.route.snapshot.params.id;
    this.p2FolderName = 'People';
    this.p2FolderName = this.dtoService.getValue('p2p1PageName')
                        ?this.dtoService.getValue('p2p1PageName'):this.p2FolderName;

}
  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {
          this.appID = data.appID;
          this.p2p1 = data.p2p1Data;
          this.infiniteScrollService.computeEntity(this.p2p1,this.p2p1Obj, this.entityListConfig);
          // this.lastFetchedItems = this.p2p1.length;
          // if(this.p2p1.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
        }
      );
    //**  P2P1 Route Moving
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .filter(event => event['url'].split('/')[5]=='p2p1')
      .takeUntil(this.ngUnsubscribe)
      .subscribe((routeData) => {
        this.p2Id = routeData['url'].split('/')[6];
        this.getAllP2P1(true);
      });
    // ** Route Move
  }

  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  // getAllP2P1FromSocket(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.p2Service
  //     .getAllp2p1(this.p2Id, entityListConfig, this.appID, false)
  //     .subscribe(
  //       (data) => {
  //         this.p2p1 = data;
  //         this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //         this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //         if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //       }
  //     );
  // }
  getAllP2P1(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
    this.p2Service
      .getAllp2p1(this.p2Id, this.entityListConfig, this.appID, false)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllP2P1(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.p2p1, this.p2p1Obj, this.entityListConfig, refreshData);
          }

        }
        //   if(refreshData) this.p2p1 = [];
        //   // remove last page entries if data less than ideal size
        //   if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
        //     this.p2p1.splice(
        //       (this.entityListConfig.params.pageNumber)
        //       *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
        //   }
        //   this.lastFetchedItems = data.length;
        //   if(data.length==this.entityListConfig.params.pageSize){
        //     this.entityListConfig.params.pageNumber++;
        //   }
        //   // push new data
        //   this.p2p1 = [...this.p2p1, ...data];
        // }
      ),(error)=>{this.fetchingData = true;};
  }
  editP2P1 (p2p1data: P2P1Model ) {
    const modalRef = this.modalService.open(P2P1UpsertComponent, {}, 'fade-in-pulse', 'fade-out-pulse');
    modalRef.componentInstance.p2p1 = p2p1data;
    modalRef.componentInstance.p2Id = this.p2Id;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.categoryID = this.componentID;
    modalRef.componentInstance.componentID = this.componentID;
  }
  onScroll(){
    this.getAllP2P1(false);
  }
  addP2P1(){
    const modalRef =  this.modalService.open(P2P1UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p2p1 = new P2P1Model;
    modalRef.componentInstance.p2Id = this.p2Id;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.categoryID = this.categoryID;
    modalRef.componentInstance.componentID = this.componentID;

  }
  deleteP2P1(p2p1data: P2P1Model ) {
    const modalRef = this.modalService.open(P2P1DeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p2p1 = p2p1data;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.p2ID = this.p2Id;
  }
}

