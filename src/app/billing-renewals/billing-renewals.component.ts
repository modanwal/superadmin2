import {AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {EntityListConfig, SocketService, InvoicesService, CartService, InfiniteScrollService} from '../shared';
import {ActivatedRoute, Router} from "@angular/router";
import { BreadCrumbService } from './../shared/breadcrumb.module';
import {CheckoutService} from "../shared/services/checkout.service";
import {TaxService} from "../shared/services/tax.service";
import {DTOService} from "../shared/services/dto.service";
import {Observable} from "rxjs/Rx";
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'page-root',
  templateUrl: './billing-renewals.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]

})
export class BillingRenewalsComponent implements OnInit{
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  billings: any;
  billingObj: any;
  data; any;
  isSubmitting: boolean;
  checkedCheckboxes: any[];
  droppedItems: any;
  tax: any;
  subTotal: number = 0;
  taxCount: number = 0;
  quantityAfterChange;
  incrementSumbitted: boolean;
  fetchingData: boolean;
  collectionDirty: boolean;
  billingsFetchSub: Subscription;
  constructor(public entityListConfig: EntityListConfig,
              private route: ActivatedRoute,
              private socketService: SocketService,
              private invoiceService: InvoicesService,
              private cartService: CartService,
              private breadCrumbService: BreadCrumbService,
              private checkOutService: CheckoutService,
              private taxService: TaxService,
              private router: Router,
              private dtoService: DTOService,
              public infiniteScrollService: InfiniteScrollService
              ) {
    this.collectionDirty = false;
    this.billingObj = {};
    this.incrementSumbitted = true;
    this.getTotalTax();
   this.isSubmitting = false;
   this.checkedCheckboxes = [];
    /*** websockets */
    this.socketService.subscribeToEvent(`cart:save`)
      .subscribe((data) => {
      this.collectionDirty = true;
      if(!this.checkedCheckboxes){
        this.infiniteScrollService.savedFromSocket(data, this.billings,this.billingObj, this.entityListConfig);
        if(this.fetchingData){
          this.billingsFetchSub.unsubscribe();
          this.getAllBillingRenewals(false);
        }
      }
    }
      )
    this.socketService.subscribeToEvent(`cart:remove`)
      .subscribe((data) => {
        this.collectionDirty = true;
        if(!this.checkedCheckboxes){
          this.infiniteScrollService.savedFromSocket(data, this.billings,this.billingObj, this.entityListConfig);
          if(this.fetchingData){
            this.billingsFetchSub.unsubscribe();
            this.getAllBillingRenewals(false);
          }
        }
      })
    /*** websockets */

    this.breadCrumbService
      .pushBreadCrumbItem({url:'/billing-renewals',label:'billing-renewals'},true);
  }
  getTotalTax(){
    this.taxService
      .get()
      .subscribe(
        (data)=>{
          this.tax = data;
          for(var value of this.tax){
            this.taxCount = value.percent;
          }
        } );
  }
  onItemDrop(e: any) {
    // Get the dropped data here
    this.droppedItems.push(e.dragData);
  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data => {
        this.billings = data;
        this.infiniteScrollService.computeEntity(this.billings,this.billingObj, this.entityListConfig);
      });
    this.breadCrumbService.pushBreadCrumbItem(
      {url: '/billing-renewals',label: 'Billing & Renewals'},
    true);
  }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  totalAmount(amount){

    return (amount.qty * ( amount.templateID.price - ((amount.templateID.discount * amount.templateID.price) / 100))).toFixed(2);
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllBillingRenewals(true);
  }
  onScroll(){
    this.getAllBillingRenewals(false);
  }
  /*** sorting & pagination */
  getByObject(object: any){
    const str =  object.applicationID.name + '-template-' + object.templateID.name;
    return str;
  }

  getSubTotal(singleCheckOut: any) {
    var grandTotal= 0;
    for(let total of singleCheckOut){
      grandTotal += total.qty * ( total.templateID.price - ((total.templateID.discount * total.templateID.price)/100));
    }
    this.subTotal = grandTotal;
    return grandTotal.toFixed(2);
  }
  removeitemaddintoCheckout(clickremove: any) {

    this.checkedCheckboxes.push(clickremove);
    let index: number = this.billings.indexOf(clickremove);
    if (index !== -1) {
      this.billings.splice(index, 1);
    }
  }
  getGrandTotal(checkout, tax){
    return (this.subTotal + (this.subTotal * this.taxCount)/100).toFixed(2);
  }
  addIntoBillingsRemoveSelcted(singleCheckBox){
    this.billings.push(singleCheckBox);
      let index: number = this.checkedCheckboxes.indexOf(singleCheckBox);
      if (index !== -1) {
        this.checkedCheckboxes.splice(index, 1);
      }
    }

  getAllBillingRenewals(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
   this.billingsFetchSub =  this.cartService
      .getAll( this.entityListConfig)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllBillingRenewals(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.billings, this.billingObj, this.entityListConfig, refreshData);
          }
        }
      ),(error)=>{this.fetchingData = false;};;
  }

  getTax(){
    return ((this.subTotal * this.taxCount)/100).toFixed(2);
  }

  hitApiIncrement(bill : any){
    this.quantityAfterChange = bill.qty;
     bill.qty++;
    Observable.timer(500)
      .subscribe(
        (value)=>{
          if(this.incrementSumbitted)
          {
            this.incrementSumbitted = false;
            const data = {
              qty: bill.qty
            };
            this.cartService
              .save(data, bill._id)
              .subscribe(
                (value1)=>{
             this.incrementSumbitted = true;
                },
                (err)=> {
                 bill.qty = this.quantityAfterChange;
                });
          }
        });

  }
  hitApiDecrement(bill : any){
    this.quantityAfterChange = bill.qty;
       bill.qty--;
    Observable.timer(500)
      .subscribe(
        (value)=> {
          if (this.incrementSumbitted) {
            this.incrementSumbitted = false;
            const data = {
              qty: bill.qty
            };
            this.cartService
              .save(data, bill._id)
              .subscribe(
                (value1) => {
                  this.incrementSumbitted = true;
                },
                (err)=> {
                  bill.qty = this.quantityAfterChange;
                });
          }
        });
  }

  checkOut(checkout: any){
    const data = {
      appID: '',
      userID: '',
      cart: []
    }
    for(let single of checkout) {
      console.log('this one is printing', checkout);
      data.appID = single.appID._id,
        data.userID = single.userID._id,
        data.cart.push(single._id);
    }
      this.checkOutService
        .save(data)
        .subscribe(
          (value)=>{
            this.dtoService.setValue('amount', value.total);
            this.dtoService.setValue('gatewayID', value.gatewayID);
            this.router.navigateByUrl(`/paymentconfirm`);
          });


  }
  getCheckedAmount(singleAmount) {

    return (singleAmount.qty * ( singleAmount.templateID.price - ((singleAmount.templateID.discount * singleAmount.templateID.price)/100))).toFixed(2);
  }
}
