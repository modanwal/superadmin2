import {Component, OnInit, Output} from '@angular/core';
import { ApplicationStatusModal, EntityListConfig,InfiniteScrollService , SocketService} from '../../shared';
import { BreadCrumbService } from "./../../shared/breadcrumb.module";
import { ActivatedRoute } from "@angular/router";
import {Subject} from "rxjs/Subject";
import {ApplicationStatusService} from "../../shared/services/applicationStatus.service";


@Component( {
  selector: 'application-status-pages',
  templateUrl: './application-status.component.html'
})
export class UserDetailsApplicationStatusComponent implements OnInit{
  user: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  lastFetchedItems: number;
  applicationstatus: ApplicationStatusModal[];
  roles: any[];
  applicationStatus: any;
  id: any;
  userData: any;
  applicationStatusObj: any;
  constructor (private breadCrumbService: BreadCrumbService,
               private route: ActivatedRoute,
               private socketService: SocketService,
               public entityListConfig: EntityListConfig,
               private applicationStatusService: ApplicationStatusService,
               public infiniteScrollService: InfiniteScrollService,) {
                 this.applicationStatusObj = {};

    this.user = this.route.parent.snapshot.data['user'];
    console.log(this.user);
    this.breadCrumbService
              .pushBreadCrumbItem({url:`/user-details/${this.user['_id']}/application-status`, label: 'Application Status'},false);

    /*** websockets */
    this.socketService.subscribeToEvent(`application-status:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.savedFromSocket(data, this.applicationStatus,this.applicationStatusObj, this.entityListConfig);
      })
    this.socketService.subscribeToEvent(`application-status:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.removedFromSocket(data, this.applicationStatus,this.applicationStatusObj,this.entityListConfig);
      })
    /*** websockets */
  }

  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllApplicationStatus(true);
  }
  onScroll(){

  }
  getAllApplicationStatus(refreshData) {

  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data =>{
        this.applicationstatus = data[0];
         this.userData = data[2];
        this.infiniteScrollService.computeEntity(this.applicationstatus,this.applicationStatusObj, this.entityListConfig);
      } );
  }
  setDefaultRole(event, applicationStatus){
    const roleID = event.target.value;
    console.log(roleID);
    const data = {};
    data['roleID'] = roleID;
    console.log(applicationStatus);
    this.applicationStatusService
      .saveRole(data, this.userData._id, applicationStatus.appID._id)
      .subscribe(data =>{
        //    this.role = data;
      });
  }
  setDefaultStatus(event, applicationStatus){
   const status = event.target.value;
   const data = {};
   data['status'] = status;
   console.log('changed Default Status', data);
    this.applicationStatusService
      .saveRole(data, this.userData._id, applicationStatus.appID._id)
      .subscribe(data =>{
        //    this.role = data;
      });
  }

  addApplication() {

  }


}
