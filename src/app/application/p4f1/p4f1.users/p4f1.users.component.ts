import {Component, ViewEncapsulation, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { P4Service, P4F1Service, P4F1Model, SocketService, EntityListConfig, InfiniteScrollService } from './../../../shared';
import { Subject } from "rxjs/Subject";


@Component({
  selector: 'p4f1.users-component',
  templateUrl: './p4f1.users.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})

export class P4f1UsersComponent implements OnInit, OnDestroy {
  p4f1Types: any[];
  p4ID: any;
  appID: any;
  p4f1Data: P4F1Model[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  lastFetchedItems: any;
  p4f1DataObj: any;
  constructor(private route: ActivatedRoute,
              private p4Service: P4Service,
              private p4f1Service: P4F1Service,
              private socketService: SocketService,
              private entityListConfig: EntityListConfig,
              public infiniteScrollService: InfiniteScrollService) {
    this.p4f1DataObj = {};
      this.entityListConfig.params.pageSize = -1;
  this.lastFetchedItems = 0;
    /*** websockets */
    this.socketService.subscribeToEvent(`p4f1:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.savedFromSocket(data, this.p4f1Data,this.p4f1DataObj, this.entityListConfig);
      })
    this.socketService.subscribeToEvent(`p4f1:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.removedFromSocket(data, this.p4f1Data,this.p4f1DataObj,this.entityListConfig);
      })
    /*** websockets */
  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data =>{
         console.log('Data:',data);
         this.appID = data[0];
         this.p4ID = data[1];
         this.p4f1Types = data[2];
         this.p4f1Data = data[3];
        this.infiniteScrollService.computeEntity(this.p4f1Data,this.p4f1DataObj, this.entityListConfig);
        // this.lastFetchedItems = this.p4f1Data.length;
        // if(this.p4f1Data.length == this.entityListConfig.params.pageSize){
        //   this.entityListConfig.params.pageNumber++;
        // }
      });
    // /*** websockets */
    // this.socketService.subscribeToEvent(`p4f1:save`)
    //     .takeUntil(this.ngUnsubscribe)
    //     .subscribe(data => this.getAllP4F1ForSockets())
    // this.socketService.subscribeToEvent(`p4f1:remove`)
    //     .takeUntil(this.ngUnsubscribe)
    //     .subscribe(data => this.getAllP4F1ForSockets())
    // /*** websockets */
  }
  submitChange(event,p4f1){
    event.preventDefault();
    event.target.disabled = true;
    let submitData = {};
    submitData['_id'] = p4f1._id;
    submitData['accept']= p4f1.accept;
    submitData['accomodation'] = p4f1.accomodation;
    this.p4f1Service.save(submitData,this.p4ID,this.appID)
        .subscribe(
          (data)=> event.target.disabled = false,
          (error)=> event.target.disabled = false
        );
  }
  onScroll(){
    this.getAllP4F1(false);
  }
  getAllP4F1(refreshData){
    this.p4f1Service
      .getAll(this.entityListConfig, this.p4ID, this.appID)
      .subscribe(
        (data) => {
          this.infiniteScrollService.getFromApi(data, this.p4f1Data, this.p4f1DataObj, this.entityListConfig, refreshData);
          // if(refreshData) this.p4f1Data = [];
          // // remove last page entries if data less than ideal size
          // if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
          //   this.p4f1Data.splice(
          //     (this.entityListConfig.params.pageNumber)
          //     *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          // }
          // this.lastFetchedItems = data.length;
          // if(data.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // // push new data
          // this.p4f1Data = [...this.p4f1Data,...data];
        });
  }
  // getAllP4F1ForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.p4f1Service
  //     .getAll(entityListConfig, this.p4ID, this.appID, true)
  //     .subscribe(
  //       (data) => {
  //         this.p4f1Data = data;
  //         this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //         this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //         if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //       });
  // }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
