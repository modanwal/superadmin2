import {Component, Input, ViewEncapsulation} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from './../../shared';
import {DTOService} from "../../shared/services/dto.service";

@Component({
  selector: 'fill-details-page',
  templateUrl: './fill-details.component.html',
  encapsulation: ViewEncapsulation.None
})

export class FillDetailsComponent {
  detailsForm: FormGroup;
  isSubmited: boolean;
  imagePreview: any;
  file: any;
  formData: FormData;
  templateID: string;
  email: string;
  @Input() appUser;
  isSubmitting: boolean;
  constructor(private route: ActivatedRoute, private router: Router,
              public formBuilder: FormBuilder,
              private appService:AppService,
              private dtoService: DTOService) {
    this.isSubmited = false;
    this.email = this.dtoService.getValue('createAppEmail');
  }
  ngOnInit(){
    this.templateID = this.route.snapshot.params.templateID;
     // use FormBuilder to create a form group
    this.detailsForm = this.formBuilder.group({
      'templateID': [this.templateID],
      'name': ['', Validators.required],
      'description': ['', Validators.required]
    });
  }
  selectFile(event){
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    const file = event.target.files[0];

    fileReader.addEventListener("load", ()=>{
      this.imagePreview = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }
  submitForm() {
    if (!this.isSubmited) {this.isSubmited = true; }
    if (this.detailsForm.valid) {
      this.detailsForm.disable();
      const app = this.detailsForm.value;
      this.formData = new FormData();
      this.formData.append('templateID', app.templateID);
      this.formData.append('name', app.name);
      this.formData.append('description', app.description);
      this.formData.append('email', this.email);
      if(this.file){
        this.formData.append('file', this.file);
      }

      this.appService
        .saveFormData(this.formData)
        .subscribe(
          (data) => {this.router.navigateByUrl(`/all-apps`)},
          (err)=>{ this.detailsForm.enable();}

        );
    }
  }
}
