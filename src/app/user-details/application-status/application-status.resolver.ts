import { Injectable, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, EntityListConfig, ApplicationStatusService } from './../../shared';


@Injectable()
export class ApplicationStatusResolver implements Resolve<any> {

  constructor(private userService: UserService ,
              private router: Router,
              public entityListConfig: EntityListConfig,
              private applicationStatusService: ApplicationStatusService
              ) {}

resolve(route: ActivatedRouteSnapshot,
       state: RouterStateSnapshot): Observable<any> {
  const userID = state['url'].split('/')[2];
  console.log(userID);
  const entityListConfig = new EntityListConfig();
  const obs1 =  this.applicationStatusService.getAll(userID, entityListConfig);
  const obs2 =  this.applicationStatusService.getAllRole(userID, entityListConfig);
  const obs3 =  this.userService.get(userID);
  return Observable.forkJoin(obs1, obs2, obs3);
    }
}
