import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { BreadCrumbService } from "./../../../shared/breadcrumb.module";
import { ResourcePrivilegeService, TemplateResourcePrivilegeService, EntityListConfig, ModalService} from './../../../shared';
import {PrivilegesUpsertComponent} from "./privileges.upsert/privileges.upsert.component";

@Component({
  selector: 'priviliges-comp',
  templateUrl: './priviliges.component.html',
  providers: [EntityListConfig]
})
export class PriviligesComponent implements OnInit {
  resource_privileges_data: any;
  resource_privileges: any;
  components: any;
  templateID: any;

  constructor(private resourcePrivilegeService: ResourcePrivilegeService,
              private templateResourcePrivilegeService: TemplateResourcePrivilegeService,
              private route: ActivatedRoute,
              public entityListConfig: EntityListConfig,
              private breadCrumbService: BreadCrumbService,
              private modalService: ModalService){
    this.templateID = this.route.snapshot.params.id;
    this.breadCrumbService
              .pushBreadCrumbItem({url:'/privileges',params:this.templateID,label:'Privileges'},false);
  }
  ngOnInit(){
    // this.templateResourcePrivilegeService
    //     .getTemplateResourcePrivilege(this.templateID, this.entityListConfig)
    //     .subscribe(
    //       (data) => {
    //         this.resource_privileges_data = data;
    //         console.log('rasource_privilege_data', this.resource_privileges_data)
    //         this.getResourcePrivileges();
    //       }
    //     )
    // this.getSelectedTemplateResourcePrivilege();
   // this.getResourcePrivileges();
    this.getSelectedTemplateResourcePrivilege();
  }
  changePrivilege(privilege: any, resourceID){
    console.log('this line is printing', privilege);
    const modalRef = this.modalService.open ( PrivilegesUpsertComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.privilege = privilege;
    modalRef.componentInstance.templateID = this.templateID;
    modalRef.componentInstance.resourceID = resourceID;
     modalRef.result.then((closeReason) => {
      if (closeReason == 'privilege:updated'){
        this.getSelectedTemplateResourcePrivilege();
      }
     });
  }
  getSelectedTemplateResourcePrivilege(){
    this.templateResourcePrivilegeService
      .getTemplateResourcePrivilege(this.templateID, this.entityListConfig)
      .subscribe(
        (data) => {
          this.resource_privileges_data = data;
          // console.log(this.resource_privileges_data, 'this is printing line no 52');
          // this.resource_privileges_data.forEach(function(e){
          //   if (typeof e === "object" ){
          //     if(e.privileges){
          //       e.privileges.unshift({_id:'1234',name:'selectAll'}, {_id:'12345',name:'unSelectAll'});
          //     }
          //   }
          // });

        });
  }

  getResourcePrivileges(){
    this.resourcePrivilegeService
      .getResourcePrivlegesForTemplate(this.templateID)
      .subscribe((data) => {
          this.resource_privileges = data;
        // console.log(this.resource_privileges, 'this line no 62');
        // this.resource_privileges.forEach(function(e){
        //   if (typeof e === "object" ){
        //    if(e.privileges){
        //      e.privileges.unshift({_id:'1234',name:'selectAll'}, {_id:'12345',name:'unSelectAll'});
        //    }
        //   }
        // });

          console.log('reasource privileges line no 72 ', this.resource_privileges);
        });
  }
  selectAllStatus(privileges){
    var count = 0;
    var status = privileges.length;
    for(var privilege of privileges){
      if(privilege.checked){
        count++;
      }
    }
    if(count === status){
      return true;
    } else {
      return false;
    }

  }
  selectCheckboxClick(event, resourceID){
    if(event.target.checked){
      const data = {
        templateID: this.templateID,
        resourceID: resourceID
      }
      this.templateResourcePrivilegeService
        .selectAllCheckBoxSave(data)
        .subscribe(
          (value)=>{
            event.target.checked = true;
            this.getSelectedTemplateResourcePrivilege();
          },
          (error)=>{
            event.target.checked = false;
          });
    } else if(!event.target.checked){
      this.templateResourcePrivilegeService
        .unSelectAllCheckBoxSave(this.templateID, resourceID )
        .subscribe(
          (value)=>{
            event.target.checked = true;
            this.getSelectedTemplateResourcePrivilege();
          },
          (error)=> {
            event.target.checked = false;
          });
    }
  }
  getCheckedStatus(resourceID,privilegeID){
    if(!this.resource_privileges_data[resourceID]) return false;
    if(this.resource_privileges_data[resourceID].indexOf(privilegeID)!=-1) return true;
    return false;
  }
  checkboxClicked(event, resourceID, privilegeID){
    event.preventDefault();
    event.target.disabled = true;
      if(event.target.checked){
        const data = {
          templateID: this.templateID,
          resourceID: resourceID,
          privilegeID: privilegeID
        }
        this.templateResourcePrivilegeService
          .save(data)
          .subscribe(data => {
              event.target.checked = true;
              event.target.disabled = false;
              this.getSelectedTemplateResourcePrivilege();
            },
            (error)=>{
              event.target.checked = false;
              event.target.disabled = false;
            }
          );
      }else{
        this.templateResourcePrivilegeService
          .destroy(this.templateID, resourceID, privilegeID)
          .subscribe(
            (data) => {
              event.target.checked = false;
              event.target.disabled = false;
              this.getSelectedTemplateResourcePrivilege();
            },
            (error) => {
              event.target.checked = true;
              event.target.disabled = false;
            }
          )
      }
  }

}
