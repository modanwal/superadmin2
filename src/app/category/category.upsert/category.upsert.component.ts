import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from '../../shared';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'category-upsert-pages',
  templateUrl: './category.upsert.component.html',
  encapsulation: ViewEncapsulation.None

})

export class CategoryUpsertComponent implements  OnInit {
  isSubmitting: boolean;
  imagePreview: any;
  categoryForm: FormGroup;
  file: any;
  formData: FormData;
  @Input() category;
  categoryID: string;

  // categoryForm: FormGroup;
  isSubmited: Boolean;
  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private categoryService: CategoryService,
              private route: ActivatedRoute) {
    this.isSubmited = false;
    this.isSubmitting = false;
  }

  selectFile(event){
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    const file = event.target.files[0];

    fileReader.addEventListener("load", ()=>{
      this.imagePreview = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }
  eventHandler(event) {
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.categoryForm.valid) {
      this.isSubmitting = true;
      this.categoryForm.disable();
      const category = this.categoryForm.value;
      this.formData = new FormData();

      this.formData.append('name', category.name);
      this.formData.append('description', category.description);
      if(this.file){
        this.formData.append('file', this.file);
      }
      if(!category._id && category.parentID) {
        this.formData.append('parentID', category.parentID);
      }
      this.categoryService
        .saveFormData(this.formData, category._id)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.categoryForm.enable();
            this.activeModal.close('category:updated');
          },
          error => {
            this.isSubmitting = false;
            this.categoryForm.enable();
          }
        );
    }
  }
  ngOnInit() {
    // use FormBuilder to create a form group
    this.categoryForm = this.formBuilder.group({
      '_id': [this.category._id],
      'name': [this.category.name, Validators.required],
      'path': [this.category.path],
      'description': [this.category.description, Validators.required],
      'parentID' : [this.category.parentID]
    });
  }




}
