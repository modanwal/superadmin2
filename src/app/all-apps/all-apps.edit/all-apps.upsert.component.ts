import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {AppService} from "../../shared/services/app.service";

@Component({
  selector: 'allapps-upsert-modal',
  templateUrl: './all-apps.upsert.component.html'
})
export class AllAppsUpsertComponent implements  OnInit {
  imagePreview: any;
  @Input() app;
  file: any;
  appForm: FormGroup;
  isSubmited: Boolean;
  isSubmitting: Boolean;
  formData: any;
  constructor( private formBuilder: FormBuilder,
               public activeModal: NgbActiveModal,
               private appService: AppService
               ) {
    this.isSubmited = false;
  }
  selectFile(event){
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    const file = event.target.files[0];
    this.imagePreview = file;
    fileReader.addEventListener("load", ()=>{
      this.imagePreview = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }
  ngOnInit() {
    //   use FormBuilder to create a form group
    this.appForm = this.formBuilder.group({
      '_id': [this.app._id],
      'name': [this.app.name, Validators.required],
      'description': [this.app.description, Validators.required],
      'path': [this.app.path],
    });
  }

  submitForm() {
    this.isSubmited = true;
    if (this.appForm.valid) {
      this.isSubmitting = true;
      this.appForm.disable();
      const apps = this.appForm.value;
      this.formData = new FormData();
      this.formData.append('name', apps.name);
      this.formData.append('description', apps.description);
      if(this.file){
        this.formData.append('file', this.file);
      }

      this.appService
        .saveWithFormData(apps, this.formData)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.appForm.enable();
            this.activeModal.close('app:save');
          },
      error => {
        this.isSubmitting = false;
        this.appForm.enable();
          }
        );
    }
  }
}
