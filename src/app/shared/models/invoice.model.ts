export class InvoiceModel {
  _id: string;
  invoiceNumber: string;
  issue_date: string;
  paymentID: string;
  appID: string;
  userID: string;
}
