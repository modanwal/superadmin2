import { Router } from '@angular/router';
import { Component, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ComponentModel, ComponentService } from '../../shared';


@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './component.delete.component.html'
})
export class NgbdModalDeleteContent {
  @Input() component: ComponentModel;
  @Input() components: ComponentModel;
  isSubmited: Boolean;

  constructor(public activeModal: NgbActiveModal, private componentService: ComponentService) {

  }

  deleteComponent(component: ComponentModel){
    this.isSubmited = true;
    this.componentService.destroy(component._id)
      .subscribe(
        data => this.activeModal.close('modal close')      );
  }
}
