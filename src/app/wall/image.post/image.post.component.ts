import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {WallPostModal} from "../../shared/models/wallPost.modal";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {PostService} from "../../shared/services/post.service";

@Component({
  selector: 'image-post-modal',
  templateUrl: './image.post.component.html',
  styleUrls: ['image.post.component.scss']
})
export class ImagePostComponent implements OnInit {
  data: {};
  @Input() file;
  submitting: Boolean;
  constructor(public activeModal: NgbActiveModal){
    this.submitting = false;
  }
  ngOnInit(){
  }
  submitForm(){
  }
}
