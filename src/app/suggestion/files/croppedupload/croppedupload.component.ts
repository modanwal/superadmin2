import { Component, Input, OnInit, ViewEncapsulation, OnChanges, SimpleChanges, SimpleChange, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {CcldService,FileUploadService} from "../../../shared";
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
import { UploaderService } from "./../../../shared/uploader/uploader.service";

@Component({
  selector: 'cropped-upload',
  templateUrl: './croppedupload.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [CropperSettings]
})
export class CroppedUpload implements  OnInit {
  cropBounds: Bounds;
  data: {};
  cropperSetting;

  @Input() files;
  @Input() path;
  @Input() cropDimension;
  file;
  cropData: any;
  imagePreview;
  editMode: any;
  isSubmited: Boolean;
  formData: FormData;
  isSubmitting: boolean;
  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;

  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private ccldService: CcldService,
                private fileUploadService: FileUploadService,
                private modalService: NgbModal,
                public cropperSettings: CropperSettings,
                private uploaderService: UploaderService
  ) {
    this.cropperSettings.noFileInput = true;
  }
  submitForm() {
    let fd = new FormData();
    //fd.append('cropBounds',JSON.stringify(this.cropBounds));
    if(this.file){
      fd.append('file',this.file);
    }
    if(this.path){
      fd.append('data',this.path);
    }
    fd.append('type','file');
    let title;
    if(this.file){
      title = this.file.name;
    }else{
      if(this.path.indexOf('staticflickr')!=-1){
        title = 'flickr';
      }
      else if(this.path.indexOf('giphy')!=-1){
        title = 'Giphy';
      }
    }
    this.uploaderService.uploadItem('/data',fd,{
      itemType: 'File',
      title: title,
    });
    this.activeModal.close();
  }
  ngOnInit() {
    if(this.cropDimension){
        this.cropperSettings.width = this.cropDimension.width;
        this.cropperSettings.height = this.cropDimension.height;
        this.data = {};
    }
    let image:any = new Image();
    if(this.path && this.cropDimension) {
      image.crossOrigin = "anonymous";
      image.onload = ()=>{
        this.cropper.setImage(image);
      }
      image.src = this.path;
    }else if(this.file && this.cropDimension){
      let myReader:FileReader = new FileReader();
      let that = this;
      myReader.onloadend = function (loadEvent:any) {
          image.src = loadEvent.target.result;
          that.cropper.setImage(image);
      };
      myReader.readAsDataURL(this.file);
    }
  }
  cropped(bounds:Bounds){
    this.cropBounds = bounds;
  }
}
