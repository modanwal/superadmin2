
import {Component, Input, OnChanges, OnInit, Output, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DTOService} from "../../services/dto.service";

@Component({
  selector: 'app-root',
  templateUrl: './image-sence.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ImageSenceComponent implements  OnInit, OnChanges {
  @Input() componentID;
  @Input() categoryID;
  @Input() ccld;
  @Input() type;

  @ViewChild('file') file: TemplateRef<any>;
  @ViewChild('string') string: TemplateRef<any>;
  constructor(public activeModal: NgbActiveModal, private dtoService: DTOService) {

  }
  submitForm() {

  }
  getType(typeValue){
    return this[typeValue];
  }
  saveImageToDB(content: any){
    this.activeModal.close(content);
  }
  close(){
    this.activeModal.close();
  }

  ngOnInit() {


  }
  ngOnChanges(){
    console.log(this.ccld,'from image');
  }
}
