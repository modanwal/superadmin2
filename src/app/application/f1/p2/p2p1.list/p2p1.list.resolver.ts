import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { EntityListConfig, P2Service } from './../../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P2P1Resolver implements Resolve<any> , OnInit{
  routeData: Subscription;
  constructor (private p2Service: P2Service) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const appID = state['url'].split("/")[2];
    const p2Id = state['url'].split('/')[6];
    console.log(p2Id);
    return this.p2Service.getAllp2p1(p2Id, entityListConfig, appID, false).map(
      (p2p1Data) =>{
        let data = {};
        data['appID'] = appID;
        data['p2p1Data'] = p2p1Data;
        return data;
      }
    );
  }
}

