import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import {WallUpsertComponent} from "./post.upsert/post.upsert.component";
import {WallPostModal, LikeService, CommentService, SocketService,ModalService,   EntityListConfig, PostService,InfiniteScrollService} from "../shared";
import {Subject} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {PostDeleteComponent} from "./post.delete/post.delete.component";

@Component({
  selector: 'wall-page',
  templateUrl: './wall.component.html',
  styleUrls:['./wall.component.scss'],
  providers: [EntityListConfig],
  encapsulation: ViewEncapsulation.None
})
export class WallComponent implements OnInit, OnDestroy {
  authenticated: Boolean;
  post: any;

  //lastFetchedItems: number;
  userData: any;
  apps: any;
  posts: any;
  postsObj: any;
  //moreComments: any[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor( private modalService: ModalService,
               private route: ActivatedRoute,
               private socketService: SocketService,
               private postService: PostService,
               public entityListConfig: EntityListConfig,
               public infiniteScrollService: InfiniteScrollService,
               private likeService: LikeService,
               private commentService: CommentService){
                 this.postsObj = {};

    /*** websockets */
    this.socketService.subscribeToEvent(`apppost:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        console.log('got socket item',data);
        this.infiniteScrollService.savedFromSocket(data,this.posts,this.postsObj,this.entityListConfig);
        /*** old code */
        // console.log("data Emitted from AppPost::save",data);
        // let postIndex = this.posts.findIndex(function(post){
        //             return post._id == data._id;
        //           });
        // if(postIndex!=-1){
        //   //update the given post
        //   this.posts[postIndex] = data;
        // }
        // else{
        //   //now push the post
        //   this.posts = [data,...this.posts];
        //   //update last fetched item & pageSize
        //   this.lastFetchedItems = (this.posts.length)%(this.entityListConfig.params.pageSize);
        //   this.entityListConfig.params.pageNumber = Math.ceil(this.posts.length/this.entityListConfig.params.pageSize);
        //   if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
        // }
      })

    this.socketService.subscribeToEvent(`apppost:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        console.log('removed from socket item',data);
        this.infiniteScrollService.removedFromSocket(data,this.posts,this.postsObj,this.entityListConfig);
        // let postIndex = this.posts.findIndex(function(post){
        //             return post._id == data._id;
        //           });
        // if(postIndex!=-1){
        //   //remove the given post
        //   delete this.posts[postIndex];
        //   //update last fetched item & pageSize
        //   this.lastFetchedItems = (this.posts.length)%(this.entityListConfig.params.pageSize);
        //   this.entityListConfig.params.pageNumber = Math.ceil(this.posts.length/this.entityListConfig.params.pageSize);
        //   if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
        // }
      })
    /*** websockets */
    /*** comments websockets */
    this.socketService.subscribeToEvent(`comment:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        const postIndex = this.posts.findIndex(post=>{
          return post.postID._id == data.postID;
        })
        if(postIndex!=-1){
          let postCommentObject = {};
          this.infiniteScrollService.savedFromSocket(data,this.posts[postIndex]['postID']['comments'],postCommentObject);
          //find commentIndex to increase total comments count!
          const commentIndex = this.posts[postIndex]['postID']['comments'].findIndex(comment=>{
            return comment._id == data._id;
          });
          if(commentIndex != -1){
            this.posts[postIndex]['postID']['comment'] += 1;
          }
          //old code
          // const commentIndex = this.posts[postIndex]['postID']['comments'].findIndex(comment=>{
          //   return comment._id == data._id;
          // })
          // if(commentIndex!=-1){
          //   //update comment
          //   this.posts[postIndex]['postID']['comments'][commentIndex] = data;
          // }else{
          //   //insert new comment
          //   this.posts[postIndex]['postID']['comments'] = [data,...this.posts[postIndex]['postID']['comments']];
          //   //update comment count
          //   this.posts[postIndex]['postID']['comment'] += 1;
          // }
        }
      })
    this.socketService.subscribeToEvent(`comment:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.posts.forEach((post,postIndex)=>{
          post.postID.comments.forEach((comment,commentIndex)=>{
            if(comment._id==data._id){
              this.posts.comment.splice(commentIndex,1);
              //update comment count
              this.posts[postIndex]['postID']['comment'] -= 1;
            }
          })
        })
      })
    /*** comments websockets */
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  deletePost(post: any){
    const modalRef = this.modalService.open(PostDeleteComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.post = post;
  }
  onScroll(){
    this.getAllPost(false);
  }

  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data =>{
        this.apps = data[0];
        this.userData = data[1];
        this.posts = data[2];

        this.infiniteScrollService.computeEntity(this.posts,this.postsObj,this.entityListConfig);

        /*** old code */
        // this.lastFetchedItems = (this.posts.length)%(this.entityListConfig.params.pageSize);
        // this.entityListConfig.params.pageNumber = Math.ceil(this.posts.length/this.entityListConfig.params.pageSize);
        // if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
      });
  }
  getAllPost(refreshData){
    this.postService
      .getAll(this.entityListConfig)
      .subscribe(
      data => {
        this.infiniteScrollService.getFromApi(data,this.posts,this.postsObj,this.entityListConfig);
        // if(refreshData)  this.posts = [];
        // if(data.length==this.entityListConfig.params.pageSize){
        //   this.entityListConfig.params.pageNumber++;
        // }
        // this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
        // data = data.slice(this.lastFetchedItems);
        // this.posts = [...this.posts, ...data];
        // //filter duplicate posts if any coming from sockets!
        // this.posts = this.posts.filter(function(item, pos, self) {
        //   return self.indexOf(item) == pos;
        // });

      });
    }
  hitComment(selectedId, appId) {
    const commentdata = 'rahul';
    this.commentService
      .saveComment(commentdata, selectedId, appId)
      .subscribe(
        data => {

        });
  }
  writeData(){
    const modalRef = this.modalService.open ( WallUpsertComponent, {
      backdrop: false
    },'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.post = new WallPostModal;
    modalRef.componentInstance.userData = this.userData;
    modalRef.componentInstance.apps = this.apps;
  }
}
