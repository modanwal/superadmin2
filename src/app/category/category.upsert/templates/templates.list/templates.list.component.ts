import { Component, Output, EventEmitter, Input } from '@angular/core';
import { TemplateModel } from '../../../../shared';
import { Router } from '@angular/router';

@Component( {
    selector: 'template-list',
    templateUrl: './templates.list.component.html'
  }
)

export class TemplatesListComponent {
  @Input() templates: TemplateModel[];
  @Input() entityListConfig: any;
  @Output('selectTemplate') selectTemplateEmitter = new EventEmitter<TemplateModel>();
  @Output('editTemplate') editTemplateEmitter = new EventEmitter<TemplateModel>();
  @Output('deleteTemplate') deleteTemplateEmitter = new EventEmitter<TemplateModel>();

  @Output('setEntityConfig') setEntityConfigEmitter = new EventEmitter<any>();
  @Output('paginateListing') paginateListingEmitter = new EventEmitter<any>();

  constructor (private router: Router) {
  }
  getListSortData(sortBy: string) {
    if(this.entityListConfig.query.sortBy === sortBy) {
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  goToPrivileges(template: TemplateModel){
    this.router.navigateByUrl(`template/${template._id}/privileges`);
  }
  setListSorting(event) {
    this.setEntityConfigEmitter.emit({sortBy: event.sortBy, sortOrder: event.sortOrder});
  }
  onScroll(){
    this.paginateListingEmitter.emit();
  }

  selectTemplate(template: TemplateModel) {
    this.selectTemplateEmitter.emit(template);
  }
  editTemplate(template: TemplateModel) {
    this.editTemplateEmitter.emit(template);
  }
  deleteTemplate(template: TemplateModel) {
    this.deleteTemplateEmitter.emit(template);
  }



}
