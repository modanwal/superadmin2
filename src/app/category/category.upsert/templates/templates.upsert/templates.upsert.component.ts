import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TemplatesService } from '../../../../shared/services';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'template-upsert-pages',
  templateUrl: './templates.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})


export class TemplatesUpsertComponent implements  OnInit{
  @Input() component;
  templatesForm: FormGroup;
  isSubmited: Boolean;
  @Input() categoryID;
  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private templatesService: TemplatesService,
              private route: ActivatedRoute,) {

  }
  setValue(value: string){
    if(value === 'FREE'){

      this.templatesForm.controls.price.setValue(0);
      this.templatesForm.controls.discount.setValue(0);
    } else if(value === 'PAID'){

      this.templatesForm.controls.price.setValue('');
      this.templatesForm.controls.discount.setValue('');
    }
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm(){
    if(!this.isSubmited){this.isSubmited = true }
    if(this.templatesForm.valid){
      this.isSubmited = true;
      this.templatesForm.disable();
      const templates = this.templatesForm.value;
      templates.categoryID = this.categoryID;
      this.templatesService
        .save(templates)
        .subscribe(
          data => {
            this.activeModal.close();
          },
          error => {
            this.templatesForm.enable();
          }
        );

    }
  }

  ngOnInit(){
    // use FormBuilder to create a form group
    this.templatesForm = this.formBuilder.group({
      '_id': [this.component._id],
      'name': [this.component.name, Validators.required],
      'type': [this.component.type, Validators.required],
      'discount': [this.component.discount, Validators.required],
      'price': [this.component.price, Validators.required]
    });


  }

}
