
import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P2Service} from '../../../../shared/services/p2.service';

@Component({
  selector: 'app-root',
  templateUrl: './p2p1.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class P2P1UpsertComponent implements  OnInit {
  P2P1Form: FormGroup;
  @Input() appID;
  imagePreview: any;
  @Input() p2p1;
  @Input() p2Id;
  isSubmited: Boolean;
  isSubmitting: Boolean;
  file: any;
  @Input() categoryID;
  @Input() componentID;
  path: any;
  selfReference: any;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private p2Service: P2Service ) {

  }

  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  onContentSelected(event){
    console.log(event,' here thing thing is printing which we want');
    console.log(event.labelName);
    if(event.labelName === 'file-1'){
      this.imagePreview = event.path;
      this.path = event.path;
    } else if (event.labelName === 'description-1'){
     this.P2P1Form.controls['description'].setValue(event.path);
    } else if (event.labelName === 'relation'){
      this.P2P1Form.controls['relation'].setValue(event.path);
    }
  }
  selectFile(event) {
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    const file = event.target.files[0];
    fileReader.addEventListener('load', () => {
      this.imagePreview = fileReader.result;
      this.p2p1.path = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }
  submitForm() {
    this.isSubmited = true;
    if (this.P2P1Form.valid && (this.p2p1.path || this.imagePreview)) {
      this.isSubmitting = true;
      this.P2P1Form.disable();
      const p2p1 = this.P2P1Form.value;
      let formData = new FormData();
      formData.append('name', p2p1.name);
      formData.append('relation', p2p1.relation);
      formData.append('description', p2p1.description);
      if(this.file){
        formData.append('file', this.file);
      }
      if(this.path){
        formData.append('path', this.path);
      }
      console.log('.////////////////////', p2p1)
      this.p2Service
        .saveFormData(formData, p2p1, this.p2Id, this.appID)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.P2P1Form.enable();
            this.activeModal.close('P2P1_Updated');
          },
          error => {
            this.isSubmitting = false;
            this.P2P1Form.enable();
          }
        );
    }
  }

  ngOnInit() {
    this.P2P1Form = this.formBuilder.group({
      '_id': [this.p2p1._id],
      'name': [this.p2p1.name, Validators.required],
      'relation': [this.p2p1.relation, Validators.required],
      'path': [this.p2p1.path, Validators],
      'description': [this.p2p1.description, Validators.required]
    });

  }
}
