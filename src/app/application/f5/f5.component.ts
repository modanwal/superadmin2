import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subject} from "rxjs/Subject";
import {FormBuilder, FormGroup} from "@angular/forms";
import {F5Service} from "../../shared/services/f5.service";
@Component({
  selector: 'app-root',
  templateUrl: './f5.component.html'
})
export class F5Component implements OnInit, OnDestroy{
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  appID: any;
  f5: any;
  ios: boolean;
  android: boolean;
  f5Form: FormGroup;
  constructor(  private route: ActivatedRoute,
                private formBuilder: FormBuilder, private router: Router, private f5Service: F5Service) {

  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {     this.appID = data.appID;
              this.f5 = data.F5Data;
              this.ios = data.F5Data.ios;
              this.android = data.F5Data.android;
        }
      );
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  checkboxClick(event){
      console.log(event);
    event.target.disabled = true;
    let submitData = {};
    submitData['android']= this.f5.android;
    submitData['ios'] = this.f5.ios;
    this.f5Service.save(submitData, this.appID)
      .subscribe(
        (data)=> event.target.disabled = false,
        (error)=> event.target.disabled = false
      );
  }
}
