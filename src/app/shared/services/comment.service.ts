
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class CommentService {
  constructor (private apiService: ApiService) {}

  getAllComment( postID, entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/comment/post/${postID}/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getCommentsForPost( postID, appID, entityListConfig:  EntityListConfig,  isSocket?){
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    return this.apiService.get(`/comment/app/${appID}/post/${postID}/${pageNumber}/${pageSize}/`, query, isSocket)

  }

  destroyComment(deleteID, postID): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/comment/${deleteID}/post/${postID}` );
  }
  getComment( commentID, postID,  entityListConfig:  EntityListConfig, isSocket? ): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/comment/${commentID}/post/${postID}`, query, isSocket);
  }

  saveComment(comment, postID, appID): Observable<any> {
    // If we're updating an existing comment
    if (comment._id) {
      return this.apiService.put(`/comment/app/${appID}/post/${postID}/`, comment);

      // Otherwise, create a new comment
    } else {
      delete comment._id;
      return this.apiService.post(`/comment/app/${appID}/post/${postID}/`, comment);
    }
  }
}
