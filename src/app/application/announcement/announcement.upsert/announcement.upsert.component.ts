import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AnnouncementService} from '../../../shared';


@Component({
  selector: 'announcementupsert-component',
  templateUrl: './announcement.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AnnouncementUpsertComponent implements  OnInit{
  isSubmitting: boolean;
  multiSelectConfig: any;
  announcementForm: FormGroup;
  announcement: any;
  @Input() appID;
  @Input() roles;
  isSubmited: Boolean;
  dropdownSettings = {};

  constructor(private formBuilder: FormBuilder,
              public activeModal: NgbActiveModal,
              private announcementService: AnnouncementService) {
    this.multiSelectConfig = {
      labelField: 'name',
      valueField:'_id',
      maxItems: 50,
      highlight: false,
      create: false,
      plugins: ['dropdown_direction', 'remove_button'],
      dropdownDirection: 'down'
    };
    this.isSubmited = false;
    this.dropdownSettings = {
      text:  'Select Role',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "myclass custom-class"
    };
  }

  submitForm() {
    this.isSubmited = true;
    if(this.announcementForm.valid){
      this.isSubmitting = true;
      this.announcementService
        .save(this.announcementForm.value, this.appID)
        .subscribe(
          data => { this.isSubmitting = false; this.activeModal.close('annoucement:updated')},
          error => {this.isSubmitting = false;}
        );
    }
    
  //  let item1 = [];
  //   for(let item of this.selectedItems)
  //   { item1.push(item.id); }

  //   var announcement = {};
  //   announcement = this.checkBoxes;
  //   announcement['text'] = this.text;
  //   announcement['roleID'] = item1;
  //     this.isSubmited = true;
      
    }
  // getSelectedItem(): any[]{
  //  // console.log('role printing', this.selectedItems);
  //   return  this.roles;
  // }

  // onItemSelect(item:any){
  // //  console.log(item);
  //   // console.log(this.selectedItems);
  // }
  // OnItemDeSelect(item:any){
  //  // console.log(item);
  // // console.log(this.selectedItems);
  // }
  // onSelectAll(items: any){
  //   console.log(items);
  // }
  // onDeSelectAll(items: any){
  //   console.log(items);
  // }

  ngOnInit() {
    // use FormBuilder to create a form group
    this.announcementForm = this.formBuilder.group ( {
      'roleID': [ '' ],
      'severity': [ 'MEDIUM' , Validators.required ],
      'text':['']
    } );

    // if(!this.role._id){
    //   this.roleForm.addControl(
    //   'appID', new FormControl(this.role.appID)
    //   )
    // }
    // if(this.roles[0]){
    //   this.selectedItems = [this.roles[0]];
    // }
  }
}
