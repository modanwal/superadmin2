import { NgModule,ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { F5Component } from './f5.component';
import { SharedModule, F5Service } from './../../shared';
import {F5Resolver} from "./f5.resolver";

export const f5Routing = [
  {
        path: 'f5',
        component: F5Component,
    resolve: {
      data: F5Resolver
    },

      }
    ];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    F5Component
  ],
providers: [  F5Resolver, F5Service
  ]
})
export class F5Module {}
