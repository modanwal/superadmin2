import { Injectable, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService } from '../shared';

@Injectable()
export class UserDetailsResolver implements Resolve<any> {

  constructor(private userService: UserService ) {}

resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const id = route.params['id'];
        return this.userService.get(id);
    }
}
