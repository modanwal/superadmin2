import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, EntityListConfig, F2Service} from './../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class F2Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (
               private router: Router,
               private f2Service: F2Service
               ) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const appID = state['url'].split("/")[2];
    const obs1 =  this.f2Service.getAllf2(appID, entityListConfig, false);
    const obs2 =  this.f2Service.getAllRole(appID, entityListConfig, true);
    const obs3 =  Observable.of(appID);
    return obs1.combineLatest(obs2, obs3,
      (val1, val2, val3) => {
        return {val1: val1, val2: val2, val3: val3};
      });
  }
}

