import {
  Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild, ElementRef,
  SimpleChange,

} from '@angular/core';
// import {saveAs} from 'file-saver';

@Component({
  selector: 'app-details-draggable',
  templateUrl: './app-details-draggable.component.html'
})
export class AppDetailsDraggableComponent implements  OnChanges {
  @ViewChild('appName') appName: ElementRef;
  constructor(){
    }
  @Input() draggableAppData: any;
  @Input() app: any;
  @Input() appStatus: any;
  @Output('close') closeEmitter = new EventEmitter<Boolean>();
  @Output('setDefaultAndroidStatus') editAppAndroidStatusEmitter = new EventEmitter<any>();
  @Output('setDefaultIosStatus') editAppIosStatusEmitter = new EventEmitter<any>();
  
  
  closeAppDetailsDraggable() {
      this.closeEmitter.emit(true);
  }
  copyToClipboard = (name, appname) => {
    this.appName.nativeElement.select();
    try {
      const successful = document.execCommand('copy');
      const msg = successful ? 'successful' : 'unsuccessful';
    } catch (err) {
      console.log(err);
    }
  }

  setDefaultAndroidStatus(status: any, appStatus: any) {
    const data =  {};
    data['android'] = status.target.value;
    data['ios'] = appStatus;
    this.editAppAndroidStatusEmitter.emit(data);
  }

  setDefaultIosStatus(status: any, appStatus: any) {
    const data =  {};
    data['android'] = appStatus;
    data['ios'] = status.target.value;
    this.editAppIosStatusEmitter.emit(data);
  }

  ngOnChanges(changes: SimpleChanges) {
    const appStatus: SimpleChange = changes.appStatus;
    // this.appStatus = appStatus.currentValue;
    // console.log(this.appStatus);
  }

  // downloadAppIcon(data: Response) {
  //   const blob = new Blob([data], { type: 'application/image' });
  //   console.log(blob);
  //   saveAs(blob, 'appIcon.jpg');
  // }
}
