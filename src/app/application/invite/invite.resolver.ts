import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { AppService, EntityListConfig, F5Service } from './../../shared';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx'
import {UserService} from '../../shared/services/user.service';
import {DTOService} from '../../shared/services/dto.service';

@Injectable()
export class InviteResolver implements Resolve<any>{
  routeData: Subscription;
  constructor (private userService: UserService, private dtoService: DTOService) {
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const appID = state['url'].split("/")[2];
    const appComponents = route.parent.data['data'][0].appComponents;
    const selectedComponent = appComponents.find((component)=> {
      if(component.componentID.route == '/invite')
      this.dtoService.setValue('componentID',component.componentID._id);
    });
    const obs1 = this.userService.getCurrentUserObservable();
    const obs3 = Observable.create(function (observer) {
      obs1.subscribe(
        (data) => {
          if (data['email']) {
            observer.next(data);
            observer.complete();
          }});
    });
    return Observable.forkJoin(Observable.of(appID), obs3);
  }
}
