import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import {PostService, UserService, AppService, EntityListConfig, CommentService, LikeService } from "../shared";

@Injectable()
export class WallPostResolver implements Resolve<any> {
  routeData: Subscription;
  constructor (private appService: AppService,
               private router: Router,
               private userService: UserService,
               private postService: PostService,
               private likeService: LikeService,
               private commentService: CommentService
               ) {
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const obs1 = this.userService.getCurrentUserObservable();
    const obs3 = Observable.create(function (observer) {
      obs1.subscribe(
        (data) => {
          if (data['email']) {
            observer.next(data);
            observer.complete();
          }});
    });

    const obs5 = this.appService
      .getAppName(entityListConfig);

    const obs4 = this.postService
      .getAll(entityListConfig);


    return Observable.forkJoin(obs5, obs3, obs4);
  }
}
