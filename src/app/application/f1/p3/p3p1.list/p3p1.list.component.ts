import { Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import { P3Service, EntityListConfig, DTOService, SocketService, InfiniteScrollService, ModalService} from './../../../../shared';

import { P3P1UpsertComponent } from './../p3p1.upsert/p3p1.upsert.component';
import { P3P1DeleteComponent } from "./../p3p1.delete/p3p1.delete.component";
import { Subject } from "rxjs/Subject";
import { DomSanitizer } from "@angular/platform-browser";
import {Subscription} from 'rxjs/Subscription';
import {P3P1Model} from '../../../../shared/models/p3p1.model';


@Component({
  selector: 'app-page',
  templateUrl: './p3p1.list.component.html',
  providers: [EntityListConfig]
})
export class P3P1ListComponent implements OnInit, OnDestroy {
  fetchingData: boolean;
  collectionDirty: boolean;
  p3p1FetchSub: Subscription;
  p3p1Obj: any;
  p3Id: string;
  p3p1: P3P1Model[];
  appID: any;
  p3p1PageName: string;
  categoryID: any;
  componentID: any;
  lastFetchedItems: any;
  idx: 1;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor( private route: ActivatedRoute,
               private dtoService: DTOService,
               private _sanitizer: DomSanitizer,
               private p3Service: P3Service,
               private socketService: SocketService,
               public entityListConfig: EntityListConfig,
               private router: Router,
               private modalService: ModalService,
               public infiniteScrollService: InfiniteScrollService) {
    this.collectionDirty = false;
    this.p3p1Obj = {};
    this.lastFetchedItems = 0;
    this.categoryID = this.dtoService.getValue('categoryID');
    this.componentID = this.dtoService.getValue('componentID');
    if(!this.componentID  && !this.categoryID){
      this.router.navigateByUrl(`/all-apps`);
    }
    this.p3Id = this.route.snapshot.params.id;
  //  console.log("p3ID", this.p3Id);
    this.p3p1PageName = 'Photos';
    this.p3p1PageName = this.dtoService.getValue('p3p1PageName')
                            ? this.dtoService.getValue('p3p1PageName') : '';

    /*** websockets */
    this.socketService.subscribeToEvent(`p3p1:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.p3p1,this.p3p1Obj, this.entityListConfig);
        if(this.fetchingData){
          this.p3p1FetchSub.unsubscribe();
          this.getAllP3P1(false);
        }
      })
    this.socketService.subscribeToEvent(`p3p1:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.p3p1,this.p3p1Obj,this.entityListConfig);
        if(this.fetchingData){
          this.p3p1FetchSub.unsubscribe();
          this.getAllP3P1(false);
        }
      })

  }
  onScroll(){
    this.getAllP3P1(false);
  }
  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {
          this.appID = data.appID;
          this.p3p1 = data.p3p1Data;
          this.infiniteScrollService.computeEntity(this.p3p1,this.p3p1Obj, this.entityListConfig);
        }
      );

    //*** Route Moving
  //  this.getAllP3P1();
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .filter(event => event['url'].split('/')[5]== 'p3p1')
      .takeUntil(this.ngUnsubscribe)
      .subscribe((routeData) => {
        this.p3Id = routeData['url'].split('/')[6];
        this.getAllP3P1(false);
      });
    //*** Route Moving
  }
  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  deletep3p1(p3p1data) {
    const modalRef = this.modalService.open(P3P1DeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p3p1 = p3p1data;
    modalRef.componentInstance.p3Id = this.p3Id;
    modalRef.componentInstance.appID = this.appID;

  }
  addP3P1(){
    const modalRef =  this.modalService.open(P3P1UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p3p1 = new P3P1Model;
    console.log('p3id goes here:', this.p3Id);
    modalRef.componentInstance.p3Id = this.p3Id;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.componentID = this.componentID;
    modalRef.componentInstance.categoryID = this.categoryID;

  }
  editP3P1(p3p1: any){
    const modalRef =  this.modalService.open(P3P1UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p3p1 = p3p1;
    modalRef.componentInstance.p3Id = this.p3Id;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.componentID = this.componentID;
    modalRef.componentInstance.categoryID = this.categoryID;
  }
  getAllP3P1(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
   this.p3p1FetchSub =  this.p3Service
      .getAllp3p1(this.p3Id, this.entityListConfig, this.appID)
      .subscribe(
        data => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllP3P1(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.p3p1, this.p3p1Obj, this.entityListConfig, refreshData);
          }
        }
      ),(error)=>{this.fetchingData = false;};
  }
  // getAllP3P1Socket(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.p3Service
  //     .getAllp3p1(this.p3Id, entityListConfig, this.appID, true)
  //     .subscribe(data =>{
  //       this.p3p1 = data;
  //       this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //       this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //       if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //     });
  // }
  getBackground(image){
      return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }
}
