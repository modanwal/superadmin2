/**
 * Created by rahul on 7/26/2017.
 */


import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, PrivilegeService , EntityListConfig } from '../../shared';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export class PrivilegeResolver implements Resolve<any> {

  routeData: Subscription;

  constructor (     private privilegeService: PrivilegeService,
                    private router: Router,
              ) {
  }

  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
   const entityListConfig = new EntityListConfig();
    const id = route.queryParams[ 'id' ];
    return this.privilegeService.getAll(entityListConfig );
  }
}

