export const SELECTIZE_DEFAULT_DROPDOWN_CONFIG: any = {
	highlight: false,
	create: false,
	persist: true,
	plugins: ['dropdown_direction', 'remove_button'],
	dropdownDirection: 'down'
};

export const SelectizeSingleSelectConfig: any = Object.assign({}, SELECTIZE_DEFAULT_DROPDOWN_CONFIG, {
	labelField: 'label',
	valueField: 'value',
	searchField: ['label']
});
