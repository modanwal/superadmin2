import { NgModule,ModuleWithProviders } from "@angular/core";
import { RouterModule } from '@angular/router';
import { F4Component } from './f4.component';
import { SharedModule } from './../../shared';
import { F4Service } from '../../shared/services';
import { F4Resolver } from './f4.resolver';

export const f4Routing = [
  {
        path: 'f4',
        component: F4Component,
        resolve: {
          data: F4Resolver
        }

  }
  ];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
  F4Component
  ],
  providers: [
    F4Resolver,
    F4Service
    
  ]
})
export class F4Module {}
