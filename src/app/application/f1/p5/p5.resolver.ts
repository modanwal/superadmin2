import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {AppService, EntityListConfig, P5Service } from './../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P5Resolver implements Resolve<any> , OnInit{
  pageName: any;

  routeData: Subscription;

  constructor (
               private p5Service: P5Service,
  ) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const appComponents = route.parent.data.data.appComponents;
    const entityListConfig = new EntityListConfig();
    const selectedComponent = appComponents.find(component=> component.componentID.route == '/p5');


    const appID = state['url'].split("/")[2];
    return this.p5Service.getAllp5(entityListConfig, appID, false).map(
      (p5Data) =>{
        let data = {};
        data['appID'] = appID;
        data['selectedComponent'] = selectedComponent;
        console.log('selectedComponent', selectedComponent);
        data['p5Data'] = p5Data;
        console.log('data printing', p5Data);
        return data;
      }
    );
  }
}

