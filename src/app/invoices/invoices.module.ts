import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule, AuthGuard } from '../shared';
import { InvoicesComponent } from "../invoices/invoices.component";
import {InvoicesResolver} from "./invoices.resolver";
import {InvoicesService} from "../shared/services/invoices.service";



export const invoicesRouting = [
  {
    path: 'invoices',
    component: InvoicesComponent,
    canActivate: [AuthGuard],
    resolve: {
      data: InvoicesResolver
    }
   }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
      InvoicesComponent
  ],
  providers: [AuthGuard, InvoicesResolver, InvoicesService
  ]

})
export class InvoicesModule {}
