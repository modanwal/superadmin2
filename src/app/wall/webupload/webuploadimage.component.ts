import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {CroppedimageComponent} from '../croppedupload/croppedimage.component';
import {FlickrService, GiphyService} from '../../shared';


@Component({
  selector: 'webimage-upload',
  templateUrl: './webuploadimage.component.html'
})
export class WebuploadComponent implements  OnInit {
  fetchingData: boolean;
  activeClass: any;
  photos: any;
  @ViewChild('input')
  input: ElementRef;
  formData: FormData;
  file: any;
  constructor(
              public activeModal: NgbActiveModal,
              private _sanitizer: DomSanitizer,
              private _flickrService: FlickrService,
              private giphyService: GiphyService,
              private modalService: NgbModal) {
  }
  ngOnInit() {

  }
  searchfromFlickr(){
    this.activeClass = 'flickr';
    const query = this.input.nativeElement.value;
    this.fetchingData = true;
    this._flickrService
      .getResult(query)
      .subscribe(
        data => {this.photos = data,this.fetchingData = false},
        error => this.fetchingData = false
      );
  }
  searchfromGiphy(){
    this.activeClass = 'giphy';
    const query = this.input.nativeElement.value;
    this.fetchingData = true;
    this.giphyService
      .getImages(query)
      .subscribe(
        data=>{this.photos = data;this.fetchingData = false},
        error => this.fetchingData = false
      );
  }
  checkstatus(url: string){
    if (typeof url === 'string') return true;
    return false;
  }
  selectPhoto(photo: any){
    const modalRef = this.modalService.open(
      CroppedimageComponent
    )
    let photoforUpload, cropDimension;
    if ((typeof photo.url === 'string') && (  photo.type !== 'gif')) {
      photoforUpload  = photo.url;
    } else if (typeof photo.url === 'string') {
      photoforUpload = photo.images.original.url;
    }
    cropDimension = {
      width: 200,
      height: 200
    }
    modalRef.componentInstance.path = photoforUpload;
    modalRef.componentInstance.cropDimension = cropDimension;
    this.activeModal.close();

  }
  getImagesfromApi(photo) {
    if ((typeof photo.url === 'string') && (  photo.type !== 'gif')) {
      return this._sanitizer.bypassSecurityTrustStyle(`url(${photo.url})`);
    } else if (typeof photo.url === 'string') {
      return this._sanitizer.bypassSecurityTrustStyle(`url(${photo.images.original.url})`);
    }
  }
  getBackground(image){
      return;
  }
  getTitlefromApi(photo){
    if ((typeof photo.url === 'string') && (  photo.type !== 'gif')) {
      return photo.title;
    } else if (typeof photo.url === 'string') {
      return photo.slug;
    }
  }
}
