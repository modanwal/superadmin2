import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { EntityListConfig, CategoryService, LabelService, TypeService  } from '../../shared';
import { Subscription } from 'rxjs/Subscription';
import {CcldService, CcltService} from '../../shared';



@Injectable()
export class SuggestionLabelTypesResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private categoryService: CategoryService,
               private labelService: LabelService,
               private typeService: TypeService,
               private ccldService: CcldService,
               private ccltService: CcltService) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {

    const entityListConfig = new EntityListConfig();
    entityListConfig.params.pageSize = -1;
    const obs1 =  this.categoryService.getAllCategory(entityListConfig);
    const obs2 =  this.labelService.getAll(entityListConfig);
    const obs3 = this.typeService.getAll(entityListConfig);
    const obs4 = this.ccltService.getAll(entityListConfig);
    // const obs4 = this.ccldService.getAllLabelTypes(ID, entityListConfig);
    return Observable.forkJoin(obs1, obs2, obs3, obs4);

  }
}
