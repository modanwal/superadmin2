import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, CategoryService, EntityListConfig } from '../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class CategoryResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private categoryService: CategoryService,
               private router: Router) {
  }
 ngOnInit(){
 }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const id = route.queryParams[ 'id' ];
      return this.categoryService.getAll( id, entityListConfig );
  }
}

