import { Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import {UserService, User} from '../shared';
import {EntityListConfig} from "../shared/models/entity-list-config.model";

@Component({
  selector: 'page-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers:[EntityListConfig]
})

export class HomeComponent implements OnInit {
  authenticated: Boolean;
  currentUser: User;
  constructor(private userService: UserService, private router: Router){
        this.userService.isAuthenticated.subscribe(authenticated => {if (!authenticated) this.router.navigateByUrl('/login')} )
  }
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
  }
}
