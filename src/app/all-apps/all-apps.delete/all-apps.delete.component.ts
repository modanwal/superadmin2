import { Router } from '@angular/router';
import { Component, ViewEncapsulation, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppModel, AppService } from '../../shared';


@Component({
  selector: 'allapps-delete-modal',
  templateUrl: './all-apps.delete.component.html'
})

export class AllAppsDeleteComponent {
  @Input() app: AppModel;
  isSubmited: boolean;

  constructor(public activeModal: NgbActiveModal, private appService: AppService) {
    this.isSubmited = false;
  }

  deleteApp() {
    this.isSubmited = true;
    // this.appService.destroy(app._id).subscribe(
    //   data => this.activeModal.close(),
    //   error => this.isSubmited = false
    // )
  }
}


