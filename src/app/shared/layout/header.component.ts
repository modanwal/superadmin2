import { Component, OnInit, HostListener, ViewChild, ElementRef, Renderer, Renderer2 } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UserService, NotificationService, SocketService } from './../services';
import { User, EntityListConfig } from './../models';
import { SharedModule } from './../shared.module';
import { TimeAgoPipe } from 'time-ago-pipe';
import { Subject } from "rxjs/Subject";

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit{
  url: any;
  lastFetchedItems: number;
  notifications: any[];
  authenticated: Boolean;
  currentUser: User;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private userService: UserService,
              private renderer: Renderer2,
              private router: Router,
              private notificationService: NotificationService,
              private entityListConfig: EntityListConfig,
              private socketService: SocketService,
              ) {
          this.router.events
          .filter(route => route instanceof NavigationEnd)
          .map(route => route['url'])
          .subscribe(
            (url) => {
              this.url = url.split('/');
            }
          );
  }
  // @ViewChild('input') input: ElementRef;
  // @ViewChild('resultscontainer') resultsContainer: ElementRef;
  // @HostListener('document:click', ['$event'])
  // clickout(event) {
  //   if(event.target==this.input.nativeElement){
  //     this.renderer.addClass(this.resultsContainer.nativeElement.firstElementChild,"open");
  //   }else{
  //     this.renderer.removeClass(this.resultsContainer.nativeElement.firstElementChild,"open");
  //   }
  // }

  ngOnInit() {
    this.notifications = [];
    this.lastFetchedItems = 0;
    this.userService.isAuthenticated.subscribe(authenticated => this.authenticated = authenticated);
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
        if(this.currentUser.email){
          this.getAllNotifications(false);
        }
      }
    );
    /*** websockets */
    this.socketService.subscribeToEvent(`notification:save`)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(data => {
          this.getAllNotificationsForSockets();})
    this.socketService.subscribeToEvent(`notification:remove`)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(data =>{
          this.getAllNotificationsForSockets()
        })
    /*** websockets */
  }
  getAllNotificationsForSockets(){
    const entityListConfig = new EntityListConfig();
    entityListConfig.query = this.entityListConfig.query;
    entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
    entityListConfig.params.pageNumber = 0;
    this.notificationService
      .getAll(entityListConfig, true)
      .subscribe(
        (data) => {
          this.notifications = data;
          this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
          this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
          if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
        }
      );
  }
  getAllNotifications(refreshData) {
   this.notificationService
      .getAll(this.entityListConfig,'000000000000000000000002')
      .subscribe(
        data =>{
          if(refreshData) this.notifications = [];
          // remove last page entries if data less than ideal size
          if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
            this.notifications.splice(
              (this.entityListConfig.params.pageNumber)
              *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          }
          this.lastFetchedItems = data.length;
          if(data.length==this.entityListConfig.params.pageSize){
            this.entityListConfig.params.pageNumber++;
          }
          // push new data
          this.notifications = [...this.notifications,...data];
        }
      );
  }
  onScroll(){
    this.getAllNotifications(false);
  }

  //
  logout() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.userService.purgeAuth();
    this.router.navigate(['/login']);
  }
  getActiveRoute(route){
    if(!this.url) return false;
    if(route=='category'){
      if((this.url.indexOf(route) != -1)
        ||(this.url.indexOf('templates') != -1)
        ||(this.url.indexOf('template') != -1)
        ||(this.url.indexOf('componentstack') != -1)){
          return true;
        }else{
          return false;
        }
    }else{
      if(this.url.indexOf(route) != -1){
        return true;
      }else{
        return false;
      }
    }
  }
  /*** search focus */
  onSearchFocus(){

  }
  onSearchFocusOut(){

  }
}

