import { Component, Renderer2, OnInit } from '@angular/core';
import { CcldService } from './../services/ccld.service';
import { EntityListConfig } from './../models/entity-list-config.model';
import { DomSanitizer } from "@angular/platform-browser";
import { UploaderService } from "./uploader.service";
import { Subject } from "rxjs/Subject";

@Component({
  selector: 'uploader',
  templateUrl: './uploader.component.html'
})
export class UploaderComponent implements OnInit {
    minimizeMode: boolean;
    private ngUnsubscribe: Subject<void> = new Subject<void>();
    items : any[];
    constructor(private renderer: Renderer2, private _sanitizer: DomSanitizer,private uploaderService: UploaderService) {
        this.items = [];
    }
  ngOnInit(){
      this.uploaderService.getUploadItems()
        .takeUntil(this.ngUnsubscribe)
        .subscribe(items => {
          this.items  = items;
      });
  }
  trackByFn(index,item){
      return index;
  }
  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  getItemImage(imageUrl:string){
        return this._sanitizer.bypassSecurityTrustStyle(`url(${imageUrl})`);
  }
  getOpenStatus(){
    return  this.items.length > 0;
  }
  getOffset(val){
        if (isNaN(val)) {
            val = 0;
        }
            let r = 90;
            var c = Math.PI*(r*2);    
            if (val < 0) { val = 0;}
            if (val > 100) { val = 100;}
            var pct = ((100-val)/100)*c;
            return pct;
    
  }
    maximize(){
        this.minimizeMode = false;
    }
    minimize(){
        this.minimizeMode = true;
    }
    cancelUploading(){
        alert('Are you sure you want to cancel all uploads?');
        this.uploaderService.cancelFileUploads();
    }
    getItemsUploading(){
        return this.items.filter(item => (item.completed == false)&&(item.error == false)).length;
    }
    getUploadedItems(){
        return this.items.filter(item => (item.completed == true)&&(item.error == false)).length;
    }
}