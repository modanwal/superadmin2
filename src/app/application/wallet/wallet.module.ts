import { NgModule,ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from './../../shared';
import { TransactionComponent } from '../wallet/transaction/transaction.component';
import { WalletComponent } from '../wallet/wallet.component';

export const walletRouting = [
  {
        path: 'wallet',
        component: WalletComponent,
        children: [
          {
            path: 'transaction',
            component: TransactionComponent
          }
        ]
      }
    ];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    WalletComponent,
    TransactionComponent
  ],
providers: [
  ]
})
export class WalletModule {}
