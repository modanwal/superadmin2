import { Injectable, } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { AppModel } from './../models/app.model';
import { EntityListConfig } from './../models/entity-list-config.model'
import { ApiService } from './api.service';


@Injectable()
  export class AppService {
  constructor (
    private apiService: ApiService
  ) {}
  getbyAppStatus(entityListConfig: EntityListConfig, isSocket?): Observable<AppModel[]>{
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/app/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getAllAppStatusCount(entityListConfig: EntityListConfig, isSocket?): Observable <any> {

    const query: URLSearchParams = new URLSearchParams();
    delete entityListConfig.query;
    return this.apiService.get(`/app/status/count`, query, isSocket);
  }
  getAppsByName(entityListConfig: EntityListConfig): Observable<AppModel[]> {
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
     return this.apiService.get(`/app/name`, query, false);
  }
  getSingleApp(appID, isSocket?): Observable<AppModel[]> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/app/${appID}`, query, isSocket);
  }
  getAll(entityListConfig: EntityListConfig,  isSocket? ): Observable<AppModel[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/app/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getAllAppsByStatus(entityListConfig: EntityListConfig,  isSocket? ): Observable<AppModel[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/app/status/${pageNumber}/${pageSize}`, query, isSocket);
  }

  getBySearch(entityListConfig: EntityListConfig, isSearch?, isSocket?, ): Observable<AppModel[]> {
     delete entityListConfig.query.sortBy;
     delete entityListConfig.query.order;
     entityListConfig['query']['name'] = isSearch;
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/app/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getAppName(entityListConfig: EntityListConfig,  isSocket?){
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/app/${pageNumber}/${pageNumber}`, query, isSocket);
  }
  get(_id, isSocket?): Observable<AppModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/app/${_id}`, query, isSocket);
  }
  getAppFeatures(_id, entityListConfig: EntityListConfig,  isSocket?): Observable<AppModel> {
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/app/${_id}/component/type`, query, isSocket);
  }
  getAppPages(_id, entityListConfig: EntityListConfig,  isSocket?): Observable<AppModel> {
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/app/${_id}/component/type`, query, isSocket);
  }
  saveFormData(appFormData, appID?){
    // If we're updating an existing app
    if (appID) {
      return this.apiService.putWithFormData('/app/user' + appID, appFormData)
        .map(data => data.category);
      // Otherwise, create a new app
    } else {
      return this.apiService.postWithFormData('/app/user', appFormData);
    }
  }
  changeAppStatusFromWorkBench(appID, app): Observable<any> {

  return this.apiService.put(`/app/${appID}`, app);
  }

  save(app, userID): Observable<AppModel> {
    // If we're updating an existing component
    if (app._id) {
      const _id = app._id;
      delete app['_id'];
      return this.apiService.put(`/app/${_id}`, app)
        .map(data => data.component);
      // Otherwise, create a new component
    } else {
      delete app._id;
      return this.apiService.post('/component/', app)
        .map(data => data.component);
    }
  }

  destroy(_id): Observable<any>{
    return this.apiService.delete(`/app/${_id}`);
  }

  changeAppStatus(appStatus, appID): Observable<AppModel> {
    // If we're updating an existing component
    return this.apiService.put(`/app/${appID}/all/status/`, appStatus);
    // Otherwise, create a new component
  }
  saveWithFormData(app, formData):Observable<any> {
    if(app._id){
      const _id = app._id;
      delete app['_id'];
      return this.apiService.putWithFormData(`/app/${_id}`, formData);
    }
  }

}
