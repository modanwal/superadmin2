import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

@Injectable()
export class FlickrService {
  result$: Observable<any>;
  key = 'c712368d2cb7e33d9ddf9eecdac8b86d';
  constructor(private _http: Http) { };

  getResult(query: string) {
    //console.log(query)
    let url = `https://api.flickr.com/services/rest/?sort=relevance&method=flickr.photos.search&api_key=${this.key}&text=${query}&per_page=12&format=json&nojsoncallback=1`;
    return this._http
      .get(url)
      .map(res => res.json())
      .map((val) => {
        if (val.stat === 'ok') {
          return val.photos.photo.map((photo: any) => {
            return {
              url: `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_m.jpg`,
              title: photo.title
            }
          })
        }
        else {
          return [];
        }
      });
  }
}




// import { Injectable } from '@angular/core';
// import { ApiService } from '../../shared/services/api.service';
// import { EntityListConfig } from '../../shared/models';
// import { Observable } from 'rxjs/Rx';
// import { URLSearchParams } from '@angular/http';
//
//
// @Injectable()
// export class FlickrService {
//
//   constructor (
//     private apiService: ApiService
//   ) {}
//   getAllF5(entityListConfig: EntityListConfig, appID, isSocket?): Observable<any[]> {
//     const query: URLSearchParams = new URLSearchParams();
//
//     Object.keys(entityListConfig.query)
//       .forEach((key) => {
//         query.set(key, entityListConfig.query[key]);
//       });
//     return this.apiService.get(`/f5/app/${appID}`, query, isSocket);
//   }
//
//
// }
