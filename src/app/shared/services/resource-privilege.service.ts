import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class ResourcePrivilegeService {

  constructor (
    private apiService: ApiService
  ) {}

  getPrivilegeData(resourceID, isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/resource-privilege/${resourceID}`, query, isSocket);
  }
  getResourcePrivlegesForTemplate(templateID, isSocket?): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/resource-privilege/template/${templateID}`, query , isSocket);
  }

  get(_id, isSocket?): Observable<any> {
     const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/privilege/${_id}`, query, isSocket);
  }
  getAll(pageNumber: Number, pageSize: Number, sortBy: String, order: String, isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/privilege/${pageNumber}/${pageSize}?sortBy=${sortBy}&order=${order}`, query, isSocket);
  }
  destroy(resourceID, privilegeID) {
    return this.apiService.delete(`/resource-privilege/${resourceID}/${privilegeID}`);
  }

  save(entity): Observable<any> {
    return this.apiService.post(`/resource-privilege/`, entity);
  }
  checkAllResourcePrivilege(resourceID): Observable<any> {
    return this.apiService.post(`/resource-privilege/${resourceID}`, );
  }
  unCheckAllResourceprivilege(resourceID):  Observable<any> {
    return this.apiService.delete(`/resource-privilege/${resourceID}`, );
  }
}
