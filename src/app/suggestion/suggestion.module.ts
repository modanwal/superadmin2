
import {SharedModule} from "../shared/shared.module";
import {NgModule} from "@angular/core";
import {SuggestionComponent} from "./suggestion.component";
import {SuggestionLabelsComponent} from "./labels/labels.component";
import {TypeService, CcltService, LabelService, DTOService } from "../shared";
import {SuggestionLabelsResolver} from "./labels/labels.resolver";
import {SuggestionLabelsUpsertComponent} from "./labels/labels.upsert/labels.upsert.component";
import {SuggestionLabelDeleteComponent} from "./labels/label.delete/label.delete.component";
import {FilesResolver} from "./files/files.resolver";
import { CroppedUpload } from "./files/croppedupload/croppedupload.component";
import { WebUpload } from "./files/webupload/webupload.component";
import {FileUploadService} from "../shared/file-upload/file-upload.service";
import {WebimagesComponent} from "./webimages/webimages.component";
import {FlickrService} from "../shared/services/flikr.service";
import {GiphyService} from "../shared/services/giphy.service";
import {DataTypeModule, dataTypeRouting} from "./data-types/data-type.module";
import {LabelNumberModule, labelNumberRouting} from "./label-number/label-number.module";
import {labelTypeRouting, LabelTypesModule} from "./label-types/label-types.module";
import {DataService} from '../shared/services/data.service';
import {StringModule, stringRouting} from './string/string.module';
import {fileRouting, FilesModule} from './files/files.module';
import {SuggestedModule, suggestedRouting} from './suggested/suggested.module';


export const suggestionRouting = [
  {
    path: 'suggestion',
    component: SuggestionComponent,
    children: [
      ...labelTypeRouting,
      ...dataTypeRouting,
      ...labelNumberRouting,
      ...stringRouting,
      ...fileRouting,
      ...suggestedRouting,
      {
        path: 'labels',
        component: SuggestionLabelsComponent,
        resolve: {
          data: SuggestionLabelsResolver
        }
      },
      {
        path: 'webimages',
        component: WebimagesComponent
      },

    ]
  }
];

@NgModule( {
  imports: [
    SharedModule, DataTypeModule, LabelNumberModule, LabelTypesModule, StringModule, FilesModule, SuggestedModule
  ],
  declarations: [
    SuggestionComponent, SuggestionLabelsComponent, SuggestionLabelsUpsertComponent, WebimagesComponent,
    SuggestionLabelDeleteComponent,
      WebUpload,
    CroppedUpload],
  providers: [TypeService,  SuggestionLabelsResolver, LabelService, FileUploadService, FlickrService, GiphyService
     , FilesResolver, DataService],
  entryComponents: [  SuggestionLabelsUpsertComponent, SuggestionLabelDeleteComponent, WebUpload, CroppedUpload
  ]
})

export class SuggestionModule {  }
