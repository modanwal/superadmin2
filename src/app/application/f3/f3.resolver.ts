import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { AppService, EntityListConfig,F3Service } from './../../shared';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class F3Resolver implements Resolve<any>{
  routeData: Subscription;
  constructor (private appService: AppService,
               private router: Router,
               private f3Service: F3Service) {
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const appComponents = route.parent.data.data[0].appComponents;
    const selectedComponent = appComponents.find(component=> component.componentID.route == '/f3');

      const appID = state['url'].split("/")[2];
      let entityListConfig = new EntityListConfig();
      entityListConfig.params.pageSize = -1;
      console.log(appID);
      return this.f3Service.getAllF3(entityListConfig,appID,false).map((allF3Data)=>{
        let data = {};
        data['appID'] = appID;
        data['allF3Data'] = allF3Data;
        data['pageName'] = selectedComponent.pageName;
        console.log(data);
        return data;
      });
  }
}

