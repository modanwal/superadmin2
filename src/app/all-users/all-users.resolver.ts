import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, EntityListConfig } from '../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class AllUsersResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (
               private router: Router,
               private userService: UserService) {
  }
 ngOnInit(){
 }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    let entityListConfig = new EntityListConfig();
            return this.userService
                .getAll(entityListConfig);
  }
}

