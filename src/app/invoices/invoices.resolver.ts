import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import { Observable } from 'rxjs/Rx';
import {  EntityListConfig, InvoicesService } from '../shared';


@Injectable()
export class InvoicesResolver implements Resolve<any> , OnInit {

  constructor (private invoiceService: InvoicesService
               ) {
  }
  ngOnInit() {
  }
  resolve (route: ActivatedRouteSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    return this.invoiceService.getAll( entityListConfig );
  }
}
