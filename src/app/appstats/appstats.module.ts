import { ModuleWithProviders, NgModule } from '@angular/core';
import { SharedModule } from '../shared';
import { AppStatesComponent } from './appstats.component';
import { RouterModule } from '@angular/router';
import { DonutComponent } from './donut/donut.component';
import { ChartsModule } from 'ng2-charts';
import {AppstatusResolver} from './appstatus.resolver';
export const appStatsRouting = [
  {
    path: 'app-stats',
    component: AppStatesComponent,
    resolve: {
      data: AppstatusResolver
    }
  }
];

@NgModule({
  imports: [
    SharedModule, ChartsModule
  ],
  declarations: [
    AppStatesComponent, DonutComponent
  ],
  providers: [AppstatusResolver]
})
export class AppStatsModule {

}
