import { Component, OnInit, Host, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sorter',
  templateUrl: './table-data-sorter.component.html'
})

export class TableDataSorterComponent {
  @Input() config: any;
  @Output('sortData') sortDataEmitter = new EventEmitter<any>();
  
  sortUp() {
      this.sortDataEmitter.emit({
        sortBy: this.config.sortBy,
        sortOrder: 1
      });
  }
  sortDown(){
      this.sortDataEmitter.emit({
        sortBy: this.config.sortBy,
        sortOrder: -1
      });
  }
}
