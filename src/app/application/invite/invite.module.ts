import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {InviteComponent} from './invite.component';
import {FacebookComponent} from './facebook/facebook.component';
import {InviteResolver} from './invite.resolver';
import {MailComponent} from './mail/mail.component';
import { Ng2CompleterModule } from 'ng2-completer';
import {MailResolver} from './mail/mail.resolver';

export const inviteRouting = [
  {
    path: 'invite',
    component: InviteComponent,
    resolve: { data: InviteResolver},
    children: [
      {
        path: 'facebook/:id',
        component: FacebookComponent
      },
      {
        path: 'mail/:id',
        component: MailComponent,
        resolve: {
          data: MailResolver
        }
      }
    ]
  }
];
@NgModule({
  imports: [SharedModule, Ng2CompleterModule],
  declarations: [InviteComponent, FacebookComponent, MailComponent],
  exports: [],
  providers: [InviteResolver, MailResolver]
})
export class InviteModule {

}
