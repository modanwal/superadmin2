import { Component, ViewEncapsulation, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { TemplateResourcePrivilegeService, ResourcePrivilegeService, EntityListConfig, RoleResourcePrivilegeService, RoleModel, RoleService } from '../../../shared';
import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { Router , ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'role-resource-privilege',
  templateUrl: './role-resource-privilege.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})
export class RoleResourcePrivilegeComponent implements OnInit {
  role: RoleModel;
  templateResourcePrivilegeData: any;
  templateResourcePrivileges: any;
  roleResourcePrivilegeData: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor (private formBuilder: FormBuilder,
               private route: ActivatedRoute,
               private router: Router,
               private templateResourcePrivilegeService: TemplateResourcePrivilegeService,
               private roleResourcePrivilegeService: RoleResourcePrivilegeService,
               public activeModal: NgbActiveModal,
               private roleService: RoleService
              ) {
  }
  ngOnInit (){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data => {
        this.role = data[0];
        console.log('resource printing', this.role);
        this.roleResourcePrivilegeData = data[1];
        // this.roleResourcePrivilegeData.forEach(function(e){
        //   if (typeof e === "object" ){
        //     if(e.privileges)
        //     {  e.privileges.unshift({_id:'1234',name:'selectAll'}, {_id:'12345',name:'unSelectAll'});}
        //   }
        // });
        console.log('for checking check listing line no 39', this.roleResourcePrivilegeData);
       // this.getTemplateResourcePrivilege();
      });
  }
  selectCheckboxClick(event, resourceID){
    if(event.target.checked){
      let data = {
        roleID: this.role._id,
        resourceID: resourceID
      };
      this.roleResourcePrivilegeService
        .selectAllCheckBoxs(data)
        .subscribe(
          (data)=>{
            event.target.checked = true;
            this.getTemplateResourcePrivilege();
          });
    }else if(!event.target.checked){
      this.roleResourcePrivilegeService
        .unSelectAllCheckBoxs(this.role._id, resourceID)
        .subscribe(
          (value)=>
          {  event.target.checked = false;
            this.getTemplateResourcePrivilege();
          });
    }
  }
  selectAllStatus(privileges){
    var count = 0;
    var status = privileges.length;
    for(var privilege of privileges){
      if(privilege.checked){
        count++;
      }
    }
    if(count === status){
      return true;
    } else {
      return false;
    }
  }
  getRoleResourcePrivilege(){
    this.roleResourcePrivilegeService
          .getRoleResourcePrivilege(this.route.snapshot.paramMap.get('id'))
          .subscribe(
            (data) => {
              this.roleResourcePrivilegeData = data;
              console.log('roleResourcePrivilegeData', this.roleResourcePrivilegeData);
          //    this.getTemplateResourcePrivilege()
            }
          )
  }
  getTemplateResourcePrivilege(){
    this.templateResourcePrivilegeService
          .getTemplateresourcePrivilegeRole(this.role._id)
          .subscribe(
            (data) =>  {
              this.roleResourcePrivilegeData = data;
              // this.roleResourcePrivilegeData.forEach(function(e){
              //   if (typeof e === "object" ){
              //     if(e.privileges)
              //     {  e.privileges.unshift({_id:'1234',name:'selectAll'}, {_id:'12345',name:'unSelectAll'});}
              //   }
              // });
              console.log('for getting listing', this.templateResourcePrivilegeData);
            });
  }
  checkboxClick(event, resourceID, privilegeID){
    event.preventDefault();
    event.target.disabled = true;
          if(event.target.checked){
            let data = {
              roleID: this.role._id,
              resourceID: resourceID,
              privilegeID: privilegeID
            };
            console.log(data);
            this.roleResourcePrivilegeService
              .save(data)
              .subscribe(
                (value)=>{
                  event.target.checked = true;
                  event.target.disabled = false;
                  this.getTemplateResourcePrivilege();
                },
                (error) =>{event.target.disabled = false;}
              )
          }else{
            this.roleResourcePrivilegeService
              .destroy(this.role._id, resourceID, privilegeID)
              .subscribe(
                (data)=>{
                  event.target.checked = false;
                  event.target.disabled = false;
                  this.getTemplateResourcePrivilege();
                },
                (error) =>{event.target.disabled = false;}
              );
          }
  }
  getCheckedStatus(resourceID, privilegeID){
    if(!this.roleResourcePrivilegeData[resourceID]) return false;
    if(this.roleResourcePrivilegeData[resourceID].indexOf(privilegeID)!=-1) return true;
    return false;
  }
  cancel(){
    this.router.navigateByUrl('/roles/role');
  }

}
