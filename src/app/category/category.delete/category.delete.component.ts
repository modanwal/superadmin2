import { Router } from '@angular/router';
import { Component, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryModel, CategoryService} from '../../shared';

@Component({
  selector: 'cadc-modal-content',
  templateUrl: './category.delete.component.html'
})
export class CategoryDeleteComponent {
  @Input () category: CategoryModel;
  isSubmited: Boolean;

  constructor (public activeModal: NgbActiveModal, private categoryService: CategoryService) {

  }

  delete (category: CategoryModel) {
    this.isSubmited = true;
    this.categoryService.destroy(category._id )
      .subscribe (
        data => this.activeModal.close ('category_deleted')
      );
  }
}

