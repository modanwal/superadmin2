import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P4Service} from '../../../../shared/services/p4.service';
import {CompleterData, CompleterService} from "ng2-completer";

@Component({
  selector: 'app-root',
  templateUrl: './p4.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class P4UpsertComponent implements  OnInit {
  invalidName: boolean;
  time: any;
  date: any;
  public address: Object;
  @Input() p4;
  P4Form: FormGroup;
  form: FormGroup;
  isSubmited: boolean;
  isSubmitting: boolean;
  @Input() appID;
  placeParser;
  imagePreview: any;
  file: any;
  path: any;
  @Input() parentReference: any;
  selfReference: any;
  @Input() componentID;
  @Input() categoryID;
  options: any;
  formData: FormData;
  constructor( private formBuilder: FormBuilder,
               public activeModal: NgbActiveModal,
               private completerService: CompleterService,
               private p4Service: P4Service) {
  //  this.dataService = completerService.remote(`${environment.api_url}/user?email=`, 'email', 'email');

    this.isSubmitting = false;
                this.invalidName = true;
  }
  getAddress(place:Object) {
    this.address = place['formatted_address'];
    console.log(place);
   this.getPlaceParser(place);
  }
  getPlaceParser(place: any){
  let  result = {};
  for(let i = 0; i < place.address_components.length; i++){
    let ac = place.address_components[i];
    result[ac.types[0]] = ac.long_name;
  }
  this.placeParser = result;
    console.log(this.placeParser);
    this.P4Form.controls['city'].setValue(this.placeParser.administrative_area_level_2);
    this.P4Form.controls['address1'].setValue(this.decodeHTMLEntities(place.adr_address.substring(0, place.adr_address.indexOf("<")-2))+ (this.placeParser.sublocality_level_2 === undefined ? '' : (place.adr_address.substring(0, place.adr_address.indexOf("<")-2))? ',' + this.placeParser.sublocality_level_2: this.placeParser.sublocality_level_2));
    this.P4Form.controls['address2'].setValue(this.placeParser.sublocality_level_1);
    this.P4Form.controls['pincode'].setValue(this.placeParser.postal_code);
    this.P4Form.controls['country'].setValue(this.placeParser.country);
    this.P4Form.controls['state'].setValue(this.placeParser.administrative_area_level_1);
    this.P4Form.controls['phoneNo'].setValue(place.formatted_phone_number);
    this.P4Form.controls['venue'].setValue(place.name);
    console.log('website', place.website, this.placeParser.administrative_area_level_1);
    this.P4Form.controls['website'].setValue(place.website);

  }
 decodeHTMLEntities(text: string): string {
    let entities = [
      ['amp', '&'],
      ['apos', '\''],
      ['#x27', '\''],
      ['#x2F', '/'],
      ['#39', '\''],
      ['#47', '/'],
      ['lt', '<'],
      ['gt', '>'],
      ['nbsp', ' '],
      ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i)
      text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);

    return text;
  }


  getAddressComponent(place, componentName, property) {
    var comps = place.address_components.filter(function(component) {
      return component.types.indexOf(componentName) !== -1;
    });

    if(comps && comps.length && comps[0] && comps[0][property]) {
      return comps[0][property];
    } else {
      return null;
    }
  }
  ngOnInit() {
    const today = new Date();
    let date = {
      year: today.getFullYear(),
      month: today.getMonth()+1,
      day: today.getDate()
    };

    let time = {
      hour: today.getHours(),
      minute: today.getMinutes(),
      second: today.getSeconds()
    }

    this.P4Form = this.formBuilder.group({
      '_id': [this.p4._id],
      'name': [this.p4.name, Validators.required],
      'venue': [this.p4.venue, Validators.required],
      'city': [this.p4.city, Validators.required],
      'date': [date],
      'time': [time],
      'country': [this.p4.country, Validators.required],
      'address1': [this.p4.address1, Validators.required],
      'address2': [this.p4.address2, Validators.required],
      'state': [this.p4.state, Validators.required],
      'website': [this.p4.website, Validators.required],
      'pincode': [this.p4.pincode, Validators.required],
      'phoneNo': [this.p4.phoneNo, Validators.required]

    });
    this.form = new FormGroup({
      address: new FormControl({value: 'Nancy', disabled: true}, Validators.required),

    });

  }
  // eventHandler(event){
  //   if(event.keyCode === 13){
  //   //  this.submitForm();
  //   }
  // }
  submitForm() {
    this.isSubmited = true;
    if (this.P4Form.valid ) {
       this.isSubmitting = true;
      this.P4Form.disable();
      let p4 = this.P4Form.value;
      const dateTime = new Date();
      dateTime.setSeconds(p4.time.second);
      dateTime.setMinutes(p4.time.minute);
      dateTime.setHours(p4.time.hour);
      dateTime.setDate(p4.date.day);
      dateTime.setMonth(p4.date.month);
      dateTime.setFullYear(p4.date.year);
      delete p4.date;
      delete p4.time;
      p4.dateTime = dateTime.toString();
      this.formData = new FormData();
      this.formData.append('name', p4.name);
      this.formData.append('dateTime', dateTime.toString());
      this.formData.append('venue', p4.venue);
      this.formData.append('city', p4.city);
      this.formData.append('country',p4.country);
      this.formData.append('address1', p4.address1);
      this.formData.append('address2', p4.address2);
      this.formData.append('state', p4.state);
      this.formData.append('website', p4.website);
      this.formData.append('pincode', p4.pincode);
      this.formData.append('phoneNo', p4.phoneNo);
      if(this.file){
        this.formData.append('file',this.file);
      }
      if(this.path){
        this.formData.append('path', this.path);
      }
      this.p4Service
        .save( this.formData, p4, this.appID)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.P4Form.enable();
            this.activeModal.close('P4_Updated');
          },
          error => {
            this.isSubmitting = false;
            this.P4Form.enable();
          });
    }
  }
  onContentSelected(event){
    if(event.labelName === 'file'){
      this.imagePreview = event.path;
      this.path = event.path;
    }
  }
  selectFile(event){
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    const file = event.target.files[0];

    fileReader.addEventListener('load', ()=> {
      this.imagePreview = fileReader.result;
      this.p4.path = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }


}
