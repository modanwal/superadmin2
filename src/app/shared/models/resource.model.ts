export class ResourceModel {
  _id: string;
  name: string;
  isComponent: boolean;
}
