export class ComponentModel {
  _id: string;
  name: string;
  type: string;
  route: string;
  isLeaf: boolean;
  description: string;
  resourceID: any;
}
