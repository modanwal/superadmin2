export class LabeltypeModel {
  _id: string;
  componentID: {
    _id: string;
   resourceID: {
     _id: string;
     name: string;
   }
  } = {
    _id: '',
    resourceID: {
      _id: '',
      name: ''
    }
  };
  categoryID: {
    _id: string;
    name: string;
  } = {
    _id: '',
    name: ''
  };
  labelID: {
    _id: string;
    name: string;
  } = {
    _id: '',
    name: ''
  };
  typeID: {
    _id: string;
    name: string;
  } = {
    _id: '',
    name: ''
  };
  minlength: number;
  maxlength: number;
}
