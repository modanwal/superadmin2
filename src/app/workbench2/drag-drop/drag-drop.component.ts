import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component( {
  selector: 'drag-drop',
  templateUrl: './drag-drop.component.html'
})
export class DragDropComponent {
    @Input() title;
    @Input() type;
    @Input() items: any[];
  model: any;
    searchMode: boolean;
    @Output('dropItem') dropItemEmitter = new EventEmitter<any>();

    droppedItems: any[];
    constructor () {
      this.searchMode = false;
    }
    ItemDropped(event){
      this.dropItemEmitter.emit({event:event,type: this.type});
    }

}
