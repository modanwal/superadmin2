export class InvoiceItemModel {
  _id: string;
  invoiceID: string;
  price: string;
  name: string;
  discount: string;
}
