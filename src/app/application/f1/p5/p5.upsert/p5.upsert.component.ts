
import {Component, Input, OnInit, ViewEncapsulation, Renderer, Renderer2} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { SuggestionService, CcldService, EntityListConfig, P5Service } from '../../../../shared';
@Component({
  selector: 'p5upsert',
  templateUrl: './p5.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class P5UpsertComponent implements  OnInit {
  isSubmitting: boolean;
  imagepreview: any;
  @Input() appID;
  pageName: any;
  file: any;
  formData: FormData;
  P5Form: FormGroup;
  selfReference: any;
  @Input() p5;
  @Input() componentID;
  @Input() categoryID;
  @Input() reference;
  path: any;
  isSubmited: Boolean;
  open; any;
  parentReference: any;

  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private p5Service: P5Service,
                private suggestionService: SuggestionService,
                private ccldService: CcldService,
                private renderer: Renderer2,
                ) {
    this.isSubmited = false;
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.P5Form.valid && (this.p5.path||this.imagepreview)) {
      this.isSubmitting = true;
      this.P5Form.disable();
      const p5 = this.P5Form.value;
      this.formData = new FormData();
      this.formData.append('title', p5.title);
      this.formData.append('description', p5.description);
      // this.formData.append('path','https://c1.staticflickr.com/5/4172/33733234374_fc90501be7_n.jpg');
      if(this.file){
        this.formData.append('file', this.file);
      }
      if(this.path){
        this.formData.append('path', this.path);
      }
      this.p5Service
        .saveP5(this.formData, p5._id, this.appID)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.P5Form.enable();
            this.activeModal.close('P5_Updated');
          },
          error => {
            this.isSubmitting = false;
            this.P5Form.enable();
            this.isSubmited = false;
          }
        );
    }
  }
  openOverLayModal(labelName: string) {
    const entityListConfig = new EntityListConfig();
    console.log(this.categoryID, this.componentID, 'mention not');
    entityListConfig.query['label'] = labelName;

   this.ccldService
     .getAllSuggestion(this.categoryID, this.componentID, entityListConfig)
     .subscribe(
       (data)=>{
         const modalRef = this.suggestionService.open();
         modalRef.componentInstance.componentID = this.componentID;
         modalRef.componentInstance.categoryID = this.categoryID;
         modalRef.componentInstance.ccld = data;
       });
  }
  selectFile(event){
    this.file = event.target.files[0];
    const fileReader = new FileReader();
    const file = event.target.files[0];

    fileReader.addEventListener('load', () => {
      this.imagepreview = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }
  onContentSelected(event){
    console.log(event.labelName);
    if(event.labelName === 'file'){
      this.imagepreview = event.path;
      this.path = event.path;
    } else if (event.labelName === 'description'){
      this.P5Form.controls['description'].setValue(event.path);
    } else if (event.labelName === 'title'){
      this.P5Form.controls['title'].setValue(event.path);
    }
  }
  ngOnInit() {

    this.P5Form = this.formBuilder.group({
      '_id': [this.p5._id],
      'title': [this.p5.title, Validators.required],
     'description': [this.p5.description, Validators.required],
     'file': [this.p5.file],
     'path': [this.p5.path]
    });

  }
  // onContentSelected(event){
  //   console.log('on content selected', event);
  // }
}
