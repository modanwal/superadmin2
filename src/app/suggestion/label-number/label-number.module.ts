import { NgModule } from '@angular/core';
import {AuthGuard, SharedModule} from '../../shared';
import {LabelNumberComponent} from "./label-number.component";
import {LabelNumberDeleteComponent} from "./label-number.delete/label-number.delete.component";
import {LabelNumberUpsertComponent} from "./label-number.upsert/label-number.upsert.component";
import {LabelNumberResolver} from "./label-number.resolver";
export const labelNumberRouting = [
  {
    path: 'label-number',
    component: LabelNumberComponent,
    canActivate: [AuthGuard],
    resolve: {
      data: LabelNumberResolver
    }
  }
];
@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    LabelNumberComponent, LabelNumberUpsertComponent, LabelNumberDeleteComponent
  ],
  providers: [
    AuthGuard, LabelNumberResolver
  ],
  entryComponents: [LabelNumberUpsertComponent, LabelNumberDeleteComponent ]
})
export class LabelNumberModule {}
