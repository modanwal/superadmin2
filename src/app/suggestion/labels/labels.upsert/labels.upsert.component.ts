import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {LabelService} from "../../../shared";

@Component({
  selector: 'app-root',
  templateUrl: './labels.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SuggestionLabelsUpsertComponent implements  OnInit {
  @Input() label;
  labelForm: FormGroup;
  isSubmited: Boolean;
  isSubmitting: boolean;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private labelService: LabelService
  ) {
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.labelForm.valid) {
      this.isSubmitting = true;
      const label = this.labelForm.value;
      this.labelForm.disable();
      this.labelService
        .save(label)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.activeModal.close('label:updated');
          },
          error => {
            this.isSubmitting = false;
            this.labelForm.enable();
          });
    }
  }
  ngOnInit() {
    this.labelForm = this.formBuilder.group({
      '_id': [this.label._id],
      'name': [this.label.name, Validators.required],
      'description': [this.label.description, Validators.required]
    });
  }
}
