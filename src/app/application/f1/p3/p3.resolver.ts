import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { UserService, AppService, EntityListConfig, P3Service } from './../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P3Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor ( private p3Service: P3Service) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const appComponents = route.parent.data['data'].appComponents;
    const selectedComponent = appComponents.find(component=> component.componentID.route == '/p3');
    const nextComponent = appComponents[appComponents.indexOf(selectedComponent)+1]
    const appID = state['url'].split('/')[2];
    return this.p3Service.getAllp3(entityListConfig, appID, false).map(
      (p3Data) =>{
        let data = {};
        data['appID'] = appID;
        data['selectedComponent'] = selectedComponent;
        data['nextComponent'] = nextComponent;
        data['p3Data'] = p3Data;
        return data;
      }
    );
  }
}

