import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx'
import {EntityListConfig, UserService, CcldService} from '../../../shared';
import {DTOService} from '../../../shared/services/dto.service';


@Injectable()
export class MailResolver implements Resolve<any>{
  routeData: Subscription;
  constructor (private userService: UserService, private dtoService: DTOService, private ccldService: CcldService) {
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    entityListConfig.query['label'] = 'file';
    const userID = state['url'].split("/")[5];
 const componentID =   this.dtoService.getValue('componentID');
 const categoryID = this.dtoService.getValue('categoryID');
    const obs1 =  this.userService.getAllContactFromMail(userID);
    const obs2 =  this.ccldService
      .getAllSuggestion(categoryID, componentID, entityListConfig);
  return Observable.forkJoin(Observable.of(componentID), obs1, Observable.of(categoryID), obs2);
  }
}
