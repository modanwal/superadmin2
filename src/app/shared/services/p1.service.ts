import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class P1Service {
  constructor (private apiService: ApiService) {}

  getAll(appID,isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/p1/app/${appID}`, query, isSocket);
    }

  get(_id, isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/p1/${_id}`, query, isSocket);
  }
  destroy(_id): Observable<any>{
    return this.apiService.delete(`/p1/${_id}` );
  }

  save(p1FormData, appID, p1?): Observable<any> {
    // If we're updating an existing component
    if (p1) {
      return this.apiService.putWithFormData(`/p1/${p1}/app/${appID}/` , p1FormData)
        .map(data => data.welcome);
      // Otherwise, create a new category
    } else {
      return this.apiService.postWithFormData(`/p1/app/${appID}`, p1FormData)
        .map(data => data.welcome);
    }
  }

}
