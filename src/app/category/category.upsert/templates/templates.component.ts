import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User, TemplateModel, TemplatesService, SocketService, EntityListConfig,InfiniteScrollService, ModalService } from '../../../shared';
import { TemplatesDeleteComponent } from './templates.delete/templates.delete.component';
import { TemplatesUpsertComponent } from './templates.upsert/templates.upsert.component';

import { Subscription } from 'rxjs/Subscription';
import { BreadCrumbService } from './../../../shared/breadcrumb.module';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'templates-pages',
  templateUrl: './templates.component.html',
  providers: [EntityListConfig]
})
export class TemplatesComponent implements OnInit{
  accessPrivilege: any;
  fetchingData: boolean;
  collectionDirty: boolean;
  templatesFetchSub: Subscription;
  authenticated: Boolean;
  lastFetchedItems: number;
  @Output() templates: TemplateModel[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  @Output() categoryID: string;
  templatesObj: any;
  constructor (private modalService: ModalService,
               private router: Router,
               private route: ActivatedRoute,
               private templatesService: TemplatesService,
               public entityListConfig: EntityListConfig,
               private breadCrumbService: BreadCrumbService,
               private socketService: SocketService,
               public infiniteScrollService: InfiniteScrollService) {
    this.templates = [];
    this.templatesObj=  {};
    this.lastFetchedItems = 0;
    this.collectionDirty = false;
    /*** websockets */
    // this.socketService.subscribeToEvent(`templates:save`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllTemplatesForSockets());
    //
    // this.socketService.subscribeToEvent(`templates:remove`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllTemplatesForSockets());
    // /*** websockets */


    /*** websockets */
    this.socketService.subscribeToEvent(`template:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.templates,this.templatesObj, this.entityListConfig);
        if(this.fetchingData){
          this.templatesFetchSub.unsubscribe();
          this.getAllTemplates(false);
        }
      })
    this.socketService.subscribeToEvent(`template:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.templates,this.templatesObj,this.entityListConfig);
        if(this.fetchingData){
          this.templatesFetchSub.unsubscribe();
          this.getAllTemplates(false);
        }
      })
    /*** websockets */

  }
  ngOnInit(){
    this.categoryID = this.route.snapshot.params.id;
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(val => {
        this.templates = val['data'];
        this.infiniteScrollService.computeEntity(this.templates,this.templatesObj, this.entityListConfig);

  });
    this.breadCrumbService
              .pushBreadCrumbItem({url: '/templates', params: this.categoryID, label: 'Templates'}, false);

  }
  setEntityConfig(event){
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllTemplates(true);
  }
  paginateListing(){
    this.getAllTemplates(false);
  }
  // getAllTemplatesForSockets(){
  //     const entityListConfig = new EntityListConfig();
  //     entityListConfig.query = this.entityListConfig.query;
  //     entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber + 1) * (this.entityListConfig.params.pageSize);
  //     entityListConfig.params.pageNumber = 0;
  //     this.templatesService
  //       .getAll(this.categoryID, entityListConfig, true)
  //       .subscribe(data => {
  //         this.templates = data;
  //         this.lastFetchedItems = (data.length) % (this.entityListConfig.params.pageSize);
  //         this.entityListConfig.params.pageNumber = Math.ceil(data.length / this.entityListConfig.params.pageSize);
  //         if (this.entityListConfig.params.pageNumber != 0) this.entityListConfig.params.pageNumber--;
  //       });
  // }
  getAllTemplates(getRefresh){
    this.fetchingData = true;
    this.collectionDirty = false;
  this.templatesFetchSub =   this.templatesService
      .getAll(this.categoryID, this.entityListConfig)
      .subscribe(
        data => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllTemplates(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.templates, this.templatesObj, this.entityListConfig, getRefresh);
          }
        }
        //   if (getRefresh) this.templates = [];
        //   // remove last page entries if data less than ideal size
        //   if (this.lastFetchedItems < this.entityListConfig.params.pageSize){
        //     this.templates.splice(
        //       (this.entityListConfig.params.pageNumber)
        //       * (this.entityListConfig.params.pageSize), this.lastFetchedItems);
        //   }
        //   this.lastFetchedItems = data.length;
        //
        //   if (data.length == this.entityListConfig.params.pageSize){
        //     this.entityListConfig.params.pageNumber++;
        //   }
        //   //push new data
        //   this.templates = [...this.templates, ...data];
        // },

          ),(error)=>{this.fetchingData = false;};;
  }
  selectTemplate(template: TemplateModel){
    this.breadCrumbService
              .pushBreadCrumbItem({url: '/template', label: template.name, params: template._id}, false);
    this.router.navigateByUrl(`template/${template._id}`);
  }
  deleteTemplate(templates: TemplateModel){
    const modalRef = this.modalService.open(TemplatesDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.component = templates;
  }
  editTemplate(templates: TemplateModel){
    const modalRef = this.modalService.open(TemplatesUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.component = templates;
  }
  addTemplate(){
    const modalRef = this.modalService.open(TemplatesUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.component = new TemplateModel;
    modalRef.componentInstance.categoryID = this.categoryID;
  }
}
