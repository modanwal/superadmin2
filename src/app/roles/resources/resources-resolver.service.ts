/**
 * Created by rahul on 7/26/2017.
 */

import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, ResourceService, EntityListConfig } from '../../shared';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export class ResourceResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor ( private resourceService: ResourceService,
                private router: Router,
               ) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
  const entityListConfig = new EntityListConfig();
    const id = route.queryParams[ 'id' ];
    return this.resourceService.getAll (entityListConfig );
  }
}
