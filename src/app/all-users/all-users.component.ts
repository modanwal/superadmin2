import {Component, Input, OnInit, ViewEncapsulation, OnDestroy, AfterViewInit, ElementRef, ViewChild} from '@angular/core';
import { AllUserModel, EntityListConfig, UserService, SocketService, InfiniteScrollService, ModalService } from '../shared';
import { AllUserUpsertComponent } from './all-users.upsert/all-users.upsert.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { AllUserDeleteComponent } from './all-users.delete/all-users.delete.component';
import { Subject } from "rxjs/Subject";
import { BreadCrumbService } from './../shared/breadcrumb.module';
import {Observable} from "rxjs/Rx";
import {Subscription} from 'rxjs/Subscription';
import { trigger, transition, style, animate, state, keyframes, query, stagger } from "@angular/animations";
import { DomSanitizer } from "@angular/platform-browser";


@Component({
  selector: 'page-root',
  templateUrl: './all-users.component.html',
  providers:[EntityListConfig]
//   styles:[`
//   .table-data-wrapper tr{
//     transform-style: preserve-3d;
//     transform-origin: 0% 0%;
//     transform: rotateX(-80deg);
//     animation: flip .8s ease-in-out forwards;
//   }
//   @keyframes flip {
//     0% { }
//     100% { -webkit-transform: rotateX(0deg); opacity: 1; }
//   }
//   // .table-data-wrapper.mask-data .table{
//   //   display:block;
//   // }
//   // .table-data-wrapper.mask-data .table:before{
//   //   display:none;
//   // }
//   // .table-data-wrapper.mask-data tbody{
//   //   display:block;
//   //   width:100%;
//   // }
//   // .table-data-wrapper.mask-data .table tr{
//   //   display:block;
//   //   width:100%;
//   // }
//   // .table-data-wrapper.mask-data .table tr td{
//   //   display:block;
//   // }
//   @-webkit-keyframes flip {
//     0% { }
//     100% { -webkit-transform: rotateX(0deg); opacity: 1; }
//   }
// `],
//     animations: [
// trigger('explainerAnim', [
//       transition('* => *', [
//         query('.col', style({ opacity: 0.8, transform: 'translateX(-40px)' })),

//         query('.col', stagger('500ms', [
//           animate('800ms 1.2s ease-out', style({ opacity: 1, transform: 'translateX(0)' })),
//         ])),

//         query('.col', [
//           animate(1000, style('*'))
//         ])

//       ])
//     ])
//     ]
    // animations: [
    //     trigger('explainerAnim', [
    //           transition('* => *', [
    //             query('.col', style({ opacity: 0.8, transform: 'translateX(-40px)' })),
    //             query('.col', stagger('586ms', [
    //               animate('800ms 1.2s ease-out', style({ opacity: 1, transform: 'translateX(0)' })),
    //             ])),
    //             query('.col', [
    //               animate(1000, style('*'))
    //             ])

    //           ])
    //         ])
    // ]
  // animations: [slideToLeft()],
  // host: {'[@slideToLeft]': ''},
})

export class AllUsersComponent implements OnInit, OnDestroy, AfterViewInit {
  //lastFetchedItems: number;
  fetchingData: boolean;
  collectionDirty: boolean;
  usersObj: any;
  term: any;
  public title: 'All User';
  users: AllUserModel[];
  staggeringUsers: any[] = [];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  @ViewChild('input')
  input: ElementRef;
  usersFetchSub: Subscription;
  searchData: any;
  constructor(public entityListConfig: EntityListConfig,
              private modalService: ModalService,
              public activeModal: NgbActiveModal,
              private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private socketService: SocketService,
              private saniteizer: DomSanitizer,
              private breadCrumbService: BreadCrumbService, public infiniteScrollService: InfiniteScrollService) {
    this.collectionDirty = false;
    this.usersObj = {};
    /*** websockets */
    this.socketService.subscribeToEvent(`user:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.users,this.usersObj, this.entityListConfig);
        if(this.fetchingData){
          this.usersFetchSub.unsubscribe();
          this.getAllUsers(false);
        }
      })
    this.socketService.subscribeToEvent(`user:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {

        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.users,this.usersObj,this.entityListConfig);
        if(this.fetchingData){
          this.usersFetchSub.unsubscribe();
          this.getAllUsers(false);
        }
      })
    /*** websockets */
  }
  ngAfterViewInit() {
    const eventObservable = Observable.fromEvent(
      this.input.nativeElement, 'keyup');
    eventObservable
      .debounce(() => Observable.timer(1000))
      .subscribe(
        (data) => {
          this.searchData = data['target']['value'];
          if (this.searchData) {
            this.userService
              .getBySearch(this.entityListConfig, this.searchData)
              .subscribe((data1) => {
                this.users = data1;
              });
          } else  {
            this.getAllUsers(true);
          }
        }
      );
  }
  ngOnInit(){
    this.breadCrumbService
              .pushBreadCrumbItem({url:'/all-users',label:'All Users'},true);
    this.route.data
      .map(data => data.users)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(users => {
        this.users = users;
        console.log(this.users);
        this.infiniteScrollService.computeEntity(this.users,this.usersObj, this.entityListConfig);
      });
  }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllUsers(true);
  }
  onScroll(){
    this.getAllUsers(false);
  }

  getAllUsers(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
  this.usersFetchSub =   this.userService
      .getAll(this.entityListConfig)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllUsers(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.users, this.usersObj, this.entityListConfig, refreshData);

          }
        }
        //   if(refreshData) this.users = [];
        //   // remove last page entries if data less than ideal size
        //   if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
        //     this.users.splice(
        //       (this.entityListConfig.params.pageNumber)
        //       *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
        //   }
        //   this.lastFetchedItems = data.length;
        //   if(data.length==this.entityListConfig.params.pageSize){
        //     this.entityListConfig.params.pageNumber++;
        //   }
        //   // push new data
        //   this.users = [...this.users,...data];
        // }
      ),(error)=>{this.fetchingData = false;};
  }
  editUser(user) {
    const modalRef = this.modalService.open(AllUserUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.user = user;
  }
  viewUser(user){
    this.router.navigateByUrl(`/user-details/${user._id}/application-status`);
  }
  addUser() {
    const modalRef = this.modalService.open(AllUserUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.user = new AllUserModel;
  }
  deleteUser(user) {
    const modalRef = this.modalService.open(AllUserDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.user = user;
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  doNext(index){
    if(index+1 < this.users.length) {
      this.staggeringUsers.push(this.users[index]);
    }
  }
  getAnimation(index){
    return this.saniteizer.bypassSecurityTrustStyle('fade-in-pulse '+0.5*index+'s forwards cubic-bezier(0.8, 0.02, 0.45, 0.91)');
  }

}
