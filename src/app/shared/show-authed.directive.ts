import {
  Directive,
  Input,
  OnInit,
  Renderer2,
  ElementRef ,
  AfterContentInit
} from '@angular/core';

import { UserService } from './services/user.service';

@Directive({ selector: '[showAuthed]' })
export class ShowAuthedDirective implements OnInit {
  privileges: any;
  constructor(
    private userService: UserService,
    private el: ElementRef,
    private renderer: Renderer2
  ) {
  }
  @Input('showAuthed') showAuthed: Object;


  ngOnInit(){
    this.userService.currentUser.subscribe(
      (data) => {
        this.privileges = data.privileges;
        if(!this.privileges) {
          this.renderer.setAttribute(this.el.nativeElement, 'disabled', '');
        }
        else if(!this.privileges[this.showAuthed['privilege']]){
          this.renderer.setAttribute(this.el.nativeElement, 'disabled', '');
        }
        else if(!this.privileges[this.showAuthed['privilege']][this.showAuthed['access']]){
          this.renderer.setAttribute(this.el.nativeElement, 'disabled', '');
        }
      },
      (error) => {}
    )
  }

}
