import {Component, OnDestroy, OnInit} from '@angular/core';
import { AppService, SocketService } from "./../../shared";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs/Rx";
import {F1UpsertComponent} from "./f1.upsert/f1.upsert.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DTOService} from "../../shared/services/dto.service";
import { ModalService } from './../../shared/modal.service';
import {F1DeleteComponent} from "./f1.delete/f1.delete.component";

@Component({
  selector: 'app-root',
  templateUrl: './f1.component.html'
})

export class F1Component implements OnDestroy, OnInit{
  appID: any;
  appComponents: any;
  categoryID: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private router: Router,
              private socketService: SocketService,
              private route: ActivatedRoute,
              private modalService: ModalService,
              private appService: AppService,
              private dtoService: DTOService) {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(val =>{
         this.appComponents = val['data']['appComponents'];
         this.appID = val['data']['appID'];
         if(!this.router.url.split('/')[4]){
          this.router.navigateByUrl(`/application/${this.appID}/f1${this.appComponents[0]['componentID']['route']}`);
         }
      });
    /*** websockets */
    this.socketService.subscribeToEvent(`f1:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => this.getAllAppComponents())
    /*** websockets */
  }
  editPageName(component: any){
    const modalRef = this.modalService.open ( F1UpsertComponent,{},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.component = component;
    modalRef.componentInstance.appID = this.appID;
  }
  deletePageName(component: any){
    const modalRef = this.modalService.open ( F1DeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.component = component;
    modalRef.componentInstance.appID = this.appID;

  }
  getAllAppComponents(){
   console.log('method called');
  }
  ngOnInit(){

  }
  getAllf1(){

  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
