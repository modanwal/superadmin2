import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class P4F1Service {
  constructor (private apiService: ApiService) {}

  getAll(  entityListConfig:  EntityListConfig,p4ID,appID, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/p4/f1/app/${appID}/p4/${p4ID}/${pageNumber}/${pageSize}`, query, isSocket);
  }
  save(p4f1,p4ID,appID): Observable<any> {
    // If we're updating an existing component
    if (p4f1._id) {
      const _id = p4f1._id;
      delete p4f1._id;
      return this.apiService.put(`/p4/f1/${_id}/app/${appID}/p4/${p4ID}`,p4f1);
      // Otherwise, create a new category
    } else {
      return this.apiService.post(`/p4/f1/app/${appID}/p4/${p4ID}`,p4f1);
    }
  }
}
