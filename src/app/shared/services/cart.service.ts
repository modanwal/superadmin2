import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class CartService {
  constructor(private apiService: ApiService) {
  }
  getAll(entityListConfig: EntityListConfig, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/cart/all/${pageNumber}/${pageSize}`, query, isSocket);
  }
  save(qty, cartID): Observable<any[]> {
    return this.apiService.put(`/cart/${cartID}`,qty);
  }
}
