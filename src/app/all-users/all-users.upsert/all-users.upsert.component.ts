import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AllUserModel, UserService } from '../../shared';
import { NG_VALIDATORS, AbstractControl , ValidatorFn } from '@angular/forms';
@Component({
  selector: 'alluserupsert-modal',
  templateUrl: './all-users.upsert.component.html'
})
export class AllUserUpsertComponent implements  OnInit {
  imagePreview: any;
  @Input() user;
  file: any;
  userForm: FormGroup;
  isSubmited: Boolean;
  submitting: Boolean;

  constructor( private formBuilder: FormBuilder,
               public activeModal: NgbActiveModal,
               private userService: UserService ) {
    this.isSubmited = false;
  }
  ngOnInit() {
  if(this.user.path){
    this.imagePreview = this.user.path;
  }
  //   use FormBuilder to create a form group
    this.userForm = this.formBuilder.group({
      '_id': [this.user._id],
      'name': [this.user.name, Validators.required],
      'mobile': [this.user.mobile, Validators.required],
      // 'mobile': [this.user.mobile, Validators.compose([Validators.minLength(10), Validators.maxLength(10)])]
    });
  }
  selectFile(event){
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    const file = event.target.files[0];
    fileReader.addEventListener("load", ()=>{
      this.imagePreview = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }
  eventHandler(event) {
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.userForm.valid) {
      this.submitting = true;
      this.userForm.disable();
      const user = this.userForm.value;
      let formData = new FormData();
      formData.append('name', user.name);
   //   formData.append('email', user.email);
      formData.append('mobile', user.mobile);
      if(this.file){
        formData.append('file', this.file);
      }
      this.userService
        .saveUser(formData, this.user._id)
        .subscribe(
          data => {
            this.activeModal.close('user:updated');
          },
          error => {
            this.userForm.enable();
          }
        );
    }
  }
}
