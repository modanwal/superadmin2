import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { PrivilegeModel, EntityListConfig, ModalService } from '../../shared';
import { PrivilegeUpsertComponent } from './privilege.upsert/privilege.upsert.component';
import { PrivilegeDeleteComponent } from './privilege.delete/privilege.delete.component';
import { PrivilegeService, SocketService, InfiniteScrollService } from '../../shared/services';
import { ActivatedRoute } from '@angular/router';
import { BreadCrumbService } from "./../../shared/breadcrumb.module";
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'app-privilege',
  templateUrl: './privilege.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})
export class PrivilegeComponent implements OnInit {
  lastFetchedItems: number;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  privileges: PrivilegeModel[];
  privilegesObj: any;
  @Output('selectComponent') selectComponentEmitter = new EventEmitter<PrivilegeModel>();
  @Output('editComponent') editComponentEmitter = new EventEmitter<PrivilegeModel>();
  constructor(private modalService: ModalService,
              private privilegeService: PrivilegeService,
              public entityListConfig: EntityListConfig,
              private route: ActivatedRoute,
              private breadCrumbService: BreadCrumbService,
              private socketService: SocketService,
              public infiniteScrollService: InfiniteScrollService) {
    this.privilegesObj = {};
    this.lastFetchedItems = 0;
  //    this.getAllPrivilege();
  //   /*** websockets */
  //   this.socketService.subscribeToEvent(`privilege:save`)
  //     .takeUntil(this.ngUnsubscribe)
  //     .subscribe(data => this.getAllPrivilegForSockets());
  //
  //   this.socketService.subscribeToEvent(`privilege:remove`)
  //     .takeUntil(this.ngUnsubscribe)
  //     .subscribe(data => this.getAllPrivilegForSockets());
  //   /*** websockets */

    /*** websockets */
    this.socketService.subscribeToEvent(`privilege:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.savedFromSocket(data, this.privileges,this.privilegesObj, this.entityListConfig);
      })
    this.socketService.subscribeToEvent(`privilege:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.removedFromSocket(data, this.privileges,this.privilegesObj,this.entityListConfig);
      })
    /*** websockets */

  }

  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        (val) => {
          this.privileges = val['data'];
          this.infiniteScrollService.computeEntity(this.privileges,this.privilegesObj, this.entityListConfig);
          // this.lastFetchedItems = this.privileges.length;
          // if(this.privileges.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
        });
    this.breadCrumbService
        .pushBreadCrumbItem({url:'/privilege',label:'Privilege'},true);
  }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if (this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  // getAllPrivilegForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.privilegeService
  //     .getAll(this.entityListConfig)
  //     .subscribe(
  //       (data) => {
  //         this.privileges = data;
  //         this.lastFetchedItems = (data.length) % (this.entityListConfig.params.pageSize);
  //         this.entityListConfig.params.pageNumber = Math.ceil(data.length / this.entityListConfig.params.pageSize);
  //         if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //       }
  //     );
  //
  // }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllPrivilege(true);
  }
  onScroll(){
    this.getAllPrivilege(false);
  }
  /*** sorting & pagination */
  getAllPrivilege(refreshData) {
   this.privilegeService
      .getAll(this.entityListConfig)
      .subscribe(
        (data) => {
          this.infiniteScrollService.getFromApi(data, this.privileges, this.privilegesObj, this.entityListConfig, refreshData);
        }
      );
  }
  //  getAllPrivilege()
  //  {

  //     this.privilegeService
  //     .getAll()
  //     .subscribe(
  //       data => this.privileges = data,
  //       (err) => alert(err)
  //     );
  //  }
  // getSortData(sortBy:string,order:string){
  //  if(this.sortBy==sortBy&&this.order==order)
  //    return true;
  // }
  // setSortData(sortBy:string,order:string){
  //  this.sortBy = sortBy;
  //  this.order = order;
  //  this.getAllResources();
  // }
  selectComponent(privilege: PrivilegeModel) {
    this.selectComponentEmitter.emit(privilege);
  }
  addComponent() {
    // this.selectComponentEmitter.emit(priviliges);
    const modalRef = this.modalService.open( PrivilegeUpsertComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.privilege = new PrivilegeModel;
   //  modalRef.result.then((closeReason) => {
   //   if (closeReason == 'privilege:updated'){
   //     this.getAllPrivilege();
   //   }
   // });
  }
  editComponent(privilege: PrivilegeModel) {
   // this.editComponentEmitter.emit(component);
    const modalRef = this.modalService.open( PrivilegeUpsertComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.privilege = privilege;
   //  modalRef.result.then((closeReason) => {
    //   if (closeReason === 'privilege:updated'){
    //     this.getAllPrivilege();
    //   }
    // });
  }
  deleteComponent (privilege: PrivilegeModel) {
    const modalRef = this.modalService.open ( PrivilegeDeleteComponent, {},'fade-in-pulse','fade-out-pulse' );
     modalRef.componentInstance.privilege = privilege;
   //    modalRef.result.then((closeReason) => {
   //   if (closeReason === 'privilege:updated'){
   //     this.getAllPrivilege();
   //   }
   // });
  }
}
