
import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P3Service} from '../../../../shared';



@Component({
  selector: 'app-root',
  templateUrl: './p3p1.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class P3P1UpsertComponent implements  OnInit {
  P3P1Form: FormGroup;
  @Input() appID;
  imagePreview: any;
  @Input() p3p1;
  @Input() p3Id;
  isSubmited: Boolean;
  isSubmitting: Boolean
  file: any;
  formData: FormData;
  @Input() componentID: any;
  @Input() categoryID: any;
  path: any;
  parentReference: any;
  selfReference: any;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private p3Service: P3Service,
              ) {
  }
  selectFile(event) {
    this.file = event.target.files[0];
    console.log(this.file);
    let fileReader = new FileReader();
    const file = event.target.files[0];
    fileReader.addEventListener('load', () => {
      this.imagePreview = fileReader.result;
      this.p3p1.path = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }
  onContentSelected(event){
    console.log(event);
    if(event.labelName === 'file-1'){
      this.path = event.path;
      this.imagePreview = event.path;
    }
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.P3P1Form.valid) {
      this.isSubmitting = true;
      this.P3P1Form.disable();
      const p3p1 = this.P3P1Form.value;
      this.formData = new FormData();

      this.formData.append('name', p3p1.name);
      if(this.file){
        this.formData.append('file', this.file);
      }
      if(this.path){
        this.formData.append('path', this.path);
      }
      this.formData.append('cover', p3p1.cover);

      this.p3Service
        .saveFormData(this.formData, p3p1, this.p3Id, this.appID)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.P3P1Form.enable();
            this.activeModal.close();
          },
          error => {
            this.isSubmitting = false;
            this.P3P1Form.enable();
          }
        );
    }
  }

  ngOnInit() {
    this.P3P1Form = this.formBuilder.group({
      '_id': [this.p3p1._id],
      'name': [this.p3p1.name, Validators.required],
      'path': [this.p3p1.path, Validators],
      'cover': [this.p3p1.cover]
    });
  this.P3P1Form.controls['cover'].setValue(true);

  }
}
