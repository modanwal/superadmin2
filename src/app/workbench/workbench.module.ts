import { NgModule } from '@angular/core';
import { SharedModule, AuthGuard } from '../shared';

import { WorkbenchComponent } from './workbench.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {WorkbenchStatusComponent} from './workbench-status/workbench-status.component';
import {RouterModule} from "@angular/router";
import {WorkbenchResolver} from "./workbench.resolver";



export const workBenchRouting = [
  {
    path: 'workbench',
    canActivate: [AuthGuard],
    component: WorkbenchComponent,
    resolve: { data : WorkbenchResolver},
    children: [
      {
        path: 'status',
        component: WorkbenchStatusComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    SharedModule,
    NgbModule, RouterModule
  ],
  declarations: [
    WorkbenchComponent, WorkbenchStatusComponent
  ],
  providers: [
    AuthGuard, WorkbenchResolver
  ],
  entryComponents: [
  ]
})
export class WorkbenchModule {

}
