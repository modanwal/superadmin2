import {Component, Input, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { F3Service } from '../../../shared';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'f3upsert-component',
  templateUrl: './f3.upsert.component.html',
})

export class F3UpsertComponent implements  OnInit{
  f3Form: FormGroup;
  @Input() f3;
  @Input() appID;
  @Input() pageName;
  isSubmited: Boolean;
   constructor(private modalService: NgbModal , private f3Service:F3Service, private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private router: Router,) {
    // router.events.subscribe((url:any) => {this.url = url.url});
   this.isSubmited = false;
  }
  submitForm() {
    if(!this.isSubmited){this.isSubmited = true }
    if(this.f3Form.valid){
      this.f3Form.disable();
      console.log('appID', this.appID);
      const f3 = this.f3Form.value;
      this.isSubmited = true;
       this.f3Service
        .saveF3(f3, this.appID)
        .subscribe(data => { this.activeModal.close('f3:updated')},error => {
          this.f3Form.enable();  this.isSubmited = false;
        });
    }
  }
  ngOnInit() {
    this.f3Form = this.formBuilder.group({
      '_id': [this.f3._id],
      'default': [this.f3.default],
      'name': [this.f3.name, Validators.required],
    });
  }
}
