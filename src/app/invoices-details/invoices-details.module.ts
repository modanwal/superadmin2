import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule, AuthGuard, InvoicesService } from '../shared';
import { InvoicesDetailsComponent } from "./invoices-details.component";
import {InvoicesDetailsResolver} from "./invoices-details.resolver";

export const invoicesDetailsRouting = [
  {
    path: ':id/invoices-details',
    component: InvoicesDetailsComponent,
    canActivate: [AuthGuard],
    resolve: {
      data: InvoicesDetailsResolver
    }
   }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
      InvoicesDetailsComponent
  ],
  providers: [
    AuthGuard, InvoicesDetailsResolver, InvoicesService
  ]
})
export class InvoicesDetailsModule {}
