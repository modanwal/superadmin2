import { CommonModule } from '@angular/common';
import { Component, OnInit, ViewEncapsulation, Input, Output, Injectable, NgModule, ModuleWithProviders } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject, BehaviorSubject } from "rxjs/Rx";
import { RouterModule} from '@angular/router';

interface BreadCrumbParam{
  params?: string;
  url: string;
  label: string;
  queryParams?: Object;
}

@Injectable()
export class BreadCrumbService{
    private breadCrumbItems:any;
    private subject = new BehaviorSubject<any>([]);
    constructor(){
      if(typeof window!=undefined && typeof window.sessionStorage!=undefined){
        this.breadCrumbItems = JSON.parse(window.sessionStorage.getItem('breadCrumbItems'));
      }
      if(!this.breadCrumbItems) this.breadCrumbItems = [];
      this.subject.next(this.breadCrumbItems);
    }
    pushBreadCrumbItem(breadcrumbItem: BreadCrumbParam,isFirstLevelRoute: boolean) {
      if(isFirstLevelRoute){
        this.breadCrumbItems = [];
      }else{
        let index = this.breadCrumbItems.findIndex(item => {
          return item.url==breadcrumbItem.url
                 &&item.params==breadcrumbItem.params
                 &&item.label==breadcrumbItem.label
                 &&(JSON.stringify(item.queryParams)==JSON.stringify(breadcrumbItem.queryParams));
        });
        if(index != -1) this.breadCrumbItems.splice(index,this.breadCrumbItems.length-index);
      }
      this.breadCrumbItems.push(breadcrumbItem);
      this.saveBreadCrumbToStorage();
      this.subject.next(this.breadCrumbItems);
    }
    removeBreadCrumbItem(breadcrumbItem: BreadCrumbParam){
      let index = this.breadCrumbItems.findIndex(item => {
          return item.url==breadcrumbItem.url
                 &&item.params==breadcrumbItem.params
                 &&item.label==breadcrumbItem.label
                 &&(JSON.stringify(item.queryParams)==JSON.stringify(breadcrumbItem.queryParams));
        });
        if(index != -1) this.breadCrumbItems.splice(index+1,this.breadCrumbItems.length-index);
        this.saveBreadCrumbToStorage();
    }
    getBreadCrumbItems(): Observable<any> {
        return this.subject.asObservable();
    }
    saveBreadCrumbToStorage(){
      if(typeof window!=undefined && typeof window.sessionStorage!=undefined){
        window.sessionStorage.setItem('breadCrumbItems',JSON.stringify(this.breadCrumbItems));
      }
    }
}

@Component({
  selector: 'breadcrumb',
  template: `
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a routerLink="/">
          <i aria-hidden="true" class="fa fa-home"></i>
        </a>
      </li>
      <li class="breadcrumb-item" *ngFor="let breadCrumbItem of breadCrumbItems">
        <a (click)="removeBreadCrumbItem(breadCrumbItem)" [routerLink]="getRouterLink(breadCrumbItem.url, breadCrumbItem.params)" [queryParams]="breadCrumbItem.queryParams">{{ breadCrumbItem.label }}</a>
      </li>
    </ol>
  `,
  encapsulation: ViewEncapsulation.None
})

export class BreadCrumbComponent implements OnInit {
  public breadCrumbItems: any;
  public breadCrumb:any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private breadCrumbService: BreadCrumbService){
    this.breadCrumbItems = []
  }
  ngOnInit(){
     // subscribe to home component messages
     this.breadCrumbService.getBreadCrumbItems()
        .takeUntil(this.ngUnsubscribe)
        .subscribe(breadcrumbItem => {
          this.breadCrumbItems = breadcrumbItem;
      });
  }
  getRouterLink(url,params){
    let link = [];
    link.push(url);
    if(params) link.push(params);
    return link;
  }
  getQueryParams(queryParams){
    var returnQueryParams = false;
    for(let k in queryParams){
      if(queryParams[k]){returnQueryParams = true;}
    }
    if(returnQueryParams){return queryParams;}
    else{return {};}
  }
  removeBreadCrumbItem(breadCrumbItem){
    this.breadCrumbService.removeBreadCrumbItem(breadCrumbItem);
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

@NgModule({
  imports: [CommonModule,RouterModule],
  exports: [BreadCrumbComponent],
  declarations: [BreadCrumbComponent]
})
export class BreadCrumbModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: BreadCrumbModule,
      providers: [BreadCrumbService]
    };
  }
}
