import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { EntityListConfig, UserDetailService} from '../../shared';


@Injectable()
export class UserAddressResolver implements Resolve<any> , OnInit{
  userID: any;
  constructor ( private userDetailService: UserDetailService,  private route: ActivatedRoute) {
  }
  ngOnInit(){
    // this.userID = this.route.parent.snapshot.data['user'];
    // console.log('user ID', this.userID['_id']);
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {

       const userID = state['url'].split('/')[2];
   //  const userID = this.route.parent.snapshot.data['user'];
  //   console.log('user ID', this.userID['_id']);
    const entityListConfig = new EntityListConfig();
    return  this.userDetailService.getAllAddress(userID, entityListConfig);

    // return Observable.forkJoin(obs1, obs2);

  }
}

