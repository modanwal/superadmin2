import {Component, OnInit} from '@angular/core';
import { UserService } from './shared';
import { ToastrConfig , ToastrService} from 'ngx-toastr';
import { Router } from '@angular/router';
import {HeaderComponent} from './shared/layout/header.component';
import {LeftSidebarComponent} from './shared/layout/left-sidebar.component';
import {RightSidebarComponent} from './shared/layout/right-sidebar.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  state: any;
  snapshot: any;
  root: any;
  constructor (
    private router: Router,
    private userService: UserService,
    toastrConfig: ToastrConfig,
    private toastrService: ToastrService
  ) {
    this.state = router.routerState;
    this.snapshot = this.state.snapshot;
    this.root = this.snapshot.root;
    toastrConfig.timeOut = 2000;

  }
  ngOnInit() {
    this.userService.populate();
  }

}
