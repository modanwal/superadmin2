import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class TypeService {
  constructor (private apiService: ApiService) {}

  getAll( entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/type/${pageNumber}/${pageSize}`, query, isSocket);
  }

  destroy(typeID): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/type/${typeID}` );
  }

  save(type): Observable<any> {
    // If we're updating an existing component
    if (type._id) {
      const typeID = type._id;
      delete type._id;
      return this.apiService.put(`/type/${typeID}`, type);

      // Otherwise, create a new category
    } else {
      delete type._id;
      return this.apiService.post(`/type/`, type);
    }
  }
}
