import { NgModule } from '@angular/core';
import { P1Service, P2Service, P4Service, P5Service, P3Service, SharedModule } from './../../../shared';
import { P2Component} from './p2.component';
import { P2ListComponent} from './p2.list/p2.list.component';
import { P2UpsertComponent} from './p2.upsert/p2.upsert.component';
import { P2DeleteComponent} from './p2.delete/p2.delete.component';
import { P2P1DeleteComponent} from './p2p1.delete/p2p1.delete.component';
import { P2P1UpsertComponent} from './p2p1.upsert/p2p1.upsert.component';
import {P2Resolver} from "./p2.resolver";
import {P2P1Resolver} from "./p2p1.list/p2p1.list.resolver";
import {P2P1Component} from './p2p1.list/p2p1.list.component';


export const p2Routing = [
      {
        path: 'p2',
        component: P2Component,
        resolve: {
          data: P2Resolver
        },
        children: [
          {
            path: '',
            component: P2ListComponent
          },
          {
          path: 'p2p1/:id',
          component: P2P1Component,
          resolve: {
            data: P2P1Resolver
          },
        }]
      }];

@NgModule({
  imports: [
      SharedModule
  ],
  declarations: [
    P2Component,P2UpsertComponent,
    P2DeleteComponent, P2P1DeleteComponent, P2P1UpsertComponent, P2P1Component, P2ListComponent],
  providers: [ P1Service, P2Service, P4Service,
    P5Service, P3Service, P2Resolver, P2P1Resolver],

  entryComponents: [
    P2UpsertComponent, P2DeleteComponent, P2P1DeleteComponent, P2P1UpsertComponent
  ]

})
export class P2Module {}
