import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class RoleResourcePrivilegeService {

  constructor (
    private apiService: ApiService
  ) {}


  getRoleResourcePrivilege(roleID, isSocket?): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/role-resource-privilege/${roleID}`, query, isSocket);
  }

  destroy(roleID, resourceID, privilegeID) {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/role-resource-privilege/${roleID}/${resourceID}/${privilegeID}`);
  }

  destroyRole(roleID, resourceID){

    return this.apiService.delete(`/role-resource-privilege/${roleID}/${resourceID}`);
  }

  save(entity): Observable<any> {
    return this.apiService.post(`/role-resource-privilege/`, entity);
  }


  selectAllCheckBoxs(data): Observable<any> {
    return this.apiService.post(`/role-resource-privilege/all`, data);
  }
  unSelectAllCheckBoxs(roleID, resourceID) {
    return this.apiService.delete(`/role-resource-privilege/${roleID}/${resourceID}/`);
  }
}
