import { Component, OnInit, Input } from '@angular/core';
import { User, EntityListConfig } from './../models';
import { Subject } from "rxjs/Subject";

@Component({
  selector: 'announcement-card',
  templateUrl: './announcement-card.component.html'
})
export class AnnouncementCardComponent implements OnInit{
  @Input() announcement: any;
  constructor() {
  }

  ngOnInit() {
    
  }

}

