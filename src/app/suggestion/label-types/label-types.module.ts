import { NgModule } from '@angular/core';
import {AuthGuard, SharedModule} from '../../shared';
import {SuggestionLabelTypesComponent} from "./label-types.component";
import {SuggestionLabelTypesResolver} from "./label-types.resolver";
import {SuggestionLabelTypesUpsertComponent} from "./label-types.upsert/label-types.upsert.component";
import {LabelTypesDeleteComponent} from "./label-types.delete/label-types.delete.component";
import {CcldService} from '../../shared/services/ccld.service';
import {CcltService} from '../../shared/services/cclt.service';

export const labelTypeRouting = [
  {
    path: 'labelstype',
    component: SuggestionLabelTypesComponent,
    canActivate: [AuthGuard],
    resolve: {
      data: SuggestionLabelTypesResolver
    }
  }
];
@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    SuggestionLabelTypesComponent, SuggestionLabelTypesUpsertComponent, LabelTypesDeleteComponent
  ],
  providers: [
    AuthGuard, SuggestionLabelTypesResolver, CcldService, CcltService
  ],
  entryComponents: [SuggestionLabelTypesUpsertComponent, LabelTypesDeleteComponent ]
})
export class LabelTypesModule {}
