import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RoleModel, RoleService  } from '../../../shared';
@Component({
  selector: 'role-delete',
  templateUrl: './role.delete.component.html'
})

export class RoleDeleteComponent {
  @Input() role: RoleModel;
  isSubmited: Boolean;
  constructor( public activeModal: NgbActiveModal ,
               private modalService: NgbModal,
               private roleService: RoleService
  ) {}
  deleteRole(role: RoleModel) {
    this.activeModal.close();
    this.roleService.destroy(role._id)
      .subscribe(
        data => this.activeModal.close()
      );
  }
}
