import {Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {WallPostModal} from "../../shared/models/wallPost.modal";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {PostService, DTOService} from "../../shared";
import { UploaderService } from '../../shared/uploader';
import {Bounds, CropperSettings, ImageCropperComponent} from "ng2-img-cropper";
import { ImagePostComponent } from "./../image.post/image.post.component";
import {ImageSelectionComponent} from "../images-selection/image-selection.component";

@Component({
  selector: 'wall-post-pages',
  templateUrl: './post.upsert.component.html',
  styleUrls: ['./post.upsert.component.scss']
})
export class WallUpsertComponent implements OnInit, OnChanges {
  fileType: string;
  submitting: boolean;
  cropBounds: any;
  imagePreview: any;
  croppedWidth:number;
  croppedHeight:number;
  postForm: FormGroup;
  path: any;
  @Input() post;
  @Input() userData;
  @Input() apps;
  data1: any;
  @ViewChild('cropper', undefined)
  cropper: ImageCropperComponent;
  cropperSettings1: any;
  isSubmited: boolean;
  file: any;
  selectedAppID: any;
  formData: FormData;

  constructor(
    private uploaderService: UploaderService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private postService: PostService, private dtoService: DTOService){
      this.submitting = false;
    this.data1 = {};
  }
  cropped(bounds: Bounds) {
    console.log(this.data1);
    console.log(bounds);
    this.croppedHeight =bounds.bottom-bounds.top;
    console.log(this.croppedHeight);
    this.croppedWidth = bounds.right-bounds.left;
    console.log(this.croppedWidth);
  }
  ngOnChanges(){
    this.path = this.dtoService.getValue('imagePath');
    this.imagePreview = this.path;
  }

  ngOnInit() {
    this.postForm = this.formBuilder.group({
      'title': [this.post.title,Validators.required],
      'path': [this.post.path],
      'selectedAppID' :[this.apps[0]?this.apps[0]['_id']:'']
    });
  }
  /*** psr added! */
  getFileType(file){
    const validImageType = ['jpg', 'jpeg', 'png'];  //acceptable file types
    const validVideoType = ['MOV', 'MPEG4', 'MP4', 'AVI','WMV','MPEGPS','FLV','3GPP'];  //acceptable file types
    let extension = file.name.split('.').pop().toLowerCase();
    const isImage = validImageType.indexOf(extension) > -1;
    const isVideo = validVideoType.indexOf(extension) > -1;
    if(isImage) return 'image';
    if(isVideo) return 'video';
    return 'unknown';
  }
  /*** psr added! */

  selectFile(event){

    let fileReader = new FileReader();
    this.file = event.target.files[0];
    fileReader.addEventListener("load",()=>{
      this.imagePreview = fileReader.result;
    });
    fileReader.readAsDataURL(this.file);

    // const fileType = this.getFileType(file);
    // if(fileType=='image'){
    //   const modalRef = this.modalService.open(ImagePostComponent);
    //   modalRef.componentInstance.file = file;
    //   modalRef.result.then((data) => {
    //     if(data){
    //       this.file = file;
    //       this.cropData = data;
    //       fileReader.addEventListener("load", ()=>{
    //         this.imagePreview = fileReader.result;
    //       });
    //       fileReader.readAsDataURL(file);
    //     }
    //   });
    // }
    // else if(fileType=='video'){
    //   //generate uploaded video preview
    // }else{
    //   //else generate nice error to user
    // }
  }
  uploadFile(){
    const modalRef =  this.modalService.open(ImageSelectionComponent,{
      size: 'sm'
    });
    modalRef.result.then((event) => {
        if(event.type == 'image'){
          this.fileType = 'image';
          this.file = event.file;
          this.cropBounds = event.cropBounds;
          let fileReader = new FileReader();
          fileReader.addEventListener("load",()=>{
            this.imagePreview = fileReader.result;
          });
          fileReader.readAsDataURL(this.file);
        }
        if(event.type == 'video'){
          this.file = event.file;
          this.fileType = 'video';
        }
        //  if(file){
        //    this.file = file;
        //    let fileReader = new FileReader();
        //    fileReader.addEventListener("load",()=>{
        //      this.imagePreview = fileReader.result;
        //    });
        //    fileReader.readAsDataURL(this.file);
        //   if( typeof file === 'string'){
        //     this.path = this.dtoService.getValue('imagePath');
        //     this.imagePreview = this.path;
        //   }
        //  }
    });
  }
  submitForm(){
    this.isSubmited = true;
    if (this.postForm.valid) {
      this.submitting = true;
      this.postForm.disable();
      const post = this.postForm.value;
      this.formData = new FormData();
      this.formData.append('title', post.title);
      if(this.file){
        this.formData.append('file', this.file);
      }
      if(this.path){
        this.formData.append('path', this.path);
      }

      if(!this.file){
        this.postService
        .saveFormData(this.formData, post.selectedAppID)
        .subscribe(
          data => {
            this.postForm.enable();
            this.activeModal.close();
            this.submitting = false;
          },
          error => {
            this.postForm.enable();
            this.submitting = false;
          }
        );
      }else{
        //upload using uploaderService
        this.postService
        .saveFormDataToUploader(this.formData, post.selectedAppID)
      }
      
    }

  }
}
