
import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DataService} from '../../../shared';

@Component({
  selector: 'app-root',
  templateUrl: './string.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StringUpsertComponent implements  OnInit {
  @Input() stringContent;
  stringForm: FormGroup;
  file;
  imagePreview;
  isSubmited: Boolean;
  formData: FormData;
  isSubmitting: boolean;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private dataService: DataService
  ) {

  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    const text = this.stringForm.value;
    console.log(text);
    if(!text._id){
      this.formData = new FormData();
      this.stringForm.disable();
      this.formData.append('data', text.data);
      this.formData.append('type', 'string');


      this.dataService.
      saveWithFormData(this.formData)
        .subscribe(
          (data)=>{
            this.activeModal.close('string:updated');
          },
          (err)=>{  this.stringForm.enable();}
        )
    } else {
      text['type'] = 'string';
      this.dataService
        .save(text)
        .subscribe(
          (data)=>{
            this.activeModal.close('string:updated');
          },
          (err)=>{  this.stringForm.enable();}
        )
    }
  }
  ngOnInit() {
    this.stringForm = this.formBuilder.group({
      '_id': [this.stringContent._id],
      'data': [this.stringContent.data, Validators.required]
    });

  }
}
