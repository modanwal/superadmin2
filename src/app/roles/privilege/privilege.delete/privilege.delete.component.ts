
import { Component, Input } from '@angular/core';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { PrivilegeModel } from '../../../shared';
import { PrivilegeService } from '../../../shared/services';
import { Router } from '@angular/router';

@Component({
  selector: 'privilige-delete-content',
  templateUrl: './privilege.delete.component.html'
})
export class PrivilegeDeleteComponent {


 @Input() privilege: PrivilegeModel;


  constructor( public activeModal: NgbActiveModal,
               private privilegeService: PrivilegeService,
               private router: Router, ) {}
  deletePrivilege(privilege: PrivilegeModel) {
    
       this.privilegeService.destroy(this.privilege._id)
      .subscribe(
        success => {
          this.activeModal.close('privilege:updated');
        }
      );
  }

}
