import { NgModule } from '@angular/core';
import { SharedModule } from './../../../shared';
import { P3Component } from './p3.component';
import { P3ListComponent } from './p3.list/p3.list.component';
import { P3UpsertComponent } from './p3.upsert/p3.upsert.component';
import { P3DeleteComponent } from './p3.delete/p3.delete.component';
import { P3P1ListComponent } from './p3p1.list/p3p1.list.component';
import { P3P1UpsertComponent } from './p3p1.upsert/p3p1.upsert.component';
import { P3P1DeleteComponent } from './p3p1.delete/p3p1.delete.component';
import { P3Resolver } from './p3.resolver';
import { P3P1Resolver } from './p3p1.list/p3p1.resolver';


export const p3Routing = [
    {
        path: 'p3',
        component: P3Component,
        resolve: {
          data: P3Resolver
        },
        children: [
            {
                path: '',
                component: P3ListComponent,
            },
            {
            path: 'p3p1/:id',
            component: P3P1ListComponent,
            resolve: { data: P3P1Resolver}
            }
        ]
    }
];

@NgModule({
  imports: [
      SharedModule
  ],
  declarations: [
      P3ListComponent,
      P3P1ListComponent,
      P3Component,
      P3UpsertComponent,
      P3DeleteComponent,
      P3P1UpsertComponent,
      P3P1DeleteComponent
    ],
  providers: [
      P3Resolver,
      P3P1Resolver
  ],

  entryComponents: [
    P3P1DeleteComponent,
    P3UpsertComponent,
    P3DeleteComponent,
    P3P1UpsertComponent
  ]

})
export class P3Module {}
