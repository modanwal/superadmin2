import { NgModule } from '@angular/core';
import {AuthGuard, SharedModule} from '../../shared';
import {StringResolver} from './string.resolver';
import {StringComponent} from './string.component';
import {StringUpsertComponent} from './string.upsert/string.upsert.component';
import {StringDeleteComponent} from './string.delete/string.delete.component';


export const stringRouting = [
      {
        path: 'string',
        component: StringComponent,
        canActivate: [AuthGuard],
        resolve: {
          data: StringResolver
        }
      }
];
@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    StringComponent, StringDeleteComponent, StringUpsertComponent
  ],
  providers: [
    AuthGuard, StringResolver
  ],
  entryComponents: [StringUpsertComponent, StringDeleteComponent ]
})
export class StringModule {}
