import {
  Directive,
  Input,
  OnInit,
  Renderer2,
  ElementRef ,
  AfterContentInit,
  AfterViewInit
} from '@angular/core';

declare var jQuery;

@Directive({ selector: '[fullScreen]' })
export class FullScreenDirective implements AfterViewInit {
  privileges: any;
  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {
  }
  @Input('container') container: string;
  containerEl: any;
  currentEl: any
  ngAfterViewInit(){
    if(typeof window != undefined){
      this.containerEl = jQuery(this.container);
      this.currentEl = jQuery(this.el.nativeElement);
      this.addListener();
    }
  }
  addListener(){
    this.el.nativeElement.addEventListener('click',(e)=>{
      e.preventDefault();
      if(!this.containerEl.hasClass('full-screen')){
        this.switchToFullScreen();
      }else{
        this.swtichToSmallWindow();
      }
    })
  }
  switchToFullScreen(){
    this.containerEl.addClass('full-screen');
    this.currentEl.addClass('maximize');
  }
  swtichToSmallWindow(){
    this.containerEl.removeClass('full-screen');
    this.currentEl.removeClass('maximize');
  }
}
