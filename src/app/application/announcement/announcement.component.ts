import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subject} from "rxjs/Subject";
import { SocketService, EntityListConfig, AnnouncementService, InfiniteScrollService, ModalService} from '../../shared';
import {AnnouncementUpsertComponent} from "./announcement.upsert/announcement.upsert.component";
import {AnnouncementModel} from '../../shared/models/announcement.model';


@Component({
  selector: 'app-root',
  templateUrl: './announcement.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AnnouncementComponent implements OnInit, OnDestroy{
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  announcements: AnnouncementModel[];
  appID: any;
  roles: any;
  lastFetchedItems: any;
  b: boolean;
  announcementsObj; any;
  constructor(  private route: ActivatedRoute,
                private router: Router,
                private modalService: ModalService,
                private socketService: SocketService,
                public entityListConfig: EntityListConfig,
                private announcementService: AnnouncementService,
                public infiniteScrollService: InfiniteScrollService) {

    // /*** websockets */
    // this.socketService.subscribeToEvent(`announcement:save`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllAnnouncementForSockets())
    // this.socketService.subscribeToEvent(`announcement:remove`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data =>  this.getAllAnnouncementForSockets())
    // /*** websockets */
   this.announcementsObj = {};
    /*** websockets */
    this.socketService.subscribeToEvent(`announcement:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.savedFromSocket(data, this.announcements,this.announcementsObj, this.entityListConfig);
      })
    this.socketService.subscribeToEvent(`announcement:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.removedFromSocket(data, this.announcements,this.announcementsObj,this.entityListConfig);
      })
    /*** websockets */
  }

  getAllAnnouncement(refreshData){
    this.announcementService
      .getAll(this.appID, this.entityListConfig)
      .subscribe(
        (data) => {
          this.infiniteScrollService.getFromApi(data, this.announcements, this.announcementsObj, this.entityListConfig, refreshData);
          // if(refreshData) this.announcements = [];
          // // remove last page entries if data less than ideal size
          // if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
          //   this.announcements.splice(
          //     (this.entityListConfig.params.pageNumber)
          //     *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          // }
          // this.lastFetchedItems = data.length;
          // if(data.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // // push new data
          // this.announcements = [...this.announcements, ...data];
        }
      );
  }

  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
    setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllAnnouncement(true);
  }
  onScroll(){
    this.getAllAnnouncement(false);
  }
  /*** sorting & pagination */

  // getAllAnnouncementForSockets() {
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber + 1) * (this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.announcementService
  //     .getAll(entityListConfig)
  //     .subscribe(
  //       (data) => {
  //         this.announcements = data;
  //         this.lastFetchedItems = (data.length) % (this.entityListConfig.params.pageSize);
  //         this.entityListConfig.params.pageNumber = Math.ceil(data.length / this.entityListConfig.params.pageSize);
  //         if (this.entityListConfig.params.pageNumber != 0) this.entityListConfig.params.pageNumber--;
  //       }
  //     );
  // }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data =>{
          this.appID = data[0];
          this.announcements = data[1];
        this.infiniteScrollService.computeEntity(this.announcements,this.announcementsObj, this.entityListConfig);
          this.roles =data[2];
        }
      );
        }

  getRoleReceiver(allRoleID: any){
    let role = [];
    for(let item of allRoleID)
    { role.push(item.name); }
    return role;
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  addAnnouncement () {
    const modalRef = this.modalService.open(AnnouncementUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.announcement = new AnnouncementModel;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.roles = this.roles;
  }

}
