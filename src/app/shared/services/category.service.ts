import { Injectable, } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CategoryModel } from './../models/category.model';
import { EntityListConfig } from './../models/entity-list-config.model'
import { ApiService } from './api.service';


@Injectable()
export class CategoryService {
  constructor (
    private apiService: ApiService
  ) {}

  getAll(parentID, entityListConfig: EntityListConfig,  isSocket?): Observable<CategoryModel[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
    .forEach((key) => {
      query.set(key, entityListConfig.query[key]);
    });

    if (parentID){
      return this.apiService.get(`/category/${pageNumber}/${pageSize}/${parentID}`, query, isSocket);
    }
    else{
      return this.apiService.get(`/category/${pageNumber}/${pageSize}/`, query, isSocket);
    }

  }
  getAllCategory(entityListConfig: EntityListConfig,  isSocket?): Observable<CategoryModel[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
      return this.apiService.get(`/category/${pageNumber}/${pageSize}/`, query, isSocket);
  }

  get(_id, isSocket?): Observable<CategoryModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/category/${_id}`, query, isSocket);
  }
  destroy(_id): Observable<any>{
    return this.apiService.delete(`/category/${_id}`);
  }
  saveFormData(categoryFormData, categoryID): Observable<CategoryModel> {
    // If we're updating an existing category
    if (categoryID) {
      console.log(delete categoryFormData['parentID']);
      return this.apiService.putWithFormData('/category/' + categoryID, categoryFormData)
        .map(data => data.category);

      // Otherwise, create a new category
    } else {
      return this.apiService.postWithFormData('/category', categoryFormData)
        .map(data => data.category);
    }
  }
}
