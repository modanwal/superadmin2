import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class CheckoutService {
  constructor(private apiService: ApiService) {
  }
  save(checkout): Observable<any> {
    // If we're updating an existing component
    return this.apiService.post(`/checkout`, checkout);

  }
}
