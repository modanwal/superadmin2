import { Router } from '@angular/router';
import { Component, ViewEncapsulation, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {AppService} from "../../shared/services/app.service";
import {CollaboratorService} from "../../shared/services/collaborator.service";

@Component({
  selector: 'collaborator-delete-modal',
  templateUrl: './collaborator.delete.component.html'
})

export class CollaboratorDeleteComponent {
  isSubmited: boolean;
 @Input() collaborator;
  @Input() app;
  constructor(public activeModal: NgbActiveModal,  private collaboratorService: CollaboratorService) {
    this.isSubmited = false;
  }

  deleteCollaborator(app, collaborator) {
    this.isSubmited = true;
    this.collaboratorService
      .destroyCollaborator(app._id, collaborator._id)
      .subscribe(
      data => this.activeModal.close('collaborator:deleted')
    )}
}


