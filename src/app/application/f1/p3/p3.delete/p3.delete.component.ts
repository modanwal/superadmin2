
import {Component, Input, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P3Model, P3Service} from '../../../../shared';


@Component({
  selector: 'app-root',
  templateUrl: './p3.delete.component.html',
  encapsulation: ViewEncapsulation.None
})
export class P3DeleteComponent {
  @Input() p3;
  @Input() appID;
  isSubmitting: Boolean;
  constructor(public activeModal: NgbActiveModal, private p3Service: P3Service) {
  }
  delete (p3: P3Model) {
    this.isSubmitting = true;
    this.p3Service.destroy(p3._id, this.appID )
      .subscribe (
        data => {
          this.activeModal.close ('P3_Delete');
          this.isSubmitting = false;
        },
        error => {
          this.isSubmitting = false;
        } 
      );
  }

}
