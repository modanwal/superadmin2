import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { EntityListConfig, P3Service } from './../../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P3P1Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (
               private p3Service: P3Service) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const appID = state['url'].split("/")[2];
    const p3Id = state['url'].split("/")[6];
    const entityListConfig = new EntityListConfig();
    return this.p3Service.getAllp3p1(p3Id, entityListConfig, appID, false).map(
      (p3p1Data) =>{
        let data = {};
        data['appID'] = appID;
        data['p3p1Data'] = p3p1Data;
        return data;
      }
    );
  }
}

