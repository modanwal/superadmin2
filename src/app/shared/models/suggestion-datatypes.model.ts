export class SuggestionDataTypesModel {
  _id: string;
  name: string;
  description: string;
}
