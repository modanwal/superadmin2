

import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import {EntityListConfig, ResourceService, ResourcePrivilegeService, PrivilegeService} from '../../../shared';



@Injectable()
export class ResourcePrivilegeResolver implements Resolve<any> {

  routeData: Subscription;

  constructor ( private router: Router,
                public entityListConfig: EntityListConfig,
                private  resourceService: ResourceService,
                private privilegeService: PrivilegeService,
                private resourcePrivilegeService: ResourcePrivilegeService) {
  }

  resolve (
           route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot
           ): Observable<any> {
    let entityListConfig = new EntityListConfig();
    entityListConfig.params.pageSize = -1;
    const resourceID = state['url'].split('/')[4];
    const obs1 = this.resourceService.get(resourceID);
    const obs2 = this.privilegeService.getAll(entityListConfig);
    const obs3 = this.resourcePrivilegeService.getPrivilegeData(resourceID);
    console.log(obs3);

    return Observable.forkJoin(obs1, obs2, obs3);

  }
}
