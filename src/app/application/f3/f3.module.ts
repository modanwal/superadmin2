import { RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/core';
import { NgModule } from '@angular/core';
import { SharedModule, F3Service } from './../../shared';
import { F3Resolver } from './f3.resolver';
import { F3Component } from '../f3/f3.component';
import { F3UpsertComponent } from '../f3/f3.upsert/f3.upsert.component';
import { F3DeleteComponent } from '../f3/f3.delete/f3.delete.component';
import { F3PrivilegeComponent } from '../f3/f3.privilege/f3.privilege.component';
import { F3PrivilegeResolver } from '../f3/f3.privilege/f3.privilege.resolver';

export const f3Routing = [
  {
        path: 'f3',      
        component: F3Component,
        resolve: {
          data: F3Resolver
        },
        children:[
          {
            path:':id',
            component: F3PrivilegeComponent,
            resolve: {
              data: F3PrivilegeResolver
            }
          }
        ]
  }
  ];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    F3Component, F3UpsertComponent,F3DeleteComponent, F3PrivilegeComponent
  ],
  providers: [
    F3Resolver,
    F3Service,
    F3PrivilegeResolver
  ],
  entryComponents: [
    F3UpsertComponent,
    F3DeleteComponent
  ]
})
export class F3Module {}
