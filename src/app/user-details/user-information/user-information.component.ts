/**
 * Created by rahul on 7/6/2017.
 */

import {Component, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {EntityListConfig} from '../../shared';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component( {
  selector: 'user-information-pages',
  templateUrl: './user-information.component.html'
})
export class UserDetailsInformationComponent implements OnInit {
  userInformation: any;
  userInformationForm: FormGroup;
  lastFetchedItems: number;
  isSubmited: boolean;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private route: ActivatedRoute,
              public entityListConfig: EntityListConfig,
              private formBuilder: FormBuilder) {

  }
  submitForm(){

  }
  // selectFile(event){
  //
  // }
  ngOnInit() {
    this.userInformationForm = this.formBuilder.group({
      '_id': [''],
      'name': ['', Validators.required],
      'email': ['', Validators.required],
      'path': ['', Validators.required],
      'mobile': ['', Validators.required]
    });

    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data => {
        this.userInformation = data;
        this.initializeForm();
        this.lastFetchedItems = this.userInformation.length;
        if (this.userInformation.length == this.entityListConfig.params.pageSize) {
          this.entityListConfig.params.pageNumber++;
        }
      });
  }
  initializeForm(){
    this.userInformationForm.controls['_id'].setValue(this.userInformation._id);
    this.userInformationForm.controls['name'].setValue(this.userInformation.name);
    this.userInformationForm.controls['email'].setValue(this.userInformation.email);
    this.userInformationForm.controls['mobile'].setValue(this.userInformation.mobile);
  }
}
