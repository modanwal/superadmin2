import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { UserService, AppService, EntityListConfig, P4Service, P4F1Service, ConstantService } from './../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P4F1UsersResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router,
               private p4Service: P4Service,
               private p4F1Service: P4F1Service,
                private constantService: ConstantService) {
  }
 ngOnInit(){
 }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      const appID = state['url'].split("/")[2];
      const p4ID = state['url'].split("/")[4];
      const entityListConfig = new EntityListConfig();
      const p4f1Data$ = this.p4F1Service.getAll(entityListConfig,p4ID,appID);
      const p4f1Type$ = this.constantService.get('p4f1/type');
      return Observable.forkJoin(Observable.of(appID),Observable.of(p4ID),p4f1Type$,p4f1Data$)
    //   return  this.p4F1Service.getAll(this.entityListConfig,p4ID,appID).map((p4f1Data)=>{
    //     let data = {};
    //     data['appID'] = appID;
    //     data['p4ID'] = p4ID;
    //     data['p4f1Data'] = p4f1Data;
    //     return data;
    //   });
    
  }
}

