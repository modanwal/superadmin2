import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { AppService, EntityListConfig, ConstantService } from '../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class AllAppsResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (
               private router: Router,
               private appService: AppService,
               private constantService: ConstantService) {
  }
 ngOnInit(){

 }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
             const entityListConfig = new EntityListConfig();
        const obs1 =  this.appService.getAll(entityListConfig);
        const obs2 =  this.constantService.getAllConstantAppStatus(entityListConfig);
    return Observable.forkJoin(obs1, obs2);
  }
}

