import { Component, OnDestroy, OnInit, } from '@angular/core';
import { AddressModel, User } from '../shared';
import { BreadCrumbService } from './../shared/breadcrumb.module';
import { ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs/Subject";
import {EntityListConfig} from '../shared';


@Component( {
  selector: 'user-detail-pages',
  templateUrl: './user-details.component.html',
  providers:[EntityListConfig]
})
export class UserDetailsComponent implements OnInit, OnDestroy {
  id: any;
  user: User;
  applicationStatus: any;
  addressa: AddressModel[];
   private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor (private route: ActivatedRoute) {
    this.id = this.route.snapshot.params.id;

    this.route.data
        .map( data=> data.user)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(user=> this.user = user);

  }
  ngOnInit(){

  }
  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.unsubscribe();
  }
  setDefaultRole(event, userData){
    // console.log(event);
    // console.log('userdata',this.userData);
    // console.log(event.target.value)
    // const roleID = event.target.value;
    // let data = {};
    // data['roleID'] = roleID;
    // this.f2Service
    //   .saveRole(data, this.appID, userData._id)
    //   .subscribe(data =>{
    //     //    this.role = data;
    //   });
  }

}
