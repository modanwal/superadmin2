import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { F3Model, F3Service, SocketService, RoleModel, RoleResourcePrivilegeService } from './../../../shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import {TemplateResourcePrivilegeService} from "../../../shared/services/template-resource-privilege.service";


@Component({
  selector: 'f3-privilege-component',
  templateUrl: './f3.privilege.component.html',
  encapsulation: ViewEncapsulation.None
})
export class F3PrivilegeComponent implements OnInit {
  appID: any;
  roleID: any;
  templateResourcePrivilegeData: any;
  templateResourcePrivileges: any;
  roleResourcePrivilegeData: any;
  constructor(private modalService: NgbModal,
              private route: ActivatedRoute,
              private templateResourcePrivilegeService: TemplateResourcePrivilegeService,
              private roleResourcePrivilegeService: RoleResourcePrivilegeService) {
  }

  ngOnInit() {
      this.route.data
        .map(data => data.data)
        .subscribe(
          (data) => {
              this.roleID = data[0];
              console.log(this.roleID);
              this.templateResourcePrivileges = data[1];
              console.log(this.templateResourcePrivileges, 'this one is template role');
              // this.roleResourcePrivilegeData = data[2];
              // console.log(this.roleResourcePrivilegeData, '////////////////');
          }
        )
    }
    getAllRoleResourcePrivilege(){
      this.templateResourcePrivilegeService
        .getAppResourcePrivilege(this.roleID,false)
        .subscribe(
          (data)=>{
            this.templateResourcePrivileges = data;
            console.log(this.templateResourcePrivilegeData, 'this line no 46');
          });
    }
    checkboxClick(event,resourceID,privilegeID){
        event.preventDefault();
        event.target.disabled = true;
        if(event.target.checked){
        let data = {
            roleID: this.roleID,
            resourceID: resourceID,
            privilegeID: privilegeID
        };
        this.roleResourcePrivilegeService
            .save(data)
            .subscribe(
                (data)=>{
                event.target.checked = true;
                event.target.disabled = false;
                  this.getAllRoleResourcePrivilege()
                },
                (error) =>{event.target.disabled = false;}
            )
        }else{
        this.roleResourcePrivilegeService
            .destroy(this.roleID,resourceID,privilegeID)
            .subscribe(
                (data)=>{
                event.target.checked = false;
                event.target.disabled = false;
                this.getAllRoleResourcePrivilege();
                },
                (error) =>{event.target.disabled = false;}
            );
        }
    }
  selectCheckboxClick(event, templateResourceID){
      if(event.target.checked){
        let data = {
          roleID: this.roleID,
          resourceID: templateResourceID
        };
        console.log(data);
        this.roleResourcePrivilegeService
          .selectAllCheckBoxs(data)
          .subscribe(
            (value)=>{
              this.getAllRoleResourcePrivilege();
              console.log(value);
            });
      }else {
        this.roleResourcePrivilegeService
          .destroyRole(this.roleID , templateResourceID)
          .subscribe(
            (data)=>{
              this.getAllRoleResourcePrivilege();
            });
      }
  }
  selectAllStatus(privileges){
    var count = 0;
    var status = privileges.length;
    for(var privilege of privileges){
      if(privilege.checked){
        count++;
      }
    }
    if(count === status){
      return true;
    } else {
      return false;
    }
  }


}
