import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class P2Service {
  constructor (private apiService: ApiService) {}

  getAllp2( entityListConfig:  EntityListConfig, appId,  isSocket?): Observable<any[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/p2/app/${appId}/${pageNumber}/${pageSize}`, query, isSocket);
  }
  /** p2p1 getting**/
  getAllp2p1( p2_id, entityListConfig:  EntityListConfig, appID, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/p2/${p2_id}/app/${appID}/${pageNumber}/${pageSize}`, query, isSocket);

    // return this.apiService.get(`/p2/p1/app/${appID}/p2/${p2_id}/${pageNumber}/${pageSize}`, query, isSocket);
  }
  destroy(_id, appID): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/p2/${_id}/app/${appID}/` );
  }
  destroyp2p1(P2P1ID, appID, p2ID): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/p2/${p2ID}/p2p1/${P2P1ID}/app/${appID}/` );
  }
  saveP2(p2, appID): Observable<any> {
    // If we're updating an existing entity
    if (p2._id) {
      const p2Id = p2._id;
      delete p2._id;
      return this.apiService.put(`/p2/${p2Id}/app/${appID}/` , p2);
      // Otherwise, create a new entity
    } else {
      return this.apiService.post(`/p2/app/${appID}/`, p2);
    }
  }
  saveFormData(p2p1FormData, p2p1, p2Id, appID): Observable<any> {
    // If we're updating an existing KeyPeople
    if (p2p1._id) {
      return this.apiService.putWithFormData(`/p2/${p2Id}/p2p1/${p2p1._id}/app/${appID}` , p2p1FormData);

      // Otherwise, create a new People
    } else {
      return this.apiService.postWithFormData(`/p2/${p2Id}/app/${appID}`, p2p1FormData);

    }
  }
}
