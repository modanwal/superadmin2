import {Component, OnInit} from '@angular/core';
import {EntityListConfig, AppService} from '../shared';
import {BreadCrumbService} from '../shared/breadcrumb.module';
import {Subject} from "rxjs/Subject";
import {ActivatedRoute} from "@angular/router";



@Component( {
  selector: 'workbench-page',
  templateUrl: './workbench.component.html',
  providers: [EntityListConfig]
})
export class WorkbenchComponent implements OnInit{
  apps: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  lastFetchedItems: any;
  workBenchCount: any;
  constructor (private entityListConfig: EntityListConfig,
               private appService: AppService,
               private breadCrumbService: BreadCrumbService,
               private route: ActivatedRoute,) {
   this.lastFetchedItems = 0;

  }
  ngOnInit() {
    this.breadCrumbService.pushBreadCrumbItem({url: '/workbench', label: 'workbench'}, true);
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        console.log(data);
        this.workBenchCount = data.data[0];
        this.apps = data.data[1];
        console.log(this.workBenchCount, 'workBench');
        // this.lastFetchedItems = this.apps.length;
        // if(this.apps.length==this.entityListConfig.params.pageSize){
        //   this.entityListConfig.params.pageNumber++;
        // }
      } );
  }
  ItemDropped(event, status){
    const app = {
        [event.dragData.type] : status
    }
    this.appService
      .changeAppStatusFromWorkBench(event.dragData._id, app)
      .subscribe(
        (data)=>{
          console.log(data);
        },
        (alert) => { console.log('sussessfully updated');}
        );
  }
  getActiveRoute(route: string){
    return true;
    // if(route  === 'REQUESTED'){
    //   return true;
    // }else{
    //   if(this.url.indexOf(route) != -1){
    //     return true;
    //   }else{
    //     return false;
    //   }
    // }
  }
  onScroll() {
    console.log('scrolled');
  }
  getWorkBenchStatus(event){
   console.log(event);
   console.log(event.dragData, 'here dragdata is working');
  }
  getAllApps(status: string){
   // this.router.navigate(['workbench'], { queryParams: { id: status } });
    delete this.entityListConfig.query.sortBy;
    delete this.entityListConfig.query.order;
    this.entityListConfig.query['status'] = status;
    this.appService
      .getAllAppsByStatus(this.entityListConfig)
      .subscribe(
        (data)=> {
          this.apps = data;
          console.log(this.apps, 'this is like');
        });
  }
}
