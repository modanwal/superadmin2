import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceModel, ResourceService, EntityListConfig, SocketService, InfiniteScrollService } from '../../shared';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ResourcesUpsertComponent } from './resources.upsert/resources.upsert.component';
import { ResourcesDeleteComponent } from './resources.delete/resources.delete.component';
import { Subscription } from 'rxjs/Subscription';
import { BreadCrumbService } from "./../../shared/breadcrumb.module";
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  providers: [EntityListConfig]
})

export class ResourcesComponent implements OnInit {
  lastFetchedItems: number;
  resources: ResourceModel[];
  fetchingData: boolean;
  collectionDirty: boolean;
  resourcesFetchSub: Subscription;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  resourcesObj: any;
  dataResolved: Boolean;
  constructor(private modalService: NgbModal,
              private resourceService: ResourceService,
              private router: Router,
              private route: ActivatedRoute,
              public entityListConfig: EntityListConfig,
              private breadCrumbService: BreadCrumbService,
              private socketService: SocketService,
              public infiniteScrollService: InfiniteScrollService) {
    this.collectionDirty = false;
    this.resources = [];
    this.resourcesObj = {};
    this.lastFetchedItems = 0;
    this.dataResolved = false;

    // /*** websockets */
    // this.socketService.subscribeToEvent(`resource:save`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllresourcesForSockets());
    //
    // this.socketService.subscribeToEvent(`resource:remove`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllresourcesForSockets());
    // /*** websockets */

    /*** websockets */
    this.socketService.subscribeToEvent(`resource:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.resources,this.resourcesObj, this.entityListConfig);
        if(this.fetchingData){
          this.resourcesFetchSub.unsubscribe();
          this.getAllResources(false);
        }
      })
    this.socketService.subscribeToEvent(`resource:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.resources,this.resourcesObj,this.entityListConfig);
        if(this.fetchingData){
          this.resourcesFetchSub.unsubscribe();
          this.getAllResources(false);
        }
      })
    /*** websockets */
  }
  // getAllresourcesForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber + 1) * (this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.resourceService
  //     .getAll(this.entityListConfig, true)
  //     .subscribe(data => {
  //       this.resources = data;
  //       this.lastFetchedItems = (data.length) % (this.entityListConfig.params.pageSize);
  //       this.entityListConfig.params.pageNumber = Math.ceil(data.length / this.entityListConfig.params.pageSize);
  //       if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //     });
  //
  // }
  getAllResources(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
  this.resourcesFetchSub =  this.resourceService.getAll(this.entityListConfig)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllResources(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.resources, this.resourcesObj, this.entityListConfig, refreshData);
          }

          //  this.resources = data
          //   if(refreshData) this.resources = [];
          //   // remove last page entries if data less than ideal size
          //   if(this.lastFetchedItems < this.entityListConfig.params.pageSize){
          //     this.resources.splice(
          //       (this.entityListConfig.params.pageNumber)
          //       *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          //   }
          //   this.lastFetchedItems = data.length;
          //   if(data.length == this.entityListConfig.params.pageSize){
          //     this.entityListConfig.params.pageNumber++;
          //   }
          //   // push new data
          //   this.resources = [...this.resources, ...data];
          // });
        }),(error)=>{this.fetchingData = true;};
  }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if (this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllResources(true);
  }
  onScroll(){
  this.getAllResources(false);
  }
  /*** sorting & pagination */
  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        (val) => {
          this.resources = val['data'];
          this.infiniteScrollService.computeEntity(this.resources,this.resourcesObj, this.entityListConfig);
          // this.lastFetchedItems = this.resources.length;
          // if(this.resources.length == this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
        });
    this.breadCrumbService
        .pushBreadCrumbItem({url:'/resources',label:'Resources'},true);
  }

  setEntityConfig(event){
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllResources(true);
  }
  addResource() {
    // this.selectComponentEmitter.emit(priviliges);
    const modalRef = this.modalService.open(ResourcesUpsertComponent);
    modalRef.componentInstance.resource = new ResourceModel;
   //  modalRef.result.then((closeReason) => {
   //   if (closeReason == 'resource:updated'){
   //  //   this.getAllResources();
   //
   //   }
   // });
  }
  editResource(resource: ResourceModel) {
    const modalRef = this.modalService.open(ResourcesUpsertComponent);
    modalRef.componentInstance.resource = resource;
   //  modalRef.result.then((closeReason) => {
   //   if (closeReason == 'resource:updated'){
   //     this.getAllResources(true);
   //   }
   // });
  }
  editResourcePrivileges(resource: ResourceModel){
    this.breadCrumbService.pushBreadCrumbItem({url:'/roles/resources/privilege',params:resource._id,label:resource.name},false);
    this.router.navigate(['/roles/resources/privilege', resource._id]);
  }
  deleteResource(resource: ResourceModel) {
    const modalRef = this.modalService.open(ResourcesDeleteComponent );
    modalRef.componentInstance.resource = resource;
    //modalRef.result.then((closeReason) => {
    //  if (closeReason == 'resource:updated'){
    //    this.getAllResources();
    //  }
    //});
  }
}
