import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class DataService {
  constructor(private apiService: ApiService) {
  }
  getAll(entityListConfig: EntityListConfig, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/data/${pageNumber}/${pageSize}`, query, isSocket);
  }
  saveWithFormData(formData): Observable<any[]> {

    return this.apiService.postWithFormData(`/data`, formData);
  }
  getAllComponent( categoryID, entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    // return this.apiService.get(`/category-component/${categoryID}/${pageNumber}/${pageSize}`, query, isSocket);
    return this.apiService.get(`/category/${categoryID}/component`, query, isSocket);

  }
  getAllLabel( dataID, categoryID,componentID, entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/data/${dataID}/category/${categoryID}/component/${componentID}/label`, query, isSocket);

  }
  save(data): Observable< any[]> {
    const Id = data._id;
    delete data._id;
    return this.apiService.put(`/data/${Id}`, data);
  }
  destroy(dataID): Observable<any []>{

    return this.apiService.delete(`/data/${dataID}`);
  }
}
