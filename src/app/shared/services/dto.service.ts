
import {Injectable} from "@angular/core";

@Injectable()
export class DTOService {
  private data: any;

  constructor(){
        this.data = {
          createAppEmail: '',
          p2p1PageName: '',
          p3p1PageName: '',
          createPrivilegeEmail: '',
          labelType: '',
          categoryID: '',
          componentID: '',
          amount: '',
          gatewayID: '',
          invoiceDate: '',
          invoiceNo: '',
          path: '',
          imagePath: '',
        };
        if(window) {
          this.data = JSON.parse(window.sessionStorage.getItem('DTOObject'))
                      ? JSON.parse(window.sessionStorage.getItem('DTOObject')) : this.data;
        }
  }
  public setValue(key,value){
    this.data[key] = value;
    this.saveDTOtoLocalStorage();
  }
  public getValue(key): string{
    return this.data[key];
  }
  saveDTOtoLocalStorage(){
    if(window){
      window.localStorage.setItem('DTOObject',JSON.stringify(this.data))
    }
  }

}
