import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule, AuthGuard} from '../shared';
import { F5Module , f5Routing } from './f5/f5.module';
import { F4Module, f4Routing } from './f4/f4.module';
import { ApplicationComponent } from './application.component';
import { F1Module, f1Routing } from './f1/f1.module';
import { LiveStreamingModule, livestreamingRouting } from './live-streaming/live-streaming.module';
import { p4f1Routing, P4f1Module } from './p4f1/p4f1.module';
import { f3Routing, F3Module } from './f3/f3.module';
import { F2Module, f2Routing} from './f2/f2.module';
import { walletRouting, WalletModule } from './wallet/wallet.module';
import { ApplicationResolver } from "./application.resolver";
import {AnnouncementModule, announcementRouting} from "./announcement/announcement.module";
import {InviteModule, inviteRouting} from './invite/invite.module';




export const appRouting = [
  {
    path: 'application/:id',
    canActivate: [AuthGuard],
    resolve: { data: ApplicationResolver },
    component: ApplicationComponent,
    children: [
      ...f4Routing,
      ...f5Routing,
      ...f1Routing,
      ...livestreamingRouting,
      ...p4f1Routing,
      ...f3Routing,
      ...f2Routing,
       ...announcementRouting,
      ...walletRouting,
      ...inviteRouting
    ]}];

@NgModule({
  imports: [
    SharedModule,
    F5Module,
    F4Module,
    F1Module,
    LiveStreamingModule,
    P4f1Module,
    F3Module, AnnouncementModule,
    F2Module,
    WalletModule, InviteModule
  ],
  declarations: [
   ApplicationComponent,
  ],
  providers: [
   ApplicationResolver
  ]
})
export class ApplicationModule {}
