import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {DataService} from '../../../shared';
@Component({
  selector: 'string-delete',
  templateUrl: './string.delete.component.html'
})

export class StringDeleteComponent {
  @Input() stringContent;
  isSubmited: Boolean;
  constructor( public activeModal: NgbActiveModal ,
               private dataService: DataService
  ) {}
  deleteFile(stringContent: any) {
    this.dataService.destroy(stringContent._id)
      .subscribe(
        (data)=>{
          this.activeModal.close('string:remove');
        }
      );
  }
}
