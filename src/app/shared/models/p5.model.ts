export class P5Model {
  _id: string;
  title: string;
  description: string;
  path: string;
}
