import {Component, Output} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DTOService, } from "../../shared";
import {CompleterData, CompleterService} from "ng2-completer";
import {environment} from "../../../environments/environment";



@Component({
  selector: 'allapps-user-modal',
  templateUrl: './all-apps.upsert.component.html'
})
export class AllAppsUsersComponent{
  @Output() appUser;
  isSubmited: Boolean;
   dataService: CompleterData;
  users: any[];
   searchStr: string;
  constructor( public activeModal: NgbActiveModal,
               private completerService: CompleterService,
               private dtoService: DTOService

  ) {
    this.isSubmited = false;
    this.dataService = completerService.remote(`${environment.api_url}/user?email=`, 'email', 'email');
  }

  setDefaultUser(event){
    const searchData = event.target.value;
    this.dtoService.setValue('createAppEmail', this.searchStr);
  }
//   setDefaultUser(event){
//     const searchData = event.target.value;
//     this.appUser= searchData;
//     console.log(event.target.value);
//     this.userService
//       .getUser(new EntityListConfig, searchData)
//       .subscribe(
//         (data) => {
//           this.users = data;
//           this.dataService = this.completerService.local(this.users, 'email', 'email');
//         });
//     if(this.searchStr){
//
//       this.dtoService.setValue('createAppEmail',this.searchStr);
//   }
// }
  closeModal(){
    this.activeModal.close('email:save');
  }

  submitForm() {
    // this.isSubmited = true;
    // if (this.appForm.valid) {
    //   this.submitting = true;
    //   this.appForm.disable();
    //   const apps = this.appForm.value;
    //   console.log('submit form', apps);
    //   this.appService
    //     .save(apps, this.app.owner._id)
    //     .subscribe(
    //       data => {
    //         this.appForm.enable();
    //         this.activeModal.close('app:save');
    //       },
    //       error => {
    //         this.appForm.enable();
    //       }
    //     );
    // }
  }
}
