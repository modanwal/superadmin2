import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {P2Model, P2P1Model, P2Service , EntityListConfig, DTOService, SocketService, InfiniteScrollService, ModalService} from './../../../../shared';
import {P2UpsertComponent} from './../p2.upsert/p2.upsert.component';
import {P2DeleteComponent} from './../p2.delete/p2.delete.component';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subject} from "rxjs/Subject";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-page',
  templateUrl: './p2.list.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]

})
export class P2ListComponent implements OnInit{
  fetchingData: boolean;
  collectionDirty: boolean;
  p2FetchSub: any;
  nextComponent: any;
  pageName: any;
  selectedComponent: any;
  p2p1: P2P1Model[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  p2: P2Model[];
  p2Id: string;
  appID: any;
  lastFetchedItems: any;
  p2Obj: any;
  constructor(
              private dtoService: DTOService,
              private _sanitizer: DomSanitizer,
              private p2Service: P2Service,
              public entityListConfig: EntityListConfig,
              private modalService: ModalService,
              private route: ActivatedRoute,
              private router: Router,
              private socketService: SocketService,
              public infiniteScrollService: InfiniteScrollService,
    ) {
    this.collectionDirty = false;
    this.lastFetchedItems = 0;
    this.p2Obj = {};
    // /*** websockets */
    // this.socketService.subscribeToEvent(`p2:save`)
    //   .subscribe(data => this.getAllP2ForSockets() )
    //
    // this.socketService.subscribeToEvent(`p2:remove`)
    //   .subscribe(data => this.getAllP2ForSockets() )
    // /*** websockets */

    /*** websockets */
    this.socketService.subscribeToEvent(`p2:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.p2,this.p2Obj, this.entityListConfig);
        if(this.fetchingData){
          this.p2FetchSub.unsubscribe();
          this.getAllP2(false);
        }
      })
    this.socketService.subscribeToEvent(`p2:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.p2,this.p2Obj,this.entityListConfig);
        if(this.fetchingData){
          this.p2FetchSub.unsubscribe();
          this.getAllP2(false);
        }
      })
    /*** websockets */

  }

  ngOnInit() {
    this.route.parent.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {
          this.appID = data.appID;
          this.pageName = data.selectedComponent.pageName;
          this.p2 = data.p2Data;
          this.infiniteScrollService.computeEntity(this.p2,this.p2Obj, this.entityListConfig);
          this.nextComponent = data.nextComponent;
        }
      );
  }
  showp2p1(p2data: P2P1Model) {
    this.dtoService.setValue('p2p1PageName', p2data.name);
    this.p2Id = p2data._id;
    this.router.navigateByUrl(`/application/${this.appID}/f1/p2/p2p1/${p2data._id}`);
  //  this.p2Service.p2Id = p2data._id;
  }
  // getAllP2ForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.p2Service
  //     .getAllp2(entityListConfig, this.appID, true)
  //     .subscribe(data =>{
  //       this.p2 = data;
  //       this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //       this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //       if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //     });
  // }
  getAllP2(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
this.p2FetchSub =     this.p2Service
      .getAllp2(this.entityListConfig, this.appID)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllP2(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.p2, this.p2Obj, this.entityListConfig, refreshData);
          }

          // if(refreshData) this.p2 = [];
          // // remove last page entries if data less than ideal size
          // if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
          //   this.p2.splice(
          //     (this.entityListConfig.params.pageNumber)
          //     *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          // }
          // this.lastFetchedItems = data.length;
          // if(data.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // // push new data
          // this.p2 = [...this.p2, ...data];
        }
      ),(error)=>{this.fetchingData = true;};
  }
  onScroll() {
   this.getAllP2(false);
  }
  // ****P2***
  editP2 (p2data: P2Model ) {
    const modalRef = this.modalService.open(P2UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p2 = p2data;
    modalRef.componentInstance.appID = this.appID;
    // modalRef.result.then((closeReason) => {
    //   if (closeReason == 'P2_Updated') {
    //     const defaultEntity = new EntityListConfig();
    //     this.entityListConfig.query = defaultEntity.query;
    //     this.getAllP2();
    //   }
    // });
  }
  getBackground(image){
      return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }
  addP2() {
    const modalRef =  this.modalService.open(P2UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p2 = new P2Model;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.pageName = this.pageName;
    // modalRef.result.then((closeReason) => {
    //   if (closeReason == 'P2_Updated') {
    //     const defaultEntity = new EntityListConfig();
    //     this.entityListConfig.query = defaultEntity.query;
    //     this.getAllP2();
    //   }
    // });
  }
   deleteP2(p2data: P2Model) {
    const modalRef = this.modalService.open(P2DeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p2 = p2data;
     modalRef.componentInstance.appID = this.appID;
     // modalRef.result.then((closeReason) => {
     //   if (closeReason == 'P2_Delete') {
     //     const defaultEntity = new EntityListConfig();
     //     this.entityListConfig.query = defaultEntity.query;
     //     this.getAllP2();
     //   }
     // });
  }


}
