import { Component, ElementRef, NgZone, Host, Directive, ModuleWithProviders, NgModule, OnInit, Input } from '@angular/core';
import { CommonModule } from "@angular/common";
declare var jQuery;
declare var RainyDay;

@Component({
  selector: 'system-ideal',
  template: `<ng-content></ng-content>`
})

export class SystemIdealComponent implements OnInit{
  ngOnInit() {
    this.zone.runOutsideAngular(() => {
        if(typeof window !=undefined){
            console.log('n')
        }
    });
  }
  initAnimation(){
    var engine = new RainyDay({
        image: this,
        parentElement: this.el.nativeElement,
        width: '100%',
        height: '100%',
        crop: [0, 0, '100%', '100%']
    });
    engine.rain([ [1, 2, 8000] ]);
    engine.rain([ [3, 3, 0.88], [5, 5, 0.9], [6, 2, 1] ], 100);
  }
  constructor(private el: ElementRef, private zone: NgZone) {
    
  }

  $carousel: any;

  initialized = false;

}


// @Directive({
//   selector: '[slick-carousel-item]',
// })
// export class SlickCarouselItem {
//   constructor(private el: ElementRef, @Host() private carousel: SlickCarouselComponent) {
//   }
//   ngAfterViewInit() {
//     if(typeof window !=undefined){
//         this.carousel.addSlide(this);
//     }
//   }
//   ngOnDestroy() {
//     if(typeof window !=undefined){
//         this.carousel.removeSlide(this);
//     }
//   }
// }

@NgModule({
  imports: [CommonModule],
  exports: [SystemIdealComponent],
  declarations: [SystemIdealComponent]
})
export class SystemIdealModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SystemIdealModule,
      providers: []
    };
  }
}