import { Router } from '@angular/router';
import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
// import { AuthService } from 'ng2-ui-auth';
import { UserService } from '../shared/services';

import {EntityListConfig} from "../shared/models/entity-list-config.model";
import {Observable} from "rxjs/Rx";
//import { EntityListConfig } from "../shared/models/entity-list-config.model";
declare let window;


@Component({
  selector: 'login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers:[EntityListConfig]
})

export class LoginComponent implements OnInit, AfterViewInit {
  userService: UserService;
  config: {};
  responses: any;

  popupWindow: any;
  constructor(
    // private auth: AuthService,

              private router: Router, userService: UserService) {
    this.userService = userService;
  }
  ngAfterViewInit(){
  }

  ngOnInit() {
    this.userService.isAuthenticated.subscribe((authenticated) => {
      if (authenticated) {
        this.router.navigateByUrl('/');
      }
    });
  }

  loginWithGoogle() {
    // this.auth.authenticate('google')
    //   .subscribe({
    //     error: (err: any) => console.log(err),
    //     next: (response: any) => {
    //       this.userService.attemptAuth('google', response.access_token)
    //         .subscribe(
    //           (data) => {
    //             this.responses = this.userService.getContacts(response.access_token);
    //             console.log(this.responses, 'here all contacts printing');
    //             this.router.navigateByUrl('/');
    //           },
    //           (alert) => {
    //             console.log(this.responses);
    //           });
    //     }
    //   });
  }

  loginWithFacebook() {
    // this.auth.authenticate('facebook')
    //   .subscribe({
    //     error: (err) => {
    //       console.log('Some erro came here!', err)
    //     },
    //     next: (response: any) => {
    //       console.log('++++++++++++++++++++++++ACCESSTOKENFACEBOOK',response.access_token);
    //       console.log('some error does');
    //       this.userService.attemptAuth('facebook', response.access_token)
    //         .subscribe(
    //           data => this.router.navigateByUrl('/'),
    //           err => console.log(err)
    //         );
    //     }
    //   });
  }

  assignActity(): void {
    if(typeof window != undefined){
      const that = this;
      if(window.addEventListener){
        window.addEventListener("message",this.listenMessage(that));
      }else{
        window.attachEvent("onmessage",this.listenMessage(that));
      }
      this.popupWindow =  window.open('http://backend.makemeproud.org/auth/google/000000000000000000000001', '_blank', 'width=700,height=500,left=200,top=100');
    }
  }
  listenMessage(context){
    return function(event){
      const cookie = event.data;
      const array = cookie.split(';');
      const tokenArray = array[array.length-1].split('token=');
      console.log('token i got is:',tokenArray[tokenArray.length-1]);
      context.popupWindow.close();
      context.userService.setAuth(tokenArray[tokenArray.length-1])
    }
  }

}
