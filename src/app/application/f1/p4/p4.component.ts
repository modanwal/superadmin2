import {Component, OnInit} from '@angular/core';
import {P4Model} from '../../../shared/models/p4.model';
import {P4DeleteComponent} from './p4.delete/p4.delete.component';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EntityListConfig} from '../../../shared/models/entity-list-config.model';
import {P4Service, SocketService, InfiniteScrollService, ModalService} from '../../../shared';
import {P4UpsertComponent} from './p4.upsert/p4.upsert.component';
import {Subject} from "rxjs/Subject";
import {ActivatedRoute} from "@angular/router";
import {DTOService} from "../../../shared/services/dto.service";
import {DomSanitizer} from "@angular/platform-browser";
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-page',
  templateUrl: './p4.component.html',
  providers: [EntityListConfig]
})

export class P4Component implements OnInit{
  pageName: any;
  selectedComponent: any;
  p4: P4Model[];
  appID: any;
  singleP4data: P4Model;
  categoryID: any;
  componentID: any;
  lastFetchedItems: number;
  p4Obj:any;
  fetchingData: boolean;
  collectionDirty: boolean;
  categoryFetchSub: Subscription;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private modalService: ModalService,
              public entityListConfig: EntityListConfig,
              public activeModal: NgbActiveModal,
              private socketService: SocketService,
              private p4Service: P4Service,
              private route: ActivatedRoute,
              private _sanitizer: DomSanitizer,
              private dtoService: DTOService,
              public infiniteScrollService: InfiniteScrollService) {
    this.collectionDirty = false;
    this.lastFetchedItems = 0;
    this.p4Obj = {};
   this.categoryID = this.dtoService.getValue('categoryID');
   console.log(this.categoryID,'here p4 categoryId is printing');
    // this.socketService.subscribeToEvent(`p4:save`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllP4ForSockets())
    // this.socketService.subscribeToEvent(`p4:remove`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllP4ForSockets())
    // /*** websockets */

    /*** websockets */
    this.socketService.subscribeToEvent(`p4:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.savedFromSocket(data, this.p4,this.p4Obj, this.entityListConfig);
        if(this.fetchingData) {
          this.categoryFetchSub.unsubscribe();
          this.getAllP4(false);
        }
      })
    this.socketService.subscribeToEvent(`p4:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.p4,this.p4Obj,this.entityListConfig);
        if(this.fetchingData){
          this.categoryFetchSub.unsubscribe();
          this.getAllP4(false);
        }
      })
    /*** websockets */
  }
  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {

          this.appID = data.appID;
          this.pageName = data.selectedComponent.pageName;
          this.componentID = data.selectedComponent.componentID._id;
          console.log(this.componentID,'here p4 categoryId is printing');
          this.p4 = data.p4Data;
          this.infiniteScrollService.computeEntity(this.p4,this.p4Obj, this.entityListConfig);
          // this.lastFetchedItems = this.p4.length;
          // if(this.p4.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // console.log(this.p4, ' line no 49');
        }
      );
  }
  onScroll(){
    this.getAllP4(true);
  }
  editp4(p4: P4Model){
    const modalRef =  this.modalService.openOverlayModal(P4UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p4 = p4;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.categoryID = this.categoryID;
    modalRef.componentInstance.componentID = this.componentID;
  }
  addP4(){
    const modalRef =  this.modalService.openOverlayModal(P4UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p4 = new P4Model;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.categoryID = this.categoryID;
    modalRef.componentInstance.componentID = this.componentID;
  }
  selectedData(p4: P4Model){
     this.singleP4data = p4;
  }
  getBackground(image){
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }
  deletep4(p4: P4Model){
    const modalRef = this.modalService.open(P4DeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p4 = p4;
    modalRef.componentInstance.appID = this.appID;
  }
  getAllP4(refreshData) {
    this.p4Service
      .getAll(this.entityListConfig, this.appID, false)
      .subscribe(
        data => {
          this.infiniteScrollService.getFromApi(data, this.p4, this.p4Obj, this.entityListConfig, refreshData);
        });
  }
          // if(refreshData)   this.p4 = data;
          // // remove last page entries if data less than ideal size
          // if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
          //   this.p4.splice(
          //     (this.entityListConfig.params.pageNumber)
          //     *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          // }
          // this.lastFetchedItems = data.length;
          // if(data.length == this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // // push new data
          // this.p4 = [...this.p4, ...data];
        //  this.activeModal.close('P4_Updated');

  // getAllP4ForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.p4Service
  //     .getAll(entityListConfig, this.appID, true)
  //     .subscribe(
  //       (data) => {
  //         this.p4 = data;
  //         this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //         this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //         if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //       });
  // }
}
