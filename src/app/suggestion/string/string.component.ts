import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {EntityListConfig} from '../../shared/models/entity-list-config.model';
import {DomSanitizer} from "@angular/platform-browser";
import {StringUpsertComponent} from "./string.upsert/string.upsert.component";
import {StringDeleteComponent} from "./string.delete/string.delete.component";
import {StringContentModel} from "../../shared/models/stringContent.model";
import {DataService, SocketService, InfiniteScrollService} from '../../shared';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'text-pages',
  templateUrl: './string.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]

})
export class StringComponent implements OnInit {
  fetchingData: boolean;
  collectionDirty: boolean;
  stringFetchSub: Subscription;
  stringObj: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
   text: any;
  constructor(
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private _sanitizer: DomSanitizer,
              private dataService: DataService,
              private entityListConfig: EntityListConfig,
              private socketService: SocketService,
              public infiniteScrollService: InfiniteScrollService,
              private router: Router

  ) {
    this.collectionDirty = false;
    this.stringObj = {};

    /*** websockets */
    this.socketService.subscribeToEvent(`data:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.text,this.stringObj, this.entityListConfig);
        if(this.fetchingData){
          this.stringFetchSub.unsubscribe();
          this.getAllFiles(false);
        }
      })
    this.socketService.subscribeToEvent(`data:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.text,this.stringObj,this.entityListConfig);
        if(this.fetchingData){
          this.stringFetchSub.unsubscribe();
          this.getAllFiles(false);
        }
      })
    /*** websockets */

  }
  viewLabelTypes(fileID){
    this.router.navigateByUrl(`/suggestion/suggested/${fileID}`);
  }
  ngOnInit() {
    this.route.data
      .map((data=> data.data))
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.text = data;
        this.infiniteScrollService.computeEntity(this.text,this.stringObj, this.entityListConfig);
      });
  }
  getBackground(image){
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }
  getAllFiles(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
      delete this.entityListConfig.query.sortBy;
      delete this.entityListConfig.query.order;
    this.entityListConfig.query['type'] = 'string';
    this.stringFetchSub = this.dataService
      .getAll(this.entityListConfig)
      .subscribe(
        (data)=>{
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllFiles(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.text, this.stringObj, this.entityListConfig, refreshData);
          }
        }
      ),(error)=>{this.fetchingData = false;};
  }
  // ****P2***
  editFile (stringContent: any) {
    const modalRef = this.modalService.open ( StringUpsertComponent );
    modalRef.componentInstance.text = this.text;
    modalRef.componentInstance.stringContent = stringContent;
  }
  deleteFile(stringContent: any) {
    const modalRef = this.modalService.open ( StringDeleteComponent );
    modalRef.componentInstance.stringContent = stringContent;
    }
  addFile() {
    const modalRef = this.modalService.open ( StringUpsertComponent );
    modalRef.componentInstance.stringContent = new StringContentModel;
  }


}
