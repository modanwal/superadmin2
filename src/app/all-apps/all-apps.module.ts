import {  NgModule } from '@angular/core';
import { SharedModule, AppDetailsDraggableComponent, AuthGuard, UserService, DTOService, ConstantService } from '../shared';
import { AllAppsComponent } from './all-apps.component';
import { AllAppsDeleteComponent } from './all-apps.delete/all-apps.delete.component';
import { AllAppsResolver } from './all-apps.resolver';
import { AllAppsUpsertComponent} from './all-apps.edit/all-apps.upsert.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AllAppsUsersComponent} from './all-apps.upsert/all-apps.upsert.component';
import { Ng2CompleterModule } from 'ng2-completer';
import { FormsModule } from '@angular/forms';
import { CollaboratorComponent} from "./collaborator.component/collaborator.component";
import { CollaboratorAcessComponent } from './collaborator.acess/collaborator.acess.component';
import { CollaboratorDeleteComponent } from './collaborator.delete/collaborator.delete.component';
import {CollaboratorService} from "../shared/services/collaborator.service";

export const allAppsRouting = [
  {
    path: 'all-apps',
    component: AllAppsComponent,
    canActivate: [AuthGuard],
    resolve: {
      users: AllAppsResolver
    }
  }
];

@NgModule({
  imports: [
    SharedModule, Ng2CompleterModule,
    FormsModule,
    Ng2SearchPipeModule
  ],
  declarations: [
    AllAppsComponent, CollaboratorComponent,
    AllAppsDeleteComponent, AllAppsUsersComponent,
    AppDetailsDraggableComponent, AllAppsUpsertComponent,
    CollaboratorAcessComponent,
    CollaboratorDeleteComponent
  ],
  providers: [
    AuthGuard,
    AllAppsResolver, UserService, DTOService, ConstantService, CollaboratorService
  ],
   entryComponents: [
    AllAppsDeleteComponent, AllAppsUpsertComponent, AllAppsUsersComponent,
    CollaboratorComponent,
     CollaboratorAcessComponent,
    CollaboratorDeleteComponent
  ]
})
export class AllAppsModule {}
