import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';


@Injectable()
export class F1Service {
  constructor (private apiService: ApiService) {}

  savef1(pageName, appID, componentID): Observable<any> {
    // If we're updating an existing component
      return this.apiService.put(`/app/${appID}/component/${componentID}`, pageName);

  }
}
