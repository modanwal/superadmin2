
import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import {EntityListConfig, RoleService, RoleResourcePrivilegeService, TemplateResourcePrivilegeService} from '../../../shared';


@Injectable()
export class RoleResourcePrivilegeResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (  private roleService: RoleService,
                 private roleResourcePrivilegeService: RoleResourcePrivilegeService,
                 private templateResourcePrivilegeService: TemplateResourcePrivilegeService
                ) {

  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
  const entityListConfig = new EntityListConfig();
    const roleID = state['url'].split('/')[4];
    const obs1 =  this.roleService.get(roleID);
    const obs2 = this.templateResourcePrivilegeService.getTemplateresourcePrivilegeRole(roleID);
    return Observable.forkJoin(obs1, obs2);
  }
}
