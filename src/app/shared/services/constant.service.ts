import { Injectable, } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { AppModel } from './../models/app.model';
import { EntityListConfig } from './../models/entity-list-config.model'
import { ApiService } from './api.service';


@Injectable()
export class ConstantService {
  constructor (
    private apiService: ApiService
  ) {}
  get(constant, isSocket?): Observable<AppModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/constant/${constant}`, query, isSocket);
  }
  getAllConstantAppStatus(entityListConfig: EntityListConfig,  isSocket?): Observable<AppModel[]> {
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/constant/app/status`, query, isSocket);
  }
}
