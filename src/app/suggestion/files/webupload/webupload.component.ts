import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { CroppedUpload } from './../croppedupload/croppedupload.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {FlickrService, DTOService, GiphyService, CcldService} from './../../../shared';

@Component({
  selector: 'web-wizard',
  templateUrl: './webupload.component.html'
})
export class WebUpload implements  OnInit {
  cropDimension: { width: number; height: number; };
  fetchingData: boolean;
  activeClass: any;
  photos: any;
  @ViewChild('input')
  input: ElementRef;
  formData: FormData;
  file: any;
  constructor(
              public activeModal: NgbActiveModal,
              private _sanitizer: DomSanitizer,
              private _flickrService: FlickrService,
              private giphyService: GiphyService,
              private ccldService: CcldService,
              private dtoService: DTOService,
              private modalService: NgbModal) {
    this.cropDimension = {
      width: 200,
      height: 200
    }

  }
  ngOnInit() {

  }
  searchfromFlickr(){
    this.activeClass = 'flickr';
    const query = this.input.nativeElement.value;
    this.fetchingData = true;
    this._flickrService
      .getResult(query)
      .subscribe(
        data => {this.photos = data,this.fetchingData = false},
        error => this.fetchingData = false
      );
  }
  searchfromGiphy(){
    this.activeClass = 'giphy';
    const query = this.input.nativeElement.value;
    this.fetchingData = true;
    this.giphyService
      .getImages(query)
      .subscribe(
        data=>{this.photos = data;this.fetchingData = false},
        error => this.fetchingData = false
      );
  }
  checkstatus(url: string){
    if (typeof url === 'string') return true;
    return false;
  }
  selectPhoto(photo: any){
    const modalRef = this.modalService.open(
      CroppedUpload
    )
    let photoforUpload,cropDimension;
    if ((typeof photo.url === 'string') && (  photo.type !== 'gif')) {
      photoforUpload  = photo.url;
    } else if (typeof photo.url === 'string') {
      photoforUpload = photo.images.original.url;
    }

    modalRef.componentInstance.path = photoforUpload;
    modalRef.componentInstance.cropDimension = this.cropDimension;

    this.activeModal.close();
  }
  getImagesfromApi(photo) {
    if ((typeof photo.url === 'string') && (  photo.type !== 'gif')) {
      return this._sanitizer.bypassSecurityTrustStyle(`url(${photo.url})`);
    } else if (typeof photo.url === 'string') {
      return this._sanitizer.bypassSecurityTrustStyle(`url(${photo.images.original.url})`);
    }
  }
  getBackground(image){
      return
  }
  getTitlefromApi(photo){
    if ((typeof photo.url === 'string') && (  photo.type !== 'gif')) {
      return photo.title;
    } else if (typeof photo.url === 'string') {
      return photo.slug;
    }
  }
}
