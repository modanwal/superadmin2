import {Component, OnInit, Input, Output, ViewEncapsulation, OnDestroy} from '@angular/core';
import { F3Model, F3Service, SocketService, EntityListConfig, ModalService } from '../../shared';
import { F3UpsertComponent } from './f3.upsert/f3.upsert.component';
import { F3DeleteComponent } from './f3.delete/f3.delete.component';
import { ActivatedRoute } from '@angular/router';
import { BreadCrumbService } from "./../../shared/breadcrumb.module";
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'f3-component',
  templateUrl: './f3.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})
export class F3Component implements OnInit , OnDestroy{
  pageName: any;
  allF3Data: any[];
  appID: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  f3: F3Model[];
  constructor(private modalService: ModalService,
              private route: ActivatedRoute,
              private socketService: SocketService,
              private f3Service: F3Service,
              private entityListConfig: EntityListConfig) {
      this.entityListConfig.params.pageSize = -1;
  }
  ngOnInit() {
    this.appID = this.route.snapshot.data['data']['appID'];
    this.allF3Data = this.route.snapshot.data['data']['allF3Data'];
    this.pageName = this.route.snapshot.data['data']['pageName'];

    /*** websockets */
    this.socketService.subscribeToEvent(`role:save`)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(data => this.getAllF3())
    this.socketService.subscribeToEvent(`role:remove`)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(data => this.getAllF3())
    /*** websockets */
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  getAllF3(){
    this.f3Service.getAllF3(this.entityListConfig, this.appID,true)
        .subscribe(data => this.allF3Data = data);
  }

  addF3(){
    console.log('twicw');
    const modalRef = this.modalService.open(F3UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.f3 = new F3Model();
    modalRef.componentInstance.pageName = this.pageName;
    modalRef.componentInstance.appID = this.appID;
  }

  editF3(f3){
    const modalRef = this.modalService.open(F3UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.f3 = f3;
    modalRef.componentInstance.pageName = this.pageName;
    modalRef.componentInstance.appID = this.appID;
  }

  deleteF3(f3){
    const modalRef = this.modalService.open(F3DeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.f3 = f3;
    modalRef.componentInstance.pageName = this.pageName;
    modalRef.componentInstance.appID = this.appID;
  }
}
