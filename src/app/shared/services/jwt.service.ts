import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

@Injectable()
export class JwtService {
  // public isBrowser: boolean = isPlatformBrowser(this.platformId);
  constructor(
    // @Inject(PLATFORM_ID) private platformId
  ){

  }

  getToken(): String {
    if (typeof  window != 'undefined'){
      return window.localStorage['jwtToken'];
    }else {
      return;
    }
  }

  saveToken(token: String) {
    if (typeof  window != 'undefined'){
      window.localStorage['jwtToken'] = token;
    }
  }

  destroyToken() {
    if(typeof  window != 'undefined'){
        window.localStorage.removeItem('jwtToken');
    }
  }

}
