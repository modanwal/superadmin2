import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BreadCrumbService} from '../../shared/breadcrumb.module';
import {Subject} from 'rxjs/Subject';
import {UserService} from '../../shared/services/user.service';
import { Observable } from 'rxjs/Rx'



@Component({
  selector: 'invite-app',
  templateUrl: './invite.component.html'

})
export class InviteComponent implements OnInit{
  appID: any;
  userData: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private router: Router,
              private breadCrumbService: BreadCrumbService,
              private route: ActivatedRoute,
              private userService: UserService){

  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data => {
        this.appID = data[0],
        this.userData = data[1];
        console.log(data);
      });

  }
}
