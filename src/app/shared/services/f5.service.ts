import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { F3Model, EntityListConfig } from '../../shared/models';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class F5Service {

  constructor (
    private apiService: ApiService, private toastrService: ToastrService
  ) {}
  getAllF5(entityListConfig: EntityListConfig, appID, isSocket?): Observable<F3Model[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/f5/app/${appID}`, query, isSocket);
  }
  save(f5,appId): Observable<any> {

    return this.apiService.put(`/f5/app/${appId}` , f5);
  }

}
