import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SocketService, EntityListConfig, PostService, NotificationService, InfiniteScrollService } from "../shared";
import {Subject} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'notification-page',
  templateUrl: './notification.component.html',
  styleUrls:['./notification.component.scss'],
  providers: [EntityListConfig],
  encapsulation: ViewEncapsulation.None
})
export class NotificationComponent implements OnInit, OnDestroy {
  notifications: any[];
  fetchingData: boolean;
  collectionDirty: boolean;
  notificationFetchSub: Subscription;
  authenticated: Boolean;
  post: any;
  notification:any;
  lastFetchedItems: number;
  userData: any;
  apps: any;
  posts: any;
  moreComments: any[];
  notificationsObj: any;
  dataResolved: Boolean;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor( private modalService: NgbModal,
               private route: ActivatedRoute,
               private socketService: SocketService,
               private notificationService: NotificationService,
               public entityListConfig: EntityListConfig,
               public infiniteScrollService: InfiniteScrollService){

    // /*** websockets */
    // this.socketService.subscribeToEvent(`notification:save`)
    //     .takeUntil(this.ngUnsubscribe)
    //     .subscribe(data => {
    //       this.getAllNotificationsForSockets();})
    // this.socketService.subscribeToEvent(`notification:remove`)
    //     .takeUntil(this.ngUnsubscribe)
    //     .subscribe(data =>{
    //       this.getAllNotificationsForSockets()
    //     })
    // /*** websockets */

    this.collectionDirty = false;
    this.notificationsObj = {};
    this.dataResolved = false;
    /*** websockets */
    this.socketService.subscribeToEvent(`notification:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.notifications,this.notificationsObj, this.entityListConfig);
        if(this.fetchingData){
          this.notificationFetchSub.unsubscribe();
          this.getAllNotifications(false);
        }
      })
    this.socketService.subscribeToEvent(`notification:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.notifications,this.notificationsObj,this.entityListConfig);
        if(this.fetchingData){
          this.notificationFetchSub.unsubscribe();
          this.getAllNotifications(false);
        }
      })
    /*** websockets */
  }
  ngOnInit(){
    this.route
        .data
        .map(data => data['data'])
        .subscribe(
          (data) => {
            this.notifications = data[0];
            this.infiniteScrollService.computeEntity(this.notifications,this.notificationsObj, this.entityListConfig);
          }
        )
  }
  // getAllNotificationsForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.notificationService
  //     .getAll(entityListConfig, true)
  //     .subscribe(
  //       (data) => {
  //         this.notifications = data;
  //         this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //         this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //         if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //       }
  //     );
  // }
  getAllNotifications(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
    this.notificationFetchSub =   this.notificationService
      .getAll(this.entityListConfig,'000000000000000000000002')
      .subscribe(
        data =>{
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllNotifications(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.notification, this.notificationsObj, this.entityListConfig, refreshData);
          }
        }
      ),(error)=>{this.fetchingData = false;};
  }
  onScroll(){
    this.getAllNotifications(false);
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
