import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class P3Service {
  constructor (private apiService: ApiService) {}

  getAllp3( entityListConfig:  EntityListConfig, appID,  isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/p3/app/${appID}/${pageNumber}/${pageSize}`, query, isSocket);
  }

  getAllp3p1( p3Id, entityListConfig:  EntityListConfig, appID, isSocket?): Observable<any[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/p3/${p3Id}/app/${appID}/${pageNumber}/${pageSize}`, query, isSocket);

    // return this.apiService.get(`/p3/p1/app/${appID}/p3/${p3Id}/${pageNumber}/${pageSize}`, query, isSocket);
  }

  destroy(_id, appID): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/p3/${_id}/app/${appID}/` );
  }

  destroyp3p1(p3p1ID, p3Id, appID): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/p3/${p3Id}/p3p1/${p3p1ID}/app/${appID}` );
  }

  save(p3, appID): Observable<any> {
    // If we're updating an existing component
    if (p3._id) {
      const p3ID = p3._id;
      delete p3._id;
      return this.apiService.put(`/p3/${p3ID}/app/${appID}/` , p3);
      // Otherwise, create a new category
    } else {
      delete p3._id;
      return this.apiService.post(`/p3/app/${appID}/`, p3)
        .map(data => data.keyPeople);
    }
  }
  saveFormData(p3p1FormData, p3p1, p3Id, appID): Observable<any> {
    console.log(p3Id);
    // If we're updating an existing KeyPeople
    if (p3p1._id) {
      return this.apiService.putWithFormData(`/p3/${p3Id}/p3p1/${p3p1._id}/app/${appID}` , p3p1FormData)
        .map(data => data.p3p1);
      // Otherwise, create a new People
    } else {
      delete p3p1._id;
      return this.apiService.postWithFormData(`/p3/${p3Id}/app/${appID}`, p3p1FormData)
        .map(data => data.p3p1);
    }
  }
}
