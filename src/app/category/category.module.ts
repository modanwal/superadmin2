
import {ModuleWithProviders, NgModule, } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CategoryComponent } from './category.component';
import { SharedModule, AuthGuard } from '../shared';
import { CategoryDeleteComponent } from './category.delete/category.delete.component';
import { CategoryUpsertComponent } from './category.upsert/category.upsert.component';
import { TemplatesComponent } from './category.upsert/templates/templates.component';
import { TemplatesDeleteComponent } from './category.upsert/templates/templates.delete/templates.delete.component';
import { TemplatesUpsertComponent } from './category.upsert/templates/templates.upsert/templates.upsert.component';
import { TemplatesListComponent } from './category.upsert/templates/templates.list/templates.list.component';
import { TemplateComponent } from './category.upsert/template/template.component';
import { TemplateDeleteComponent } from './category.upsert/template/tempate.delete/template.delete.component';
import { TemplateUpsertComponent } from './category.upsert/template/template.upsert/template.upsert.component';
import { TemplateListComponent } from './category.upsert/template/tempate.list/template.list.componant';
import { PriviligesComponent } from './category.upsert/privileges/priviliges.component';
import { ComponentStackComponent } from './category.upsert/componentstack/componentstack.component';
import { FileuploaderDirective } from './category.upsert/fileuploader.directive';
import { ComponentStackModalComponent } from './category.upsert/componentstack/componentstackmodal.component';
import { HasTemplateComponent } from './category.upsert/hasTemplate/hasTemplate.component';
import { CategoryResolver } from './category-resolver.service';
import {PrivilegesUpsertComponent} from "./category.upsert/privileges/privileges.upsert/privileges.upsert.component";
import {TemplatesResolver} from './category.upsert/templates/templates.resolver';

export const categoryRouting = [
  {
    path: 'category',
    component: CategoryComponent,
    canActivate: [AuthGuard],
    resolve: { data: CategoryResolver }
  },
  {
    path: 'templates',
    component: TemplatesComponent,
    pathMatch: 'full'
  },
  {
    path: 'templates/:id',
    component: TemplatesComponent,
    pathMatch: 'full',
    resolve: { data : TemplatesResolver}
  },
  {
    path: 'template/:id',
    children: [
      {
        path: '',
        component: TemplateComponent,
        pathMatch: 'full'
      },
      {
        path: 'privileges',
        component: PriviligesComponent,
        pathMatch: 'full'
      }
    ]
  },
  {
    path: 'componentstack/:id',
    component: ComponentStackComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  }
  ];

@NgModule( {
  imports: [
    SharedModule
  ],
  declarations: [
    CategoryComponent,
    TemplateComponent ,
    CategoryDeleteComponent,
    CategoryUpsertComponent,
    TemplatesComponent ,
    TemplatesListComponent ,
    TemplatesDeleteComponent,
    TemplatesUpsertComponent,
    TemplateDeleteComponent,
    TemplateUpsertComponent,
    TemplateListComponent,
    PriviligesComponent,
    ComponentStackComponent,
    FileuploaderDirective,
    ComponentStackModalComponent,
    HasTemplateComponent, PrivilegesUpsertComponent
  ],
  providers: [AuthGuard, CategoryResolver, TemplatesResolver],
  entryComponents: [
    CategoryDeleteComponent,
    CategoryUpsertComponent,
    TemplatesDeleteComponent,
    TemplatesUpsertComponent,
    TemplateDeleteComponent,
    TemplateUpsertComponent,
    PriviligesComponent,
    ComponentStackModalComponent,
    HasTemplateComponent, PrivilegesUpsertComponent
    ]
})



export class CategoryModule {  }
