import { Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ComponentService } from '../../shared';
import {EntityListConfig} from '../../shared/models/entity-list-config.model';

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './component.upsert.component.html'
})
export class NgbdModalUpsertContent  implements OnInit{
  @Input() component;
  @Input() parentID;
  @Input() resourceName;

  componentForm: FormGroup;
  isSubmited: Boolean;

  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private router: Router,
              private componentService: ComponentService) {
    this.isSubmited=false;

  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm(){
    if(!this.isSubmited){this.isSubmited = true }
  if(this.componentForm.valid){
    this.isSubmited = true;
    this.componentForm.disable();
    const component = this.componentForm.value;
    console.log(component);
    if(this.parentID){
      component.parentID = this.parentID;
    }
    this.componentService
      .save(component)
      .subscribe(
        data => { this.componentForm.enable();
          this.activeModal.close();
          this.componentService.getResourceName(new EntityListConfig)
        },
        error => {
          this.componentForm.enable();
        }
      );
  }
}
  setRoute(event) {
  const resource =  this.resourceName.find((value)=> {return event.target.value === value._id}  );
  const afterChange = resource.name.toLowerCase();
  this.componentForm.controls['route'].setValue('/'+ afterChange);

  }
  ngOnInit(){
    // use FormBuilder to create a form group
    console.log('component id coming',this.component);
if(this.component._id){

  var resourceID:string=this.component.resourceID._id;
  console.log('----------------->>>>>>>>>',resourceID);
  this.componentForm = this.formBuilder.group({
    '_id': [this.component._id],
    'type': [this.component.type, Validators.required],
    'route': [this.component.route, Validators.required],
    'description': [this.component.description, Validators.required],
    'resourceID': [this.component.resourceID.name]

  });
}
else{

  console.log('data else', this.resourceName[0]['_id']);
  this.componentForm = this.formBuilder.group({
    '_id': [this.component._id],
    'type': [this.component.type, Validators.required],
    'route': [this.component.route, Validators.required],
    'description': [this.component.description, Validators.required],
    'resourceID': [this.resourceName[0] ? this.resourceName[0]['_id']: '' ]
  });
}

    // if(this.component.resourceID){
    //   this.componentForm.controls.resourceID.disable();
    // }

    if(this.resourceName[0]  && ! this.component._id){
      const name= this.resourceName[0]['name'].toLowerCase();
      this.componentForm.controls['route'].setValue('/'+name);
    }
  }
}
