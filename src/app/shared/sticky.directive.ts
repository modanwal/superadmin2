import { Component, ElementRef, NgZone, Host, Directive, ModuleWithProviders, NgModule, Renderer2, Input } from '@angular/core';
import { CommonModule } from "@angular/common";
declare var jQuery;

@Directive({
  selector: '[stickyEl]',
})
export class StickyElement {
  @Input() stickyEl;
  height: number;
  constructor(private el: ElementRef,private renderer: Renderer2) {
  }
  ngAfterViewInit() {
    if(typeof window !=undefined){
        jQuery(this.el.nativeElement).sticky({topSpacing:this.stickyEl});
    }
  }
  ngOnDestroy() {
    
  }
}
