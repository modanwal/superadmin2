import {Component, Input, OnInit} from '@angular/core';
import {Subject} from "rxjs/Subject";
import {F2Service} from "../../shared";
import {ActivatedRoute} from "@angular/router";
import {F2Model} from "../../shared";
import {EntityListConfig} from "../../shared/models/entity-list-config.model";
import {FormBuilder, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-root',
  templateUrl: './f2.component.html',
  providers: [EntityListConfig]
})

export class F2Component implements OnInit{
   appID: any;
   userData: F2Model[];
   role: any[];
   formdata: any;
  //  singleSelectConfig = {
  //   labelField: 'name',
  //   valueField: '_id'
  // };
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private f2Service: F2Service,
              private route: ActivatedRoute,
              private entityListConfig: EntityListConfig,
              private formBuilder: FormBuilder,
  ) {


  }
  ngOnInit(){

    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data =>{
        this.userData = data['data']['val1'];
        this.appID = data['data']['val3'];
        this.role = data['data']['val2'];
      });

  }
  setDefaultRole(event, userData){
   // console.log(event);
   // console.log('userdata',this.userData);
   // console.log(event.target.value)
     const roleID = event.target.value;
    let data = {};
      data['roleID'] = roleID;
    this.f2Service
      .saveRole(data, this.appID, userData._id)
      .subscribe(data =>{
                //    this.role = data;
    });
  }

}
