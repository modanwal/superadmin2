

import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {P3Model, P3Service, EntityListConfig, FileUploadComponent} from '../../../shared';
import {P3UpsertComponent} from './p3.upsert/p3.upsert.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {P3DeleteComponent} from './p3.delete/p3.delete.component';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subject} from "rxjs/Subject";


@Component({
  selector: 'app-page',
  templateUrl: './p3.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})
export class P3Component implements OnInit {
  
  f1Id: any;
  appID: any;

  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(
              private modalService: NgbModal,
              private route: ActivatedRoute,
              private router: Router) {
  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {
          
          
        }
      );

  }
  
  
  
  

 

}
