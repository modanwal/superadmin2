
/**
 * Created by rahul on 7/15/2017.
 */
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RoleService, AppService, EntityListConfig } from '../../../shared';
import { SafeHtml, DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: 'role-upsert',
  templateUrl: './role.upsert.component.html'
})
export class RoleUpsertComponent implements  OnInit {
  apps: { "_id": string; "name": string; "component": any[]; }[];
  roleForm: FormGroup;
  @Input() role: any;
  isSubmited: Boolean;


  constructor (private formBuilder: FormBuilder,
               public activeModal: NgbActiveModal,
               private roleService: RoleService,
                private _sanitizer: DomSanitizer,
               private appService: AppService,
               private entityListConfig: EntityListConfig) {
  }
  appsObservableSource(keyword: any){
     const entityListConfig = new EntityListConfig();
     delete  entityListConfig.query.order;
    delete  entityListConfig.query.sortBy;
    entityListConfig.query['name'] = keyword;
    return this.appService.getAppsByName(entityListConfig);
  }
  eventHandler(event){
    if(event.keyCode === 13){
      console.log('event fire');
      this.submitForm();
    }
  }

  submitForm () {
    if(!this.isSubmited){this.isSubmited = true }
    if (this.roleForm.valid) {
      this.roleForm.disable();
      const roleData = this.roleForm.value;
      if(this.role._id){
        delete roleData.appID;
      }
      else{
        roleData.appID = this.roleForm.value['appID']['_id'];
      }
      // console.log ( roleData );
      this.roleService
        .save(roleData)
        .subscribe(
          data => {
            this.roleForm.enable();
            this.activeModal.close('component:updated');
          },
          error => {
            this.roleForm.enable();
          }
        );

    }
  }

  ngOnInit () {
    // use FormBuilder to create a form group
    this.roleForm = this.formBuilder.group ( {
      '_id': [ this.role._id ],
      'name': [ this.role.name, Validators.required ]
    } );
    if(!this.role._id){
      this.roleForm.addControl(
      'appID', new FormControl(this.role.appID)
      )
    }
  }
  autocompleListFormatter = (data: any) : SafeHtml => {
    let html = `<span>${data.name}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }
}
