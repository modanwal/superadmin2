import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ComponentService, TemplateService, ComponentModel, EntityListConfig } from './../../../shared';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Observable } from "rxjs/Rx";

@Component({
  selector: 'app-root',
  templateUrl: './componentstackmodal.component.html',
  providers: [EntityListConfig]
})

export class ComponentStackModalComponent implements OnInit {
  imagePreview: Array<any>;
  isSubmited: boolean;
  componentsFormArray: FormArray;
  componentStackForm: FormGroup;
    @Input() templateID: String;
    @Input() component: ComponentModel;
    components: ComponentModel[];
    disabled: Boolean;
    files:any;
    formDataArray: FormData[];

    constructor(
      private activeModal: NgbActiveModal,
      private componentService: ComponentService,
      private templateService: TemplateService,
      private router: Router,
      private formBuilder: FormBuilder){
        this.disabled = true;
        this.imagePreview = [];
    }
    ngOnInit(){
      this.components = [];
      this.files = [];
      this.formDataArray = [];
      //initializeForm
      this.componentStackForm = this.formBuilder.group({
        'components': this.formBuilder.array([])
      });
      this.componentsFormArray = <FormArray>this.componentStackForm.get('components');
      this.componentService
        .getAllTreeComponents(this.component._id)
        .subscribe(
          (data)=>{
            this.components = data;
            this.disabled = false;
            this.renderComponents();
          }
        )
    }
    selectFile(event,index){
      this.files[index] = event.target.files[0];
      let fileReader = new FileReader();
      const file = event.target.files[0];
      fileReader.addEventListener("load", ()=>{
        this.imagePreview[index] = fileReader.result;
      });
      fileReader.readAsDataURL(file);
    }
    renderComponents(){
      //for each component a file will exist!

      //parentcomponent added here
      for(let key in this.components){
        this.files[key] = '';
        this.componentsFormArray.push(this.formBuilder.group({
        'componentID': [this.components[key]._id],
        'pageName': [this.components[key].name, Validators.required],
        'order': ['', Validators.required],
        'path' : ['']
        }));
      }
    }
    submitForm(){

      if (this.componentStackForm.valid) {
        this.isSubmited = true;
        this.createFormDataArray();
        this.uploadComponentsRecursively(0);
      }
    }
    createFormDataArray(){
      this.componentStackForm.value.components.forEach((component,i)=>{
          this.formDataArray[i] = new FormData();
          this.formDataArray[i].append('pageName',component.pageName);
          this.formDataArray[i].append('order',component.order);
          this.formDataArray[i].append('file',this.files[i]);
          this.formDataArray[i].append('componentID',component.componentID);
      });
    }
    uploadComponentsRecursively(uploadIndex){
      if(uploadIndex==this.formDataArray.length){
        this.activeModal.close('components-added');
        return;
      }
      this.templateService
          .saveComponent(this.formDataArray[uploadIndex],this.templateID)
          .subscribe(
            (data) => this.uploadComponentsRecursively(uploadIndex+1),
            (error) => {this.activeModal.close('components-added');}
          )
    }
    close(){
      this.activeModal.close('canceled');
    }
}
