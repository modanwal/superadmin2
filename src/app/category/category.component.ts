import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { CategoryDeleteComponent } from './category.delete/category.delete.component';
import { CategoryUpsertComponent } from './category.upsert/category.upsert.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EntityListConfig, SocketService, InfiniteScrollService,ModalService,CategoryModel, User , CategoryService } from './../shared';
import { HasTemplateComponent } from './category.upsert/hasTemplate/hasTemplate.component';
import { Subject } from "rxjs/Subject";
import { BreadCrumbService } from "./../shared/breadcrumb.module";
import { Subscription } from "rxjs/Subscription";


@Component({
  selector: 'page-root',
  templateUrl: './category.component.html',
  providers: [EntityListConfig]
})


export class CategoryComponent implements OnDestroy, OnInit {
  fetchingData: boolean;
  collectionDirty: boolean;
  categoryFetchSub: Subscription;
  parentID: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  categories: CategoryModel[];
  categoriesObj: any;
  dataResolved: Boolean;
  constructor (private modalService: ModalService,
               private socketService: SocketService,
               private categoryService: CategoryService,
               private router: Router,
               private route: ActivatedRoute,
               public entityListConfig: EntityListConfig,
               public activeModal: NgbActiveModal,
               private breadCrumbService: BreadCrumbService,
               public infiniteScrollService: InfiniteScrollService,
           ) {
    this.collectionDirty = false;
    this.categoriesObj = {};
    this.dataResolved = false;

    /*** websockets */
    this.socketService.subscribeToEvent(`category:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.categories,this.categoriesObj, this.entityListConfig);
        if(this.fetchingData){
          this.categoryFetchSub.unsubscribe();
          this.getAllCategories(false);
        }
      })
    this.socketService.subscribeToEvent(`category:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.categories,this.categoriesObj,this.entityListConfig);
        if(this.fetchingData){
          this.categoryFetchSub.unsubscribe();
          this.getAllCategories(false);
        }
      })
    /*** websockets */
    this.route
        .queryParams
        .takeUntil(this.ngUnsubscribe)
        .subscribe(params => {
          this.parentID = params.id;
          if(!this.parentID){
            this.parentID='';
            this.breadCrumbService
              .pushBreadCrumbItem({url:'/category',label:'Category'},true);
          }
          if(this.dataResolved){
              this.getAllCategories(true);
            }
          this.dataResolved = true;
        });
  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(val => {
        this.categories = val['data'];
        this.infiniteScrollService.computeEntity(this.categories,this.categoriesObj, this.entityListConfig);
      } );
  }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllCategories(true);
  }
  onScroll(){
    this.getAllCategories(false);
  }
  /*** sorting & pagination */

  getAllCategories(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
    this.categoryFetchSub = this.categoryService
      .getAll(this.parentID, this.entityListConfig)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllCategories(false);
          }else{
              this.infiniteScrollService.getFromApi(data, this.categories, this.categoriesObj, this.entityListConfig, refreshData);
          }
        }
        ),(error)=>{this.fetchingData = false;};
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  view(category : CategoryModel) {
    if(!category.isLeaf) {
      this.breadCrumbService
              .pushBreadCrumbItem({url:'/category',queryParams:{id: category._id},label:category.name},false);
      this.router.navigate(['category'], { queryParams: { id: category._id } });
    }
    else if(category.hasTemplate)
    {
      this.router.navigateByUrl(`/templates/${category._id}`);
    }
    else
      if(category.isLeaf && !category.hasTemplate) {
        const modalRef = this.modalService.open(HasTemplateComponent,{},'fade-in-pulse','fade-out-pulse');
        modalRef.componentInstance.category = category;
    }

  }


  addTemplate(){
    this.router.navigateByUrl(`/templates/${this.parentID}`);
  }
  addCategory () {
    const modalRef = this.modalService.open ( CategoryUpsertComponent,{},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.category = new CategoryModel;
    modalRef.componentInstance.category.parentID = this.parentID;
  }
  editCategory (category: CategoryModel) {
    const modalRef = this.modalService.open ( CategoryUpsertComponent ,{},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.category = category;
  }
  deleteCategory(category: CategoryModel) {
    const modalRef = this.modalService.open(CategoryDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.category = category;
  }


}
