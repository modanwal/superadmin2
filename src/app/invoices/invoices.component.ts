import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subject} from 'rxjs/Subject';
import {SocketService, InvoicesService, EntityListConfig, DTOService, InfiniteScrollService} from '../shared';
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'invoices-page',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})
export class InvoicesComponent implements OnInit, OnDestroy{
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  fetchingData: boolean;
  collectionDirty: boolean;
  invoicesFetchSub: Subscription;
    invoices: any[];
  invoicesObj: any;
  lastFetchedItems: number;
 constructor(private router: Router,
             private route: ActivatedRoute,
             private socketService: SocketService,
             private invoiceService: InvoicesService,
             public entityListConfig: EntityListConfig,
             public infiniteScrollService: InfiniteScrollService,
             private dtoService: DTOService) {
   this.lastFetchedItems = 0;
   this.collectionDirty = false;
   // /*** websockets */
   // this.socketService.subscribeToEvent(`invoice:save`)
   //   .takeUntil(this.ngUnsubscribe)
   //   .subscribe(data => this.getAllInvoicesForSockets());
   // this.socketService.subscribeToEvent(`invoice:remove`)
   //   .takeUntil(this.ngUnsubscribe)
   //   .subscribe(data => this.getAllInvoicesForSockets());
   // /*** websockets */

   /*** websockets */
   this.socketService.subscribeToEvent(`invoice:save`)
     .takeUntil(this.ngUnsubscribe)
     .subscribe(data => {
       this.collectionDirty = true;
       this.infiniteScrollService.savedFromSocket(data, this.invoices,this.invoicesObj, this.entityListConfig);
       if(this.fetchingData){
         this.invoicesFetchSub.unsubscribe();
         this.getAllInvoice(false);
       }
     });
   this.socketService.subscribeToEvent(`invoice:remove`)
     .takeUntil(this.ngUnsubscribe)
     .subscribe(data => {
       this.collectionDirty = true;
       this.infiniteScrollService.removedFromSocket(data, this.invoices,this.invoicesObj,this.entityListConfig);
       if(this.fetchingData){
         this.invoicesFetchSub.unsubscribe();
         this.getAllInvoice(false);
       }
     });
   /*** websockets */

 }
 ngOnInit(){
   this.route.data
     .takeUntil(this.ngUnsubscribe)
     .map(resolvedData => resolvedData.data)
     .subscribe(data => {
         this.invoices = data;
       this.infiniteScrollService.computeEntity(this.invoices,this.invoicesObj, this.entityListConfig);
       });
 }
  // getAllInvoicesForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //    this.invoiceService
  //      .getAll( entityListConfig )
  //      .subscribe(
  //        (data)=> {
  //          this.invoices = data;
  //          console.log(this.invoices, 'invoice printing');
  //        });
  // }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllInvoice(true);
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  onScroll() {
    this.getAllInvoice(false);
  }
  getAllInvoice(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
 this.invoicesFetchSub =    this.invoiceService
      .getAll(this.entityListConfig)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllInvoice(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.invoices, this.invoicesObj, this.entityListConfig, refreshData);
          }
        }
        //   if(refreshData) this.invoices = [];
        //   // remove last page entries if data less than ideal size
        //   if(this.lastFetchedItems < this.entityListConfig.params.pageSize){
        //     this.invoices.splice(
        //       (this.entityListConfig.params.pageNumber)
        //       *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
        //   }
        //   this.lastFetchedItems = data.length;
        //   if( data.length == this.entityListConfig.params.pageSize){
        //     this.entityListConfig.params.pageNumber++;
        //   }
        //   // push new data
        //   this.invoices = [...this.invoices, ...data];
        // }
      ),(error)=>{this.fetchingData = false;};;
  }
 viewInvoice(invoice: any) {
    this.dtoService.setValue('invoiceDate', invoice.date);
    this.dtoService.setValue('invoiceNo', invoice.invoiceNumber);
    this.router.navigateByUrl(`/${invoice._id}/invoices-details`);
  }
  downloadInvoice(invoice: any){

  }
}
