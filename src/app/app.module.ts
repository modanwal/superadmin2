import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {ApplicationModule, appRouting} from './application/application.module';
import {HomeModule, homeRouting} from './home/home.module';
import {LoginModule, loginRouting} from './login/login.module';
import {componentRouting, ComponentModule} from './component/component.module';
import {AllAppsModule, allAppsRouting} from './all-apps/all-apps.module';
import {CategoryModule, categoryRouting} from './category/category.module';
import {WallModule, wallRouting} from './wall/wall.module';
import {CreateAppModule, createAppRouting} from './create-app/create-app.module';
import {NotificationModule, notificationRouting} from './notification/notification.module';
import {RolesModule, rolesRouting} from './roles/roles.module';
import {AppStatsModule, appStatsRouting} from './appstats/appstats.module';
import {UserDetailsModule, userDetailsRouting} from './user-details/user-details.module';
import {InvoicesModule, invoicesRouting} from './invoices/invoices.module';
import {InvoicesDetailsModule, invoicesDetailsRouting} from './invoices-details/invoices-details.module';
import {FlickrModule, flickrRouting} from './flickr/flickr.module';
import {AllUserModule, allUsersRouting} from './all-users/all-users.module';
import {BillingRenewalsModule, billingRenewalsRouting} from './billing-renewals/billing-renewals.module';
import {GiphyModule, giphyRouting} from './giphy/giphy.module';
import {WorkbenchModule2, workBenchRouting2} from './workbench2/workbench2.module';
import {WorkbenchModule, workBenchRouting} from './workbench/workbench.module';
import {SuggestionModule, suggestionRouting} from './suggestion/suggestion.module';
import {paymentconfirmation, PaymentconfirmModule} from './paymentconfirm/paymentconfirm.module';
import {TestAppModule, testAppRouting} from './test-app/test-app.module';
import {NotFound404Module, notFound404Routing} from './404/404.module';
import {HeaderComponent} from './shared/layout/header.component';
import {LeftSidebarComponent} from './shared/layout/left-sidebar.component';
import {RightSidebarComponent} from './shared/layout/right-sidebar.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import {SharedModule} from './shared/shared.module';
import {ToasterService} from './shared/services/toaster.service';
import {ApiService} from './shared/services/api.service';
import {JwtService} from './shared/index';
import {AuthGuard} from './shared/services/auth-guard.service';
import {UserService} from './shared/services/user.service';
import {CategoryService} from './shared/services/category.service';
import {ComponentService} from './shared/services/components.service';
import {RoleService} from './shared/services/role.service';
import {ResourceService} from './shared/services/resource.service';
import {ResourcePrivilegeService} from './shared/services/resource-privilege.service';
import {TemplateService} from './shared/services/template.service';
import {TemplatesService} from './shared/services/templates.service';
import {RoleResourcePrivilegeService} from './shared/services/role-resource-privilege.service';
import {TemplateResourcePrivilegeService} from './shared/services/template-resource-privilege.service';
import {SocketService} from './shared/services/socket.service';
import {AppService} from './shared/services/app.service';
import {P4F1Service} from './shared/services/p4f1.service';
import {P4Service} from './shared/services/p4.service';
import {ConstantService} from './shared/services/constant.service';
import {UtilsService} from './shared/services/utils.service';
import {InfiniteScrollService} from './shared/services/infinitescroll.service';
import {ModalService} from './shared/modal.service';
import {NgbModalStack} from "@ng-bootstrap/ng-bootstrap/modal/modal-stack";
import {NgbDropdownConfig, NgbTooltipConfig} from "@ng-bootstrap/ng-bootstrap";

export const GOOGLE_CLIENT_ID = '450614066198-vrk23s38trkdk1mk1vplhidkh7mpq59m.apps.googleusercontent.com';
export const FACEBOOK_CLIENT_ID = '189479951551249';

@NgModule({
  declarations: [
    AppComponent, HeaderComponent, LeftSidebarComponent, RightSidebarComponent
  ],
  imports: [ TestAppModule,
    WorkbenchModule, NotFound404Module, HomeModule, LoginModule, CategoryModule, BillingRenewalsModule, ComponentModule,
    AllAppsModule, ApplicationModule, RolesModule, AllUserModule, InvoicesDetailsModule, UserDetailsModule, CreateAppModule,
    AppStatsModule, InvoicesModule, WallModule, NotificationModule, GiphyModule, WorkbenchModule2, FlickrModule, SuggestionModule,
    PaymentconfirmModule,
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    RouterModule.forRoot([
      ...appRouting,
      ...homeRouting,
      ...loginRouting,
      ...componentRouting,
      ...allAppsRouting,
      ...categoryRouting,
      ...flickrRouting,
      ...createAppRouting,
      ...wallRouting,
      ...notificationRouting,
      ...rolesRouting,
      ...allUsersRouting,
      ...userDetailsRouting,
      ...appStatsRouting,
      ...invoicesRouting,
      ...invoicesDetailsRouting,
      ...billingRenewalsRouting,
      ...giphyRouting,
      ...workBenchRouting2,
      ...workBenchRouting,
      ...suggestionRouting,
      ...paymentconfirmation,
      ...testAppRouting,
      ...notFound404Routing
    ]),
    ToastrModule.forRoot({positionClass: 'inline'}), BrowserModule.withServerTransition({appId: 'cli-universal-demo'}),
    RouterModule, NoopAnimationsModule, ToastContainerModule.forRoot(),
    SharedModule,
  ],
  exports: [RouterModule],
  providers: [ToasterService, ApiService, AuthGuard, JwtService, UserService, CategoryService, ComponentService, ModalService,
    RoleService, ResourceService, ResourcePrivilegeService, TemplateService, TemplatesService, RoleResourcePrivilegeService,
    TemplateResourcePrivilegeService, SocketService, AppService, P4F1Service, P4Service, ConstantService, UtilsService,
    InfiniteScrollService, NgbModalStack, NgbTooltipConfig, NgbDropdownConfig],
  bootstrap: [AppComponent]
})
export class AppModule { }
