import { Injectable } from '@angular/core';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';


@Injectable()
export class ToasterService {
  constructor(
    private toastrService: ToastrService){}
  showError(error: any){
  //  const condition = navigator.onLine ? console.log("ONLINE") : console.log("OFFLINE")
    console.log('data coming', error);
    this.toastrService.error('404 Error coming');
  }
  internetIssue(){
    this.toastrService.error('Please Connect to Any Internet Connection');
  }
  showIssue(){
    console.log("404");
    this.toastrService.error('Any data');
  }
}
