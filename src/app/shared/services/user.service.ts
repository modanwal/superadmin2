import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
import { User, EntityListConfig } from '../models';


@Injectable()
export class UserService {
  private currentUserSubject = new BehaviorSubject<User>(new User());
  public currentUser = this.currentUserSubject.asObservable().distinctUntilChanged();

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor (
    private apiService: ApiService,
    private http: Http,
    private jwtService: JwtService,
    private router: Router
  ) {}

  // Verify JWT in localstorage with server & load user's info.
  // This runs once on application startup.
  populate() {
    // If JWT detected, attempt to get & store user's info
    if (this.jwtService.getToken()) {
      this.setAuth(this.jwtService.getToken());
    } else {
      // Remove any potential remnants of previous auth states
      this.purgeAuth();
    }
  }
  getAllContactFromMail(userID, isSocket?): Observable<any>{
    const entityListConfig = new EntityListConfig();
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/user/${userID}/google/email`, query, isSocket);
  }
  getUser( entityListConfig:  EntityListConfig, searchData, isSocket?){
    delete entityListConfig.query.sortBy;
    delete entityListConfig.query.order;
    entityListConfig['query']['email'] = searchData;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });

    return this.apiService.get(`/user`, query , isSocket );
  }

  setAuth(token) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(token);
    const query: URLSearchParams = new URLSearchParams();
    this.apiService.get('/user/me', query, false)
      .subscribe(
        currentUser => this.currentUserSubject.next(currentUser),
        err =>{
          console.log(`error while getting profile/....`);
          this.purgeAuth();
        }
      );
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
    // Set current user to an empty object
    this.currentUserSubject.next(new User());
    // Set auth status to false
    this.isAuthenticatedSubject.next(false);
    this.router.navigateByUrl('/login');
  }

  attemptAuth(provider, access_token) {
    return this.apiService.postAuth(`/000000000000000000000001`,{provider: provider, code: access_token})
    .map(
      data => {
        this.setAuth(data.token);
        return data;
      }
    );
  }
  getContacts( accessToken): any{
    console.log(accessToken);
    let url = `https://www.google.com/m8/feeds/contacts/default/full?access_token=`;
    const apiLink = url + accessToken + `&alt=json`;
    return this.http
      .get(apiLink)
      .map(res => res.json().data);
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  getUserPrivileges(): any {
    return this.currentUserSubject.value.privileges;
  }
  getCurrentUserObservable(){
    return this.currentUserSubject.asObservable();
  }
  saveUser(userFormData, userID) {
    if (userID) {
      return this.apiService.putWithFormData(`/user/${userID}`, userFormData);
    } else {
      return this.apiService.postWithFormData(`/user`, userFormData);
    }
  }
  // Update the user on the server (email, pass, etc)
  update(user): Observable<User> {
    return this.apiService
    .put('/user', { user })
    .map(data => {
      // Update the currentUser observable
      this.currentUserSubject.next(data.user);
      return data.user;
    });
  }
  getAll(entityListConfig: EntityListConfig, isSocket?){
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/user/all/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getBySearch(entityListConfig: EntityListConfig, isSearch?, isSocket?){
    entityListConfig.query.sortBy = isSearch;
    delete entityListConfig.query.sortBy;
    delete entityListConfig.query.order;
    entityListConfig.query['name'] = isSearch;
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/user/all/${pageNumber}/${pageSize}`, query, isSocket);
  }
  get(_id, isSocket?) {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get ( `/user/${_id}`, query,  isSocket );
  }

  destroy(_id) {
    return this.apiService.delete('/user/' + _id);
  }

}
