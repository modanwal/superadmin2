import { ModuleWithProviders, NgModule } from '@angular/core';
import { SharedModule, AuthGuard , UserDetailService, ApplicationStatusService} from '../shared';
import {RouterModule, RouterStateSnapshot} from '@angular/router';
import { UserDetailsComponent } from '../user-details/user-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddressComponent } from '../user-details/addresses/address.component';
import { UserDetailsInformationComponent } from '../user-details/user-information/user-information.component';
import { UserDetailsApplicationStatusComponent } from '../user-details/application-status/application-status.component';
import { AddressUpsertComponent } from './addresses/address.upsert.component/address.upsert.component';
import { AddressDeleteComponent } from './addresses/address.delete.component/address.delete.component';
import { UserDetailsResolver } from './user-details.resolver';
import {UserAddressResolver} from "./addresses/address.resolver";
import {UserInformationResolver} from "./user-information/user-information.resolver";
import {UserInformationService} from "../shared/services/userInformation.service";
import {ApplicationStatusResolver} from "./application-status/application-status.resolver";


export const userDetailsRouting = [
  {
    path: 'user-details/:id',
    canActivate: [AuthGuard],
    component: UserDetailsComponent,
    resolve: {
      user: UserDetailsResolver
    },
    children: [
      {
        path: 'address',
        component: AddressComponent,
        resolve: { data: UserAddressResolver }
      },
      {
        path: 'application-status',
        component: UserDetailsApplicationStatusComponent,
        resolve: { data: ApplicationStatusResolver}
      }
    ]
  }
];

@NgModule({
  imports: [
    SharedModule,
    NgbModule
  ],
  declarations: [
    UserDetailsComponent,
    AddressComponent,
    UserDetailsInformationComponent,
    UserDetailsApplicationStatusComponent,
    AddressUpsertComponent,
    AddressDeleteComponent
  ],
  providers: [
    AuthGuard, UserAddressResolver, UserInformationResolver, UserInformationService,
    UserDetailsResolver, UserDetailService, ApplicationStatusResolver, ApplicationStatusService
  ],
  entryComponents: [
    AddressUpsertComponent , AddressDeleteComponent
  ]
})
export class UserDetailsModule {

}
