import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation , OnDestroy} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { ResourceService, ResourcePrivilegeService, PrivilegeService, ResourceModel, PrivilegeModel, EntityListConfig } from '../../../shared';
import { Subscription } from 'rxjs/Subscription';
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'resource-privilege',
  templateUrl: './resource-privilege.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})
export class ResourcePrivilegeComponent implements OnInit {
  resource: ResourceModel;
  privileges: PrivilegeModel[];
  disabledPrivileges: any[];
  selectedPrivilegeIDs: any[];
  privilegesFormArray: FormArray;
  resourcesForm: FormGroup;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  count: number = 0;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private resourceService: ResourceService,
              private resourcePrivilegeService: ResourcePrivilegeService,
              private privilegeService: PrivilegeService,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              public activeModal: NgbActiveModal,) {
  }

  ngOnInit() {
    this.disabledPrivileges = [];
    // this.resourceService.get(paramsID).subscribe(
    //         (data) => {
    //             this.resource = data;
    //
    //             this.getPrivileges();
    //         },
    //         (error) => {
    //             this.router.navigate(['/roles/resource']);
    //         }
    //     );
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data => {
        this.resource = data[0];
        console.log('resource printing', this.resource);
        this.privileges = data[1];
        console.log('privilege coming', this.privileges);
        this.selectedPrivilegeIDs = data[2].privilegeIDs;
        console.log('selectedPrivilegeIDs', this.selectedPrivilegeIDs);
      });
  }

  getDisabledStatus(privilegeId) {
    return this.disabledPrivileges.indexOf(privilegeId) != -1;
  }
  selectAll(event){
    if(event.target.checked){
      this.resourcePrivilegeService
        .checkAllResourcePrivilege(this.resource._id)
        .subscribe(
          (data)=>{
            event.target.checked = true;
            this.getSelectedPrivilegeData();
          },
          (error)=> {
            event.target.checked = false;
          });
    }
  }
  unSelectAll(event){
    if(event.target.checked){
      this.resourcePrivilegeService
        .unCheckAllResourceprivilege(this.resource._id)
        .subscribe(
          (data)=>{
            event.target.checked = true;
            this.getSelectedPrivilegeData();
          },
          (error)=>{
            event.target.checked = false;
          });
    }
  }
  getCheckedStatus(privilegeID){
   // const value =  this.selectedPrivilegeIDs.find(element => element == privilegeID );
 //   console.log(this.selectedPrivilegeIDs.indexOf(privilegeID));
     if(this.selectedPrivilegeIDs.indexOf(privilegeID)> -1)
      return true;
  }
  getAllCheckedStatus(){
    const value1 = this.selectedPrivilegeIDs.length;
    return value1 == this.privileges.length;
  }
  getSelectAllCheckStatus(){
    return  this.privileges.length == this.selectedPrivilegeIDs.length;
  }
  getResourcePrivilegeData() {
    this.resourcePrivilegeService.getPrivilegeData(this.resource._id).subscribe(
            (savedData) => {
                this.selectedPrivilegeIDs = savedData.privilegeIDs;
                console.log('selectedPrivilefeIDs', this.selectedPrivilegeIDs);
            }
    );
  }
  getSelectedPrivilegeData(){
    this.resourcePrivilegeService
      .getPrivilegeData(this.resource._id)
      .subscribe(
        (data)=> {
          this.selectedPrivilegeIDs = data.privilegeIDs;;
        });
  }
  checkboxClick(event: any,privilegeID){
    event.target.disabled = true;
      if (event.target.checked) {
          //save privilege
          let data = {
              resourceID: this.resource._id,
              privilegeID: privilegeID
          }
          this.resourcePrivilegeService.save(data)
            .subscribe(
              (value) => {
                event.target.checked = true;
                this.getResourcePrivilegeData();
                event.target.disabled = false;

              },
              (error)=>{
                event.target.checked = false;
                event.target.disabled = false;
              }
            );
      }else {
          //remove privilege
          this.resourcePrivilegeService.destroy(this.resource._id, privilegeID)
            .subscribe((value) => {
              this.getResourcePrivilegeData();
              event.target.disabled = false;
                event.target.checked = false;

              // const index =  this.selectedPrivilegeIDs.indexOf(privilegeID);
              // console.log("remove index",index);
              // console.log("selectedPrivileges",this.selectedPrivilegeIDs)
              // if(index != -1){
              //   this.selectedPrivilegeIDs.splice(index,1);
              // }
              // console.log("selectedPrivileges after remove",this.selectedPrivilegeIDs)
            },
              (error)=>{
                event.target.disabled = false;
                event.target.checked = true;
              }
          );
      }
  }
  // getPrivileges(){
  //   let entityListConfig = new EntityListConfig();
  //   entityListConfig.params.pageSize = -1;
  //   this.privilegeService.getAll(entityListConfig).subscribe(
  //       (privileges) => {
  //           this.privileges = privileges;
  //         console.log(' this.privileges',  this.privileges);
  //           this.getResourcePrivilegeData();
  //       }
  //   );
  // }

  cancel() {
    this.router.navigate(['/roles/resources']);
  }
}
