import { Component, ElementRef, NgZone, Host, Directive, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
declare var jQuery;


@Component({
  selector: 'slick-carousel',
  template: `<ng-content></ng-content>`
})
export class SlickCarouselComponent {
  constructor(private el: ElementRef, private zone: NgZone) {
  }

  $carousel: any;

  initialized = false;

  initCarousel() {
    this.zone.runOutsideAngular(() => {
        if(typeof window !=undefined){
            this.$carousel = jQuery(this.el.nativeElement).slick({});
        }
    });
    this.initialized = true;
  }

  slides = [];

  addSlide(slide) {
    !this.initialized && this.initCarousel();
    this.slides.push(slide);
    this.$carousel.slick('slickAdd', slide.el.nativeElement);
  }

  removeSlide(slide) {
    const idx = this.slides.indexOf(slide);
    this.$carousel.slick('slickRemove', idx);
    this.slides = this.slides.filter(s => s != slide);
  }
}


@Directive({
  selector: '[slick-carousel-item]',
})
export class SlickCarouselItem {
  constructor(private el: ElementRef, @Host() private carousel: SlickCarouselComponent) {
  }
  ngAfterViewInit() {
    if(typeof window !=undefined){
        this.carousel.addSlide(this);
    }
  }
  ngOnDestroy() {
    if(typeof window !=undefined){
        this.carousel.removeSlide(this);
    }
  }
}

@NgModule({
  imports: [CommonModule],
  exports: [SlickCarouselComponent,SlickCarouselItem],
  declarations: [SlickCarouselComponent,SlickCarouselItem]
})
export class SlickCaroselModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SlickCaroselModule,
      providers: []
    };
  }
}