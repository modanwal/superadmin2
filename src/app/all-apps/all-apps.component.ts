import {
  Component, ViewEncapsulation, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit
} from '@angular/core';
import { AllAppsDeleteComponent } from './all-apps.delete/all-apps.delete.component';
import { Router, ActivatedRoute } from '@angular/router';
import { EntityListConfig, AppService, SocketService, CollaboratorService, InfiniteScrollService } from './../shared';
import { Subject } from "rxjs/Rx";
import { BreadCrumbService } from "./../shared/breadcrumb.module";
import {AllAppsUpsertComponent} from "./all-apps.edit/all-apps.upsert.component";
import {Observable} from "rxjs/Rx";
import {AllAppsUsersComponent} from "./all-apps.upsert/all-apps.upsert.component";
import { ModalService } from './../shared/modal.service';
import { CollaboratorComponent} from "./collaborator.component/collaborator.component";
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'page-root',
  templateUrl: './all-apps.component.html',
  encapsulation: ViewEncapsulation.None,
  // animations: [slideToLeft()],
  // host: {'[@slideToLeft]': ''},
  providers:[EntityListConfig]
})

export class AllAppsComponent implements OnInit, OnDestroy, AfterViewInit{
  lastFetchedItems: number;
  draggableAppData: any;
  apps: any;
  selectedAppID: string;
  selectedOption: string;
  appStatus; any;
  hideDraggableAppComponent: Boolean;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  @ViewChild('input')
  input: ElementRef;
  searchData: any[];
  applicationStatus: any;
  searchByApplicationStatusData: any;
  appsObj: any;
  fetchingData: boolean;
  singleSelectConfig = {
    labelField: 'label',
    valueField: 'value'
  };
  allAppsFetchSub: Subscription;
  collectionDirty: boolean;
  constructor(public entityListConfig: EntityListConfig,
              private modalService: ModalService,
              private router: Router,
              private appService: AppService,
              private socketService: SocketService,
              private route: ActivatedRoute,
              private breadCrumbService: BreadCrumbService,
              public infiniteScrollService: InfiniteScrollService,
              private collaboratorService: CollaboratorService
             ) {
    this.collectionDirty = false;
    this.appsObj = {};
    this.applicationStatus = {
      type: 'android'
    };
    this.searchByApplicationStatusData = 'PUBLISH_INPROGRESS';
    this.hideDraggableAppComponent = true;
    this.lastFetchedItems = 0;
    this.apps = [];
    /*** websockets */
    this.socketService.subscribeToEvent(`app:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.apps,this.appsObj, this.entityListConfig);
        if(this.fetchingData){
          this.allAppsFetchSub.unsubscribe();
          this.getAllApps(false);
        }
      })
    this.socketService.subscribeToEvent(`app:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.apps,this.appsObj,this.entityListConfig);
        if(this.fetchingData){
          this.allAppsFetchSub.unsubscribe();
          this.getAllApps(false);
        }
      })
    /*** websockets */
  }

  getAppStatus(event){
    console.log('line printing', event.target.value);
  }
  openCollaborator(appID: any){
    const entityListConfig = new EntityListConfig();
    this.collaboratorService
      .getAll(appID._id, entityListConfig)
      .subscribe(
        (data)=>{
          const modalRef = this.modalService.open(CollaboratorComponent,{},'fade-in-pulse','fade-out-pulse');
          modalRef.componentInstance.app = appID;
          modalRef.componentInstance.collaborators = data;
        },
        (error)=>{
          console.log(error);
        }
      )

  }
  /** search when we enter any text into search box**/
  ngAfterViewInit() {
    // const eventObservable = Observable.fromEvent(
    //   this.input.nativeElement, 'keyup');
    // eventObservable
    //   .debounce(() => Observable.timer(1000))
    //   .subscribe(
    //   (data) => {
    //     this.searchData = data['target']['value'];
    //     if(this.searchData){
    //       this.appService
    //         .getBySearch(this.entityListConfig, this.searchData)
    //         .subscribe(
    //           (value) =>{
    //             this.apps = value;
    //           }
    //         );
    //     }
    //     else {
    //       this.getAllAppsbySearch();
    //     }
    //   }
    // );
  }
  selectUser() {
    const modalRef = this.modalService.open(AllAppsUsersComponent, {}, 'fade-in-pulse','fade-out-pulse' );
  }
  filter() {
    if (this.searchByApplicationStatusData && this.applicationStatus) {
      const entityListConfig = new EntityListConfig();
      delete entityListConfig.query.order;
      delete entityListConfig.query.sortBy;
      entityListConfig.query['type'] =  this.applicationStatus.type;
      entityListConfig.query['status'] = this.searchByApplicationStatusData;
      if (this.searchData) {
        entityListConfig.query['name'] = this.searchData;
      }
        this.appService
          .getbyAppStatus(entityListConfig)
          .subscribe(
            (data) => {
              this.apps = data;
            }
          );

    }
  }

  searchByApplicationName(event){
   this.applicationStatus.type = event.target.value;
    // console.log('event coping',app);
    // const entityListConfig = new EntityListConfig();
    // delete entityListConfig.query.order;
    // delete entityListConfig.query.sortBy;
    // entityListConfig.query['type'] = app;
    // entityListConfig.query['status'] = this.searchByApplicationStatusData;
    // if (this.searchData){
    //   entityListConfig.query['name'] = this.searchData;
    // }
    // if(app){
    //     this.appService
    //       .getbyAppStatus(entityListConfig)
    //       .subscribe(
    //         (data)=>{
    //           this.apps = data;
    //         }
    //       );
    // }
  }
  searchByApplicationStatus(event){
    console.log('some value changed!');
    this.searchByApplicationStatusData = event.target.value;

  }
/***run when we remove text from input search box**/
  getAllAppsbySearch(){
    this.appService
      .getAll(new EntityListConfig)
      .subscribe(
        data =>{
         this.apps = data;
        }
      );
  }
  ngOnInit(){
    this.breadCrumbService
              .pushBreadCrumbItem({url:'/all-apps',label:'All Apps'},true);
    this.route.data
        .takeUntil(this.ngUnsubscribe)
        .map(data => data.users)
        .subscribe(data => {
        this.apps = data[0];
        console.log('all Data Printing of Apps',this.apps);
        this.appStatus = data[1].map(item=>{
          let obj = {};
          obj['label'] = item;
          obj['value'] = item;
          return obj;
        })
        //this.appStatus = data[1];
          this.infiniteScrollService.computeEntity(this.apps,this.appsObj, this.entityListConfig);
      });
  }
  getAppByStatus(event, status){
    console.log('printing', status);
  }
  viewApp(app){
    this.selectedAppID = app._id;
    this.hideDraggableAppComponent = false;
    this.draggableAppData = app;
  }
  goToApp(app){
    this.router.navigateByUrl(`/application/${app._id}`);
  }
  editApp(app){
    console.log(app);
    const modalRef = this.modalService.open(AllAppsUpsertComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.app = app;
  }
  deleteApp(app){
    const modalRef = this.modalService.open(AllAppsDeleteComponent, {}, 'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.app = app;
  }
  changeAndroidDefaultStatus(data){
    console.log(data);
    this.appService
      .changeAppStatus(data, this.selectedAppID)
      .subscribe(
        (data1) => {
          console.log('put Requested', data1);
          this.getAllApps(true);
        },
        (error) =>{ console.log('put Not Requested')}
      );
  }
  changeIosDefaultStatus(data){
    console.log(data);
    this.appService
      .changeAppStatus(data, this.selectedAppID)
      .subscribe(
        (data1) => {
          console.log('put Requested', data1);
          this.getAllApps(true);
        },
        (error) => { console.log('put Not Requested')}
      );
  }
  closeAppDraggableComponent(){
    this.hideDraggableAppComponent = true;
  }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event) {
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllApps(true);
  }
  onScroll(){
    this.getAllApps(false);
  }
  /*** sorting & pagination */
  getAllApps(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
 this.allAppsFetchSub =  this.appService
      .getAll(this.entityListConfig)
      .subscribe(
        data =>{
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllApps(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.apps, this.appsObj, this.entityListConfig, refreshData);
          }

          // if(refreshData) this.apps = [];
          // // remove last page entries if data less than ideal size
          // if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
          //   this.apps.splice(
          //     (this.entityListConfig.params.pageNumber)
          //     *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          // }
          // this.lastFetchedItems = data.length;
          // if(data.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // // push new data
          // this.apps = [...this.apps,...data];
        }
      ), (error)=>{this.fetchingData = false;};
  }
  // getAllAppsForSockets(){
  //  const entityListConfig = new EntityListConfig();
  //  entityListConfig.query = this.entityListConfig.query;
  //  entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //  entityListConfig.params.pageNumber = 0;
  //  this.appService
  //     .getAll(entityListConfig, true)
  //     .subscribe(data =>{
  //       this.apps = data;
  //       this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //       this.entityListConfig.params.pageNumber = Math.floor(data.length/this.entityListConfig.params.pageSize);
  //       if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //     });
  // }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  clearFilter(){
    this.searchByApplicationStatusData = '';
    this.applicationStatus.type = '';
  }
  valueChanged(){
    console.log('value chaged!');
  }

}
