import {Component, OnInit, Output} from '@angular/core';
import { AddressUpsertComponent } from './address.upsert.component/address.upsert.component';
import { AddressDeleteComponent } from './address.delete.component/address.delete.component';
import {ActivatedRoute} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {EntityListConfig, SocketService, AddressModel, InfiniteScrollService, ModalService} from '../../shared';
import {UserDetailService} from '../../shared/services/userAddress.service';


@Component( {
  selector: 'user-detail-addresses',
  templateUrl: './address.component.html'
})
export class AddressComponent implements  OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  addresses: any;
  userID: string;
  addressesObj: any;
  lastFetchedItems: number;
  constructor(private modalService: ModalService,
               private route: ActivatedRoute,
               private socketService: SocketService,
               public entityListConfig: EntityListConfig,
               private userDetailService: UserDetailService,
              public infiniteScrollService: InfiniteScrollService) {
    // /*** websockets */
    // this.socketService.subscribeToEvent(`address:save`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllAddressForSockets());
    // this.socketService.subscribeToEvent(`address:remove`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllAddressForSockets());
    // /*** websockets */
    this.addressesObj = {};
    this.userID = this.route.parent.snapshot.data['user'];
    /*** websockets */
    this.socketService.subscribeToEvent(`address:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.savedFromSocket(data, this.addresses,this.addressesObj, this.entityListConfig);
      })
    this.socketService.subscribeToEvent(`address:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.infiniteScrollService.removedFromSocket(data, this.addresses,this.addressesObj,this.entityListConfig);
      })
    /*** websockets */
  }
  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllAddress(true);
  }

  onScroll(){
    this.getAllAddress(false);
  }
  addAddress() {
    const modalRef = this.modalService.open(AddressUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.address = new AddressModel;
    modalRef.componentInstance.userID = this.userID;
  }
  // getAllAddressForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.userDetailService
  //     .getAllAddress(this.userID['_id'], entityListConfig)
  //     .subscribe(
  //       (data) => {
  //         this.addresses = data;
  //         this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //         this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //         if (this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //       }
  //     );
  // }
  getAllAddress(refreshData) {
    const entityListConfig = new EntityListConfig();
    this.userDetailService
      .getAllAddress(this.userID['_id'], entityListConfig)
      .subscribe(
        (data) => {
          this.infiniteScrollService.getFromApi(data, this.addresses, this.addressesObj, this.entityListConfig, refreshData);
          // if(refreshData) this.addresses = [];
          // // remove last page entries if data less than ideal size
          // if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
          //   this.addresses.splice(
          //     (this.entityListConfig.params.pageNumber)
          //     *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          // }
          // this.lastFetchedItems = data.length;
          // if(data.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // // push new data
          // this.addresses = [...this.addresses,...data];
        }
      );
  }
  editAddress(address: AddressModel){
    const modalRef = this.modalService.open( AddressUpsertComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.address = address;
    modalRef.componentInstance.userID = this.userID;

  }
  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data => {
        this.addresses = data;
        this.infiniteScrollService.computeEntity(this.addresses,this.addressesObj, this.entityListConfig);
      } );
  }
  deleteAddress(address: any){
    const modalRef = this.modalService.open(AddressDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.userID = this.userID;
    modalRef.componentInstance.address = address;
  }
}
