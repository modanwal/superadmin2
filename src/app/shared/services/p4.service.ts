import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class P4Service {
  constructor (private apiService: ApiService) {}

  getAll(  entityListConfig:  EntityListConfig, appID, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/p4/app/${appID}/${pageNumber}/${pageSize}`, query, isSocket);
  }

  get(_id, appID, isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/p4/${_id}`, query, isSocket);
  }
  destroy(_id, appID): Observable<any> {
    return this.apiService.delete(`/p4/${_id}/app/${appID}/`);
  }

  save(formData, p4, appID): Observable<any> {
    // If we're updating an existing
    if (p4._id) {
      const p4Id = p4._id;
      delete p4._id;
      return this.apiService.putWithFormData(`/p4/${p4Id}/app/${appID}/` , formData);
      // Otherwise, create a new category
    } else {
      delete p4._id;
      return this.apiService.postWithFormData(`/p4/app/${appID}`, formData);
    }
  }
}
