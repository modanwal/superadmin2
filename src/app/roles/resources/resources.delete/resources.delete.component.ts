import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ResourceModel,ResourceService } from '../../../shared';

@Component({
  selector: 'resources-delete-content',
  templateUrl: './resources.delete.component.html'
})


export class ResourcesDeleteComponent {
  @Input() resource: ResourceModel;
  submitting: Boolean;

  constructor( private resourceService: ResourceService,public activeModal: NgbActiveModal , private modalService: NgbModal) {

  }
  delete(resource: ResourceModel){
    this.submitting = true;
    this.resourceService.destroy(resource._id).
      subscribe(data => {this.submitting = false;this.activeModal.close('resource:updated')})
  }


}
