import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import {P1Service , EntityListConfig , P1Model, SocketService, FileUploadService } from '../../../shared';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs/Subject";


@Component({
  selector: 'app-root',
  templateUrl: './p1.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})
export class P1Component implements OnInit {
  cropBounds: any;
  editMode: boolean;
  pageName: any;
  selectedComponent: any;
  nextComponent: any;
  appID: any;
  @ViewChild('title') title: ElementRef;
  p1Form: FormGroup;
  isSubmited: Boolean;
  formData: FormData;
  p1: P1Model;
  file: any;
  imagePreview: string;
  isSubmitting: boolean;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(
               private p1Service: P1Service ,
               private socketService: SocketService,
               private formBuilder: FormBuilder,
               public activeModal: NgbActiveModal,
               public entityListConfig: EntityListConfig,
               private route: ActivatedRoute,
               private fileUploadService: FileUploadService,
               private router: Router) {
                 this.imagePreview = '';
                 this.file = '';
                 this.editMode = false;
    /*** websockets */
    this.socketService.subscribeToEvent(`p1:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => this.initializeForm());
    /*** websockets */
  }
  ngOnInit() {
    // use FormBuilder to create a form group
    this.p1Form = this.formBuilder.group({
      '_id': [''],
      'title': ['', Validators.required]
    });
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {
          this.appID = data.appID;
          this.pageName = data.selectedComponent.pageName;
          this.p1 = data.p1Data;
          this.nextComponent = data.nextComponent;
          this.initializeForm();
        }
      );
  }
  initializeForm(){
    this.p1Form.controls['_id'].setValue(this.p1._id);
    this.p1Form.controls['title'].setValue(this.p1.title);
  }
  selectFile(event){
    if(!this.editMode) return;
    if(event.target.files[0]){
      const cropDimension = {
        width: 200,
        height: 200
      }
      const modalRef = this.fileUploadService.open();
      modalRef.componentInstance.file = event.target.files[0];
      modalRef.componentInstance.cropDimension = cropDimension;
      modalRef.result.then((data)=>{
        if(data){
            this.file = data.file;
            this.cropBounds = data.cropBounds;
            this.generateImagePreview(this.file);
        }
      })

    }
  }
  activateEditMode(){
    this.editMode = true;
    setTimeout(()=>{
      this.focusElement();
    })
  }
  focusElement(){
    this.title.nativeElement.focus();
  }
  generateImagePreview(file){
    const fileReader = new FileReader();
    fileReader.addEventListener('load', () => {
      this.imagePreview = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }
  submitForm( ) {
    this.isSubmited = true;
    if(this.p1Form.valid) {
      this.isSubmitting = true;
      this.p1Form.disable();
      const p1 = this.p1Form.value;
      this.formData = new FormData();
      this.formData.append('title', p1.title);
    //  this.formData.append('path','https://c1.staticflickr.com/6/5605/15502646231_b970ee7139_m.jpg');
      if (this.file){
        this.formData.append('file', this.file);
      }
      // if(this.cropBounds){
      //   this.formData.append('bounds',this.cropBounds);
      // }
      this.p1Service
        .save(this.formData, this.appID, p1._id)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.p1Form.enable();
            this.router.navigateByUrl(`application/${this.appID}/f1${this.nextComponent['componentID']['route']}`);
          },
        error=> {
          this.isSubmitting = false;
          this.p1Form.enable();
        });
    }
  }

}
