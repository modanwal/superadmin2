import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { AppService, F3Service, TemplateResourcePrivilegeService, RoleResourcePrivilegeService } from './../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class F3PrivilegeResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router,
               private f3Service: F3Service,
               private templateResourcePrivilegeService: TemplateResourcePrivilegeService,
               private roleResourcePrivilegeService: RoleResourcePrivilegeService) {
  }
 ngOnInit(){
 }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      const roleID = state['url'].split("/")[4];
      const appID = state['url'].split("/")[2];
      const observable1 = this.templateResourcePrivilegeService.getAppResourcePrivilege(roleID,false);
  //    const observable2 = this.roleResourcePrivilegeService.getRoleResourcePrivilege(roleID);
      return Observable.forkJoin(Observable.of(roleID), observable1);
  }
}

