
import { ComponentModel } from './component.model';
export class AppModel {
  _id: string;
  name: string;
  components: ComponentModel[];
}
