import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { User } from '../models';
import { UserService } from '../services';
import { ShowAuthedDirective } from './../show-authed.directive';

@Component({
  selector: 'layout-left-sidebar',
  templateUrl: './left-sidebar.component.html',

})
export class LeftSidebarComponent implements OnInit {
  url: any;
  currentUser: User;
    authenticated: Boolean;
    constructor(
      private userService: UserService,
      private router: Router) {
      this.router.events
          .filter(route => route instanceof NavigationEnd)
          .map(route => route['url'])
          .subscribe(
            (url) => {
              this.url = url.split('/');
              console.log(this.url,' vurbfirf---------------------')
            }
          );
  }
  getActiveRoute(route){
    if(!this.url) return false;
    if(route=='category'){
      if((this.url.indexOf(route) != -1)
        ||(this.url.indexOf('templates') != -1)
        ||(this.url.indexOf('template') != -1)
        ||(this.url.indexOf('componentstack') != -1)){
          return true;
        }else{
          return false;
        }
    }else{
      if(this.url.indexOf(route) != -1){
        return true;
      }else{
        return false;
      }
    }
  }
  ngOnInit() {
    this.userService.isAuthenticated.subscribe(authenticated => this.authenticated = authenticated )
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
  }
}
