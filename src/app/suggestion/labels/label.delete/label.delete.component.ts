import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {LabelService} from "../../../shared";
@Component({
  selector: 'label-delete',
  templateUrl: './label.delete.component.html'
})
export class SuggestionLabelDeleteComponent {
  @Input() label;
  isSubmited: Boolean;
  constructor( public activeModal: NgbActiveModal ,
               private labelService: LabelService
  ) {}
  deleteLabel(label: any) {
    this.labelService.destroy(label._id)
      .subscribe(
        data => this.activeModal.close('label:remove')
      );
  }
}
