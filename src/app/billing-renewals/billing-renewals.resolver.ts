import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve} from '@angular/router';
import { Observable } from 'rxjs/Rx';
import {  EntityListConfig, CartService } from '../shared';



@Injectable()
export class BillingRenewalsResolver implements Resolve<any> , OnInit {

  constructor ( private cartService: CartService) {
  }
  ngOnInit() {
  }
  resolve (route: ActivatedRouteSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    return this.cartService.getAll(entityListConfig);
  }
}
