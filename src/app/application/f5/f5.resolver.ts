import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { AppService, EntityListConfig, F5Service } from './../../shared';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx'

@Injectable()
export class F5Resolver implements Resolve<any>{
  routeData: Subscription;
  constructor (private appService: AppService,
               private router: Router,
               private f5Service: F5Service) {
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const appID = state['url'].split("/")[2];
    return this.f5Service.getAllF5(entityListConfig, appID, false).map((F5Data)=>{
      let data = {};
      data['appID'] = appID;
      data['F5Data'] = F5Data;
      return data;
    });
  }
}

