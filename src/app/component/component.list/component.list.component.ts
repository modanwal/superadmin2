import { Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { UserService, User , ComponentModel,EntityListConfig } from './../../shared';


@Component({
  selector: 'component-list',
  templateUrl: './component.list.component.html',
  encapsulation: ViewEncapsulation.None
})

export class ComponentListComponent {
  @Input() components: ComponentModel[];
  @Input() entityListConfig: any;
  @Output('editComponent') editComponentEmitter = new EventEmitter<ComponentModel>();
  @Output('deleteComponent') deleteComponentEmitter = new EventEmitter<ComponentModel>();
  @Output('viewComponent') viewComponentEmitter = new EventEmitter<ComponentModel>();

  @Output('setEntityConfig') setEntityConfigEmitter = new EventEmitter<any>();
  @Output('paginateListing') paginateListingEmitter = new EventEmitter<any>();



  constructor(private router: Router) {
  }

  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy==sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    }
    else{
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  setListSorting(event){
    this.setEntityConfigEmitter.emit({sortBy:event.sortBy,sortOrder:event.sortOrder});
  }
  onScroll(){
    this.paginateListingEmitter.emit();
  }


  ViewChildComponents(component: ComponentModel) {
      this.viewComponentEmitter.emit(component);
  }
  editComponent(component: ComponentModel){

      this.editComponentEmitter.emit(component);
  }
  deleteComponent(component: ComponentModel) {
    this.deleteComponentEmitter.emit(component);
  }
  viewComponent(component: ComponentModel) {
    this.viewComponentEmitter.emit(component);
  }


}
