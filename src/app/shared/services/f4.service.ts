import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class F4Service {
  constructor (private apiService: ApiService) {}

  getAll(appID, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/f4/app/${appID}`, query, isSocket);
    }

  save(f4, appId): Observable<any> {

    // If we're updating an existing component

      return this.apiService.put(`/f4/app/${appId}` , f4);
      // Otherwise, create a new category

  }

}
