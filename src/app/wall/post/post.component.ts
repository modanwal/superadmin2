import { Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, ViewChild, ElementRef, ViewChildren, QueryList, Renderer2, SimpleChanges, SimpleChange, OnChanges } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { EntityListConfig, CommentService, LikeService, InfiniteScrollService } from './../../shared';



@Component({
  selector: 'wall-post',
  templateUrl: './post.component.html',
  providers: [EntityListConfig]
})

export class WallPostComponent implements OnChanges {
  loadingComments: boolean;
  @ViewChild('comment') input: ElementRef;
  hideShowMore=false;
  addComment=false;
  submitting=false;
  lastFetchedItems: number;
  initialCommentsFetch = true;
  @Input() post: any;
  @Input() userData: any;
  @Output() postChange = new EventEmitter<any>();
  @Output('deletePost') deletePostEmitter = new EventEmitter<any>();
  //Output('hitLike') hitLikeEmitter = new EventEmitter<any>();


  constructor(private likeService: LikeService,
              private entityListConfig: EntityListConfig,
              private infiniteScrollService: InfiniteScrollService,
              private commentService: CommentService) {
    this.lastFetchedItems = 0;
    this.initialCommentsFetch = true;
  }
  ngOnChanges(changes: SimpleChanges) {
    const post: SimpleChange = changes.post;
    this.infiniteScrollService.computeEntity(post.currentValue.postID.comments,{},this.entityListConfig);

//    this.lastFetchedItems = (post.currentValue.postID.comments.length)%(this.entityListConfig.params.pageSize);
//    this.entityListConfig.params.pageNumber = Math.ceil(post.currentValue.postID.comments.length/this.entityListConfig.params.pageSize);
//    if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
    this.post = post.currentValue;
  }
  deletePost(post){
    this.deletePostEmitter.emit(post);
  }
  hitLike(postID,appID){
    this.likeService
      .saveLike(postID, appID)
      .subscribe();
    // this.hitLikeEmitter.emit({postID: postID,appID: appID});
  }
  postComment(comment, postID, appID){
    this.submitting = true;
    // this.postCommentEmitter.emit({comment: comment,postID: postID,appID: appID});
    let data = {};
    data['message'] = comment;
    this.commentService
      .saveComment(data,postID,appID )
      .subscribe(
        data => {
          this.input.nativeElement.value='';
          this.submitting = false;
        },
        error=> {
          this.submitting = false;
        }
      );
  }
  toggleCommentForm(){
    this.addComment=!this.addComment;
    this.focusElement();
  }
  eventHandler(event, comment, postID, appID ) {
    if(event.keyCode == 13)
    {
     this.postComment(comment, postID, appID);
    }
  }
  focusElement(){
    setTimeout(()=>{
      this.input.nativeElement.focus();
    },0)
  }
  loadMoreComments(postID,appID){
    this.loadingComments = true;
    // if(this.initialCommentsFetch){
    //   this.entityListConfig.params.pageNumber = 0;
    // }
    console.log('entityListconfig',JSON.parse(JSON.stringify(this.entityListConfig)));
    this.commentService
        .getCommentsForPost(postID,appID,this.entityListConfig)
        .subscribe(
          (data) =>{
            console.log('INitial comments!',JSON.parse(JSON.stringify(this.post['postID']['comments'])));
            console.log('initial comments fetch value!',this.initialCommentsFetch)
          if(this.initialCommentsFetch){
            //this.post['postID']['comments'] = [];
            this.initialCommentsFetch = false;
          }
          if(data.length<this.entityListConfig.params.pageSize){
            this.hideShowMore = true;
          }
          let postCommentObject = {};
          this.infiniteScrollService.computeEntity(this.post['postID']['comments'],postCommentObject,this.entityListConfig)
          this.infiniteScrollService.getFromApi(data,this.post['postID']['comments'],postCommentObject,this.entityListConfig);
          // // remove last page entries if data less than ideal size
          // if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
          //   this.post['postID']['comments'].splice(
          //     (this.entityListConfig.params.pageNumber)
          //     *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          // }
          // this.lastFetchedItems = data.length;
          // if(data.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
          // // push new data
          // this.post['postID']['comments'] = [...this.post['postID']['comments'],...data];
          this.loadingComments = false;
          //update post to the parent component
          this.postChange.emit(this.post);
          },
        error=>{this.loadingComments = false;}
        )
  }
}

