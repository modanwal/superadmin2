

import {Component, Input, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P4Model, P4Service} from '../../../../shared';



@Component({
  selector: 'p4-delete',
  templateUrl: './p4.delete.component.html',
  encapsulation: ViewEncapsulation.None

})
export class P4DeleteComponent {
  isSubmitting: boolean;
  @Input() p4: P4Model;
 @Input() appID;
  constructor(public activeModal: NgbActiveModal,
             private p4Service: P4Service) {
               this.isSubmitting = false;
  }
  delete (p4: P4Model) {
    this.isSubmitting = true;
    this.p4Service.destroy(p4._id, this.appID )
      .subscribe (
        data => {
          this.isSubmitting = false;
          this.activeModal.close ('P4_Delete');
        },
        error => this.isSubmitting = false 
      );
  }
}
