import { Component, Input, OnInit, ViewEncapsulation, OnChanges, SimpleChanges, SimpleChange, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
import {DTOService} from "../../shared/services/dto.service";

@Component({
  selector: 'cropped-image',
  templateUrl: './croppedimage.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [CropperSettings]
})
export class CroppedimageComponent implements  OnInit {
  cropBounds: Bounds;
  data: {};
  @Input() path;
  @Input() cropDimension;
  @Input() file;
  cropData: any;
  imagePreview;
  editMode: any;
  isSubmited: Boolean;
  formData: FormData;
  isSubmitting: boolean;
  cropperSetting;
  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;

  constructor(  public activeModal: NgbActiveModal,
                public cropperSettings: CropperSettings,
                private dtoService: DTOService
  ) {
    this.cropperSettings.noFileInput = true;
  }

  submitForm() {
      if(this.path){
        this.dtoService.setValue('imagePath', this.path);
        this.activeModal.close(this.path);
      }  if(this.file){
      this.activeModal.close({
        file: this.file,
        cropBounds: this.cropBounds
      });
    }
  }
  ngOnInit() {
    if(this.cropDimension){
        this.cropperSettings.width = this.cropDimension.width;
        this.cropperSettings.height = this.cropDimension.height;
        this.data = {};
    }
    let image:any = new Image();
    if(this.path && this.cropDimension) {
      image.crossOrigin = "anonymous";
      image.onload = ()=>{
        this.cropper.setImage(image);
      }
      image.src = this.path;
    }else if(this.file && this.cropDimension){
      let myReader:FileReader = new FileReader();
      let that = this;
      myReader.onloadend = function (loadEvent:any) {
          image.src = loadEvent.target.result;
          that.cropper.setImage(image);
      };
      myReader.readAsDataURL(this.file);
    }
  }
  cropped(bounds:Bounds){
    this.cropBounds = bounds;
  }
}
