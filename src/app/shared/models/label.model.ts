export class LabelModel {
  _id: string;
  name: string;
  description: string;
}
