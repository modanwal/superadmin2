import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import {EntityListConfig, TemplatesService} from '../../../shared';

@Injectable()
export class TemplatesResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private templatesService: TemplatesService){
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {

    const entityListConfig = new EntityListConfig();
    const id =  state['url'].split("/")[2];
    return this.templatesService.getAll( id, entityListConfig );
  }
}
