import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';
import { UploaderService } from './../uploader/uploader.service';


@Injectable()
export class PostService {
  constructor (private apiService: ApiService,private uploaderService: UploaderService) {}

  getTitle(appID,isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/wall/app/${appID}`, query, isSocket);
  }
  getAppName( isSocket?){
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/app/0/-1`, query, isSocket);
  }
  getAll(entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/post/all/${pageNumber}/${pageSize}`,query,isSocket);
    //return this.apiService.get(URLRESOLVER.GET_ALL_POSTS(pageNumber,pageSize), query, isSocket);
  }
  get( isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/post/all`, query, isSocket);
  }
  destroy(post): Observable<any>{
    return this.apiService.delete(`/post/${post.postID._id}/app/${post.appID._id}` );
  }
  saveFormData(postFormData, appID): Observable<any> {
    // If we're updating an existing component
      return this.apiService.postWithFormData(`/post/app/${appID}`, postFormData);
  }
  saveFormDataToUploader(postFormData, appID){
    //this.uploaderService.uploadItem(`/post/app/${appID}`)
  }

}
