import { Injectable, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, ComponentService, EntityListConfig } from '../shared';

@Injectable()
export class ComponentResolver implements Resolve<any> {

  constructor(private componentService: ComponentService ,
              private router: Router) {}

resolve(route: ActivatedRouteSnapshot,
       state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const id = route.params['id'];
   const obs1 =  this.componentService.getAll(id, entityListConfig);
   const obs2 = this.componentService.getResourceName(entityListConfig);

    return Observable.forkJoin(obs1, obs2);
  }
}
