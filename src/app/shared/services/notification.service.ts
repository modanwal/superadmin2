/**
 * Created by life on 8/6/2017.
 */

import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class NotificationService {
  constructor (private apiService: ApiService) {}

  getAll(entityListConfig:  EntityListConfig,appID,isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/notification/app/${appID}/${pageNumber}/${pageSize}`, query, isSocket);
  }

}
