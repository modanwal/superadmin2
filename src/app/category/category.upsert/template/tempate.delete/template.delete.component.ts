import { Component, Input } from '@angular/core';
import { TemplateModel, TemplateService } from '../../../../shared';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component( {
  selector: 'template-del-group',
  templateUrl: './template.delete.component.html'
})

export class TemplateDeleteComponent {
  isSubmitting: boolean;
  @Input() templateComponent: any;
  @Input() templateID: any;
  constructor(public activeModal: NgbActiveModal,private templateService: TemplateService){

  }
  delete(){
    this.isSubmitting = false;
    this.templateService
        .deleteComponent(this.templateComponent._id, this.templateID)
        .subscribe(
          (data) => {
            this.isSubmitting = false;
            this.activeModal.close('template-component:deleted')
          },
          (error)=> {
            this.isSubmitting = false;
            this.activeModal.close();
          }
        )
  }
}
