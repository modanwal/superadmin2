import { Component } from '@angular/core';


@Component( {
  selector: 'workbench-page2',
  templateUrl: './workbench2.component.html'
})
export class WorkbenchComponent2 {
    items1 = [
        {
          name: 'testing',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing2',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing3',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing4',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        }
      ];
    items2 = [
        {
          name: 'testing',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing2',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        }
      ];
    items3 = [
        {
          name: 'testing',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing2',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing3',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing4',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        }
      ];
    items4 = [
        {
          name: 'testing',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing2',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing3',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing4',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        }
      ];
    items5 = [
        {
          name: 'testing',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing2',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing3',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        },
        {
          name: 'testing4',
          path: 'https://s3-us-west-2.amazonaws.com/makemeproud/app/22858630-866e-11e7-9969-3933c0164d18.jpg'
        }
      ];
  constructor () {


  }
   itemDropped(event){
     console.log('itemDropped;;;',event);
   }
}
