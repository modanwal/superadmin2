
import { Component, OnInit} from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import {GiphyService} from "../shared/services/giphy.service";

@Component({

  selector: 'giphy-app',
  templateUrl: 'giphy.component.html'
})
export class GiphyComponent {
  title = 'Welcome to GiphySearch';
  link = 'http://api.giphy.com/v1/gifs/search?api_key=dc6zaTOxFJmzC&q=';
  http: Http;
  giphies = [];

  constructor(http: Http, private giphyService: GiphyService) {
    this.http = http;
  }

  performSearch(searchTerm: HTMLInputElement){
    this.giphyService
      .getImages(searchTerm)
      .subscribe(
        (data)=>{
          console.log(data);
          this.giphies = data;
          console.log(this.giphies);
        });
  }

  // performSearch(searchTerm: HTMLInputElement): void {
  //   var apiLink = this.link + searchTerm.value;
  //
  //   this.http.request(apiLink)
  //     .subscribe((res: Response) => {
  //       this.giphies = res.json().data;
  //       console.log(this.giphies);
  //     });
  // }
}
