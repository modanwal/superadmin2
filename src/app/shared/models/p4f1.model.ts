export class P4F1Model {
  _id: string;
  accomodation: string;
  accept: string;
  p4ID: string;
  userID: string;
  appID: string;
}