import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, AppService } from '../shared';
import { Subscription } from 'rxjs/Subscription';
import {EntityListConfig} from "../shared/models/entity-list-config.model";

@Injectable()
export class ApplicationResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router) {
  }
 ngOnInit(){
 }
  // resolve (route: ActivatedRouteSnapshot,
  //          state: RouterStateSnapshot): Observable<any> {
  //     const appID = route.params['id'];
  //     return this.appService.getAppFeatures(appID,false).map((appComponents)=>{
  //       let data = {};
  //       data['appID'] = appID;
  //       console.log(data['appID'],'++++++++++++++++++coming');
  //       data['appComponents'] = appComponents;
  //       console.log(data['appComponents'],'++++++++++++++++++');
  //       return data;
  //     });
  // }

  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const appID = route.params['id'];
    const entityListConfig = new EntityListConfig();
    delete entityListConfig.query.sortBy;
    delete entityListConfig.query.order;
    entityListConfig.query['type'] = 'F';
    const obs1 =  this.appService.getAppFeatures(appID, entityListConfig , false).map((appComponents)=> {
      let data = {};
      data['appID'] = appID;
      data['appComponents'] = appComponents;
      console.log(appComponents);
      return data;
    });
    const obs2 = this.appService.getSingleApp(appID);

    return Observable.forkJoin(obs1, obs2);
  }
}

