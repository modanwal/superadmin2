/**
 * Created by rahul on 7/7/2017.
 */
export class AppStatsModal {
  _id: string;
  status: string;
  total: number;
  android: number;
  ios: number;
}
