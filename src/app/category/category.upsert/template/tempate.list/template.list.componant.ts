import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';


@Component( {
  selector: 'template-list-group',
  templateUrl: './template.list.component.html'
})


export class TemplateListComponent {

  @Input() entityListConfig: any;
  @Input() templateComponents: any;
  @Output('editComponent') editComponentEmitter = new EventEmitter<any>();
  @Output('deleteComponent') deleteComponentEmitter = new EventEmitter<any>();

  @Output('setEntityConfig') setEntityConfigEmitter = new EventEmitter<any>();

  constructor (private router: Router){

  }
  getListSortData(sortBy: string) {
    if(this.entityListConfig.query.sortBy === sortBy) {
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  editComponent(component: any){
    this.editComponentEmitter.emit(component);
  }
  setListSorting(event){
    this.setEntityConfigEmitter.emit({sortBy: event.sortBy, sortOrder: event.sortOrder});
  }

  deleteComponent(component: any){
    this.deleteComponentEmitter.emit(component);
  }

}
