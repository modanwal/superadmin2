import { RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/core';
import { NgModule } from '@angular/core';
import { SharedModule } from './../../shared';
import { LiveStreamingComponent } from '../live-streaming/live-streaming.component';
import { StreamingHistoryComponent } from '../live-streaming/streaming-history/streaming-history.component';
export const livestreamingRouting = [

  {
     path: 'live-streaming',
        component: LiveStreamingComponent,
        children: [
          {
            path: 'streaming-history',
            component: StreamingHistoryComponent
          }
        ]
      }
    ];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    LiveStreamingComponent,
    StreamingHistoryComponent,
   
  ],
  providers: [
  ]
})
export class LiveStreamingModule {}
