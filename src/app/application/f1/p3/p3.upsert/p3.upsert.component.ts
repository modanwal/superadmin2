import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {P3Service} from '../../../../shared';

@Component({
  selector: 'app-root',
  templateUrl: './p3.upsert.component.html'
})
export class P3UpsertComponent implements OnInit {
  invalidName: boolean;
  isSubmited: boolean;
  isSubmitting: boolean;
  @Input() p3;
  @Input() appID;
  P3Form: FormGroup;
  constructor(private formBuilder: FormBuilder ,
              private modalService: NgbModal,
              private p3Service: P3Service,
              public activeModal: NgbActiveModal) {
                this.invalidName = true;
    }
    ngOnInit(){
      this.P3Form = this.formBuilder.group({
        '_id': [this.p3._id],
        'name': [this.p3.name, Validators.required]
      });
    }
  checkNameNotChanged(event){
    this.invalidName = false;
    (event.target.value==this.p3.name) && (this.invalidName=true);
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm(){
    this.isSubmited = true;
      if (this.P3Form.valid && !this.invalidName) {
        this.isSubmitting = true;
        this.P3Form.disable();
        const p3 = this.P3Form.value;
        this.p3Service
          .save(p3, this.appID )
          .subscribe(
            data => {
              this.isSubmitting = false;
              this.P3Form.enable();
              this.activeModal.close('P3_Updated');
            },
          error => {
            this.isSubmitting = false;
          });
      }
    }
}
