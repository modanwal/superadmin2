import { Component, OnInit} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SuggestionDataTypeUpsertComponent} from "./data-type.upsert/data-type.upsert.component";
import { EntityListConfig, TypeService, DTOService} from "../../shared";
import {SuggestionDataTypesModel} from "../../shared/models/suggestion-datatypes.model";
import {ActivatedRoute, Router} from "@angular/router";
import {Subject} from "rxjs/Subject";
import {SuggestionDataTypeDeleteComponent} from "./data-types.delete/data-type.delete.component";
@Component({
  selector: 'suggestion-datatypes',
  templateUrl: 'data-types.component.html',
  providers: [EntityListConfig]
})
export class SuggestionDataTypesComponent implements OnInit {
  dataTypes: any[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor( private modalService: NgbModal,
               private typeService: TypeService,
               private route: ActivatedRoute
              ) {
  }
  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        (val) => {
          this.dataTypes = val['data'];
        });
  }
  onScroll(){
  }
  getAllType(){
    const entityListConfig = new EntityListConfig();
    this.typeService
      .getAll(entityListConfig)
      .subscribe(
        (data)=>{
          this.dataTypes = data;
        });
  }
  addDataTypes(){
    const modalRef = this.modalService.open(SuggestionDataTypeUpsertComponent);
    modalRef.componentInstance.dataType = new SuggestionDataTypesModel();
    modalRef.result.then((closeReason) => {
      if (closeReason == 'type:updated'){
        this.getAllType();
      }
    });

  }
  editDatatypes(dataType: any){
    const modalRef = this.modalService.open(SuggestionDataTypeUpsertComponent);
    modalRef.componentInstance.dataType = dataType;
    modalRef.result.then((closeReason) => {
      if (closeReason == 'type:updated'){
        this.getAllType();
      }
    });
  }
  deleteDataTypes(dataType: any){
    const modalRef = this.modalService.open(SuggestionDataTypeDeleteComponent);
    modalRef.componentInstance.dataType = dataType;
    modalRef.result.then((closeReason) => {
      if (closeReason == 'type:remove'){
        this.getAllType();
      }
    });
  }

}
