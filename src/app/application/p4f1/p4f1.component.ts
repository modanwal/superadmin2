import { Component, ViewEncapsulation,OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { P4Service,P4F1Service,P4Model } from './../../shared';
import { Subject } from "rxjs/Subject";


@Component({
  selector: 'p4f1-component',
  templateUrl: './p4f1.component.html',
  encapsulation: ViewEncapsulation.None
})

export class P4f1Component implements OnInit {
  appID: any;
  p4Data: P4Model[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private route: ActivatedRoute, private p4Service: P4Service,private p4f1Service: P4F1Service) {

  }
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data =>{
         this.p4Data = data['p4Data'];
         this.appID = data['appID'];
      });
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
