import {Injectable} from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from  './../../shared/modal.service';
import { ApiService } from './../services/api.service';
import { BehaviorSubject } from "rxjs";
import { Observable } from "rxjs/Rx";


@Injectable()
export class UploaderService {
  uploadSubscriptions: any;
  uploadItems: any[];
  private subject = new BehaviorSubject<any>([]);
  constructor(private modalService: ModalService, private apiService: ApiService) {
    this.uploadItems = [];
    this.uploadSubscriptions = {};
    this.subject.next(this.uploadItems);
  }
  public uploadItem(uploadUrl,formData,uploadMeta?){
    if(!uploadMeta){
      uploadMeta={};
    }
    let defaultData = {
      itemType: 'File',
      dateTime: new Date(),
      title: 'some good title goes here',
      progress: '0',
      error: false,
      completed: false,
      uuid: new Date().valueOf()
    };
    uploadMeta = Object.assign(uploadMeta,defaultData);
    const index = this.uploadItems.push(uploadMeta);
    const subscription = this.apiService
                              .postWithProgressFormData(uploadUrl,formData,this.onProgress(index-1))
                              .subscribe(
                                      (data) => {},
                                      this.onError(index-1)
                                );
    this.uploadSubscriptions[uploadMeta['uuid']] = subscription;
    this.subject.next(this.uploadItems);
  }
  private onProgress(index){
    return (progress)=>{
      this.uploadItems[index].progress = progress.percentage;
      if(progress.percentage == 100){
        setTimeout(()=>{
          this.uploadItems[index].completed = true;
        },1000);
      }
      this.subject.next(this.uploadItems);
    }
  }
  private onError(index){
    console.log('onError called!')
    const that = this;
    return function(error){
      that.uploadItems[index].error = true;
      that.subject.next(that.uploadItems);
    }
  }
  public getUploadItems(){
    return this.subject;
  }
  public cancelFileUploads(){
    this.uploadItems.forEach((uploadItem,index)=>{
      if(uploadItem.progress!='100'){
        this.uploadSubscriptions[uploadItem['uuid']].unsubscribe();
        this.uploadItems[index].error = true;
      }
    })
    this.subject.next(this.uploadItems);
  }
}
