import { environment } from '../../../environments/environment';
const API_URL = environment.api_url;
const SOCKET_URL = environment.socket_url;
const AUTH_URL = environment.auth_url;

/**
 * POST SERVICE
 */
export var GET_ALL_POSTS = (pageNumber,pageSize) => `${API_URL}/post/all/${pageNumber}/${pageSize}`;
export var POST_WALL = (appID) => `${API_URL}/post/app/${appID}`;