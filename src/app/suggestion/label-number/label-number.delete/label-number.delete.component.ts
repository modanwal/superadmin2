import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {CcldService} from '../../../shared';
@Component({
  selector: 'labelnumber-delete',
  templateUrl: './label-number.delete.component.html'
})

export class LabelNumberDeleteComponent {
  @Input() labelNumbers;
  isSubmited: Boolean;
  constructor( public activeModal: NgbActiveModal ,
               private modalService: NgbModal,
               private ccldService: CcldService
  ) {}
  deleteFile(labelNumber: any) {
    this.activeModal.close();
    this.ccldService.destroy(labelNumber._id)
      .subscribe(
        data => this.activeModal.close('labelNumber:remove')
      );
  }
}
