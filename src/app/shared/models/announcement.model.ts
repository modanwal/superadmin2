
export class AnnouncementModel {
  _id: string;
  date: string;
  message: string;
  severity: string;
  receiver: string;
}

