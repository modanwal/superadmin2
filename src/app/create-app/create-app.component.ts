import { Component, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'create-app-page',
  templateUrl: './create-app.component.html',
  encapsulation: ViewEncapsulation.None
})

export class CreateAppComponent {
  constructor(private router: Router) {
    
  }
  goToStep(stepUrl){
    this.router.navigateByUrl(`create-app/${stepUrl}`);
  }
}
