/**
 * Created by rahul on 7/6/2017.
 */
export class AllUserModel {
  _id: string;
  name: string;
  email: string;
  mobile: string;
  application: string;
  address: string;
}
