import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BreadCrumbService } from "./../../../shared/breadcrumb.module";

@Component({
  selector: 'has-template',
  templateUrl: './hasTemplate.component.html',
  encapsulation: ViewEncapsulation.None

})
export class HasTemplateComponent {

  @Input () category: any;
  constructor(private router: Router,
              private route: ActivatedRoute,
              public activeModal: NgbActiveModal,
              private breadCrumbService: BreadCrumbService) {

  }
  addCategory(category: any){
    this.activeModal.close();
    this.breadCrumbService
        .pushBreadCrumbItem({url:'/category',queryParams:{id: category._id},label:category.name},false);
    this.router.navigate(['category'], { queryParams: { id: category._id } });
  //  this.router.navigateByUrl(`/category/${this.category._id}`);
  }
  addTemplates(category: any){
    this.activeModal.close();
   this.router.navigateByUrl(`/templates/${this.category._id}`);

  }
}
