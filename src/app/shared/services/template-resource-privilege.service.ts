import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class TemplateResourcePrivilegeService {

  constructor (
    private apiService: ApiService
  ) {}


  getTemplateResourcePrivilege(templateID, isSocket): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/template-resource-privilege/template/${templateID}`, query, isSocket);
  }
  getTemplateresourcePrivilegeRole(roleID, isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/template-resource-privilege/role/${roleID}`, query, isSocket);
  }
  getAppResourcePrivilege(roleID, isSocket?): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/template-resource-privilege/role/${roleID}`, query, isSocket);
  }

  destroy(templateID, resourceID, privilegeID) {
    return this.apiService.delete(`/template-resource-privilege/${templateID}/${resourceID}/${privilegeID}`);
  }

  save(entity): Observable<any> {
    return this.apiService.post(`/template-resource-privilege/`, entity);
  }
  editPrivilege(privilege) : Observable<any> {

    return this.apiService.put(`/template-resource-privilege`, privilege);
  }
  selectAllCheckBoxSave(data) : Observable<any> {

    return this.apiService.post(`/template-resource-privilege/all`, data);
  }
  unSelectAllCheckBoxSave(templateID, resourceID): Observable<any>{

    return this.apiService.delete(`/template-resource-privilege/${templateID}/${resourceID}`);

  }
}
