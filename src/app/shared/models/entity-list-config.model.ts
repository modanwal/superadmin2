export class EntityListConfig {
  params: {
    pageNumber?: number,
    pageSize?: number
  } = {
    pageNumber: 0,
    pageSize: 10
  };
  query: {
    sortBy?: String,
    order?: any
  } = {
    sortBy: 'createdOn',
    order: -1
  };
  remainingItems: number = 0
}
