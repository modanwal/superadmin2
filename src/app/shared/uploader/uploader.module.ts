import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { ImageCropperModule } from 'ng2-img-cropper';
import { UploaderService } from "./uploader.service";
import { UploaderComponent } from "./uploader.component";


@NgModule({
  imports: [NgbModule,CommonModule],
  exports: [UploaderComponent],
  declarations: [UploaderComponent],
  providers: [UploaderService]
})


export class UploaderModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UploaderModule,
      providers: [UploaderService]
    };
  }
}
