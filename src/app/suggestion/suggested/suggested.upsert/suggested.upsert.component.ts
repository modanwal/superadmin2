import {Component, Input, OnInit} from '@angular/core';
import {EntityListConfig} from '../../../shared/models/entity-list-config.model';
import {CcltService} from '../../../shared';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CcldService} from '../../../shared/services/ccld.service';
import {ModalService} from '../../../shared/modal.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'suggested-upsert',
  templateUrl: 'suggested.upsert.component.html'
})
export class SuggestedUpsertComponent implements OnInit{
  @Input() categories;
  @Input() mode;
  @Input() suggested;
  labels: any;
  setCategoryID: any;
  components: any;
  suggestedForm: FormGroup;
  isSubmited: Boolean;
  isSubmitting: boolean;
  constructor(private ccltService: CcltService,
              private formBuilder: FormBuilder,
              private ccldService: CcldService,
              public activeModal: NgbActiveModal){
    this.isSubmited = false;
    this.isSubmitting = false;

  }
  submitForm(){
    this.isSubmited = true;
    if (this.suggestedForm.valid) {
      this.isSubmitting = true;
      const suggested = this.suggestedForm.value;
      suggested['dataID'] = this.mode;
      this.suggestedForm.disable();
      this.ccldService
        .save(suggested)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.activeModal.close('labelType:updated');
          },
          error => {
            this.isSubmited = false;
            this.suggestedForm.enable();
          });
    }
  }
  eventHandler(event){

  }
  getComponent(event){
    this.setCategoryID = event.target.value;
    const entityListConfig = new EntityListConfig();
    entityListConfig.params.pageSize = -1;
    this.ccltService
      .getAllComponent(this.mode, event.target.value, entityListConfig)
      .subscribe(
        (data)=>{
          this.components = data;
        });
  }
  getLabel(event){
    const entityListConfig = new EntityListConfig();
    entityListConfig.params.pageSize = -1;
    this.ccltService
      .getAllLabel(this.mode, this.setCategoryID ,event.target.value, entityListConfig)
      .subscribe(
        (data)=>{
          this.labels = data;
        });
  }
  setComponent(event){
    console.log('getting setComponent', event.target.value);
  }
  ngOnInit(){
    this.suggestedForm = this.formBuilder.group({
      'categoryID': [this.suggested.categoryID, Validators.required],
      'componentID': [this.suggested.componentID, Validators.required],
      'labelID': [this.suggested.labelID, Validators.required]
    });
  }
}
