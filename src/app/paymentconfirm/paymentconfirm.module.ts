import { SharedModule, AuthGuard } from '../shared';
import {NgModule} from "@angular/core";
import {PayementconfirmComponent} from "./payementconfirm.component";
import {InvoicesService} from "../shared/services/invoices.service";
import {DTOService} from "../shared/services/dto.service";



export const paymentconfirmation = [
  {
    path: 'paymentconfirm',
    component: PayementconfirmComponent,
  }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [PayementconfirmComponent

  ],
  providers: [AuthGuard, InvoicesService, DTOService]

})
export class PaymentconfirmModule {}
