import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {DataService} from "../../../shared";
@Component({
  selector: 'files-delete',
  templateUrl: './files.delete.component.html'
})

export class FilesDeleteComponent {
  @Input() files;
  isSubmited: Boolean;
  constructor( public activeModal: NgbActiveModal ,
               private dataService: DataService
  ) {}
  deleteFile(files: any) {

    this.dataService.destroy(files._id)
      .subscribe(
        data => this.activeModal.close('files:remove')
      );
  }
}
