import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Component, OnInit, ViewEncapsulation, Input, Output, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { UserService, User, ComponentModel, ComponentService, EntityListConfig, SocketService, InfiniteScrollService, ModalService } from '../shared';
import { NgbdModalUpsertContent} from './component.upsert/component.upsert.component';
import { NgbdModalDeleteContent} from './component.delete/component.delete.component';
import { BreadCrumbService } from './../shared/breadcrumb.module';
import { Subject } from 'rxjs/Subject';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'component-page',
  templateUrl: './component.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})

export class ComponentComponent implements OnInit, OnDestroy {
  lastFetchedItems: number;
  authenticated: Boolean;
  fetchingData: boolean;
  collectionDirty: boolean;
  componentID: string;
  neverFetch: boolean;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  @Output() components: any;
  resourceName: any;
  componentsObj: any;
  componentFetchSub: Subscription;
  constructor(private modalService: ModalService,
              private userService: UserService,
              private componentService: ComponentService,
              private route: ActivatedRoute,
              private router: Router,
              public entityListConfig: EntityListConfig,
              private socketService: SocketService,
              private breadCrumbService: BreadCrumbService,
              public infiniteScrollService: InfiniteScrollService) {
    this.collectionDirty = false;
    this.componentsObj = {};
    this.components = [];
    this.lastFetchedItems = 0;

    // /*** websockets */
    // this.socketService.subscribeToEvent(`component:save`)
    //   .subscribe(data => this.getAllComponentsForSockets())
    //
    // this.socketService.subscribeToEvent(`component:remove`)
    //   .subscribe(data => this.getAllComponentsForSockets())
    // /*** websockets */

    /*** websockets */
    this.socketService.subscribeToEvent(`component:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.components,this.componentsObj, this.entityListConfig);
        if(this.fetchingData){
          this.componentFetchSub.unsubscribe();
          this.getAllComponents(false);
        }
      })
    this.socketService.subscribeToEvent(`component:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.components,this.componentsObj,this.entityListConfig);
        if(this.fetchingData){
          this.componentFetchSub.unsubscribe();
          this.getAllComponents(false);
        }
      })
    /*** websockets */

    router.events
          .filter(event => event instanceof NavigationEnd)
          .filter(event => event['url'].indexOf('/component')!=-1)
          .takeUntil(this.ngUnsubscribe)
          .subscribe((routeData) => {
          this.componentID = routeData['url'].substring(11);
          if(!this.componentID){
            this.componentID='';
            this.breadCrumbService.pushBreadCrumbItem({url:'/component',params:this.componentID,label:'Component'},true);
          }
          //this.getAllComponents(true);
      });
  }
  ngOnInit(){
    this.componentID = this.route.snapshot.params.id;
    this.route.data
        .takeUntil(this.ngUnsubscribe)
        .map(data => data.data)
        .subscribe(data => {
          console.log(data);
          this.components = data[0];
          console.log(this.components);
          this.resourceName = data[1];
          console.log(this.resourceName);
          this.infiniteScrollService.computeEntity(this.components,this.componentsObj, this.entityListConfig);
          // this.lastFetchedItems = this.components.length;
          // if(this.components.length==this.entityListConfig.params.pageSize){
          //   this.entityListConfig.params.pageNumber++;
          // }
        });
  }
  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  setEntityConfig(event){
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy =event.sortBy;
    this.entityListConfig.query.order =event.sortOrder;
    this.getAllComponents(true);

  }

  paginateListing(){
    this.getAllComponents(false);
  }

  getAllComponents(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
  this.componentsObj =  this.componentService
      .getAll(this.componentID, this.entityListConfig)
      .subscribe(
        data =>{
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllComponents(false);
          }else {
            this.infiniteScrollService.getFromApi(data, this.components, this.componentsObj, this.entityListConfig, refreshData);
          }

        }
        //   if(refreshData) this.components = [];
        //   // remove last page entries if data less than ideal size
        //   if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
        //     this.components.splice(
        //       (this.entityListConfig.params.pageNumber)
        //       *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
        //   }
        //   this.lastFetchedItems = data.length;
        //   if(data.length==this.entityListConfig.params.pageSize){
        //     this.entityListConfig.params.pageNumber++;
        //   }
        //   // push new data
        //   this.components = [...this.components,...data];
        // }
      ),(error)=>{this.fetchingData = true;};
  }

  // getAllComponentsForSockets(){
  //  const entityListConfig = new EntityListConfig();
  //  entityListConfig.query = this.entityListConfig.query;
  //  entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //  entityListConfig.params.pageNumber = 0;
  //  this.componentService
  //     .getAll(this.componentID, entityListConfig, true)
  //     .subscribe(data =>{
  //       this.components = data;
  //       this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //       this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //       if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //     });
  // }


  viewComponent(component: ComponentModel) {
    this.componentID = component._id;
    this.breadCrumbService.pushBreadCrumbItem({url: '/component', params:this.componentID,label:component.resourceID.name},false);
    this.router.navigateByUrl(`/component/${this.componentID}`);
  }

  deleteComponent(component: ComponentModel){
      const modalRef = this.modalService.open(NgbdModalDeleteContent, {},'fade-in-pulse','fade-out-pulse');
      modalRef.componentInstance.component = component;
  }
  editComponent(component: ComponentModel){
    const modalRef = this.modalService.open(NgbdModalUpsertContent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.component = component;
    modalRef.componentInstance.resourceName = this.resourceName;
  }
  addComponent(){
      const modalRef = this.modalService.open(NgbdModalUpsertContent, {},'fade-in-pulse','fade-out-pulse');
      modalRef.componentInstance.component = new ComponentModel;
      modalRef.componentInstance.parentID = this.componentID;
      modalRef.componentInstance.resourceName = this.resourceName;

    }

}
