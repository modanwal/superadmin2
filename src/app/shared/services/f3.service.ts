import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { F3Model, EntityListConfig } from '../../shared/models';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class F3Service {

  constructor (
    private apiService: ApiService, private toastrService: ToastrService
  ) {}

  get(_id, isSocket): Observable<F3Model> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get ( `/f3/${_id}`, query,  isSocket );
  }

  getAllF3(entityListConfig: EntityListConfig, appID, isSocket?): Observable<F3Model[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/f3/app/${appID}/role/${pageNumber}/${pageSize}/`, query, isSocket);
  }
  destroy(_id,appID) {
    return this.apiService.delete(`/f3/app/${appID}/role/${_id}`);
  }

  saveF3(f3,appID): Observable<F3Model> {
    if (f3._id) {
      const _id = f3._id;
      delete f3._id;
      return this.apiService.putWithFormData(`/f3/app/${appID}/role/${_id}`, f3);
    } else {
      delete f3._id;
      return this.apiService.postWithFormData(`/f3/app/${appID}/role`, f3);
    }
  }
}
