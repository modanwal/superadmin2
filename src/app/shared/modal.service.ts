import { Injectable, NgModule, RenderComponentType } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from "@angular/common";
declare var jQuery;

@Injectable()
export class ModalService {
    constructor (private ngbModal: NgbModal) {}

    open (content: any, config?: any , animationStart? : any,animationEnd? : any,focus=true) {
        let modal = this.ngbModal.open(content, config);
        modal.componentInstance.selfReference = modal['_windowCmptRef'].instance._elRef.nativeElement;
        let instance = (modal as any)._windowCmptRef.instance;
        if(animationStart){
            instance.windowClass = animationStart;
        }
        if(animationEnd){
            let fx = (modal as any)._removeModalElements.bind(modal);
            (modal as any)._removeModalElements = () => {
                instance.windowClass = animationEnd;
                setTimeout(fx, 250)
            }
        }
        if(focus){
             let formChildren = [].slice.call(modal['_windowCmptRef'].instance._elRef.nativeElement.children);
             formChildren.every(child => {
                 let input = this._getInputElement(child);
                 if (input) {
                     input.focus();
                     //this.renderer.invokeElementMethod(input, 'focus', []);
                     return false; // break!
                 }
                 return true; // continue!
             });
        }
        return modal
    }
    openOverlayModal(content: any, config?: any, animationStart? : any,animationEnd? : any){
      if(!config){
        config = {};
      }
        config.windowClass = 'gradient-overlay overlay-modal';
        let modal = this.ngbModal.open(content, config);
        let instance = (modal as any)._windowCmptRef.instance;

        if(animationStart){
            instance.windowClass =instance.windowClass+' '+animationStart;
        }
        if(animationEnd){
            let fx = (modal as any)._removeModalElements.bind(modal);
            (modal as any)._removeModalElements = () => {
                instance.windowClass = instance.windowClass+' '+animationEnd;
                setTimeout(fx, 250)
            }
        }
        if(focus){
             let formChildren = [].slice.call(modal['_windowCmptRef'].instance._elRef.nativeElement.children);
             formChildren.every(child => {
                 let input = this._getInputElement(child);
                 if (input) {
                     input.focus();
                     //this.renderer.invokeElementMethod(input, 'focus', []);
                     return false; // break!
                 }
                 return true; // continue!
             });
        }
        return modal;
    }
    private _getInputElement(nativeElement: any): any {
        if (!nativeElement || !nativeElement.children) return undefined;
        if (!nativeElement.children.length && nativeElement.localName === 'input' && !nativeElement.hidden) return nativeElement;
        
        let input;
        
        [].slice.call(nativeElement.children).every(c => {
        input = this._getInputElement(c);
        if (input) return false; // break
        return true; // continue!
        });

        return input;
    }
}