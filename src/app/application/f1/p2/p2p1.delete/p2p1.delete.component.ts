

import {Component, Input, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P2P1Model} from '../../../../shared/models/p2p1.model';
import {P2Service} from '../../../../shared/services/p2.service';

@Component({
  selector: 'app-root',
  templateUrl: './p2p1.delete.component.html',
  encapsulation: ViewEncapsulation.None

})
export class P2P1DeleteComponent {
  @Input() p2p1;
  @Input() appID;
  @Input() p2ID;
  isSubmitting: Boolean;
  constructor(public activeModal: NgbActiveModal, private p2Service: P2Service) {

  }
  deletep2p1 (p2p1: P2P1Model) {
    this.isSubmitting = true;
    this.p2Service.destroyp2p1(p2p1._id , this.appID, this.p2ID)
      .subscribe (
        data => {
          this.isSubmitting = false;
          this.activeModal.close ('P2P1_Deleted')
        },
        error => this.isSubmitting = false
      );
  }
}
