import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { UserService, AppService, EntityListConfig, P2Service } from './../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P2Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router,
               private p2Service: P2Service,
               ) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const appComponents = route.parent.data.data.appComponents;

    const selectedComponent = appComponents.find(component=> component.componentID.route == '/p2');
    const nextComponent = appComponents[appComponents.indexOf(selectedComponent)+1]
      const appID = state['url'].split("/")[2];
    return this.p2Service.getAllp2(entityListConfig, appID, false).map(
      (p2Data) =>{
        let data = {};
        data['appID'] = appID;
        data['selectedComponent'] = selectedComponent;
        data['nextComponent'] = nextComponent;
        data['p2Data'] = p2Data;
        return data;
      }
    );
  }
}

