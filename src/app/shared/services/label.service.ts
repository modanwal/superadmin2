import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class LabelService {
  constructor (private apiService: ApiService) {}

  getAll( entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/label/${pageNumber}/${pageSize}`, query, isSocket);
  }

  destroy(labelID): Observable<any>{

    return this.apiService.delete(`/label/${labelID}` );
  }

  save(label): Observable<any> {
    // If we're updating an existing component
    if (label._id) {
      const labelID = label._id;
      delete label._id;
      return this.apiService.put(`/label/${labelID}`, label);

      // Otherwise, create a new category
    } else {
      delete label._id;
      return this.apiService.post(`/label/`, label);
    }
  }
}
