import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';


import { Subscription } from 'rxjs/Subscription';
import {EntityListConfig, AppService} from "../shared";



@Injectable()
export class WorkbenchResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor ( private appService: AppService) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    delete entityListConfig.query.sortBy;
    delete entityListConfig.query.order;
    entityListConfig.query['status'] = 'UNPUBLISHED';
   const obs1 = this.appService.getAllAppStatusCount(new EntityListConfig);
   const obs2 = this.appService.getAllAppsByStatus(entityListConfig);
   return Observable.forkJoin(obs1, obs2);
    // return this.appService.getAllAppStatusCount(new EntityListConfig);
  }
}
