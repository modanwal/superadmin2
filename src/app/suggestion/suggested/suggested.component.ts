import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {ModalService} from '../../shared';
import {SuggestedUpsertComponent} from './suggested.upsert/suggested.upsert.component';
import {SuggestedModel} from '../../shared/models/suggested.model';
import {SuggestedDeleteComponent} from './suggested.delete/suggested.delete.component';
import {Subscription} from 'rxjs/Subscription';
import {CcldService, SocketService, InfiniteScrollService, EntityListConfig} from '../../shared';


@Component({
  selector: 'suggested-app',
  templateUrl: 'suggested.component.html',
  providers: [EntityListConfig]
})
export class SuggestedComponent implements OnInit{
  fetchingData: boolean;
  collectionDirty: boolean;
  suggestedFetchSub: Subscription;
  suggestedObj: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  categories: any;
  labels: any;
  suggested; any;
  mode: any;
  constructor(private route: ActivatedRoute,
              private modalService: ModalService,
              private socketService: SocketService,
              private ccldService: CcldService,
              private entityListConfig: EntityListConfig,
              public infiniteScrollService: InfiniteScrollService
              ){
    this.collectionDirty = false;
    this.suggestedObj = {};

    /*** websockets */
    this.socketService.subscribeToEvent(`suggested:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.suggested,this.suggestedObj, this.entityListConfig);
        if(this.fetchingData){
          this.suggestedFetchSub.unsubscribe();
          this.getAllSuggested(false);
        }
      })
    this.socketService.subscribeToEvent(`sugested:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.suggested,this.suggestedObj,this.entityListConfig);
        if(this.fetchingData){
          this.suggestedFetchSub.unsubscribe();
          this.getAllSuggested(false);
        }
      })
    /*** websockets */
  }
  onScroll(){

  }
  ngOnInit(){
    this.route
      .params
      .subscribe(params => {
        // Récupération des valeurs de l'URL
        this.mode = params['id']; // --> Name must match wanted paramter
      });
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map((data=> data.data))
      .subscribe(
        (data) => {
          // console.log(data);
          this.categories = data[0];
          // this.labels = data[1];
          this.suggested = data[1];
          console.log(this.suggested);
          this.infiniteScrollService.computeEntity(this.suggested,this.suggestedObj, this.entityListConfig);
        });
  }
  getAllSuggested(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
    this.suggestedFetchSub =  this.ccldService
      .getAllLabelTypes(this.mode, this.entityListConfig )
      .subscribe(
        (data)=>{
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllSuggested(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.suggested, this.suggestedObj, this.entityListConfig, refreshData);
          }
        }
      ),(error)=>{this.fetchingData = false;};
  }
  deleteSuggested(suggest){
    const modalRef = this.modalService.open(SuggestedDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.suggested = suggest;
  }

  addSuggest(){
    const modalRef = this.modalService.open(SuggestedUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.categories = this.categories;
    modalRef.componentInstance.suggested = new SuggestedModel();
    modalRef.componentInstance.mode = this.mode;
  }
}
