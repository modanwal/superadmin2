import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResourceModel, ResourceService,PrivilegeService } from '../../../shared';
import {Router} from "@angular/router";


@Component({
  selector: 'resources-upsert-content',
  templateUrl: './resources.upsert.component.html',
  encapsulation : ViewEncapsulation.None
})

export class ResourcesUpsertComponent implements OnInit {
  submitting: boolean;
  @Input() resource: ResourceModel;
  resourcesForm: FormGroup;
  privileges:Array<any> = [];
  newItem: any;
  isSubmited: Boolean;

  constructor (private resourceService:ResourceService,
               private modalService:NgbModal,
               private formBuilder:FormBuilder,
               public activeModal:NgbActiveModal,
               private router: Router,) {

  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm () {
    if(!this.isSubmited){this.isSubmited = true }
    if(this.resourcesForm.valid){
      this.resourcesForm.disable();
      let resourceData = {
          "name": this.resourcesForm.value.name,
          "_id": this.resourcesForm.value.resourceID,
          "isComponent": this.resourcesForm.value.isComponent
        };
      if(resourceData['primary'] == false){
        delete resourceData['primary'];  }
      this.resourceService.save(resourceData)
          .subscribe(data =>
          {
            this.activeModal.close('resource:update');
            this.newItem = data;
            if(this.newItem._id){
              this.router.navigate(['/roles/resources/privilege', this.newItem._id]);
            }
          }
          );
    }
  }
  ngOnInit () {
    this.resourcesForm = this.formBuilder.group({
      'name' : [this.resource.name, Validators.required],
      'isComponent': [this.resource.isComponent],
      'resourceID' : [this.resource._id]
    });
  }

}
