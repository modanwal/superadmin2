import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TemplateService } from '../../../../shared';


@Component( {
  selector: 'template-upsert-group',
  templateUrl: './template.upsert.component.html'
})


export class TemplateUpsertComponent implements OnInit{
  isSubmitting: boolean;
  file: any;
  @Input() component;
  @Input() templateID;

  templateComponentForm: FormGroup;
  isSubmited: Boolean;

  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private templateService: TemplateService) {}
  ngOnInit(){
    // use FormBuilder to create a form group
    this.templateComponentForm = this.formBuilder.group({
      '_id': [this.component._id],
      'pageName': [this.component.pageName, Validators.required],
      'order': [this.component.order, Validators.required]
    });
  }
  selectFile(event){
    this.file = event.target.files[0];
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.templateComponentForm.valid) {
      this.isSubmitting = true;
      this.templateComponentForm.disable();
      const component = this.templateComponentForm.value;
      let fd = new FormData();
      fd.append('pageName',component.pageName);
      fd.append('order',component.order);
      if(this.file){
        fd.append('file',this.file);
      }
      this.templateService
        .updateComponent(fd,component._id,this.templateID)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.activeModal.close('template-component:updated');
          },
          error => {
            this.isSubmitting = false;
            this.templateComponentForm.enable();
          }
        );
    }
  }
}
