import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ComponentComponent  } from './component.component';
import { ComponentListComponent } from './component.list/component.list.component';
import { NgbdModalUpsertContent} from './component.upsert/component.upsert.component';
import { NgbdModalDeleteContent} from './component.delete/component.delete.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SharedModule, AuthGuard, EntityListConfig} from '../shared';
import { ComponentResolver } from './component-resolver.service';

export const componentRouting = [
  {
    path: 'component',
    component: ComponentComponent,
    resolve: { data: ComponentResolver },
    canActivate: [AuthGuard]
  },
  {
    path: 'component/:id',
    component: ComponentComponent,
    resolve: { data: ComponentResolver },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    SharedModule,
    NgbModule
  ],
  declarations: [
    ComponentComponent,
    ComponentListComponent,
    NgbdModalUpsertContent,
    NgbdModalDeleteContent
  ],
  providers: [ ComponentResolver, EntityListConfig, AuthGuard
  ],
  entryComponents: [
      NgbdModalUpsertContent,
      NgbdModalDeleteContent
  ]
})
export class ComponentModule {}
