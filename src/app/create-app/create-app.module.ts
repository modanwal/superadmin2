import {  NgModule } from '@angular/core';
import { SharedModule,AuthGuard ,DTOService } from '../shared';
import { CreateAppComponent } from './create-app.component';
import { SelectCategoryComponent } from './select-category/select-category.component';
import { SelectTemplateComponent } from './select-template/select-template.component';
import { FillDetailsComponent } from './fill-details/fill-details.component';


export const createAppRouting = [
  {
    path: 'create-app',
    component: CreateAppComponent,
    canActivate: [AuthGuard],
    children:[
      {
        path: 'category',
        component: SelectCategoryComponent
      },
      {
        path: 'template/:categoryID',
        component: SelectTemplateComponent
      },
      {
        path: 'details/:templateID',
        component: FillDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    CreateAppComponent,
    SelectCategoryComponent,
    SelectTemplateComponent,
    FillDetailsComponent
  ],
  providers: [
    AuthGuard, DTOService
  ]
})
export class CreateAppModule {}
