import {Router, RouterStateSnapshot} from '@angular/router';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {UserDetailService} from '../../../shared/';
@Component({
  selector: 'address-upsert',
  templateUrl: './address.upsert.component.html'
})
export class AddressUpsertComponent implements OnInit{
  @Input() address;
 @Input() userID: string;
  addressForm: FormGroup;
  isSubmited: Boolean;

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder, private modalService: NgbModal,
              private userAddressService: UserDetailService) {
    this.isSubmited = false;
  }
  submitForm(){
          if(!this.isSubmited){this.isSubmited = true }
          if(this.addressForm.valid) {
            const address = this.addressForm.value;
            this.addressForm.disable();
            if(address.primary == false){
            delete address.primary;  }
            console.log(address);
            this.userAddressService
              .save(address, this.userID['_id'])
              .subscribe(
                data => {
                  this.addressForm.enable();
                  this.activeModal.close('address:save');
                }
              );
          }
  }
  ngOnInit(){
    // use FormBuilder to create a form group
    this.addressForm = this.formBuilder.group({
      '_id': [this.address._id],
      'address1': [this.address.address1, Validators.required],
      'address2': [this.address.address2, Validators.required],
      'city': [this.address.city, Validators.required],
      'state': [this.address.state, Validators.required],
      'country': [this.address.country, Validators.required],
      'pincode': [this.address.pincode, Validators.required],
      'primary': [this.address.primary]
    });
    if(this.address.primary){
      this.addressForm.controls.primary.disable();
    }
  }
}
