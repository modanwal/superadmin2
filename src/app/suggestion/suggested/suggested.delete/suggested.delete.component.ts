import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {CcldService} from '../../../shared';
@Component({
  selector: 'suggested-delete',
  templateUrl: './suggested.delete.component.html'
})
export class SuggestedDeleteComponent {
  @Input() suggested;
  isSubmited: Boolean;
  constructor( public activeModal: NgbActiveModal ,
               private ccldService: CcldService
  ) {}
  deleteLabel(suggested: any) {
    this.ccldService.destroy(suggested._id)
      .subscribe(
        data => this.activeModal.close()
      );
  }
}
