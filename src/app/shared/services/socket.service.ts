import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Socket } from 'ng-socket-io';

@Injectable()
export class SocketService {

  constructor (private socket: Socket ) {}
  subscribeToEvent(subscribeUrl): Observable<any>{
    return this.socket.fromEvent(subscribeUrl);
  }
  emitToSocket(messageType,message){
    this.socket.emit(messageType,message);
  }
}
