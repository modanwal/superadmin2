import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { FileUploadComponent } from './file-upload.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Injectable()
export class FileUploadService {
  constructor (private modalService: NgbModal) {
  }
  public open(){
    const modalRef = this.modalService.open(FileUploadComponent,{
      windowClass: 'file-upload',
      size: 'lg',
      keyboard: false
    });
    return modalRef;
  }
//   public makeFileRequest (url: string, params: string[], file: any) {
//      url = 'http://backend.makemeproud.org/api/p3/p1/app/5997fe3888b0267806bd4f76/p3/5998237a637b367a21bdeef1';
//      let progressObserver;
//      let progress$ = Observable.create(observer => {
//         progressObserver = observer;
//     }).share();
//     return Observable.create(observer => {
//         let formData: FormData = new FormData(),
//             xhr: XMLHttpRequest = new XMLHttpRequest();

//         // for (let i = 0; i < files.length; i++) {
//         //     formData.append("uploads[]", files[i], files[i].name);
//         // }
//         formData.append('file', file);
//         formData.append("name","Some Good Name");

//         xhr.onreadystatechange = () => {
//             if (xhr.readyState === 4) {
//                 if (xhr.status === 200) {
//                     observer.next(JSON.parse(xhr.response));
//                     observer.complete();
//                 } else {
//                     observer.error(xhr.response);
//                 }
//             }
//         };

//         xhr.upload.onprogress = (event) => {
//             let progress = Math.round(event.loaded / event.total * 100);
//             observer.next(progress);
//         };

//         xhr.open('POST', url, true);
//         xhr.send(formData);
//     });
//   }
}