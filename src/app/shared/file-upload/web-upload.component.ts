import {
  Component, ViewEncapsulation, Input, OnInit, ViewChild, ElementRef, Output, EventEmitter,
  AfterViewInit
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FlickrService} from "../services";
import {FormControl} from '@angular/forms';
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'web-upload',
  templateUrl: './web-upload.component.html'
})

export class WebUploadComponent implements  AfterViewInit {
   @Input() settings;
   @Output('submit') submitEmitter = new EventEmitter<any>();
   @Output('close') closeEmitter = new EventEmitter<any>();
  @ViewChild('searchControl')
  searchControl: ElementRef;
  searchItem;
 // searchControl = new FormControl();
  model$: Observable<any>;
  photos: Object;
   constructor(public activeModal: NgbActiveModal, private _flickrService: FlickrService) {
  }

  ngAfterViewInit() {
    console.log('data Changing');
    const eventObservable = Observable.fromEvent(
      this.searchControl.nativeElement, 'keyup');
    eventObservable
      .debounce(() => Observable.timer(500))
      .subscribe(
        (data) => {
          this.searchItem = data['target']['value'];
          if(this.searchItem){
            console.log('event Data', data['target']['value']);
            this._flickrService.getResult( this.searchItem)
              .subscribe(
                (value) =>{
                  this.photos = value;
                  console.log('console.log', value);
                }
              );
          }
        }
      );
  }

  // ngOnInit() {
  //
  //    console.log(this.searchControl);
  //   this.searchControl.valueChanges
  //     .debounceTime(500)
  //     .distinctUntilChanged()
  //     .switchMap((query: string) => this._flickrService.getResult(query))
  //     .subscribe(value => {
  //       this.photos = value;
  //     });
  // }
  submit(){
      this.submitEmitter.emit({
      })
  }
  close(){
      this.closeEmitter.emit();
  }
}
