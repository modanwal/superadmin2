

import {Component, Input, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {P2P1Model} from '../../../../shared/models/p2p1.model';
import {P2Service} from '../../../../shared/services/p2.service';

@Component({
  selector: 'app-root',
  templateUrl: './p2.delete.component.html',
  encapsulation: ViewEncapsulation.None
})
export class P2DeleteComponent {
  isSubmitting: boolean;
  @Input() p2;
  @Input() appID;
  constructor(public activeModal: NgbActiveModal, private p2Service: P2Service) {

  }
  delete (p2: P2P1Model) {
    this.isSubmitting = true;
    this.p2Service.destroy(p2._id, this.appID )
      .subscribe (
        data => {
          this.activeModal.close ('P2_Delete');
          this.isSubmitting = false;
        },
        error => this.isSubmitting = false
      );
  }

}
