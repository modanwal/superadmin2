import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {EntityListConfig , P2Model ,P2P1Model, DTOService} from '../../../shared';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {P2UpsertComponent} from './p2.upsert/p2.upsert.component';
import {P2DeleteComponent} from './p2.delete/p2.delete.component';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subject} from "rxjs/Subject";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'p2',
  templateUrl: './p2.component.html'
})
export class P2Component implements OnInit{
  p2p1Active: boolean;
  componentID: any;
  categoryID: any;
  nextComponent: any;
  pageName: any;
  selectedComponent: any;
  p2p1: P2P1Model[];
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  p2: P2Model[];
  p2Id: string;
  appID: any;

  constructor(
              private _sanitizer: DomSanitizer,
              public entityListConfig: EntityListConfig,
              private modalService: NgbModal,
              private route: ActivatedRoute,
              private router: Router,
              private dtoService: DTOService
    ) {
    this.categoryID = this.dtoService.getValue('categoryID');

  }
  ngOnInit() {
    this.router.events
      .filter(data=> data instanceof NavigationEnd)
      .map(data => data['url'])
      .map(data => data.split('/')[5])
      .subscribe(
       data => {
         this.p2p1Active = false;
         if(data=='p2p1') this.p2p1Active = true;
       }
    )
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {

          this.appID = data.appID;
          this.pageName = data.selectedComponent.pageName;
          this.p2 = data.p2Data;
          this.nextComponent = data.nextComponent;
          this.componentID = data.selectedComponent.componentID._id;
          console.log(this.componentID,'hete are printing  ');
          this.dtoService.setValue('componentID', this.componentID);
        }
      );
  }

}
