import { Component, ViewEncapsulation, Input, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { CropperSettings, ImageCropperComponent } from 'ng2-img-cropper';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilsService } from '../../shared';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'media-upload',
  templateUrl: './media-upload.component.html',
  providers: [CropperSettings]
})

export class MediaUploadComponent implements  OnInit {
    invalidFileSize: boolean;
  @Input() settings;
  @Output('close') closeEmitter = new EventEmitter<any>();
  @Output('submit') submitEmitter = new EventEmitter<any>();

  @ViewChild('inputFile') inputEl: ElementRef;
  @ViewChild('videoPreview') videoEl: ElementRef;
  @ViewChild('localImageCropper') localImageCropper: ImageCropperComponent;
  imageUploadDragIn: Boolean;
  imageSelected: boolean;
  videoUpload: boolean;
  invalidFileType: boolean;
  cropperSettings: CropperSettings;
  cropBounds: any;
  cropPreview: any;
  localFile: any;
  constructor(private formBuilder: FormBuilder) {
  }
  ngOnInit(){
      this.cropPreview = {};
      this.cropperSettings = new CropperSettings();
      this.cropperSettings.noFileInput = true;
      this.cropperSettings.width = this.settings['image'].width;
      this.cropperSettings.height = this.settings['image'].height;
  }
  /*** events */
  dragover(event){
    event.preventDefault();
  }
  dragenter(event){
    event.preventDefault();
    this.imageUploadDragIn = true;
    return false;
  }
  dragleave(event){
     this.imageUploadDragIn = false;
  }
  onDrop(event){
     event.preventDefault();
     this.imageUploadDragIn = false;
     this.triggerInputClick();
     return false;
  }
  processFile(event){
      if(["image/gif", "image/jpeg", "image/png"].indexOf(event.target.files[0]['type']) != -1 && this.settings['image']){
          this.processLocalImage(event);
      }
      else if(this.videoEl.nativeElement.canPlayType(event.target.files[0]['type']) && this.settings['video']){
          this.processLocalVideo(event);
      }else{
          this.invalidFileType = true;
          alert('Invalid File Type!');
      }
  }
  processLocalImage(event){
      if(event.target.files[0]){
            let fileReader = new FileReader();
            const file = event.target.files[0];
            fileReader.addEventListener("load", ()=>{
                let image = new Image();
                image.src = fileReader.result;

                // if(image.width < this.settings['image']['width'] || image.width < this.settings['image']['width']){
                //     alert('image size not ideal for upload!');
                //     this.invalidFileSize = true;
                // }else{
                    this.localFile = event.target.files[0];
                    this.imageSelected = true;
                    this.localImageCropper.setImage(image);
                //}
            });
            fileReader.readAsDataURL(file);
      }
  }
  processLocalVideo(event){
      this.videoUpload = true;
      this.videoEl.nativeElement.src = URL.createObjectURL(event.target.files[0]);
  }
  localImageCropped(event){
    //   let imageMeta = this.localImagePreview.image.split(',')[0];
    //   let image64data = this.localImagePreview.image.substr(imageMeta.length+1);
    //   let contentType = imageMeta.split(';')[0].substr(5);
    //   const blob  = this.utilsService.b64toBlob(image64data,contentType)
    //   this.localFile = new File([blob],'someGoodFile.jpg');
    this.cropBounds = event;
    // if(this.cropBounds.width<this.settings['image']['width'] || this.cropBounds.height < this.settings['image']['height']){
    //     this.invalidFileSize  = true;
    //     alert('invalid File Size!');
    // }else{
    //     this.cropBounds = event;
    // }
  }
  triggerInputClick(){
      this.inputEl.nativeElement.click();
  }
  submit() {
      this.submitEmitter.emit({
          type: this.videoUpload?'video':'image',
          image: this.localFile,
          cropBounds: this.cropBounds
      })
    // let observable = this.fileUploadService.makeFileRequest('http://backend.makemeproud.org/api/p3/p1/app/5997fe3888b0267806bd4f76/p3/5998237a637b367a21bdeef1',[],this.localFile);
    // observable.subscribe(
    //             (progress) => {
    //                 this.uploadProgress = progress;
    //                 console.log(progress);
    //                 if(this.uploadProgress == 100){
    //                     setTimeout(()=>{
    //                         this.uploadInProgress = false;
    //                         this.uploadCompleted = true;
    //                     },2000);
    //                 }
    //             },
    //             (error) => {
    //                 console.log(error);
    //             }
    //         );
  }
  close(){
      this.closeEmitter.emit();
  }
}
