import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule, PrivilegeService, AuthGuard} from '../shared';
import { RolesComponent } from './roles.component';
import { PrivilegeComponent } from './privilege/privilege.component';
import { RoleComponent } from './role/role.component';
import { ResourcesComponent } from './resources/resources.component';
import { ResourcePrivilegeComponent } from './resources/resource-privilege/resource-privilege.component';
import { PrivilegeUpsertComponent } from './privilege/privilege.upsert/privilege.upsert.component';
import { PrivilegeDeleteComponent } from './privilege/privilege.delete/privilege.delete.component';
import { ResourcesUpsertComponent } from './resources/resources.upsert/resources.upsert.component';
import { ResourcesDeleteComponent } from './resources/resources.delete/resources.delete.component';

import { RoleResourcePrivilegeComponent } from './role/role-resource-privilege/role-resource-privilege.component';
import { RoleDeleteComponent } from './role/role.delete/role.delete.component';
import { RoleUpsertComponent } from './role/role.upsert/role.upsert.component';
import { RoleResolver } from './role/role-resolver.service';
import { ResourceResolver } from './resources/resources-resolver.service';
import { PrivilegeResolver } from './privilege/privilege-resolver.service';
import {ResourcePrivilegeResolver} from "./resources/resource-privilege/resource-privilege.resolver";
import {RoleResourcePrivilegeResolver} from "./role/role-resource-privilege/role-resource-privilege.resolver";

export const rolesRouting = [
  {
    path: 'roles',
    component: RolesComponent,
    canActivate: [AuthGuard],
    children : [
      {
          path: 'privilege',
          component: PrivilegeComponent,
          resolve: { data: PrivilegeResolver }
      },
      {
        path: 'resources',
        children: [
          {
            path: '',
            component: ResourcesComponent,
            resolve: { data: ResourceResolver }
          },
          {
            path: 'privilege/:id',
            pathMatch: 'full',
            component: ResourcePrivilegeComponent,
            resolve: { data: ResourcePrivilegeResolver }
          }
        ]
      },
      {
        path: 'role',
        children: [
          {
            path: '',
            component: RoleComponent,
            resolve: { data: RoleResolver }
          },
          {
            path: 'resource-privilege/:id',
            component: RoleResourcePrivilegeComponent,
            resolve: { data: RoleResourcePrivilegeResolver }
          }
        ]
      }
    ]
  }
];
@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    RolesComponent,
    PrivilegeComponent,
    ResourcesComponent,
    ResourcePrivilegeComponent,
    RoleComponent,
    PrivilegeUpsertComponent,
    PrivilegeDeleteComponent,
    ResourcesUpsertComponent,
    ResourcesDeleteComponent,
    RoleResourcePrivilegeComponent,
    RoleDeleteComponent,
    RoleUpsertComponent

  ],
  entryComponents: [
    PrivilegeUpsertComponent,
    PrivilegeDeleteComponent,
    ResourcesUpsertComponent,
    ResourcesDeleteComponent,
    RoleDeleteComponent,
    RoleUpsertComponent
  ],
  providers: [
    PrivilegeService, RoleResolver, ResourceResolver, PrivilegeResolver, AuthGuard, ResourcePrivilegeResolver, RoleResourcePrivilegeResolver
  ]
})
export class RolesModule {}
