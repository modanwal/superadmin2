import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule, AuthGuard } from '../shared';
import { PostService, LikeService, CommentService, NotificationService } from "../shared";
import { NotificationResolver } from "./notification.resolver";
import { NotificationComponent } from "./notification.component";

export const notificationRouting  = [
  {
    path: 'notification',
    component: NotificationComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
    resolve: { data : NotificationResolver}
  }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    NotificationComponent
  ],
  providers: [
    AuthGuard, NotificationResolver, NotificationService
  ],
  entryComponents: [
  ]
})
export class NotificationModule {}
