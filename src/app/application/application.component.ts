import { Component, ViewEncapsulation, OnDestroy } from '@angular/core';
import { AppService, AppModel, EntityListConfig } from './../shared';
import { ActivatedRoute, Router, RouterStateSnapshot } from "@angular/router";
import { Subject } from "rxjs/Subject";
import {DTOService} from "../shared/services/dto.service";
@Component({
  selector: 'page-root',
  templateUrl: './application.component.html',
  encapsulation: ViewEncapsulation.None,
  providers:[EntityListConfig]
})

export class ApplicationComponent implements OnDestroy{
  appID: any;
  appComponents: any;
  singleApp: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private appService: AppService,
              private route: ActivatedRoute,
              private router: Router,
              private dtoService: DTOService) {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .subscribe(val => {
         this.appComponents = val['data'][0]['appComponents'];
         console.log(this.appComponents);
         this.appID = val['data'][0]['appID'];
        console.log(this.appID);
         this.singleApp = val['data'][1];
         console.log(this.singleApp, 'this one is printing');
         if(!this.router.url.split('/')[3]){
          this.router.navigateByUrl(`/application/${this.appID + this.appComponents[0]['componentID']['route']}`);
         }
      });
    this.dtoService.setValue('categoryID', this.singleApp.templateID.categoryID);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
