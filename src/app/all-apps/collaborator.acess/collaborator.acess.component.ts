import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {CollaboratorService} from '../../shared/services/collaborator.service';
import {ModalService} from "../../shared/modal.service";
import {EntityListConfig} from "../../shared/models/entity-list-config.model";
import {Observable} from "rxjs/Rx";
@Component({
  selector: 'collaborator-access',
  templateUrl: './collaborator.acess.component.html',
  providers: [EntityListConfig]
})
export class CollaboratorAcessComponent implements OnInit{
 @Input() collaboratorPrivilege;
 @Input() collaborator;
 @Input() app;
 @Input() collaboratorAcess: any;

  isSubmitted: boolean;
  isSubmitting: boolean;
  @ViewChild('email')
  email: ElementRef;
  constructor(public activeModal: NgbActiveModal,
              private modalService: ModalService,
              private collaboratorService: CollaboratorService,
             ) {
    this.isSubmitting = true;
    this.isSubmitted = false;
  }
  ngOnInit() {
  }

  getAllCollaboratorAcess(appID, userID) {
    this.collaboratorService
      .getAllCollaboratorAcess(appID, userID)
      .subscribe(
        (data)=>{
          this.collaboratorPrivilege = data;
        },
        (error)=>{
          console.log(error);
        });
  }
  getAllColleborator(appID){
      this.collaboratorService
        .getAllCollaborator(appID)
        .subscribe(
          (data) => {
            this.collaboratorPrivilege = data;
          },
          (error) => {
            console.log(error);
          });
  }
  getCheckedStatus(collaboratorID) {
    if(!this.collaboratorAcess[collaboratorID]) return false;
    if(this.collaboratorAcess[collaboratorID] == true ) return true;
    return false;
  }
  getCollaboratorCheckedStatus(collaboratorID) {
       if(this.collaboratorAcess[collaboratorID] != undefined)
         return true;
      return false;
    }
  setCollaborator(event, appComponentID: any ) {
    event.target.disabled = true;
    if(event.target.checked){
      const data = {
        appID: this.app._id,
        userID: this.collaborator._id,
        appComponentID: appComponentID

      }
      this.collaboratorService
        .checkBoxClicked(data)
        .subscribe(
          (value)=>{
            this.getAllCollaboratorAcess(this.app._id, this.collaborator._id);
            event.target.disabled = false;
          },
          (error)=>{
            event.target.disabled = false;
          });
    }else {
      this.collaboratorService
        .deleteCollaboratorAcess(this.app._id, this.collaborator._id, appComponentID)
        .subscribe(
          (data)=>{
            this.getAllCollaboratorAcess(this.app._id, this.collaborator._id);
            event.target.disabled = false;
          },
          (error)=>{
            event.target.disabled = false;
          });
    }
  }
  setPrivate(event, collaboratorID){
    event.target.disabled = true;
    if(!event.target.checked){
      const data = {
        appID: this.app._id,
        userID: this.collaborator._id,
        appComponentID: collaboratorID,
        private: false
      }
      this.collaboratorService
        .checkBoxSetPrivate(data)
        .subscribe(
          (value)=>{
            this.getAllCollaboratorAcess(this.app._id, this.collaborator._id);
            event.target.disabled = false;

          },
          (error)=>{
            event.target.disabled = false;
            event.target.checked = true;
          });
    }else
    {
      const data = {
        appID: this.app._id,
        userID: this.collaborator._id,
        appComponentID: collaboratorID,
        private: true
      }
      this.collaboratorService
        .checkBoxSetPrivate(data)
        .subscribe(
          (value)=>{
            this.getAllCollaboratorAcess(this.app._id, this.collaborator._id);
            event.target.disabled = false;
          },
          (error)=>{
            event.target.disabled = false;
            event.target.checked = false;
          });
    }
  }
  eventHandler(event, app) {
    if(event.keyCode === 13){
      this.submitForm(app);
    }
  }
  submitForm(app){
    this.isSubmitting = false;
    this.isSubmitted = true;
  const data = {
      email: this.email.nativeElement.value,
      appID: app._id
    };
      this.collaboratorService
        .save(data)
        .subscribe(
          (value)=>{
            const entityListConfig = new EntityListConfig();
       this.collaboratorService
              .getAll(this.app._id, entityListConfig)

        .subscribe(
          (value1)=>{
            this.collaborator = value1[0];

            this.collaboratorService
              .getAllCollaboratorAcess(this.app._id, this.collaborator._id)
              .subscribe(
                (data)=>{
               this.collaboratorPrivilege = data;
                });

          });


       //     this.activeModal.close('collaborator:updated');
          },
          (error)=>{
            console.log('error');
            this.isSubmitting = true;
            this.isSubmitted = false;
          });
  }
}
