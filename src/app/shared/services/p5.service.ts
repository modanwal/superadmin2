import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class P5Service {
  constructor (private apiService: ApiService) {}

  getAllp5( entityListConfig:  EntityListConfig, appID, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/p5/app/${appID}/${pageNumber}/${pageSize}`, query, isSocket);
  }

  destroy(_id, appID): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/p5/${_id}/app/${appID}/` );
  }

  saveP5(p5,_id, appID): Observable<any> {
    // If we're updating an existing component
    if (_id) {
      return this.apiService.putWithFormData(`/p5/${_id}/app/${appID}/`, p5);

      // Otherwise, create a new category
    } else {
      delete p5._id;
      return this.apiService.postWithFormData(`/p5/app/${appID}/`, p5);
    }
  }
}
