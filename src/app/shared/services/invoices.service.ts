import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class InvoicesService {
  constructor (private apiService: ApiService) {}
  getAll(entityListConfig, isSocket?): Observable<any[]> {

    const query: URLSearchParams = new URLSearchParams();
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/invoice/all/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getCheckOut( entityListConfig: EntityListConfig, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/invoice/callback`, query, isSocket);
  }
  get(invoiceID, entityListConfig, isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/invoice-item/invoice/${invoiceID}/${pageNumber}/${pageSize}`, query, isSocket);
  }
  destroy(_id): Observable<any>{
    return this.apiService.delete(`/invoice/${_id}` );
  }
  save(invoices, appID, welcomeID?): Observable<any> {
    // If we're updating an existing component
    if (welcomeID) {
      return this.apiService.put(`/invoice/${welcomeID}/app/${appID}/` , invoices)
        .map(data => data.welcome);

      // Otherwise, create a new category
    } else {
      return this.apiService.post(`/invoice/app/${appID}`, invoices)
        .map(data => data.welcome);
    }
  }
  savepayBill(applicationID): Observable<any> {
    // If we're updating an existing component
    if (applicationID._id) {
      return this.apiService.post(`/invoice/app/${applicationID._id}/` );
      // Otherwise, create a new category
    }
  }
}
