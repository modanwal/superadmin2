import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CroppedimageComponent} from "../croppedupload/croppedimage.component";
import {ModalService} from "../../shared/modal.service";
import {WebuploadComponent} from "../webupload/webuploadimage.component";

@Component({
  selector: 'app-root',
  templateUrl: './image-selection.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ImageSelectionComponent implements  OnInit {
 isSubmitting: boolean;
 file: any;
 path: any;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private modalService: NgbModal,
                private modalService2: ModalService) {

  }
  ngOnInit() {

  }
  selectImageFile(event){
    if(this.getFileType(event.target.files[0])!='image'){alert('file type not supported!');return;}
    this.file = event.target.files[0];
    /*** open modal for local modal */
    const modalRef  = this.modalService.open(CroppedimageComponent)
    const cropDimension = {
      width: 200,
      height: 200
    };
    modalRef.componentInstance.cropDimension = cropDimension;
    modalRef.componentInstance.file = this.file;
    modalRef.result.then((data) => {
      if(data){
        this.activeModal.close({
          type: 'image',
          file: this.file,
          bounds: data.cropBounds
        })
      }else{
        //just wait...
      }
    });
  }
  selectVideoFile(event){
    if(this.getFileType(event.target.files[0])!='video'){alert('file type not supported!');return;}
    this.file = event.target.files[0];
    this.activeModal.close({
      type: 'video',
      file: this.file
    })
  }
  getFileType(file){
    const validImageType = ['jpg', 'jpeg', 'png'];  //acceptable file types
    const validVideoType = ['MOV', 'MPEG4', 'MP4', 'AVI','WMV','MPEGPS','FLV','3GPP'];  //acceptable file types
    let extension = file.name.split('.').pop().toLowerCase();
    const isImage = validImageType.indexOf(extension) > -1;
    const isVideo = validVideoType.indexOf(extension) > -1;
    if(isImage) return 'image';
    if(isVideo) return 'video';
    return 'unknown';
  }
  // openWebWizard(){
  //  const  modalRef2 =    this.modalService2.openOverlayModal(WebuploadComponent,{
  //     keyboard: false
  //   });
  //   modalRef2.result.then((data) => {
  //     this.path = data;
  //     console.log(this.path, '---------------');
  //     this.activeModal.close(this.path);
  //   });
  // }
  uploadModal(){
    const uploadSettings = {
      'local':{
        image:{
          width: 200,
          height: 200
        }
      },
      'web':{
        width: 200,
        height: 200
      }
    }
  }
}
