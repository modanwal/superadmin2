

import {Component, Input, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { P5Model } from '../../../../shared/models/p5.model';
import {P5Service} from '../../../../shared/services/p5.service';

@Component({
  selector: 'p5delete',
  templateUrl: './p5.delete.component.html',
  encapsulation: ViewEncapsulation.None

})
export class P5DeleteComponent {
  @Input() p5;
  @Input() appID;
  isSubmitting: Boolean;
  constructor(public activeModal: NgbActiveModal, private p5Service: P5Service) {

  }
  delete (p5: P5Model) {
  this.isSubmitting = true;
    this.p5Service.destroy(p5._id, this.appID )
      .subscribe (
        data => {
          this.isSubmitting = false;
          this.activeModal.close ('P5_Delete');
        },
        error => {
          this.isSubmitting = false;
        }
      );
  }

}
