import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {EntityListConfig} from '../../shared/models/entity-list-config.model';
import { SocketService, ModalService, InfiniteScrollService, DataService} from '../../shared';
import {DomSanitizer} from "@angular/platform-browser";
import {FilesUpsertComponent} from "./files.upsert/files.upsert.component";
import {FilesDeleteComponent} from "./files.delete/files.delete.component";
import {LabelNumberModel} from "../../shared/models/labelNumber.model";
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'file-pages',
  templateUrl: './files.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]

})
export class FilesComponent implements OnInit {
  fetchingData: boolean;
  collectionDirty: boolean;
  stringFetchSub: Subscription;
  fileObj: any;
  lastFetchedItems: number;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  files;
  file;
  cropData: any;
  imagePreview;
  constructor(
              private route: ActivatedRoute,
              private modalService: ModalService,
              private entityListConfig: EntityListConfig,
              private socketService: SocketService,
              private _sanitizer: DomSanitizer,
              public infiniteScrollService: InfiniteScrollService,
              private dataService: DataService,
              private router: Router

  ) {
    this.collectionDirty = false;
    this.fileObj = {};

    /*** websockets */
    this.socketService.subscribeToEvent(`data:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.files,this.fileObj, this.entityListConfig);
        if(this.fetchingData){
          this.stringFetchSub.unsubscribe();
          this.getAllFiles(false);
        }
      })
    this.socketService.subscribeToEvent(`data:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.files,this.fileObj,this.entityListConfig);
        if(this.fetchingData){
          this.stringFetchSub.unsubscribe();
          this.getAllFiles(false);
        }
      })
    /*** websockets */
  }
  onScroll(){
    this.getAllFiles(false);
  }
  ngOnInit() {
    this.route.data
      .map((data=> data.data))
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        console.log(data);
          this.files = data;

      });

  }
  getBackground(image){
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }
  getAllFiles(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
    delete this.entityListConfig.query.sortBy;
    delete this.entityListConfig.query.order;
    this.entityListConfig.query['type'] = 'file';
    this.stringFetchSub = this.dataService
      .getAll(this.entityListConfig)
      .subscribe(
        (data)=>{
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllFiles(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.files, this.fileObj, this.entityListConfig, refreshData);
          }
        }
      ),(error)=>{this.fetchingData = false;};
  }

  deletefile(file: any) {
    const modalRef = this.modalService.open ( FilesDeleteComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.files = file;

  }
  add(){
    const modalRef =  this.modalService.open(FilesUpsertComponent,{
      size: 'sm'
    },'fade-in-pulse','fade-out-pulse');
  }
  addfile() {
    // const uploadSettings = {
    //   'local':{
    //     image:{
    //       width: 200,
    //       height: 200
    //     }
    //   },
    //   'web':{
    //     width: 200,
    //     height: 200
    //   }
    // }
    // const modalRef = this.fileUploadService.open();
    // modalRef.componentInstance.pageTitle='Welcome Page Image';
    // modalRef.componentInstance.uploadSettings = uploadSettings;
    // modalRef.result.then((data)=>{
    //   if(data){
    //     if(data.image){
    //       this.file = data.image;
    //       this.cropData = data.cropData;
    //       this.generateImagePreview(this.file);
    //     }
    //     if(data.video){
    //       //this.file
    //     }
    //   }
    // });
    const modalRef = this.modalService.open ( FilesUpsertComponent, {},'fade-in-pulse','fade-out-pulse' );
    modalRef.componentInstance.files = new LabelNumberModel();
    modalRef.result.then((closeReason) => {
      if (closeReason == 'files:updated'){
        this.getAllFiles(false);
      }
    });
  }
  viewLabelTypes(fileID){
   this.router.navigateByUrl(`/suggestion/suggested/${fileID}`);
  }
  selectFile(event){
    this.file = event.target.files[0];
    this.generateImagePreview(this.file)
  }
  generateImagePreview(file){
    const fileReader = new FileReader();
    fileReader.addEventListener('load', () => {
      this.imagePreview = fileReader.result;
    });
    fileReader.readAsDataURL(file);
  }


}
