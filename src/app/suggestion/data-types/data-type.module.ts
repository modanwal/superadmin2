import { NgModule } from '@angular/core';
import {AuthGuard, SharedModule} from '../../shared';
import {SuggestionDataTypesComponent} from './data-types.component';
import {SuggestionDataTypeResolver} from "./data-type.resolver";
import {SuggestionDataTypeUpsertComponent} from "./data-type.upsert/data-type.upsert.component";
import {SuggestionDataTypeDeleteComponent} from "./data-types.delete/data-type.delete.component";

export const dataTypeRouting = [
  {
    path: 'datatypes',
    component: SuggestionDataTypesComponent,
    canActivate: [AuthGuard],
    resolve: {
      data: SuggestionDataTypeResolver
    }
  }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    SuggestionDataTypesComponent, SuggestionDataTypeUpsertComponent, SuggestionDataTypeDeleteComponent
  ],
  providers: [
    AuthGuard, SuggestionDataTypeResolver,
  ],
  entryComponents: [SuggestionDataTypeUpsertComponent, SuggestionDataTypeDeleteComponent ]
})
export class DataTypeModule {}
