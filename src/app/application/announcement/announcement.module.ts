import { NgModule} from '@angular/core';
import { SharedModule, AnnouncementService } from './../../shared';
import {AnnouncementComponent} from './announcement.component';
import {AnnouncementResolver} from './announcement.resolver';
import {AnnouncementUpsertComponent} from "./announcement.upsert/announcement.upsert.component";


export const announcementRouting = [
  {
    path: 'announcement',
    component: AnnouncementComponent,
    resolve: {
      data: AnnouncementResolver
    }
  }
];
@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    AnnouncementComponent, AnnouncementUpsertComponent
  ],
  providers: [ AnnouncementResolver, AnnouncementService
  ],
  entryComponents: [AnnouncementUpsertComponent]
})
export class AnnouncementModule {}
