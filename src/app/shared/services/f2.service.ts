import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class F2Service {
  constructor (private apiService: ApiService) {}

  getAllf2( AppId, entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/f2/app/${AppId}/user/${pageNumber}/${pageSize}`, query, isSocket);
  }

  destroy(_id): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/f2/app/${_id}/app/000000000000000000000001/` );
  }
  getAllRole( AppId, entityListConfig:  EntityListConfig, isSocket? ): Observable<any[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/f2/app/${AppId}/role/`, query, isSocket);
  }
  saveRole(role, appID, userID){
    return this.apiService.put(`/f2/app/${appID}/user/${userID}`, role)

}

  saveP5(p5,_id): Observable<any> {
    // If we're updating an existing component
    if (_id) {
      return this.apiService.putWithFormData(`/f2/app/${_id}/app/000000000000000000000001/`, p5);

      // Otherwise, create a new category
    } else {
      return this.apiService.postWithFormData('/f2/app/000000000000000000000001/', p5);
    }
  }
}
