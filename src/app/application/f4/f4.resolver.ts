import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { UserService, AppService, F4Service } from '../../shared';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export class F4Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router,
               private f4Service: F4Service) {
  }
 ngOnInit(){
 }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      
      const appID = state['url'].split("/")[2];
      return this.f4Service.getAll(appID).map(
        (f4) =>{
          let data = {};
          data['appID'] = appID;
          data['f4'] =  f4;
          return data;
        }
      );
  }
}

