import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WallComponent } from './wall.component';
import { SharedModule, AuthGuard } from '../shared';
import { WallUpsertComponent } from "./post.upsert/post.upsert.component";
import { PostService, LikeService, CommentService, NotificationService, FlickrService, GiphyService } from "../shared";
import { WallPostResolver } from "./wall.resolver";
import { ImagePostComponent } from './image.post/image.post.component';
import { PostDeleteComponent } from "./post.delete/post.delete.component";
import { WallPostComponent } from './post/post.component';
import {ImageSelectionComponent} from "./images-selection/image-selection.component";
import {CroppedimageComponent} from "./croppedupload/croppedimage.component";
import {WebuploadComponent} from "./webupload/webuploadimage.component";

export const wallRouting  = [
  {
    path: 'wall',
    component: WallComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
    resolve: { data : WallPostResolver}
  }
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    WallComponent, WallUpsertComponent, ImagePostComponent, PostDeleteComponent,
    WallPostComponent, ImageSelectionComponent, CroppedimageComponent, WebuploadComponent
  ],
  providers: [
    AuthGuard, PostService, WallPostResolver, LikeService, CommentService, NotificationService, FlickrService, GiphyService
  ],
  entryComponents: [
    WallUpsertComponent, ImagePostComponent, PostDeleteComponent, ImageSelectionComponent, CroppedimageComponent, WebuploadComponent
  ]
})
export class WallModule {}
