import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { TypeService, EntityListConfig } from '../../shared';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export class SuggestionDataTypeResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor ( private typeService: TypeService) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    return this.typeService.getAll (entityListConfig );
  }
}
