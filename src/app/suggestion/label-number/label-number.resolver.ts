import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { EntityListConfig } from '../../shared';
import { Subscription } from 'rxjs/Subscription';
import {DTOService, CcldService} from '../../shared';

@Injectable()
export class LabelNumberResolver implements Resolve<any> , OnInit {
  cclt;
  routeData: Subscription;

  constructor (private dtoService: DTOService, private ccldService: CcldService ) {
    this.cclt = this.dtoService.getValue('labelType');
    console.log(this.cclt);
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    return this.ccldService
      .getAll(this.cclt, entityListConfig);
  }
}
