import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { EntityListConfig, CategoryService, LabelService } from '../../shared';
import { Subscription } from 'rxjs/Subscription';
import {CcldService,CcltService } from '../../shared';

@Injectable()
export class SuggestedResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private categoryService: CategoryService,
               private labelService: LabelService,
               private ccldService: CcldService,
               private ccltService: CcltService
              ) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {

    const entityListConfig = new EntityListConfig();
    entityListConfig.params.pageSize = -1;
    const dataID = state['url'].split("/")[3];
     const obs1 = this.ccltService.getAllCategories(dataID);
    // const obs1 =  this.ccltService.getAllCategory(entityListConfig);
    // const obs2 =  this.labelService.getAll(entityListConfig);
      const obs2 = this.ccldService.getAllLabelTypes(dataID, entityListConfig);
    return Observable.forkJoin(obs1, obs2);

  }
}
