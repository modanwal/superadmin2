import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';
import { ComponentModel} from './../models/component.model';

@Injectable()
export class ComponentService {
  constructor (private apiService: ApiService) {}

  getAll(parentID, entityListConfig:  EntityListConfig, isSocket?): Observable<ComponentModel[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
    .forEach((key) => {
      query.set(key, entityListConfig.query[key]);
    });
    if(parentID){
      return this.apiService.get(`/component/${pageNumber}/${pageSize}/${parentID}`, query, isSocket);
    }else{
      return this.apiService.get(`/component/${pageNumber}/${pageSize}`, query, isSocket);
    }
  }
  getAllComponent( categoryID, entityListConfig:  EntityListConfig, isSocket?): Observable<ComponentModel[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
      return this.apiService.get(`/category-component/${categoryID}/${pageNumber}/${pageSize}`, query, isSocket);
    }
  getResourceName(entityListConfig:  EntityListConfig, isSocket?){
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize= -1;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/resource/component/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getAllTreeComponents(parentID, isSocket?): Observable<ComponentModel[]> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/component/tree/${parentID}`, query, isSocket);
  }

  get(_id, isSocket?): Observable<ComponentModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/component/${_id}`, query, isSocket);
  }
  destroy(_id): Observable<any>{
    return this.apiService.delete(`/component/${_id}` );
  }

  save(component): Observable<ComponentModel> {
    // If we're updating an existing component
    if (component._id) {
      const _id = component._id;
      delete component['_id'];
      delete component['resourceID'];
      return this.apiService.put('/component/' + _id, component)
             .map(data => data.component);

    // Otherwise, create a new component
  } else {
      delete component._id;
      return this.apiService.post('/component/', component)
             .map(data => data.component);
    }
  }

}
