import {Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {Subject} from "rxjs/Subject";
import {ActivatedRoute, Router} from '@angular/router';
import {EntityListConfig, InvoicesService, ItemTypeEnum} from '../shared';


@Component({
  selector: 'invoices-details',
  templateUrl: './invoices-details.component.html',
  styleUrls: ['./invoices-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})

export class InvoicesDetailsComponent implements OnInit{
  @ViewChild('template') template: TemplateRef<any>;
  @ViewChild('tax') tax: TemplateRef<any>;
  invoiceno: number = 0;
  invoiceDate: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  invoiceItem: any[];
  invoiceID: any;
  lastFetchedItems: any;
  subTotal: any;
  grandTotalValue: any;
  taxs: any;
  totalTaxFromInvoiceItem: any;
  constructor(private route: ActivatedRoute,
              public entityListConfig: EntityListConfig,
              private invoiceService: InvoicesService,
              private router: Router
           ) {
    this.lastFetchedItems = 0;
  }
  getTemplate(invoice) {
    switch(invoice.itemType){
      case ItemTypeEnum.TEMPLATE:return this.template;
      case ItemTypeEnum.TAX:return this.tax;
      default:return this.template;
    }
  }

  /*** sorting & pagination */
  getListSortData(sortBy: string){
    if(this.entityListConfig.query.sortBy === sortBy){
      return {
        value: this.entityListConfig.query.order,
        sortBy: sortBy
      };
    } else {
      return {
        value: 0,
        sortBy: sortBy
      };
    }
  }
  checkStatus(invoice){
    if ( typeof invoice === 'object'){
      return true;
    }
    return false;
  }
  getGrandTotal(){
    var value: number = 0;
    for ( let new1 of this.invoiceItem){
      value += new1['total'];
    }
    this.subTotal = value;
    return value;
  }
  getSubTotal(subTotal: any) {
    return  (this.subTotal - this.totalTaxFromInvoiceItem);
  }

  getTaxAmount(amount){
    this.totalTaxFromInvoiceItem = amount;
    return amount;
  }
  setListSorting(event) {
    this.entityListConfig = new EntityListConfig();
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllInvoiceItem(true);
  }
  onScroll() {
    this.getAllInvoiceItem(false);
  }
  /*** sorting & pagination */
  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data => {
        this.invoiceItem = data[0];
        this.invoiceID = data[1];
        this.invoiceDate = data[2];
        this.invoiceno = data[3];
        this.taxs = data[4];
        if(!this.invoiceno && !this.invoiceDate){
          this.router.navigateByUrl(`/invoices`);
        }
      });
  }
  grandTotal(){
    var value: number = 0;
    for ( let new1 of this.invoiceItem){
      value += new1['total'];
    }
    this.subTotal = value;
    return value;

  }
  getAllInvoiceItem(refreshData){
    this.invoiceService
      .get(this.invoiceID, this.entityListConfig)
      .subscribe(
        (data) => {
          if (refreshData) this.invoiceItem = [];
          // remove last page entries if data less than ideal size
          if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
            this.invoiceItem.splice(
              (this.entityListConfig.params.pageNumber)
              *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          }
          this.lastFetchedItems = data.length;
          if(data.length==this.entityListConfig.params.pageSize){
            this.entityListConfig.params.pageNumber++;
          }
          // push new data
          this.invoiceItem = [...this.invoiceItem, ...data];
        }
      );
  }

}
