import { Router } from '@angular/router';
import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TemplateModel, TemplatesService } from '../../../../shared';


@Component({
  selector: 'cadc1-modal-content',
  templateUrl: './templates.delete.component.html'
})
export class TemplatesDeleteComponent {
  @Input() component: any;

  constructor(public activeModal: NgbActiveModal,
              private templatesService: TemplatesService) {

  }
  delete(component){
    this.templatesService.destroy(component._id)
      .subscribe(
        data => this.activeModal.close()
      );


  }
}
