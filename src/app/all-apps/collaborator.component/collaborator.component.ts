import {Component, Input} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalService, EntityListConfig, CollaboratorService } from "./../../shared";
import { CollaboratorAcessComponent } from "../collaborator.acess/collaborator.acess.component";
import { CollaboratorDeleteComponent } from "./../collaborator.delete/collaborator.delete.component";

import {Observable} from "rxjs/Rx";
import {CollaboratorModel} from '../../shared/models/collaborator.model';

@Component({
  selector: 'collaborator-modal',
  templateUrl: './collaborator.component.html',
  providers: [EntityListConfig]
})
export class CollaboratorComponent  {
  @Input() app;
  @Input() collaborators: any[];
  constructor(public activeModal: NgbActiveModal,
              private modalService: ModalService,
              private collaboratorService: CollaboratorService) {

  }
  getAllCollaborator(appID){
    const entityListConfig = new EntityListConfig();
    this.collaboratorService
      .getAll(appID, entityListConfig)
      .subscribe(
        (data)=>{
          this.collaborators = data;
        },
        (error)=>{
           console.log(error);
        });
  }
  editCollaboratorAcess(collaborator: any){
  const obs1 =   this.collaboratorService
      .getAllCollaboratorAcess(this.app._id, collaborator._id)
    .subscribe(
      (data)=> {
        const modalRef = this.modalService.open(CollaboratorAcessComponent, {},'fade-in-pulse','fade-out-pulse');
        modalRef.componentInstance.collaborator = collaborator;
        modalRef.componentInstance.collaboratorPrivilege = data
        modalRef.componentInstance.app = this.app;
      }
    )
  }
  removeCollaborator(app, collaborator){
    const modalRef = this.modalService.open(CollaboratorDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.collaborator = collaborator;
    modalRef.componentInstance.app = app;
    modalRef.result.then((closeReason) => {
      const defaultEntity = new EntityListConfig();
      this.getAllCollaborator(this.app._id);
    });
  }
  addCollaborator(app){
    const modalRef = this.modalService.open(CollaboratorAcessComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.collaborator = new CollaboratorModel;
    modalRef.componentInstance.app = app;
    // modalRef.result.then((closeReason) => {
    //   const defaultEntity = new EntityListConfig();
    //   this.getAllCollaborator(this.app._id);
    // });
  }
}
