import { NgModule } from '@angular/core';
import { SharedModule, P1Service, P4Service, P5Service  } from './../../shared';
import { AccommodationComponent } from './accommodation/accommodation.component';
import { P4Component } from './p4/p4.component';
import { StallsComponent } from './stalls/stalls.component';
import { ShagunComponent } from './shagun/shagun.component';
import { P1Component } from './p1/p1.component';
import { F1Component } from './f1.component';
import { Ng2CompleterModule } from 'ng2-completer';
import { P2Module, p2Routing } from './p2/p2.module';
import {GooglePlaceModule} from 'ng2-google-place-autocomplete';

import {P4DeleteComponent} from './p4/p4.delete/p4.delete.component';
import { F1Resolver } from './f1.resolver';
import { P5UpsertComponent } from './p5/p5.upsert/p5.upsert.component';
import { P5DeleteComponent } from './p5/p5.delete/p5.delete.component';
import { P5Component } from './p5/p5.component';

import { P4UpsertComponent } from './p4/p4.upsert/p4.upsert.component';
import { P1Resolver } from './p1/p1.resolver';
import { P3Module, p3Routing } from './p3/p3.module';
import {P4Resolver} from "./p4/p4.resolver";
import {P5Resolver} from "./p5/p5.resolver";
import {F1UpsertComponent} from './f1.upsert/f1.upsert.component';
import {F1Service} from "../../shared/services/f1.service";
import {F1DeleteComponent} from "./f1.delete/f1.delete.component";

export const f1Routing = [
  {
    path: 'f1',
    component: F1Component,
    resolve: {data: F1Resolver},
    children: [
      {
        path: 'p1',
        component: P1Component,
        resolve: {data: P1Resolver}
      },
      {
        path: 'p5',
        component: P5Component,
        resolve: {
          data: P5Resolver
        },
      },
      ...p2Routing,
      ...p3Routing,
      {
        path: 'p4',
        component: P4Component,
        resolve: {
          data: P4Resolver
        },
      },
      {
        path: 'accommodation',
        component: AccommodationComponent
      },
      {
        path: 'stalls',
        component: StallsComponent
      },
      {
        path: 'shagun',
        component: ShagunComponent
      }
    ]
  }];

@NgModule({
  imports: [
    P2Module, GooglePlaceModule,
    P3Module,
    SharedModule, Ng2CompleterModule,
  ],
  declarations: [
    F1Component, P1Component , P4Component, P5Component, F1DeleteComponent,
    AccommodationComponent, StallsComponent, ShagunComponent,
     P4DeleteComponent,
    P5UpsertComponent, P5DeleteComponent, P4UpsertComponent,
    F1UpsertComponent
  ],
  providers: [
    P1Service, P4Service, F1Resolver, P1Resolver , P4Resolver, F1Service,
    P5Service, P5Resolver ],

  entryComponents: [
    P4DeleteComponent, P5UpsertComponent, P5DeleteComponent, F1UpsertComponent, P4UpsertComponent, F1DeleteComponent
  ]

})
export class F1Module {}
