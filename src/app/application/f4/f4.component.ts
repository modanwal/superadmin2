import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs/Subject";
import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {F4Service , EntityListConfig , F4Model } from '../../shared';

@Component({
  selector: 'app-root',
  templateUrl: './f4.component.html',
  providers: [EntityListConfig]
})

export class F4Component implements OnInit, OnDestroy{
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  f4Form: FormGroup;
  isSubmited: Boolean;
  f4: F4Model;
  // allF4Data: any[];
  appID: any;
  constructor(private f4Service: F4Service ,
               private formBuilder: FormBuilder,
               public activeModal: NgbActiveModal,
               public entityListConfig: EntityListConfig,
               private route: ActivatedRoute,
               private router: Router) {
    this.route.data
      .map(resolvedData => resolvedData.data)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data =>{
          this.f4 = data.f4;
          this.appID = data.appID;
        }
      );
}
  ngOnInit() {
    // use FormBuilder to create a form group
    this.f4Form = this.formBuilder.group({
      'GA_ID': [this.f4.GA_ID, Validators.required]
    });
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  submitForm( ) {
    this.isSubmited = true;
    if (this.f4Form.valid) {
      this.f4Form.disable();
      const f4 = this.f4Form.value;
      this.f4Service
        .save(f4, this.appID)
        .subscribe(
          data => {
            this.isSubmited= false;
            this.f4Form.enable();

          },
        error=>{
          this.isSubmited= false;
          this.f4Form.enable();
        });
    }
  }

}

