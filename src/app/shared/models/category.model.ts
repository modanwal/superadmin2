
export class CategoryModel {
  _id: string;
  name: string;
  icon: string;
  path: string;
  description: string;
  isLeaf: boolean;
  parentID: string;
  hasTemplate: boolean;
}
