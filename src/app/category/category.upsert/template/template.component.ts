import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ComponentModel, UserService, InfiniteScrollService } from '../../../shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TemplateDeleteComponent } from './tempate.delete/template.delete.component';
import { TemplateUpsertComponent } from './template.upsert/template.upsert.component';

import { TemplateService, EntityListConfig , SocketService, ModalService} from '../../../shared';
import { Subject } from "rxjs/Subject";
import {Subscription} from 'rxjs/Subscription';


@Component( {
  selector: 'template-upsert-group',
  templateUrl: './template.component.html',
  providers: [EntityListConfig]
})


export class TemplateComponent implements OnInit{
  fetchingData: boolean;
  collectionDirty: boolean;
  templateFetchSub: Subscription;
  lastFetchedItems: number;
  authenticated: Boolean;
  @Output() templateComponents;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  templateID: string;
  templateComponentsObj: any;
  constructor (private modalService: ModalService,
               private userService: UserService,
               private router: Router,
               private route: ActivatedRoute,
               private templateService: TemplateService,
               public  entityListConfig: EntityListConfig,
               public infiniteScrollService: InfiniteScrollService,
              //  private breadCrumbService: BreadCrumbService,
               private socketService: SocketService) {
    this.templateComponentsObj = {};
    this.collectionDirty = false;
    this.userService.isAuthenticated.subscribe ( authenticated => {
      if (!authenticated) this.router.navigateByUrl ( '/login' );
    } );

    this.lastFetchedItems = 0;
    // /*** websockets */
    // this.socketService.subscribeToEvent(`template:save`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllTemplateForSockets());
    //
    // this.socketService.subscribeToEvent(`templates:remove`)
    //   .takeUntil(this.ngUnsubscribe)
    //   .subscribe(data => this.getAllTemplateForSockets());
    // /*** websockets */

    /*** websockets */
    this.socketService.subscribeToEvent(`template:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.templateComponents,this.templateComponentsObj, this.entityListConfig);
        if(this.fetchingData){
          this.templateFetchSub.unsubscribe();
          this.getAllTemplate(false);
        }
      })
    this.socketService.subscribeToEvent(`template:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.templateComponents,this.templateComponentsObj,this.entityListConfig);
        if(this.fetchingData){
          this.templateFetchSub.unsubscribe();
          this.getAllTemplate(false);
        }
      })
    /*** websockets */
  }

  ngOnInit(){
    this.lastFetchedItems = 0;
    this.templateID = this.route.snapshot.params.id;
    // this.breadCrumbService
    //           .pushBreadCrumbItem({url:'/template',params:this.templateID,label:'Template'},false);
    this.getAllTemplate(true);
  }
  getAllTemplate(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
  this.templateFetchSub =   this.templateService
      .getAllComponentsForTemplate(this.templateID, this.entityListConfig)
      .subscribe(
        (data) => {
          this.fetchingData = false;
          if (this.collectionDirty) {
            this.getAllTemplate(false);
          } else {
            this.templateComponents = data;
            this.infiniteScrollService.getFromApi(data, this.templateComponents, this.templateComponentsObj, this.entityListConfig, refreshData);
          }
        }
      ),(error)=>{this.fetchingData = false;};;
  }
  // getAllTemplateForSockets(){
  //   this.templateService
  //     .getAllComponentsForTemplate(this.templateID,this.entityListConfig)
  //     .subscribe(data => {
  //       this.templateComponents = data;
  //     });
  // }
  setEntityConfig(event){
    this.entityListConfig.query.sortBy = event.sortBy;
    this.entityListConfig.query.order = event.sortOrder;
    this.getAllTemplate(true);
  }
  paginateListing(){
    this.getAllTemplate(false);
  }
  deleteComponent(templateComponent: any){
    const modalRef = this.modalService.open(TemplateDeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.templateComponent = templateComponent;
    modalRef.componentInstance.templateID = this.templateID;
  }
  editComponent(component: any) {
    const modalRef = this.modalService.open(TemplateUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.component = component;
    modalRef.componentInstance.templateID = this.templateID;
  }
  addComponent(temp : any) {
    const modalRef = this.modalService.open(TemplateUpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.temp = new ComponentModel();
    modalRef.componentInstance.parentID = this.templateID;
  }
  goToPrivileges() {
    this.router.navigate(['privileges'], {relativeTo: this.route});
    // this.router.navigateByUrl('/priviliges');
  }
  ComponentStack() {
    this.router.navigateByUrl(`/componentstack/${this.templateID}`);
  }


}
