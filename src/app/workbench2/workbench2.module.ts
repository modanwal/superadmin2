import { ModuleWithProviders, NgModule } from '@angular/core';
import { SharedModule, AuthGuard , UserDetailService, ApplicationStatusService} from '../shared';
import { RouterModule, RouterStateSnapshot} from '@angular/router';
import { WorkbenchComponent2 } from './workbench2.component';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WorkbenchResolver } from "./workbench2.resolver";


export const workBenchRouting2 = [
  {
    path: 'workbench2',
    canActivate: [AuthGuard],
    component: WorkbenchComponent2
    // resolve: {
    //   user: UserDetailsResolver
    // },
  }
];

@NgModule({
  imports: [
    SharedModule,
    NgbModule
  ],
  declarations: [
    WorkbenchComponent2,
    DragDropComponent
  ],
  providers: [
    AuthGuard
  ],
  entryComponents: [
  ]
})
export class WorkbenchModule2 {

}
