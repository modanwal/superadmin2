import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';

@Injectable()
export class PaymentService {
  constructor (private apiService: ApiService) {}
  getAll(appID, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/payment/${appID}`, query, isSocket);
  }
  get(_id, isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/payment/${_id}`, query, isSocket);
  }
  destroy(_id): Observable<any>{
    return this.apiService.delete(`/payment/${_id}` );
  }
  save(invoices, appID, welcomeID?): Observable<any> {
    // If we're updating an existing component
    if (welcomeID) {
      return this.apiService.put(`/payment/${welcomeID}/app/${appID}/` , invoices)
        .map(data => data.welcome);
      // Otherwise, create a new category
    } else {
      return this.apiService.post(`/payment/app/${appID}`, invoices)
        .map(data => data.welcome);
    }
  }
}
