import { Component, Input } from '@angular/core';
import {  NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {PostService} from "../../shared";


@Component({
  selector: 'post-delete',
  templateUrl: './post.delete.component.html'
})
export class PostDeleteComponent {
  @Input() post;
  submitting: Boolean;
  constructor(public activeModal: NgbActiveModal, private postService: PostService){
  }
  deletePost(post: any){
    this.submitting = true;
    this.postService.destroy(post)
      .subscribe (
        data => this.activeModal.close ('address: delete')
      );

  }
}
