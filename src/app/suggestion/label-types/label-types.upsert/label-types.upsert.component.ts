import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CcltService, ComponentService} from '../../../shared';
import {EntityListConfig} from "../../../shared/models/entity-list-config.model";


@Component({
  selector: 'app-root',
  templateUrl: './label-types.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SuggestionLabelTypesUpsertComponent implements  OnInit {
  @Input() labelType;
  @Input() categories;
  @Input() labels;
  @Input() types;
  components: any[];
  labelTypeForm: FormGroup;
  isSubmited: Boolean;
  isSubmitting: boolean;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private ccltService: CcltService,
                private componentService: ComponentService
  ) {
    this.isSubmited = false;
    this.isSubmitting = false;
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    this.isSubmited = true;
    if (this.labelTypeForm.valid) {
      this.isSubmitting = true;
      const cclt = this.labelTypeForm.value;
      // cclt['dataID'] = this.mode;
      var splitted = this.labelTypeForm.value.typeID.split(" ",2 );
      cclt.typeID = splitted[0];
      if(splitted[1] !== 'string'){
        delete cclt.minlength;
        delete cclt.maxlength;
      }
      this.labelTypeForm.disable();
      this.ccltService
        .save(cclt)
        .subscribe(
          data => {
            this.isSubmitting = false;
            this.activeModal.close('labelType:updated');
          },
          error => {
            this.isSubmited = false;
            this.labelTypeForm.enable();
          });
    }
  }
  ngOnInit() {
    this.labelTypeForm = this.formBuilder.group({
      '_id': [this.labelType._id],
      'categoryID': [this.labelType.categoryID, Validators.required],
      'componentID': [this.labelType.componentID, Validators.required],
      'labelID': [this.labelType.labelID, Validators.required],
      'typeID': [this.labelType.typeID, Validators.required],
      'minlength': [this.labelType.minlength],
      'maxlength': [this.labelType.maxlength]
    });
  }
  getComponent(event){
    console.log('value printing', event.target.value);
    const entityListConfig = new EntityListConfig();
    entityListConfig.params.pageSize = -1;
    this.componentService
      .getAllComponent(event.target.value, entityListConfig)
      .subscribe(
        (data)=>{
          this.components = data;
        });
  }
  setComponent(event){
    console.log('getting setComponent', event.target.value);
  }
}
