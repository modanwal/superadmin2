export class PrivilegeModel {
  _id: string;
  code: string;
  name: string;
}
