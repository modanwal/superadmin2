import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { PrivilegeModel, PrivilegeService } from '../../../shared';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from "@angular/router";
@Component({
  selector: 'privilege-upsert-content',
  templateUrl: './privilege.upsert.component.html',
  encapsulation : ViewEncapsulation.None
})
export class PrivilegeUpsertComponent implements OnInit{
  privlegeForm: FormGroup;
  isSaving = false;
  @Input() privilege;
  isSubmited: Boolean;
  constructor(private modalService: NgbModal , private privilegeService:PrivilegeService, private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private router: Router, ) {
    this.isSubmited = false;
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    if(!this.isSubmited){this.isSubmited = true }
    if(this.privlegeForm.valid){
      this.isSubmited = true;
      this.privlegeForm.disable();
      const privilege = this.privlegeForm.value;

       this.privilegeService
        .save(privilege)
        .subscribe(data => { this.privlegeForm.enable();this.activeModal.close('privilege:updated')});
    }
  }

  ngOnInit() {
    // use FormBuilder to create a form group
    this.privlegeForm = this.formBuilder.group({
      '_id': [this.privilege._id],
      'name': [this.privilege.name, Validators.required],
      'code': [this.privilege.code, Validators.required]
    });
  }
}
