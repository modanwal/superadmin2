import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {F1Service} from "../../../shared/services/f1.service";


@Component({
  selector: 'f1-upsert-pages',
  templateUrl: './f1.upsert.component.html'
})
export class F1UpsertComponent implements OnInit {
  pageNameForm: FormGroup;
  isSubmited: boolean;
  @Input() component;
  @Input() appID;

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder, public activeModal: NgbActiveModal, private f1Service: F1Service){

  }
  ngOnInit() {
    this.pageNameForm = this.formBuilder.group({
      'pageName': [this.component.pageName]
    });
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm(){
    if (this.pageNameForm.valid) {
      this.isSubmited = true;
      this.pageNameForm.disable();
      const pageName = this.pageNameForm.value;
      console.log(pageName);
        this.f1Service
          .savef1(pageName, this.appID, this.component._id )
          .subscribe(
            data => {
              this.activeModal.close('f1:updated');
            },
            (err) => {    this.pageNameForm.enable();}
          );
      }
    }

  }

