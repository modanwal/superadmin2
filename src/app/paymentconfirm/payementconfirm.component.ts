import { Component, OnInit} from '@angular/core';
import {InvoicesService} from "../shared/services/invoices.service";
import {DTOService} from "../shared/services/dto.service";
import {EntityListConfig} from "../shared/models/entity-list-config.model";
import {Router} from "@angular/router";
@Component({
  selector: 'payment-page',
  templateUrl: 'paymentconfirm.component.html',
})
export class PayementconfirmComponent implements OnInit {
  checkout: any;
  amount: any;
  changeMode: any;
  gatewayID: any;
  mode: string[] = ['DEBITCARD', 'CREDITCARD']
  constructor(private invoiceService: InvoicesService, private dtoService: DTOService, private router: Router) {
  this.amount =   this.dtoService.getValue('amount');
    this.gatewayID =   this.dtoService.getValue('gatewayID');
    this.changeMode = 'DEBITCARD';
   // this.getInvoiceCallBack();
  }

  ngOnInit() {

  }
  hitApi(){
    const entityListConfig = new EntityListConfig();
    delete entityListConfig.query.order;
    delete entityListConfig.query.sortBy;
    entityListConfig.query['amount'] = this.amount;
    entityListConfig.query['mode'] = this.changeMode;
    entityListConfig.query['gatewayID'] = this.gatewayID;
    this.invoiceService
      .getCheckOut(entityListConfig)
      .subscribe(
        (data)=> {
          console.log(data);
          this.checkout = data;
          this.router.navigateByUrl(`/invoices`);
        });
  }
  sendMode(event){
    this.changeMode = event.target.value;
  }
}
