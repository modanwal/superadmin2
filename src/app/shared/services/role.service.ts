import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { EntityListConfig, RoleModel } from '../../shared/models';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class RoleService {

  constructor (
    private apiService: ApiService
  ) {}
  query(config: EntityListConfig, isSocket?): Observable<{components: RoleModel[], componentsCount: number}> {
    // Convert any filters over to Angular's URLSearchParams
    const params: URLSearchParams = new URLSearchParams();

    // Object.keys(config.filters)
    //   .forEach((key) => {
    //     params.set(key, config.filters[key]);
    //   });

    return this.apiService
      .get('/roles/role', params , isSocket).map(data => data);
  }
  get(_id, isSocket?): Observable<RoleModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/role/${_id}/`, query, isSocket);
  }
  getRoleResourcePrivilege(roleID, isSocket?): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();

    return this.apiService.get(`/role-resource-privilege/${roleID}`, query, isSocket);
  }
  getAll(entityListConfig, isSocket?): Observable<RoleModel[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
    .forEach((key) => {
      query.set(key, entityListConfig.query[key]);
    });
    return this.apiService.get(`/role/${pageNumber}/${pageSize}/`, query, isSocket);
  }

  destroy(_id) {
    return this.apiService.delete('/role/' + _id);
  }

  save(role): Observable<RoleModel> {
    // If we're updating an existing component
    if (role._id) {
      const _id = role._id;
      delete role['_id'];
      return this.apiService.put('/role/' + _id, role);

      // Otherwise, create a new component
    } else {
      delete role['_id'];
      return this.apiService.post('/role/', role);
    }
  }
  saveRoleResourcePrivilege(roleResourcePrivilegeData:any): Observable<any>{
    return this.apiService.put('/role-resource-privilege/', roleResourcePrivilegeData);
  }
}
