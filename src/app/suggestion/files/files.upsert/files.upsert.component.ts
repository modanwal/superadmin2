import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {CcldService} from "../../../shared/services/ccld.service";
import {FileUploadService} from "../../../shared/file-upload/file-upload.service";
import { CroppedUpload} from './../croppedupload/croppedupload.component';
import { WebUpload } from './../webupload/webupload.component';
import { ModalService } from './../../../shared/modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './files.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class FilesUpsertComponent implements  OnInit {
  cropDimension: { width: number; height: number; };
  @Input() files;
  file;
  cropData: any;
  imagePreview;
  editMode: any;
  isSubmited: Boolean;
  formData: FormData;
  isSubmitting: boolean;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private ccldService: CcldService,
                private fileUploadService: FileUploadService,
                private modalService: ModalService
  ) {
   this.cropDimension = {
      width: 200,
      height: 200
    };
  }
  selectFile(event){
    this.file = event.target.files[0];
    /*** open modal for local modal */
    const modalRef  = this.modalService.open(CroppedUpload, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.file = this.file;
    modalRef.componentInstance.cropDimension = this.cropDimension;
    this.activeModal.close();
  }
  openWebWizard(){
    const modalRef = this.modalService.openOverlayModal(WebUpload,{
      keyboard: false
    });
    modalRef.componentInstance.cropDimension = this.cropDimension;
    this.activeModal.close();
  }

  ngOnInit() {

  }
}
