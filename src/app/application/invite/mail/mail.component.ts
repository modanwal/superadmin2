import {Component, OnChanges, OnInit} from '@angular/core';
import {EntityListConfig} from '../../../shared/models/entity-list-config.model';
import { SafeHtml, DomSanitizer } from "@angular/platform-browser";
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {CompleterData, CompleterService} from 'ng2-completer';
import {environment} from '../../../../environments/environment';
import {ActivatedRoute} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {DTOService} from '../../../shared/services/dto.service';
@Component({
  selector: 'mail-invite',
  templateUrl: './mail.component.html',
  providers: [EntityListConfig]
})
export class MailComponent implements OnInit{

  mail: any;
  searchStr: string;
  dataService: CompleterData;
  userData: any;
  componentID: any;
  categoryID: any;
  filetemplate: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private completerService: CompleterService,
              private route: ActivatedRoute,
              private dtoService: DTOService){

    this.dataService = completerService.remote(`${environment.api_url}/user?email=`, 'email', 'email');
  }






  ngOnInit(){
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(data => data.data)
      .subscribe(data =>{
        this.componentID = data[0];
        this.userData = data[1];
        this.categoryID = data[2];
        this.filetemplate = data[3].data;
        console.log(this.filetemplate.data);
      });
  }

}
