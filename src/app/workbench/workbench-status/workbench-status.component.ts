import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {EntityListConfig, AppService} from "../../shared";



@Component( {
  selector: 'workbench-status-page',
  templateUrl: './workbench-status.component.ts.html',
  providers: [EntityListConfig]
})
export class WorkbenchStatusComponent{
  @Input() apps: any;
  @Output('workbenchStatus') editWorkbenchComponent = new EventEmitter<any>();
  lastFetchedItems: any;
  constructor (private entityListConfig: EntityListConfig,
               private appService: AppService) {
  this.lastFetchedItems = 0;
  }
  onScroll() {
    console.log('scrolled');
  //  this.getAllApps(false);
  }

  getAllApps( refreshData){
    delete this.entityListConfig.query.sortBy;
    delete this.entityListConfig.query.order;
    this.entityListConfig['status'] = status;
    this.appService
      .getAll(this.entityListConfig)
      .subscribe(
        (data) => {
          if(refreshData) this.apps = [];
          // remove last page entries if data less than ideal size
          if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
            this.apps.splice(
              (this.entityListConfig.params.pageNumber)
              *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
          }
          this.lastFetchedItems = data.length;
          if(data.length == this.entityListConfig.params.pageSize){
            this.entityListConfig.params.pageNumber++;
          }
          // push new data
          this.apps = [...this.apps, ...data];
        }
      );
  }
}
