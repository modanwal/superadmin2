import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class CcldService {
  constructor(private apiService: ApiService) {
  }

  getAll(cclt, entityListConfig: EntityListConfig, isSocket?): Observable<any[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/ccld/category/${cclt.categoryID._id}/component/${cclt.componentID._id}/label/${cclt.labelID._id}/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getAllLabelTypes(dataID , entityListConfig: EntityListConfig, isSocket?): Observable<any[]>{
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/ccld/data/${dataID}/${pageNumber}/${pageSize}`, query, isSocket );
  }
  getAllSuggestion(categoryID, componentID, entityListConfig: EntityListConfig, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });


    return this.apiService.get(`/ccld/category/${categoryID}/component/${componentID}/${pageNumber}/${pageSize}`, query, isSocket);
  }
  destroy(ccldID): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/ccld/${ccldID}`);
  }

  saveFormData(ccld, ccldID?): Observable<any> {
    if(ccldID){
      return this.apiService.putWithFormData(`/ccld/${ccldID}`, ccld);
    } else
      return this.apiService.postWithFormData('/ccld', ccld);

    }
    save(ccld): Observable<any>{
    if(ccld._id){
      delete ccld._id;
      return this.apiService.put('/ccld',ccld);
    }else{
      delete ccld._id;
      return this.apiService.post('/ccld', ccld);
    }
    }
  }


