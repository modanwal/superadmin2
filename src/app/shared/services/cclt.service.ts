import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { EntityListConfig } from './../models/entity-list-config.model';

@Injectable()
export class CcltService {
  constructor (private apiService: ApiService) {}

  getAll( entityListConfig:  EntityListConfig, isSocket?): Observable<any[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/cclt/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getAllCategories(dataID, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    const entityListConfig = new EntityListConfig();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/cclt/data/${dataID}/category`, query, isSocket)
  }
  getAllComponent(dataID, categoryID,  isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    const entityListConfig = new EntityListConfig();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/cclt/data/${dataID}/category/${categoryID}/component`, query, isSocket)
  }
  getAllLabel(dataID, categoryID, componentID,  isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    const entityListConfig = new EntityListConfig();
    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/cclt/data/${dataID}/category/${categoryID}/component/${componentID}/label`, query, isSocket)
  }

  destroy(ccldID): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.delete(`/cclt/${ccldID}` );
  }

  save(cclt): Observable<any> {
    // If we're updating an existing component
    if (cclt._id) {
      const ccltID = cclt._id;
      delete cclt._id;
      delete cclt.categoryID;
      delete cclt.componentID;
      delete cclt.labelID;
      return this.apiService.put(`/cclt/${ccltID}`, cclt);

      // Otherwise, create a new category
    } else {
      delete cclt._id;
      return this.apiService.post(`/cclt/`, cclt);
    }
  }
}
