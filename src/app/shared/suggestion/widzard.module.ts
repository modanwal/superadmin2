import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { ImageCropperModule } from 'ng2-img-cropper';
import { SuggestionService } from "./widzard.service";
import { SuggestionComponent } from "./widzard.component";
import {ImageSenceComponent} from "./imageSense/image-sence.component";

@NgModule({
  imports: [NgbModule,CommonModule],
  exports: [SuggestionComponent],
  declarations: [SuggestionComponent, ImageSenceComponent],
  entryComponents: [SuggestionComponent, ImageSenceComponent],
  providers: [SuggestionService]
})


export class SuggestionModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SuggestionModule,
      providers: [SuggestionService]
    };
  }
}
