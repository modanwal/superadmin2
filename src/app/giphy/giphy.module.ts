
import {SharedModule} from "../shared/shared.module";
import {NgModule} from "@angular/core";
import {GiphyComponent} from "./giphy.component";
import {GiphyService} from "../shared/services/giphy.service";

export const giphyRouting = [
  {
    path: 'giphy',
    component: GiphyComponent
  }
];

@NgModule( {
  imports: [
    SharedModule
  ],
  declarations: [
    GiphyComponent
  ],
  providers: [GiphyService],
  entryComponents: [

  ]
})



export class GiphyModule {  }
