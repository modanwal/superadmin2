import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { UserService, AppService, P1Service } from './../../../shared';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class P1Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router,
               private p1Service: P1Service) {
  }
 ngOnInit(){
 }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      const appComponents = route.parent.data.data.appComponents;

      const selectedComponent = appComponents.find(component=> component.componentID.route == '/p1');
      const nextComponent = appComponents[appComponents.indexOf(selectedComponent)+1]
      const appID = state['url'].split("/")[2];
      return this.p1Service.getAll(appID).map(
        (p1Data) =>{
          let data = {};
          data['appID'] = appID;
          data['selectedComponent'] = selectedComponent;
          data['nextComponent'] = nextComponent;
          data['p1Data'] = p1Data;
          return data;
        }
      );
  }
}

