import { RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core/src/core";
import { NgModule } from '@angular/core';
import { SharedModule, F2Service } from './../../shared';
import {F2Component} from "./f2.component";
import {F2Resolver} from "./f2.resolver";
export const f2Routing = [
  {
    path: 'f2',
    component: F2Component,
    resolve: { data : F2Resolver}
  }
  ];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    F2Component
  ],
  providers: [ F2Resolver, F2Service
  ]
})
export class F2Module {}
