import {NgModule} from '@angular/core';
import {NotFound404Component} from './404.component';


export const notFound404Routing = [
  {
    path: '**',
    component: NotFound404Component
  }
];


@NgModule({
  imports: [],
  declarations: [NotFound404Component],
  providers: []
})
  export class NotFound404Module {}
