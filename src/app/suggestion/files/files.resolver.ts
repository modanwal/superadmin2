import { Injectable, OnInit, } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { EntityListConfig, DataService} from '../../shared';
import { Subscription } from 'rxjs/Subscription';



@Injectable()
export class FilesResolver implements Resolve<any> , OnInit {
  cclt;
  routeData: Subscription;

  constructor ( private dataService: DataService,  private router: Router, ) {

  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    delete entityListConfig.query.order;
    delete entityListConfig.query.sortBy;
    entityListConfig.query['type']= 'file';
    return this.dataService
      .getAll(entityListConfig);
  }
}
