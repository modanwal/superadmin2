import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { F3Model, EntityListConfig } from '../../shared/models';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class LikeService {

  constructor (
    private apiService: ApiService
  ) {}
  getAllLikes(entityListConfig: EntityListConfig, postID, isSocket?): Observable<F3Model[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/like/post/${postID}/like`, query, isSocket);
  }

  saveLike( postId, appId): Observable<any> {
    return this.apiService.post(`/like/app/${appId}/post/${postId}/`);
  }

}
