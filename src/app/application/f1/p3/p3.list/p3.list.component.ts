import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { EntityListConfig, P3Model, P3Service, DTOService, SocketService, InfiniteScrollService, ModalService } from './../../../../shared';
import { P3UpsertComponent } from './../p3.upsert/p3.upsert.component';
import { P3DeleteComponent } from './../p3.delete/p3.delete.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from "rxjs/Subject";
import { DomSanitizer } from "@angular/platform-browser";
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'p3-list',
  templateUrl: './p3.list.component.html',
  encapsulation: ViewEncapsulation.None

})
export class P3ListComponent implements OnInit{
  fetchingData: boolean;
  collectionDirty: boolean;
  p3FetchSub: Subscription;
  pageName: any;
  selectedComponent: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  p3: P3Model[];
  f1Id: any;
  appID: any;
  componentID: any;
  lastFetchedItems: any;
  p3Obj: any;
  constructor(
              private dtoService: DTOService,
              private p3Service: P3Service,
              public entityListConfig: EntityListConfig,
              private modalService: ModalService,
              private route: ActivatedRoute,
              private router: Router,
              private _sanitizer: DomSanitizer,
              private socketService: SocketService,
              public infiniteScrollService: InfiniteScrollService
    ) {
    this.lastFetchedItems = false;
    this.collectionDirty = false;
    this.p3Obj = {};
    // /*** websockets */
    // this.socketService.subscribeToEvent(`p3:save`)
    //   .subscribe(data => this.getAllP3ForSockets());
    //
    // this.socketService.subscribeToEvent(`p3:remove`)
    //   .subscribe(data => this.getAllP3ForSockets());
    // /*** websockets */

    /*** websockets */
    this.socketService.subscribeToEvent(`p3:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.p3,this.p3Obj, this.entityListConfig);
        if(this.fetchingData){
          this.p3FetchSub.unsubscribe();
          this.getAllP3(false);
        }
      })
    this.socketService.subscribeToEvent(`p3:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.p3,this.p3Obj,this.entityListConfig);
        if(this.fetchingData){
          this.p3FetchSub.unsubscribe();
          this.getAllP3(false);
        }
      })
    /*** websockets */
  }
  ngOnInit(){
    this.route.parent.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {
          this.appID = data.appID;
          this.pageName = data.selectedComponent.pageName;
          this.p3 = data.p3Data;
          this.infiniteScrollService.computeEntity(this.p3,this.p3Obj, this.entityListConfig);
          this.componentID = data.selectedComponent.componentID._id;
          this.dtoService.setValue('componentID', this.componentID);
        });

  }
  onScroll(){
    this.getAllP3(true);
  }
  editP3(p3data: P3Model) {
    const modalRef = this.modalService.open(P3UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p3 = p3data;
    modalRef.componentInstance.appID = this.appID;


  }
  deleteP3(p3data: P3Model) {
    const modalRef = this.modalService.open(P3DeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p3 = p3data;
    modalRef.componentInstance.appID = this.appID;
  }
  addP3() {
    const modalRef =  this.modalService.open(P3UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p3 = new P3Model;
    modalRef.componentInstance.appID = this.appID;
  }
  getAllP3(refreshData){
    this.fetchingData = true;
    this.collectionDirty = false;
    this.p3Service.getAllp3(this.entityListConfig, this.appID)
      .subscribe(
        data => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllP3(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.p3, this.p3Obj, this.entityListConfig, refreshData);
          }
        }
      ),(error)=>{this.fetchingData = true;};
  }
        //   if(refreshData) this.p3 = [];
        //   // remove last page entries if data less than ideal size
        //   if(this.lastFetchedItems<this.entityListConfig.params.pageSize){
        //     this.p3.splice(
        //       (this.entityListConfig.params.pageNumber)
        //       *(this.entityListConfig.params.pageSize), this.lastFetchedItems);
        //   }
        //   this.lastFetchedItems = data.length;
        //   if(data.length==this.entityListConfig.params.pageSize){
        //     this.entityListConfig.params.pageNumber++;
        //   }
        //   // push new data
        //   this.p3 = [...this.p3, ...data];
        // }

  // getAllP3ForSockets(){
  //   const entityListConfig = new EntityListConfig();
  //   entityListConfig.query = this.entityListConfig.query;
  //   entityListConfig.params.pageSize = (this.entityListConfig.params.pageNumber+1)*(this.entityListConfig.params.pageSize);
  //   entityListConfig.params.pageNumber = 0;
  //   this.p3Service
  //     .getAllp3(entityListConfig, this.appID, true)
  //     .subscribe(data =>{
  //       this.p3 = data;
  //       this.lastFetchedItems = (data.length)%(this.entityListConfig.params.pageSize);
  //       this.entityListConfig.params.pageNumber = Math.ceil(data.length/this.entityListConfig.params.pageSize);
  //       if(this.entityListConfig.params.pageNumber!=0) this.entityListConfig.params.pageNumber--;
  //     });
  // }
  getBackground(image){
    // if(!image){
    //   return this._sanitizer.bypassSecurityTrustStyle(`url('assets/img/')`);
    // }
    if(!image){image = 'assets/img/gallery.jpg'};
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  // **p3p1
    getP3Data(p3data: P3Model) {
      this.dtoService.setValue('p3p1PageName', p3data.name);
      this.router.navigateByUrl(`/application/${this.appID}/f1/p3/p3p1/${p3data._id}`);
    }


}
