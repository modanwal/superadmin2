import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import {EntityListConfig} from "../models/entity-list-config.model";


@Injectable()
export class CollaboratorService {
  constructor (private apiService: ApiService) {}

  getAll(appID, entityListConfig: EntityListConfig, isSocket?): Observable<any[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/app/${appID}/collaborator/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getAllCollaborator(appID, isSocket?): Observable<any[]> {
    const entityListConfig = new EntityListConfig();
    const query: URLSearchParams = new URLSearchParams();
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;

    return this.apiService.get(`/app/${appID}/appComponent/${pageNumber}/${pageSize}`, query, isSocket);
  }
  getAllCollaboratorAcess(appID, userID, isSocket?): Observable<any[]> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/app/${appID}/user/${userID}/appComponent`, query, isSocket);
  }
  get(_id, isSocket?): Observable<any> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/collaborator/${_id}`, query, isSocket);
  }
  destroyCollaborator(appID, userID): Observable<any>{
    return this.apiService.delete(`/app/${appID}/user/${userID}` );
  }

  save(data): Observable<any> {
    // If we're updating an existing component
      return this.apiService.post(`/app/collaborator/`, data);
  }
  checkBoxClicked(data) : Observable<any> {
  // If we're updating an existing component
    return this.apiService.post(`/app/collaborator/appcomponent`, data);
    }
  checkBoxSetPrivate(data) : Observable<any> {
    // If we're updating an existing component
    return this.apiService.put(`/app/collaborator/access`, data);
  }

  deleteCollaboratorAcess(appID, collaboratorID, appComponentID): Observable<any> {
    return this.apiService.delete(`/app/${appID}/user/${collaboratorID}/appcomponent/${appComponentID}` );
  }
}
