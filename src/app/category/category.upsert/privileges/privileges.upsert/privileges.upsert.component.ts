import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {TemplateResourcePrivilegeService} from "../../../../shared";


@Component({
  selector: 'privileges-upsert-pages',
  templateUrl: './privileges.upsert.component.html'
})
export class PrivilegesUpsertComponent implements OnInit {
  privilegeForm: FormGroup;
  isSubmited: boolean;
  @Input() privilege;
  @Input() resourceID;
  @Input() templateID;
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private templateResourcePrivilegeService: TemplateResourcePrivilegeService){

  }
  ngOnInit() {
    this.privilegeForm = this.formBuilder.group({
      'name': [this.privilege.name]
    });
  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm(){

    if (this.privilegeForm.valid) {
      this.isSubmited = true;
      this.privilegeForm.disable();
      const pageName = this.privilegeForm.value;
      const privilegeData = {
        templateID: this.templateID,
        resourceID: this.resourceID,
        privilegeID: this.privilege._id,
        name: pageName.name
      }
      this.templateResourcePrivilegeService
        .editPrivilege(privilegeData)
        .subscribe(
          (data)=>{
            this.activeModal.close('privilege:updated');
          },
          (err)=>{
            this.privilegeForm.enable();
          });
    }
  }

}

