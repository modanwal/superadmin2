export const environment = {
  production: true,
  api_url: 'http://backend.makemeproud.org/api',
  socket_url: 'http://backend.makemeproud.org',
  auth_url: 'http://backend.makemeproud.org/auth/local'
};
