import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { UserService, EntityListConfig, NotificationService } from "../shared";

@Injectable()
export class NotificationResolver implements Resolve<any> {
  routeData: Subscription;
  constructor (
               private router: Router,
               private userService: UserService,
               private notificationService: NotificationService
               ) {
  }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    const userObservable = this.userService.getCurrentUserObservable();
    const notificationObservable = this.notificationService
      .getAll(entityListConfig,false);

    const obs3 = Observable.create(function (observer) {
      userObservable.subscribe(
        (data) => {
          if (data['email']) {
            observer.next(data);
            observer.complete();
          }});
    });




    return Observable.forkJoin(notificationObservable, obs3);
  }
}
