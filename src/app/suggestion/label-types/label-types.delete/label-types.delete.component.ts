import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {CcltService} from '../../../shared';
@Component({
  selector: 'labelType-delete',
  templateUrl: './label-types.delete.component.html'
})
export class LabelTypesDeleteComponent {
  @Input() labelType;
  isSubmited: Boolean;
  constructor( public activeModal: NgbActiveModal ,
               private ccltService: CcltService
  ) {}
  deleteLabel(labelType: any) {
    this.ccltService.destroy(labelType._id)
      .subscribe(
        data => this.activeModal.close('labelType:remove')
      );
  }
}
