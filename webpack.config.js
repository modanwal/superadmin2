const path = require('path');
var nodeExternals = require('webpack-node-externals');
module.exports = {
  entry: {
    server: './src/server.ts'
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      'main.server': path.join(__dirname, 'dist', 'server', 'main.bundle.js')
    }
  },
  target: 'node',
  externals: [/(node_modules|main\..*\.js)/,
    nodeExternals({
    whitelist: [
      /^@angular\/material/,
      /@ng-bootstrap\/ng-bootstrap/,
      /^ng2-dnd/,
      /^ng-selectize/,
      /^ng2-slim-loading-bar/,
      /^ngx-toastr/,
      /^ng2-img-cropper/,
      /^angular-progress-http/,
      /^ng2-completer/,
      /^ng2-google-place-autocomplete/
    ]
  })
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      { test: /\.ts$/, loader: 'ts-loader' }
    ]
  }
}
