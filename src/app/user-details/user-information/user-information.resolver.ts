import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import {AppService, EntityListConfig, UserInformationService} from '../../shared';
import {UserService} from "../../shared/services/user.service";

@Injectable()
export class UserInformationResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router,
              private userService: UserService) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const userID = state['url'].split("/")[2];
    return this.userService.get(userID);
  }
}

