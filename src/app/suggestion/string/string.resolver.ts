import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { EntityListConfig } from '../../shared';
import { Subscription } from 'rxjs/Subscription';
import {DTOService, DataService} from "../../shared";



@Injectable()
export class StringResolver implements Resolve<any> , OnInit {
  cclt;
  routeData: Subscription;

  constructor (private dtoService: DTOService, private dataService: DataService ) {
    // this.cclt = this.dtoService.getValue('labelType');
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    delete entityListConfig.query.sortBy;
    delete  entityListConfig.query.order;
    entityListConfig.query['type']= 'string';
    return  this.dataService
                .getAll(entityListConfig);
  }
}
