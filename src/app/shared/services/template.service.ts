import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { TemplateModel, EntityListConfig } from '../models';

@Injectable()
export class TemplateService {
  constructor (
    private apiService: ApiService
  ) {}


  getAll(entityListConfig: EntityListConfig, isSocket?): Observable<TemplateModel[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/template/${pageNumber}/${pageSize}`, query, isSocket);
    //}

  }
  getAllComponentsForTemplate(templateID,entityListConfig, isSocket?): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();
    Object.keys(entityListConfig.query)
    .forEach((key) => {
      query.set(key, entityListConfig.query[key]);
    });
    return this.apiService.get(`/template/${templateID}/component`, query, isSocket)
  }

  get(_id, isSocket?): Observable<TemplateModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/template/${_id}`, query, isSocket);
  }
  destroy(_id): Observable<any>{
    return this.apiService.delete(`/template/${_id}` );
  }

  save(template): Observable<TemplateModel> {
    // If we're updating an existing template
    if (template._id) {
      const _id = template._id;
      delete template['_id'];
      return this.apiService.put('/template/' + _id, template)
        .map(data => data.template);

      // Otherwise, create a new template
    } else {
      delete template['_id'];
      return this.apiService.post('/template/', template)
        .map(data => data.template);
    }
  }
  saveComponent(categoryFormData, templateID): Observable<any> {
    return this.apiService
            .postWithFormData(`/template/${templateID}/component` , categoryFormData);

  }
  updateComponent(categoryFormData,_id,templateID): Observable<any> {
      return this.apiService
            .putWithFormData(`/template/${templateID}/component/${_id}` , categoryFormData);

  }
  deleteComponent(_id,templateID){
    return this.apiService
            .delete(`/template/${templateID}/component/${_id}`);
  }

}
