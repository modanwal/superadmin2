import {Component,OnInit} from '@angular/core';
import { ComponentStackModalComponent } from "./componentstackmodal.component";
import { ActivatedRoute, Router } from '@angular/router';
import { TemplateService, ComponentService, EntityListConfig, ModalService } from './../../../shared';
import { BreadCrumbService } from "./../../../shared/breadcrumb.module";

@Component({
    selector: 'componentstack',
    templateUrl:'./componentstack.component.html',
    styleUrls: ['./componentstack.component.scss'],
    providers: [EntityListConfig]

})
export class ComponentStackComponent implements OnInit {

    templateComponents = [];
    allComponents = [];

    templateID: string;
    closeResult: string;
    sourceList: any;
    constructor(private modalService: ModalService,
                private router: Router,
                private route: ActivatedRoute,
                private templateService: TemplateService,
                private componentService: ComponentService,
                private breadCrumbService: BreadCrumbService) {
}

    ngOnInit(){
        this.templateID = this.route.snapshot.params.id;
        this.breadCrumbService
              .pushBreadCrumbItem({url:'/componentstack',params:this.templateID,label:'Component Stack'},false);
        this.getAllData();
    }
    getAllData(){
    const componentsListConfig = new EntityListConfig();
          componentsListConfig.params.pageSize = 1000;
          //get template components
          this.templateService
              .getAllComponentsForTemplate(this.templateID, componentsListConfig)
              .subscribe(
                  (templateComponents)=>{
                      this.templateComponents =  templateComponents;
                      //get all components
                      this.componentService
                          .getAll('', componentsListConfig)
                          .subscribe(
                              (components)=>{
                                  this.allComponents = [];
                                  components.forEach((component,i)=>{
                                      const index = this.templateComponents.findIndex((templateComponent)=>{
                                          return templateComponent.componentID._id==component._id;
                                      });
                                      if(index==-1){
                                          //push element to allComponents Array
                                          this.allComponents= [...this.allComponents,component];
                                      }
                                  });
                              }
                          )
                  }
              )
    }
    addComponentToTemplate(event: any) {

      const modalRef = this.modalService.open ( ComponentStackModalComponent,{
          backdrop: 'static',
          keyboard: false
      },'fade-in-pulse','fade-out-pulse');
      modalRef.componentInstance.component = event.dragData;
      modalRef.componentInstance.templateID = this.templateID;
      modalRef.result.then((closeReason) => {
      if (closeReason == 'components-added'){
        this.getAllData();
        }
      });

    }
}
