import { Router } from '@angular/router';
import { Component, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { F3Model, F3Service} from './../../../shared';

@Component({
  selector: 'f3-delete-component',
  templateUrl: './f3.delete.component.html'
})
export class F3DeleteComponent {
  @Input () f3: any;
  @Input () appID: any;
  @Input () pageName: string;
  isSubmited: Boolean;

  constructor (public activeModal: NgbActiveModal, private f3Service: F3Service) {

  }

  delete (category: F3Model) {
    this.isSubmited = true;
    this.f3Service.destroy(this.f3._id,this.appID)
      .subscribe (
        data => this.activeModal.close('f3_deleted')
      );
  }
}

