import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { EntityListConfig, RoleModel } from '../../shared/models';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class UserDetailService {

  constructor (
    private apiService: ApiService
  ) {}
  query(config: EntityListConfig, isSocket?): Observable<{components: RoleModel[], componentsCount: number}> {
    // Convert any filters over to Angular's URLSearchParams
    const params: URLSearchParams = new URLSearchParams();

    // Object.keys(config.filters)
    //   .forEach((key) => {
    //     params.set(key, config.filters[key]);
    //   });

    return this.apiService
      .get('/roles/role', params , isSocket).map(data => data);
  }
  get(_id, isSocket?): Observable<RoleModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/role/${_id}`, query, isSocket);
  }
  getRoleResourcePrivilege(roleID, isSocket?): Observable<any>{
    const query: URLSearchParams = new URLSearchParams();

    return this.apiService.get(`/role-resource-privilege/${roleID}`, query, isSocket);
  }
  getAllAddress(userID, entityListConfig, isSocket?): Observable<RoleModel[]> {

    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/address/user/${userID}/${pageNumber}/${pageSize}/`, query, isSocket);
  }
  // getAllAddressfromComponent(userID, entityListConfig, isSocket?): Observable<RoleModel[]> {
  //
  //   const pageNumber = entityListConfig.params.pageNumber;
  //   const pageSize = entityListConfig.params.pageSize;
  //   const query: URLSearchParams = new URLSearchParams();
  //
  //   Object.keys(entityListConfig.query)
  //     .forEach((key) => {
  //       query.set(key, entityListConfig.query[key]);
  //     });
  //   return this.apiService.get(`/address/user/${userID._id}/${pageNumber}/${pageSize}/`, query, isSocket);
  // }

  destroy(addressID, userID) {
    return this.apiService.delete(`/address/${addressID}/user/${userID}`);
  }

  save(address, userID): Observable<RoleModel> {
    // If we're updating an existing component
    if (address._id) {
      const _id = address._id;
      delete address['_id'];
      return this.apiService.put(`/address/${_id}/user/${userID}` , address);
      // Otherwise, create a new component
    } else {
      delete address['_id'];
      return this.apiService.post(`/address/user/${userID}`, address);
    }
  }
  saveRoleResourcePrivilege(roleResourcePrivilegeData: any): Observable<any>{
    return this.apiService.put('/role-resource-privilege/', roleResourcePrivilegeData);
  }
}
