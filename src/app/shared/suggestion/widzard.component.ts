import {
  Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, Renderer2, TemplateRef,
  ViewChild
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CcldService } from './../services/ccld.service';
import { EntityListConfig } from './../models/entity-list-config.model';
import {ModalService} from '../modal.service';
import {ImageSenceComponent} from "./imageSense/image-sence.component";

@Component({
  selector: 'content-suggestion',
  templateUrl: './widzard.component.html',
})
export class SuggestionComponent implements OnChanges {
  @Input() parentReference: any;
  open: boolean;
  @Input() componentID;
  @Input() categoryID;
  @Input() type;
  @Input() ccld;
  content: any;
  currentTemplate: any;
  @ViewChild('dropdown') dropdown: TemplateRef<any>;
  @Output('onContentSelected') editComponentEmitter = new EventEmitter<any>();
  constructor(private renderer: Renderer2,
              public activeModal: NgbActiveModal,
              private ccldService: CcldService,
              private modalService: ModalService) {}
  ngOnChanges(changes: SimpleChanges): void {
      console.log(this.componentID,'nfurehufrf');
      console.log(this.categoryID,'udbfurbfurfrfrf');
  }
  eventfire(event, label){
    const data = {
      path: event,
      labelName: label
    }
    console.log('data', data);
    this.editComponentEmitter.emit(data);
  }
  getSuggestions(){
  if(this.type === 'file-1' || this.type == 'description-1' || this.type === 'title' || this.type === 'file'){
    if(this.type === 'file-1' || this.type === 'file' ){
      const entityListConfig = new EntityListConfig();
      console.log(this.categoryID, this.componentID, 'mention not');
      entityListConfig.query['label'] = this.type;
      this.ccldService
        .getAllSuggestion(this.categoryID, this.componentID, entityListConfig)
        .subscribe(
          (data)=> {
            const modalRef = this.modalService.openOverlayModal(ImageSenceComponent);
            this.open = false;
            /*** psr code added! */
            this.open = true;
            if(this.parentReference){
              this.renderer.removeClass(this.parentReference,'show');
            }
            /*** psr code added! */

            modalRef.componentInstance.componentID = this.componentID;
            modalRef.componentInstance.categoryID = this.categoryID;
            console.log(data, 'from Wizard');
            modalRef.componentInstance.ccld = data;
            modalRef.result.then((modalData) => {
              if(modalData){
                this.content =  modalData;
                this.eventfire(this.content, this.type);
              }
              this.close();
            })

          });
    }else  if(this.type === 'title'){
      const entityListConfig = new EntityListConfig();
      console.log(this.categoryID, this.componentID, 'mention not');
      entityListConfig.query['label'] = this.type;

      this.ccldService
        .getAllSuggestion(this.categoryID, this.componentID, entityListConfig)
        .subscribe(
          (data)=> {
            this.ccld = data;
          });


    }else  if(this.type === 'description-1'){
      const entityListConfig = new EntityListConfig();
      console.log(this.categoryID, this.componentID, 'mention not');
      entityListConfig.query['label'] = this.type;

      this.ccldService
        .getAllSuggestion(this.categoryID, this.componentID, entityListConfig)
        .subscribe(
          (data)=> {
            const modalRef = this.modalService.openOverlayModal(ImageSenceComponent);
            this.open = false;
            if(this.parentReference){
              this.renderer.addClass(this.parentReference, 'show');
            }
            modalRef.componentInstance.componentID = this.componentID;
            modalRef.componentInstance.categoryID = this.categoryID;
            modalRef.componentInstance.type = this.type;
            console.log(data, 'from Wizard');
            modalRef.componentInstance.ccld = data;
            modalRef.result.then((modalData) => {
              if(modalData){
                this.content =  modalData;
                this.eventfire(this.content, this.type);
              }
              this.close();
            });
          });
    }
  }

  }

  getTemplate() {

    if(this.type == 'title' && this.ccld){
      return this.dropdown;
    }
    return;


    // if(this.label === 'title'){
    //   const entityListConfig = new EntityListConfig();
    //   console.log(this.categoryID, this.componentID, 'mention not');
    //   entityListConfig.query['label'] = this.label;
    //   this.ccldService
    //     .getAllSuggestion(this.categoryID, this.componentID, entityListConfig)
    //     .subscribe(
    //       (data)=> {
    //          this.ccld = data;
    //         return this.label;
    //       });
    //
    // }
    }


  close(){
    /*** psr code added! */
      this.open = false;
      if(this.parentReference){
        this.renderer.addClass(this.parentReference,'show');
      }
      /*** psr code added! */
  }

}
