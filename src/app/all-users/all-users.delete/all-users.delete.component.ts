/**
 * Created by rahul on 7/6/2017.
 */
import { Router } from '@angular/router';
import { Component, ViewEncapsulation, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AllUserModel, UserService } from '../../shared';


@Component({
  selector: 'alluser-delete-modal',
  templateUrl: './all-users.delete.component.html'
})
export class AllUserDeleteComponent {
  isSubmited: boolean;
  @Input() user: AllUserModel;

  constructor(public activeModal: NgbActiveModal, private userService: UserService) {
    this.isSubmited = false;
  }

  deleteUser(user: AllUserModel){
    this.isSubmited = true;
    this.userService.destroy(user._id )
      .subscribe (
        data => this.activeModal.close('user_deleted')
      );
  }

}
