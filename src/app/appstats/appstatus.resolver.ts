import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';


import { Subscription } from 'rxjs/Subscription';
import {EntityListConfig, AppService} from "../shared";



@Injectable()
export class AppstatusResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor ( private appService: AppService) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {

    const obs1 = this.appService.getAllAppStatusCount(new EntityListConfig);

     return this.appService.getAllAppStatusCount(new EntityListConfig);
  }
}
