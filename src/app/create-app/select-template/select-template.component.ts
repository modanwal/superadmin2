import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { TemplatesService, EntityListConfig, TemplateModel, TemplateService, ComponentModel } from "./../../shared";

@Component({
  selector: 'select-template-page',
  templateUrl: './select-template.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})

export class SelectTemplateComponent implements OnInit {
  templateComponents: ComponentModel[];
  templates: TemplateModel[];
  categoryID: any;
  constructor(private entityListConfig: EntityListConfig,
              private templateService: TemplateService,
              private route: ActivatedRoute,
              private router: Router,
              private templatesService: TemplatesService) {
    this.entityListConfig.params.pageSize = -1;
    this.templateComponents = [];
  }
  ngOnInit(){
    this.categoryID = this.route.snapshot.params.categoryID;
    this.templatesService.getAll(this.categoryID,this.entityListConfig)
        .subscribe(data => this.templates = data);
  }
  getTemplateComponents(template){
    this.templateService.getAllComponentsForTemplate(template._id,this.entityListConfig)
        .subscribe(data => this.templateComponents = data);
  }
  selectTemplate(template){
    this.router.navigateByUrl(`create-app/details/${template._id}`);
  }
}
