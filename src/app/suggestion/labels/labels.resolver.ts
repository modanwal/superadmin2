import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { LabelService, EntityListConfig } from '../../shared';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export class SuggestionLabelsResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor ( private labelService: LabelService) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const entityListConfig = new EntityListConfig();
    return this.labelService.getAll (entityListConfig );
  }
}
