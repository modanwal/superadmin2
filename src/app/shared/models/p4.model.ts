export class P4Model {

  _id: string;
  name: string;
  dateTime: string;
  venue: string;
  city: string;
  address1: string;
  address2: string;
  pincode: string;
  state: string;
  website: string;
  phoneNo: string;
}
