import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { BreadCrumbService } from './../../shared/breadcrumb.module';
import { CategoryService, EntityListConfig, CategoryModel } from './../../shared';

@Component({
  selector: 'select-category-page',
  templateUrl: './select-category.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]
})

export class SelectCategoryComponent implements OnInit {
  categories: CategoryModel[];
  constructor(private router: Router, private breadCrumbService: BreadCrumbService, private categoryService: CategoryService, private entityListConfig: EntityListConfig) {
    
  }
  ngOnInit(){
    this.getCategories(null);
    this.breadCrumbService
        .pushBreadCrumbItem({url:'/create-app/category',label:"Select Category"},true);
  }
  
  getCategories(parentID){
    this.entityListConfig.params.pageSize = -1;
    this.categoryService.getAll(parentID,this.entityListConfig)
        .subscribe(data => this.categories = data);
  }
  view(category: CategoryModel){
    if(category.hasTemplate){
      this.router.navigateByUrl(`/create-app/template/${category._id}`);
    }else{
      this.getCategories(category._id);
    }
  }

  
}
