export class PaymentModel {
  _id: string;
  date: string;
  mode: string;
  amount: string;
}
