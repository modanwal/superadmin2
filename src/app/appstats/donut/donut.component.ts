import { Component, Input } from '@angular/core';
import {Item} from './item';
@Component({
  moduleId: module.id,
  selector: 'donut-chart',
  templateUrl: './donut.component.html'

})
export class DonutComponent{
 //
 @Input() items: Array<Item>=[];
 @Input() radius: number = 50;
 @Input() width: number = 20;
 @Input() centerText: {name:string,value:string};
 @Input() fontColor: string= "black";
 @Input() fontSize:number=10;
 constructor() {
 }
  public doughnutChartLabels:string[] = ['Published', 'Publish-InProgress', 'Publish-requested', 'Rejected', 'UnPublished', 'UnPublish-Deleted'];
  public doughnutChartData:number[] = [350, 450, 100, 200, 150, 157];
  public doughnutChartType:string = 'doughnut';

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }


}
