
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import {P5Model, SocketService, P5Service, EntityListConfig, DTOService, InfiniteScrollService} from '../../../shared';
//import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from './../../../shared/modal.service';
import {P5UpsertComponent} from './p5.upsert/p5.upsert.component';
import { P5DeleteComponent } from './p5.delete/p5.delete.component';
import {ActivatedRoute, Router} from "@angular/router";
import {Subject} from "rxjs/Subject";
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'p5-content',
  templateUrl: './p5.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [EntityListConfig]

})
export class P5Component implements OnInit {
  pageName: any;
 selectedComponent: any;
  categoryID: any;
  p5: P5Model[];
  appID: any;
  singleP5data: P5Model;
  componentID: any;
  p5Obj: any;
  fetchingData: boolean;
  collectionDirty: boolean;
  p5FetchSub: Subscription;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private p5Service: P5Service,
              private socketService: SocketService,
              public entityListConfig: EntityListConfig,
              private modalService: ModalService,
              private route: ActivatedRoute,
              private _sanitizer: DomSanitizer,
              private dtoService: DTOService,
              private router: Router,
              public infiniteScrollService: InfiniteScrollService
    ) {
    this.categoryID = this.dtoService.getValue('categoryID');
    this.collectionDirty = false;
    this.p5Obj = {};
    // if(!this.categoryID){
    //   this.router.navigateByUrl(`/all-apps`);
    // }

    this.socketService.subscribeToEvent(`p5:save`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.savedFromSocket(data, this.p5,this.p5Obj, this.entityListConfig);
        if(this.fetchingData){
          this.p5FetchSub.unsubscribe();
          this.getAllP5(false);
        }
      })
    this.socketService.subscribeToEvent(`p5:remove`)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(data => {
        this.collectionDirty = true;
        this.infiniteScrollService.removedFromSocket(data, this.p5,this.p5Obj,this.entityListConfig);
        if(this.fetchingData){
          this.p5FetchSub.unsubscribe();
          this.getAllP5(false);
        }
      })
    /*** websockets */
  }

  getAllP5(refreshData) {
    this.fetchingData = true;
    this.collectionDirty = false;
  this.p5FetchSub =  this.p5Service
      .getAllp5(this.entityListConfig, this.appID)
      .subscribe(
        data => {
          this.fetchingData = false;
          if(this.collectionDirty){
            this.getAllP5(false);
          }else{
            this.infiniteScrollService.getFromApi(data, this.p5, this.p5Obj, this.entityListConfig, refreshData);
          }
        }
      );
  }

  editP5(p5data: P5Model) {
    const modalRef = this.modalService.open(P5UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p5 = p5data;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.pageName = this.pageName;
    modalRef.componentInstance.componentID = this.componentID;
    modalRef.componentInstance.categoryID = this.categoryID;
  }
  getBackground(image){
      return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }
  onScroll(){
    this.getAllP5(true);
  }
  addP5() {

  const modalRef =  this.modalService.open(P5UpsertComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p5 = new P5Model;
    modalRef.componentInstance.appID = this.appID;
    modalRef.componentInstance.componentID = this.componentID;
    modalRef.componentInstance.categoryID = this.categoryID;
    modalRef.componentInstance.reference = modalRef;
  }
  selectedData(p5: P5Model){
     this.singleP5data = p5;
  }
   deleteP5(p5data: P5Model) {
    const modalRef = this.modalService.open(P5DeleteComponent, {},'fade-in-pulse','fade-out-pulse');
    modalRef.componentInstance.p5 = p5data;
    modalRef.componentInstance.appID = this.appID;
   }
  ngOnInit() {
    this.route.data
      .takeUntil(this.ngUnsubscribe)
      .map(resolvedData => resolvedData.data)
      .subscribe(data =>
        {
          this.appID = data.appID;
          this.pageName = data.selectedComponent.pageName;
          this.componentID = data.selectedComponent.componentID._id;
          this.p5 = data.p5Data;
          this.infiniteScrollService.computeEntity(this.p5,this.p5Obj, this.entityListConfig);
        }
      );
  }

}

