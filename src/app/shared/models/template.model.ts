export class TemplateModel {

  _id: string;
  name: string;
  type: string;
  discount: string;
  price: number;
  categoryID: string;
  components: [string];

}
