
import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CcldService} from "../../../shared";

@Component({
  selector: 'app-root',
  templateUrl: './label-number.upsert.component.html',
  encapsulation: ViewEncapsulation.None
})
export class LabelNumberUpsertComponent implements  OnInit {
  @Input() cclt;
  @Input() labelNumbers;
  labelNumberForm: FormGroup;
  file;
  imagePreview;
  isSubmited: Boolean;
  formData: FormData;
  isSubmitting: boolean;
  constructor(  private formBuilder: FormBuilder,
                public activeModal: NgbActiveModal,
                private ccldService: CcldService
  ) {

  }
  eventHandler(event){
    if(event.keyCode === 13){
      this.submitForm();
    }
  }
  submitForm() {
    const labelNumber = this.labelNumberForm.value;
    console.log(labelNumber);
    this.formData = new FormData();
    this.formData.append('data', labelNumber.data);
    this.formData.append('categoryID', this.cclt.categoryID._id);
    this.formData.append('componentID', this.cclt.componentID._id);
    this.formData.append('labelID', this.cclt.labelID._id);
    //  this.formData.append('data', 'https://farm8.staticflickr.com/7631/16236321533_1216582801.jpg');
    this.ccldService.
    saveFormData(this.formData, labelNumber._id)
      .subscribe(
        (data)=>{
          console.log('printing');
          this.activeModal.close('labelNumber:updated');
        });
  }
  ngOnInit() {
    this.labelNumberForm = this.formBuilder.group({
      '_id': [this.labelNumbers._id],
      'data': [this.labelNumbers.data, Validators.required]
    });

  }
}
