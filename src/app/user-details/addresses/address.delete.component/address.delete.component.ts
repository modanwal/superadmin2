import { Component, Input } from '@angular/core';
import {  NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AddressModel, UserDetailService } from '../../../shared';

@Component({
  selector: 'address-delete',
  templateUrl: './address.delete.component.html'
})
export class AddressDeleteComponent {

  @Input() address: AddressModel;
  @Input() userID;
  isSubmited: Boolean;
  constructor(public activeModal: NgbActiveModal,  private userAddressService: UserDetailService) {
  }
  deleteAddress(address: any){
    this.isSubmited = true;
    this.userAddressService.destroy(address._id, this.userID._id )
      .subscribe (
        data => this.activeModal.close ('address: delete')
      );
  }
}
