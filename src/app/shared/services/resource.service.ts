import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { ResourceModel, EntityListConfig } from '../../shared/models';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class ResourceService {

  constructor (
    private apiService: ApiService
  ) {}

  get(_id, isSocket?): Observable<ResourceModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get(`/resource/${_id}`, query, isSocket);
  }
  getAll(entityListConfig: EntityListConfig, isSocket?): Observable<ResourceModel[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
    .forEach((key) => {
      query.set(key, entityListConfig.query[key]);
    });
    return this.apiService.get(`/resource/${pageNumber}/${pageSize}`, query, isSocket);
  }
  destroy(_id) {
    return this.apiService.delete('/resource/' + _id);
  }

  save(resource): Observable<ResourceModel> {
    // If we're updating an existing resource
    if (resource._id) {
      const _id = resource._id;
      delete resource['_id'];
      return this.apiService.put('/resource/' + _id, resource);

      // Otherwise, create a new resource
    } else {
      delete resource['_id'];
      return this.apiService.post('/resource/', resource);
    }
  }
}
