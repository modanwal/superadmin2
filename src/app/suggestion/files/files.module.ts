import { NgModule } from '@angular/core';
import {AuthGuard, SharedModule} from '../../shared';
import {FilesComponent} from './files.component';
import {FilesResolver} from './files.resolver';
import {FilesUpsertComponent} from './files.upsert/files.upsert.component';
import {FilesDeleteComponent} from './files.delete/files.delete.component';

export const fileRouting = [
  {
    path: 'file',
    component: FilesComponent,
    canActivate: [AuthGuard],
    resolve: {
      data: FilesResolver
    }
  }
];
@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    FilesComponent, FilesDeleteComponent, FilesUpsertComponent
  ],
  providers: [
    AuthGuard, FilesResolver
  ],
  entryComponents: [FilesUpsertComponent, FilesDeleteComponent ]
})
export class FilesModule {}
