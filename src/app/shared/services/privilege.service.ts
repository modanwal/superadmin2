import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { PrivilegeModel, EntityListConfig } from '../../shared/models';
import { Observable } from 'rxjs/Rx';
import { URLSearchParams } from '@angular/http';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class PrivilegeService {

  constructor (
    private apiService: ApiService, private toastrService: ToastrService
  ) {}

  get(_id, isSocket): Observable<PrivilegeModel> {
    const query: URLSearchParams = new URLSearchParams();
    return this.apiService.get ( `/privilege/${_id}`, query,  isSocket );
  }

  // getAll(pageNumber: Number, pageSize: Number, sortBy: String, order: String): Observable<PrivilegeModel[]> {
  //  return this.apiService.get(`/api/privilege/${pageNumber}/${pageSize}?sortBy=${sortBy}&order=${order}`);
  // }

  getAll(entityListConfig: EntityListConfig, isSocket?): Observable<PrivilegeModel[]> {
    const pageNumber = entityListConfig.params.pageNumber;
    const pageSize = entityListConfig.params.pageSize;
    const query: URLSearchParams = new URLSearchParams();

    Object.keys(entityListConfig.query)
      .forEach((key) => {
        query.set(key, entityListConfig.query[key]);
      });
    return this.apiService.get(`/privilege/${pageNumber}/${pageSize}/`, query, isSocket);
  }
  destroy(_id) {
    return this.apiService.delete('/privilege/' + _id);
  }

  save(privilege): Observable<PrivilegeModel> {
    // If we're updating an existing privilege
    if (privilege._id) {
      const _id = privilege._id;
      delete privilege['_id'];
      return this.apiService.put('/privilege/' + _id, privilege);

      // Otherwise, create a new privilege
    } else {
      delete privilege['_id'];
      return this.apiService.post('/privilege/', privilege);
    }
  }
}
