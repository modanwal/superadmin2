import { Injectable, OnInit, } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Rx';

import {  UserService, AppService } from './../../shared';
import { Subscription } from 'rxjs/Subscription';
import {EntityListConfig} from "../../shared/models/entity-list-config.model";

@Injectable()
export class F1Resolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private appService: AppService,
               private router: Router, private route: ActivatedRoute,) {
  }
 ngOnInit(){
 }
  resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      const appID = state['url'].split("/")[2];
      const entityListConfig = new EntityListConfig();
    delete entityListConfig.query.sortBy;
    delete entityListConfig.query.order;
    entityListConfig.query['type'] = 'P';
      return this.appService.getAppPages(appID, entityListConfig , false).map((appComponents)=>{
        let data = {};
        data['appID'] = appID;
        data['appComponents'] = appComponents;
        return data;
      });
  }
}

