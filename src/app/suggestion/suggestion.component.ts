import { Component, OnInit} from '@angular/core';
import { BreadCrumbService } from './../shared/breadcrumb.module';
@Component({
  selector: 'suggestion-page',
  templateUrl: 'suggestion.component.html',
})
export class SuggestionComponent implements OnInit {
  constructor(private breadCrumbService: BreadCrumbService) {
  }
  ngOnInit() {
    this.breadCrumbService.pushBreadCrumbItem({url:'/suggestion/datatypes',label:'Suggestion'},true);
  }
}
