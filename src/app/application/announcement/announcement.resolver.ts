import { Injectable, OnInit, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import {AnnouncementService, EntityListConfig} from '../../shared';

@Injectable()
export class AnnouncementResolver implements Resolve<any> , OnInit{

  routeData: Subscription;

  constructor (private announcementService: AnnouncementService) {
  }
  ngOnInit(){
  }
  resolve (route: ActivatedRouteSnapshot,
           state: RouterStateSnapshot): Observable<any> {
    const appID = state['url'].split('/')[2];
    console.log(appID);
    const obs1 = Observable.of(appID);
    const obs2 = this.announcementService.getAll(appID,new EntityListConfig);
    const obs3 = this.announcementService.getAllRole(appID , new EntityListConfig);
    return Observable.forkJoin(obs1, obs2, obs3);
  }
}
